SaltTiger Book List (201203-201308)
======

[[Photoshop.CC：The.Missing.Manual(2013.6)].Lesa.Snider.文字版.pdf](ed2k://|file|[Photoshop.CC：The.Missing.Manual(2013.6)].Lesa.Snider.文字版.pdf|98040113|1d0a62ef3fcd969f146e1f788672f78b|h=my6djz5duk4z3oq4s6i6nev3fa34qbyh|/)

[[Photoshop.CC：The.Missing.Manual(2013.6)].Lesa.Snider.文字版.epub](ed2k://|file|[Photoshop.CC：The.Missing.Manual(2013.6)].Lesa.Snider.文字版.epub|37142485|1ac471b268c714dd5c0f3b94516f4898|h=g4ozp4vktdh5mmup4tpnqh44xodfj7ze|/)

[[Parallel.and.Concurrent.Programming.in.Haskell(2013.7)].Simon.Marlow.文字版.pdf](ed2k://|file|[Parallel.and.Concurrent.Programming.in.Haskell(2013.7)].Simon.Marlow.文字版.pdf|18886490|894a3cea8dd242816a1d8fe29a17995c|h=a3f6uyjzur7ww7yo3iv6uojf5dcf32pk|/)

[[Parallel.and.Concurrent.Programming.in.Haskell(2013.7)].Simon.Marlow.文字版.epub](ed2k://|file|[Parallel.and.Concurrent.Programming.in.Haskell(2013.7)].Simon.Marlow.文字版.epub|2732600|e6eeef2e3299deacca72c0e10d19f924|h=lbhtngmz555qhlwbyal6hedtkuy4honh|/)

[[Lift.Cookbook(2013.6)].Richard.Dallaway.文字版.pdf](ed2k://|file|[Lift.Cookbook(2013.6)].Richard.Dallaway.文字版.pdf|10477938|c3961b8aa29db79b02a2e4ad06dcff92|h=jcamdkfpz63gjpylwp4gsyhkfurpkles|/)

[[Lift.Cookbook(2013.6)].Richard.Dallaway.文字版.epub](ed2k://|file|[Lift.Cookbook(2013.6)].Richard.Dallaway.文字版.epub|2024079|563bb4f8a863107b1bcbf489c1148fdc|h=7indybjywico6sb7hwhyjrmq4lcvg53q|/)

[[Learning.SPARQL(2nd,2013.7)].Bob.DuCharme.文字版.pdf](ed2k://|file|[Learning.SPARQL(2nd,2013.7)].Bob.DuCharme.文字版.pdf|13382658|59a900b075e8e9cdb9f789137a06ed03|h=4olocv2qiqb7lxbrmtx3y7brwt2z6cxs|/)

[[Learning.SPARQL(2nd,2013.7)].Bob.DuCharme.文字版.epub](ed2k://|file|[Learning.SPARQL(2nd,2013.7)].Bob.DuCharme.文字版.epub|3634701|a00212cffe3eb2a9f1e4acf73345491d|h=dzppoa7vchuxme2bhjotgmuodcc65pv6|/)

[[JavaMail.API(2013.7)].Elliotte.Rusty.Harold.文字版.pdf](ed2k://|file|[JavaMail.API(2013.7)].Elliotte.Rusty.Harold.文字版.pdf|5242934|66505463ef5321fb48070f349c27a63a|h=5gu5gpajq6k4vq7y7l6sel6z6yqjz5c3|/)

[[JavaMail.API(2013.7)].Elliotte.Rusty.Harold.文字版.epub](ed2k://|file|[JavaMail.API(2013.7)].Elliotte.Rusty.Harold.文字版.epub|1470659|7a89be6df7f37dd0a185b26a92686b34|h=txjvzmhnnwi4yfmcxrreomwougl6ueti|/)

[[Gradle.Beyond.the.Basics(2013.7)].Tim.Berglund.文字版.pdf](ed2k://|file|[Gradle.Beyond.the.Basics(2013.7)].Tim.Berglund.文字版.pdf|9936068|9324b6a2564648136e1b6b574521fe05|h=766zcqatllb3frfrcue7rh4agg2qdifz|/)

[[Gradle.Beyond.the.Basics(2013.7)].Tim.Berglund.文字版.epub](ed2k://|file|[Gradle.Beyond.the.Basics(2013.7)].Tim.Berglund.文字版.epub|1181142|d450d42aed01e4284e80bd76aa56e680|h=ii2qa6goypoe7r2kuprycqf4pumravxk|/)

[[Enterprise.Data.Workflows.with.Cascading(2013.7)].Paco.Nathan.文字版.pdf](ed2k://|file|[Enterprise.Data.Workflows.with.Cascading(2013.7)].Paco.Nathan.文字版.pdf|13113769|e6d01a6883bec585fb317fe5cb8d7b16|h=2dfazl3ktl5tiohwqjgi4td4dg4jy6af|/)

[[Enterprise.Data.Workflows.with.Cascading(2013.7)].Paco.Nathan.文字版.epub](ed2k://|file|[Enterprise.Data.Workflows.with.Cascading(2013.7)].Paco.Nathan.文字版.epub|4371937|10aee57b9083ff7e1f1f8ab290f013fa|h=gwekhtwks3uhleon6l3vw5uqkzj7k45o|/)

[[Developing.Business.Intelligence.Apps.for.SharePoint(2013.7)].David.Feldman.文字版.pdf](ed2k://|file|[Developing.Business.Intelligence.Apps.for.SharePoint(2013.7)].David.Feldman.文字版.pdf|40989058|559ab75755235dc94485065d8e8dae94|h=zfrq7ql7vo3ixfmkaybqhrtxpnyrelr6|/)

[[Developing.Business.Intelligence.Apps.for.SharePoint(2013.7)].David.Feldman.文字版.epub](ed2k://|file|[Developing.Business.Intelligence.Apps.for.SharePoint(2013.7)].David.Feldman.文字版.epub|69594051|9d0a1847c76a9239b7f50ec7f02fbcee|h=chiylhpj5oukciuj2lvdwh7rvhyhfe6p|/)

[[CSS.Fonts(2013.7)].Eric.A.Meyer.文字版.pdf](ed2k://|file|[CSS.Fonts(2013.7)].Eric.A.Meyer.文字版.pdf|11521979|7de01dd4f34c020b3914398ce3b4e673|h=cuo7yuduqjv4fkrxobp3ivzieb4pzwqy|/)

[[CSS.Fonts(2013.7)].Eric.A.Meyer.文字版.epub](ed2k://|file|[CSS.Fonts(2013.7)].Eric.A.Meyer.文字版.epub|1919085|02e88de4cb5f61d9d76cdb7e18bec3f3|h=dozg5pdius2ckeklgzjevxotvt4vvj5c|/)

[[Building.Hybrid.Android.Apps.with.Java.and.JavaScript(2013.7)].Nizamettin.Gok.文字版.pdf](ed2k://|file|[Building.Hybrid.Android.Apps.with.Java.and.JavaScript(2013.7)].Nizamettin.Gok.文字版.pdf|17471454|1f2ad6d65492cbfaceb97231243d940f|h=xynboufxo2vqng26wknkfcyjrh4zkulc|/)

[[Building.Hybrid.Android.Apps.with.Java.and.JavaScript(2013.7)].Nizamettin.Gok.文字版.epub](ed2k://|file|[Building.Hybrid.Android.Apps.with.Java.and.JavaScript(2013.7)].Nizamettin.Gok.文字版.epub|5386528|a218197f3ab24a314ba6c904b8f0f1db|h=7nye4wrxycseutqvwj2hqgfc4c4k57ln|/)

[[Apache.Sqoop.Cookbook(2013.7)].Kathleen.Ting.文字版.pdf](ed2k://|file|[Apache.Sqoop.Cookbook(2013.7)].Kathleen.Ting.文字版.pdf|9193795|825e2e46fcf400a5426e6df9a647a0ac|h=5bxfnxpl3rts6c2wjlape47nzxaeq4ru|/)

[[Apache.Sqoop.Cookbook(2013.7)].Kathleen.Ting.文字版.epub](ed2k://|file|[Apache.Sqoop.Cookbook(2013.7)].Kathleen.Ting.文字版.epub|1185051|fdcb517fa900b3f93beebb49a4ff0bca|h=i6qewi7zyj7nwhzrchqvesljygdjeiei|/)

[[802.11ac：A.Survival.Guide(2013.7)].Matthew.Gast.文字版.pdf](ed2k://|file|[802.11ac：A.Survival.Guide(2013.7)].Matthew.Gast.文字版.pdf|11515170|ff57ec65833c3e59d2776000a41282a0|h=lxzxte27ejsdwrzfogfdcctsr2z6x4ir|/)

[[802.11ac：A.Survival.Guide(2013.7)].Matthew.Gast.文字版.epub](ed2k://|file|[802.11ac：A.Survival.Guide(2013.7)].Matthew.Gast.文字版.epub|5947527|7f9a7fbb52c4e066444063200ab87861|h=lgjmrvmjzl5qbljnywsf2geolehlt7kt|/)

[[Learning.Python(5th,2013.6)].Mark.Lutz.文字版.pdf](ed2k://|file|[Learning.Python(5th,2013.6)].Mark.Lutz.文字版.pdf|18167148|58DF016258E1E88D9A6BFB307DFFDEB4|h=ZBU4KI37X3KRG4ZT4DTSMJUP4B4ACG3N|/)

[[Learning.Python(5th,2013.6)].Mark.Lutz.文字版.epub](ed2k://|file|[Learning.Python(5th,2013.6)].Mark.Lutz.文字版.epub|6225075|D3BAE3A6CD51D2448C4251C3D333ED6B|h=QR5JNIKJCIV2CZDEYCHAES4TJGEXC5VC|/)

[[Learning.Java(4th,2013.6)].Patrick.Niemeyer.文字版.pdf](ed2k://|file|[Learning.Java(4th,2013.6)].Patrick.Niemeyer.文字版.pdf|23986457|C485F4CB4D87649EB8C3A72B8805D1D9|h=2T2TAB3WDZDNOU76VGITDWMQMHSDNSM2|/)

[[Learning.Java(4th,2013.6)].Patrick.Niemeyer.文字版.epub](ed2k://|file|[Learning.Java(4th,2013.6)].Patrick.Niemeyer.文字版.epub|11147881|CE4023842A42667B1FDE85E03A99742F|h=WDFOWKHG4T6WUHZZHOQ7NKA2H63F24PM|/)

[[Juniper.SRX.Series(2013.6)].Brad.Woodberg.文字版.pdf](ed2k://|file|[Juniper.SRX.Series(2013.6)].Brad.Woodberg.文字版.pdf|41360393|87AD6E3CD1544EC329F381C7EF0732C9|h=QZLZFSFZ5RGIPO2XYX7IFCGT5OP3BEWT|/)

[[Juniper.SRX.Series(2013.6)].Brad.Woodberg.文字版.epub](ed2k://|file|[Juniper.SRX.Series(2013.6)].Brad.Woodberg.文字版.epub|19589315|A86871645E531BAC541691C5EF995A10|h=KXHT3CRTIZJG3BUMINMOV7PEJ7QLCB4V|/)

[[Graph.Databases(2013.6)].Ian.Robinson.文字版.pdf](ed2k://|file|[Graph.Databases(2013.6)].Ian.Robinson.文字版.pdf|22475907|5EC9D80A4F8D9CE9EB5939CB3E3A126B|h=FERD3KWGVFEEEPIJVCFH4HJKOJYJV2CF|/)

[[Graph.Databases(2013.6)].Ian.Robinson.文字版.epub](ed2k://|file|[Graph.Databases(2013.6)].Ian.Robinson.文字版.epub|12251848|C3E6EF186D3F6B47417F46710AB7EED0|h=LAJQ3VJGEB5JWIN5EJHBD4ZSUVZBFT6E|/)

[[Functional.JavaScript(2013.6)].Michael.Fogus.文字版.pdf](ed2k://|file|[Functional.JavaScript(2013.6)].Michael.Fogus.文字版.pdf|9669705|3555e61c6768f2b75196b161c3f6cc6c|h=rcv372bwg2ovlznrphegqjgetkvrppqh|/)

[[Functional.JavaScript(2013.6)].Michael.Fogus.文字版.epub](ed2k://|file|[Functional.JavaScript(2013.6)].Michael.Fogus.文字版.epub|3273605|d1f19238c972163d4470ed2c9e923331|h=krowztcatgmkoxfguu4q6zncq5nhtbfm|/)

[[Java.7.Pocket.Guide(2nd,2013.7)].Robert.Liguori.文字版.pdf](ed2k://|file|[Java.7.Pocket.Guide(2nd,2013.7)].Robert.Liguori.文字版.pdf|9570389|A6E1542650C0B10DFB12388EAF10447B|h=2YGY37AWXFRUESYFWVADTSLOV77HUKZS|/)

[[Java.7.Pocket.Guide(2nd,2013.7)].Robert.Liguori.文字版.epub](ed2k://|file|[Java.7.Pocket.Guide(2nd,2013.7)].Robert.Liguori.文字版.epub|1643204|67367582FAC851D7AF7D27895BB39ED9|h=56FIACWVP7LJRRAQXXA34SOVIPNKLJWU|/)

[[HTML5.Pocket.Reference(5th,2013.7)].Jennifer.Niederst.Robbins.文字版.pdf](ed2k://|file|[HTML5.Pocket.Reference(5th,2013.7)].Jennifer.Niederst.Robbins.文字版.pdf|4520818|535156105A5950C0140A9438E31A90C0|h=IHXFTTXACAVGZZPAO34IVUBCC2KP33UD|/)

[[HTML5.Pocket.Reference(5th,2013.7)].Jennifer.Niederst.Robbins.文字版.epub](ed2k://|file|[HTML5.Pocket.Reference(5th,2013.7)].Jennifer.Niederst.Robbins.文字版.epub|1318997|9B3E8AC54A274CDBE77F7B24AD54739C|h=AQDJNXNUUM43HC7LWAWS5MOH4DTQQUHA|/)

[[Git.Pocket.Guide(2013.6)].Richard.E.Silverman.文字版.pdf](ed2k://|file|[Git.Pocket.Guide(2013.6)].Richard.E.Silverman.文字版.pdf|7744560|D6FBC066B919A9B12093C1422EEE3124|h=SYMTA2DV6K5NY4X2HU7F22NX5OBPHON3|/)

[[Git.Pocket.Guide(2013.6)].Richard.E.Silverman.文字版.epub](ed2k://|file|[Git.Pocket.Guide(2013.6)].Richard.E.Silverman.文字版.epub|1856172|901BE2EA358C4A5DD7E77CFD47F69146|h=NQLOLG6TO4TJBKJ62AQZIGI6Z3LJAUXA|/)

[[Introduction.to.Online.Payments.Risk.Management(2013.6)].Ohad.Samet.文字版.pdf](ed2k://|file|[Introduction.to.Online.Payments.Risk.Management(2013.6)].Ohad.Samet.文字版.pdf|10768033|E0709672CD216D8AA4586983E090629F|h=KOEZKL7X4KSDADUUXUHGHLMWWSLJNECX|/)

[[Introduction.to.Online.Payments.Risk.Management(2013.6)].Ohad.Samet.文字版.epub](ed2k://|file|[Introduction.to.Online.Payments.Risk.Management(2013.6)].Ohad.Samet.文字版.epub|1242905|8E17288435C256A0AB0F953024521750|h=R5CUVCDPW25KLDZPLE23THAYZVQUOTLM|/)

[[Analyzing.the.Analyzers(2013.6)].Harlan.Harris.文字版.pdf](ed2k://|file|[Analyzing.the.Analyzers(2013.6)].Harlan.Harris.文字版.pdf|5016062|317ED91DB3753E438A2036538AD15F18|h=FWYWGK5LXWQ24Q6UFEIYRXHEI6JXG2DA|/)

[[Analyzing.the.Analyzers(2013.6)].Harlan.Harris.文字版.epub](ed2k://|file|[Analyzing.the.Analyzers(2013.6)].Harlan.Harris.文字版.epub|1800359|5D1C0D444DDE3DDDC8752923F9328FD4|h=ILRJKD6TM7WTBTZ6BSVLCDDFCOXF6URL|/)

[[Redis.in.Action(2013.6)].Josiah.L.Carlson.文字版.pdf](ed2k://|file|[Redis.in.Action(2013.6)].Josiah.L.Carlson.文字版.pdf|8139581|0007017313EEF7336CBA2EED752E45EA|h=P3CTR5WMWVHGVDR6B64SAG44ZYDLRINQ|/)

[[Redis.in.Action(2013.6)].Josiah.L.Carlson.文字版.epub](ed2k://|file|[Redis.in.Action(2013.6)].Josiah.L.Carlson.文字版.epub|8779667|804D9A6FD576BDDA78E6EF0973195533|h=P6RC2OWXS6P4EWHLYP76ONGEMRKQL3JS|/)

[[Windows.Store.App.Development(2013.6)].Pete.Brown.文字版.pdf](ed2k://|file|[Windows.Store.App.Development(2013.6)].Pete.Brown.文字版.pdf|34799559|03B5201488FBE71C955411DD5E53D337|h=E5PF23EL7E3FO2ZT4TUZHYOAT5JACLL2|/)

[[Windows.Store.App.Development(2013.6)].Pete.Brown.文字版.epub](ed2k://|file|[Windows.Store.App.Development(2013.6)].Pete.Brown.文字版.epub|51044699|0EFA198071F0BF4DA2562571D3FE32CB|h=VUSXRCMUZ5WUV22L2MVOG5Q2G5CYTIBR|/)

[[AOP.in.NET(2013.6)].Matthew.D.Groves.文字版.pdf](ed2k://|file|[AOP.in.NET(2013.6)].Matthew.D.Groves.文字版.pdf|8238271|4384AFBA6B32ABD18D0688F09637E151|h=RT6SNRB2BPGGXOC5S5OPTSW3JDXKWZGQ|/)

[[AOP.in.NET(2013.6)].Matthew.D.Groves.文字版.epub](ed2k://|file|[AOP.in.NET(2013.6)].Matthew.D.Groves.文字版.epub|21364805|E19BE5633A7A2E09BFF8DAB7AF9165E3|h=UTHO4SQYEEISHABVWJPRYHW2FBGZV5JP|/)

[[50.Android.Hacks(2013.6)].Carlos.Sessa.文字版.pdf](ed2k://|file|[50.Android.Hacks(2013.6)].Carlos.Sessa.文字版.pdf|10318355|8CD0C4C688AF3808D359682C4C01AD28|h=JZIKWOTMW6MOJURCHNTBMSW5IM2BSG3B|/)

[[50.Android.Hacks(2013.6)].Carlos.Sessa.文字版.epub](ed2k://|file|[50.Android.Hacks(2013.6)].Carlos.Sessa.文字版.epub|14594402|245B16D43B7DC8EBDE141F5691CDC27A|h=25HSZIKTJ7YFXZCUGZNV76YGOPYNB3GO|/)

[[Spring.in.Practice(2013.5)].Willie.Wheeler.文字版.pdf](ed2k://|file|[Spring.in.Practice(2013.5)].Willie.Wheeler.文字版.pdf|18631435|74CD457DDF990132C3E17A3D44BBDD65|h=ILE335BXISVCBPJCADFCNPH6YNFDPESQ|/)

[[Spring.in.Practice(2013.5)].Willie.Wheeler.文字版.epub](ed2k://|file|[Spring.in.Practice(2013.5)].Willie.Wheeler.文字版.epub|44641419|C01E04002995E2079E9CBB36118A5EEB|h=CW2WCO67FGO54IIMTDJZ4TBLJHTJE5A5|/)

[[Scala.in.Action(2013.4)].Nilanjan.Raychaudhuri.文字版.pdf](ed2k://|file|[Scala.in.Action(2013.4)].Nilanjan.Raychaudhuri.文字版.pdf|9703473|CA39DB3FC4D8D4E78789F33A3F4796AC|h=OHBI6CKTZ77V2Y5E4GS7YUVPCBLFOILE|/)

[[Scala.in.Action(2013.4)].Nilanjan.Raychaudhuri.文字版.epub](ed2k://|file|[Scala.in.Action(2013.4)].Nilanjan.Raychaudhuri.文字版.epub|13373117|E22D45EA97BD290BBCFD6ED2B36071F4|h=QJHJE6265QUHYAXGAHK6MYBVC74AYQ33|/)

[[OCA.Java.SE.7.Programmer.I.Certification.Guide(2013.4)].Mala.Gupta.文字版.pdf](ed2k://|file|[OCA.Java.SE.7.Programmer.I.Certification.Guide(2013.4)].Mala.Gupta.文字版.pdf|16215791|7E252D851ED41C79D381710422CBE657|h=XOZRDRSDBJJFGRPYZFHVY47EQKU4CQ5J|/)

[[OCA.Java.SE.7.Programmer.I.Certification.Guide(2013.4)].Mala.Gupta.文字版.epub](ed2k://|file|[OCA.Java.SE.7.Programmer.I.Certification.Guide(2013.4)].Mala.Gupta.文字版.epub|21403046|F5D5EC62FF8024155FB8A28E215C3E73|h=MS7QPA5GESJNO5VXNCD6YD3ONEVE6KLM|/)

[[Arduino.in.Action(2013.5)].Martin.Evans.文字版.pdf](ed2k://|file|[Arduino.in.Action(2013.5)].Martin.Evans.文字版.pdf|14562926|30A144A7208878166FF960D56FFF7ECA|h=XAD4CMTEISMRJVCKU2GZ6TDQ2NZAJ7DW|/)

[[Arduino.in.Action(2013.5)].Martin.Evans.文字版.epub](ed2k://|file|[Arduino.in.Action(2013.5)].Martin.Evans.文字版.epub|30069999|65BE9FECA52F61F09C2CB02A3951F8EA|h=GX6BXFAQE26F2D6ICNDGP4VQZTQWLVC6|/)

[[Understanding.Computation(2013.5)].Tom.Stuart.文字版.pdf](ed2k://|file|[Understanding.Computation(2013.5)].Tom.Stuart.文字版.pdf|9907063|5390496ac03b75fcabfc9e98cac8b84f|h=h6ikr4qwzmqf6zqnkso5ym7bfsr3abk2|/)

[[Understanding.Computation(2013.5)].Tom.Stuart.文字版.epub](ed2k://|file|[Understanding.Computation(2013.5)].Tom.Stuart.文字版.epub|3236861|54a92e8825cd496d15488bd5e84d7f5a|h=arsp4hbx2bandpuseytbypjkqmu6pbxq|/)

[[Relational.Theory.for.Computer.Professionals(2013.5)].C.J.Date.文字版.pdf](ed2k://|file|[Relational.Theory.for.Computer.Professionals(2013.5)].C.J.Date.文字版.pdf|26329320|e7736583600ea78a6aa213f9ef2e4ed6|h=sqsrefxcrm3soqoclgzrpfnvgy5vrigk|/)

[[Relational.Theory.for.Computer.Professionals(2013.5)].C.J.Date.文字版.epub](ed2k://|file|[Relational.Theory.for.Computer.Professionals(2013.5)].C.J.Date.文字版.epub|1990202|22ac9e041a88ad08abb270c65f732ff5|h=yuwxirv7thcirp3r5py3csgtcnmfarn3|/)

[[Vagrant：Up.and.Running(2013.5)].Mitchell.Hashimoto.文字版.pdf](ed2k://|file|[Vagrant：Up.and.Running(2013.5)].Mitchell.Hashimoto.文字版.pdf|6912008|0f97f97cb960219f55569d7fd5bc7569|h=rplv6hpqd2ymulp23dazqoemxtquknzh|/)

[[Vagrant：Up.and.Running(2013.5)].Mitchell.Hashimoto.文字版.epub](ed2k://|file|[Vagrant：Up.and.Running(2013.5)].Mitchell.Hashimoto.文字版.epub|1827235|32cfb7528e7d361e18433defcc2f4add|h=6xf7idx5nj7axydrzb76lij442weaon6|/)

[[Propose.Prepare.Present(2013.5)].Alistair.Croll.文字版.pdf](ed2k://|file|[Propose.Prepare.Present(2013.5)].Alistair.Croll.文字版.pdf|5751047|3149a14040955fac224a649b259fefb8|h=dtjk2youn5xflhhbvnawoildverwvdq5|/)

[[Propose.Prepare.Present(2013.5)].Alistair.Croll.文字版.epub](ed2k://|file|[Propose.Prepare.Present(2013.5)].Alistair.Croll.文字版.epub|1717884|fc7d42119964d17ab3e53f9913cd482c|h=mbt6evyddeeg23izxgul4ftfw3uxmw5y|/)

[[Office.2013：The.Missing.Manual(2013.5)].Nancy.Conner.文字版.pdf](ed2k://|file|[Office.2013：The.Missing.Manual(2013.5)].Nancy.Conner.文字版.pdf|61832755|7953fcdb1534d78431071803b354f63e|h=7ev5h4el7oe3onebxfr3pye2mmbqhdpq|/)

[[Office.2013：The.Missing.Manual(2013.5)].Nancy.Conner.文字版.epub](ed2k://|file|[Office.2013：The.Missing.Manual(2013.5)].Nancy.Conner.文字版.epub|30847611|6fe6d74907ee9ad7b07287f82d3682cf|h=x3fjkevybzokwgcvbtp4j2fcolzx4uaj|/)

[[Active.Directory.Cookbook(4th,2013.5)].Brian.Svidergol.文字版.pdf](ed2k://|file|[Active.Directory.Cookbook(4th,2013.5)].Brian.Svidergol.文字版.pdf|13783304|fbf4229021e21e2947a18c752b8f11e6|h=2nnb7mtfxatwemy26puf7aok36p3zpiy|/)

[[Active.Directory.Cookbook(4th,2013.5)].Brian.Svidergol.文字版.epub](ed2k://|file|[Active.Directory.Cookbook(4th,2013.5)].Brian.Svidergol.文字版.epub|3291971|7fcbc0043e7ad795ce0d85cf9989581a|h=gxyvwtkjlcmavosce6ro2ffulaq3npoi|/)

[[UX.for.Lean.Startups(2013.5)].Laura.Klein.文字版.pdf](ed2k://|file|[UX.for.Lean.Startups(2013.5)].Laura.Klein.文字版.pdf|7041944|96b69cba17cbb403290cb5b8a2503107|h=mp46u7k6itiwximhb374tqkzdyh3na6y|/)

[[UX.for.Lean.Startups(2013.5)].Laura.Klein.文字版.epub](ed2k://|file|[UX.for.Lean.Startups(2013.5)].Laura.Klein.文字版.epub|3312741|dda9ac7b65e5f69fff471255b0940885|h=hx6oboytskfumka4zrps4cmo3fzqwlab|/)

[[Python.Cookbook(3rd,2013.5)].David.Beazley.文字版.pdf](ed2k://|file|[Python.Cookbook(3rd,2013.5)].David.Beazley.文字版.pdf|10031645|b5b6f3ffc4b1ca744a09520814191c1a|h=dmflin2f5pstwkevbdg4e7wki5yzlrgk|/)

[[Python.Cookbook(3rd,2013.5)].David.Beazley.文字版.epub](ed2k://|file|[Python.Cookbook(3rd,2013.5)].David.Beazley.文字版.epub|1931252|21e88ba688784182f79f14dd65cfcdd6|h=oof42gykirfrrzmvvqje3r5fgagjfm6m|/)

[[MongoDB：The.Definitive.Guide(2nd,2013.5)].Kristina.Chodorow.文字版.pdf](ed2k://|file|[MongoDB：The.Definitive.Guide(2nd,2013.5)].Kristina.Chodorow.文字版.pdf|12203971|16adf9e5527bd0a13b06339d2256a707|h=3mqvy6um2kzn3z5gpcoxscrqnyiuiiyy|/)

[[MongoDB：The.Definitive.Guide(2nd,2013.5)].Kristina.Chodorow.文字版.epub](ed2k://|file|[MongoDB：The.Definitive.Guide(2nd,2013.5)].Kristina.Chodorow.文字版.epub|5924060|df645afd11eed3b41af40f6e70c6ddf4|h=jgeehwal2sjm77vsgi4h5jzfd4oaryg3|/)

[[Linux.System.Programming(2nd,2013.5)].Robert.Love.文字版.pdf](ed2k://|file|[Linux.System.Programming(2nd,2013.5)].Robert.Love.文字版.pdf|9615120|2e7af33104582241dc8ddec97c1b4b6e|h=uhayjgobxnqqunnevosfebdwtu3kn2lt|/)

[[Linux.System.Programming(2nd,2013.5)].Robert.Love.文字版.epub](ed2k://|file|[Linux.System.Programming(2nd,2013.5)].Robert.Love.文字版.epub|1612555|5b904c43add896fe60b5223886adf80f|h=rv4e44lk7k2vl2f3z34yv4h4ui4olmi4|/)

[[Developing.Backbone.js.Applications(2013.5)].Addy.Osmani.文字版.pdf](ed2k://|file|[Developing.Backbone.js.Applications(2013.5)].Addy.Osmani.文字版.pdf|14108168|353b26122b0d828baf226cda02e565ef|h=fbmjuvguzafgz6a2dej4jbqpxjr3j3yb|/)

[[Developing.Backbone.js.Applications(2013.5)].Addy.Osmani.文字版.epub](ed2k://|file|[Developing.Backbone.js.Applications(2013.5)].Addy.Osmani.文字版.epub|6342962|ab94d81c1c7e725056072ebb7c398c91|h=r5l567h46aeztb2o25d5y6ey3yht6kvq|/)

[[Bootstrap(2013.5)].Jake.Spurlock.文字版.pdf](ed2k://|file|[Bootstrap(2013.5)].Jake.Spurlock.文字版.pdf|13388951|7941ab86817ab7dee5a5746760e4ff7a|h=chy4rn4emtclavsdmjex43t3nalj52eg|/)

[[Bootstrap(2013.5)].Jake.Spurlock.文字版.epub](ed2k://|file|[Bootstrap(2013.5)].Jake.Spurlock.文字版.epub|6798982|9cfef5c1401f7e88d313ea97e2eea50d|h=aiv7t22ryxvwk6salmuxpn66zjilxk6g|/)

[[Asterisk：The.Definitive.Guide(4th,2013.5)].Russell.Bryant.文字版.pdf](ed2k://|file|[Asterisk：The.Definitive.Guide(4th,2013.5)].Russell.Bryant.文字版.pdf|16415627|75a1fe856a113d960c0d43d56d0bf9a2|h=dxzohfmx7xhvfr7pvd6zddtfrtyjyg5g|/)

[[Asterisk：The.Definitive.Guide(4th,2013.5)].Russell.Bryant.文字版.epub](ed2k://|file|[Asterisk：The.Definitive.Guide(4th,2013.5)].Russell.Bryant.文字版.epub|7073693|e38209fb1aace79eb111461d84ddd35d|h=5n2qcnmqi4gi72sxlv7jb5bo2kosbt3i|/)

[[Ajax.in.Action(2005.10)].Dave.Crane.文字版.pdf](ed2k://|file|[Ajax.in.Action(2005.10)].Dave.Crane.文字版.pdf|10171494|5206E8A26A5283894B001306D51F31D6|h=Y6ATC3C2A4X5M5ZZSDG4AXS43LJP4HKK|/)

[[Algorithms.of.the.Intelligent.Web(2009.5)].Haralambos.Marmanis.文字版.pdf](ed2k://|file|[Algorithms.of.the.Intelligent.Web(2009.5)].Haralambos.Marmanis.文字版.pdf|10039398|0B565573F5FDDB7CDE17E2CA48A31F81|h=ND4W6HUNG6OOHBACYGT2PYNE4OUK6PCB|/)

[[Adobe.AIR.in.Action(2008.7)].Joey.Lott.文字版.pdf](ed2k://|file|[Adobe.AIR.in.Action(2008.7)].Joey.Lott.文字版.pdf|15241392|6D64F2E2CC481787DC07B60F16EE9AF7|h=RCHFSDZKHR5ROOPO4Q776XPDO22N4W3E|/)

[[Lucene.in.Action(2nd,2010.7)].Michael.McCandless.文字版.pdf](ed2k://|file|[Lucene.in.Action(2nd,2010.7)].Michael.McCandless.文字版.pdf|15761372|8E819AC23EF2348EFE43BE6C6BBBE9AE|h=QDJBZ34DXJKNNRHD3IRGD7IQQU2AXJQH|/)

[[Struts.2.in.Action(2008.5)].Donald.Brown.文字版.pdf](ed2k://|file|[Struts.2.in.Action(2008.5)].Donald.Brown.文字版.pdf|6257182|ABF4DCF5832CA96110A196EAD2224F18|h=2V6LEQCMZI6FWSCB5IKGM565SDZWADZJ|/)

[[Hibernate.in.Action(2004)].Christian.Bauer.文字版.pdf](ed2k://|file|[Hibernate.in.Action(2004)].Christian.Bauer.文字版.pdf|2619383|8DB055EA2F741338AECD4FE92E4BA322|h=45YDRO47PUZY3ZNPLO55JIVRAI6NTSRS|/)

[[Hibernate.Search.in.Action(2008.12)].Emmanuel.Bernard.文字版.pdf](ed2k://|file|[Hibernate.Search.in.Action(2008.12)].Emmanuel.Bernard.文字版.pdf|11501372|B86AE735B1AFAF77B7A4B49337443E5C|h=ALPGTSV2E23FSRFLBFKRVQBFJEEXX7JZ|/)

[[Java.Persistence.with.Hibernate(2006.11)].Christian.Bauer.文字版.pdf](ed2k://|file|[Java.Persistence.with.Hibernate(2006.11)].Christian.Bauer.文字版.pdf|10829912|58ED3AEE5A143C9E49497C24217C3095|h=IF6DWHWEIOZB4D622KMFCLGQVM4VHNVE|/)

[[PHP.Web.Services(2013.4)].Lorna.Jane.Mitchell.文字版.pdf](ed2k://|file|[PHP.Web.Services(2013.4)].Lorna.Jane.Mitchell.文字版.pdf|10737294|fc58f5dd9ea9b6de7df8190b774fd0a1|h=rtbqsw5qrpkj6dyhz3ci7mp6ptvxihul|/)

[[PHP.Web.Services(2013.4)].Lorna.Jane.Mitchell.文字版.epub](ed2k://|file|[PHP.Web.Services(2013.4)].Lorna.Jane.Mitchell.文字版.epub|3751310|4ad0d7f14a107a20ba4c1eee29db5f24|h=eogos665gfavlj2d24il4y2glj7a5dwh|/)

[[JavaScript.for.PHP.Developers(2013.4)].Stoyan.Stefanov.文字版.pdf](ed2k://|file|[JavaScript.for.PHP.Developers(2013.4)].Stoyan.Stefanov.文字版.pdf|8317943|c15b25ed4329ee1cf872438a9b5f759a|h=pwqt4vzp2lwv2xhjihjvfkerucver4xz|/)

[[JavaScript.for.PHP.Developers(2013.4)].Stoyan.Stefanov.文字版.epub](ed2k://|file|[JavaScript.for.PHP.Developers(2013.4)].Stoyan.Stefanov.文字版.epub|2186812|039d061ab0bfeac9cb9f42670f8a03a8|h=maqivrivjx4ny6h3vpydvq3sfldjr7g4|/)

[[HTML5.Canvas(2nd,2013.4)].Steve.Fulton.文字版.pdf](ed2k://|file|[HTML5.Canvas(2nd,2013.4)].Steve.Fulton.文字版.pdf|30651902|114ac3016895f096a853fc73703d1279|h=kbvzova74pstnsrgmbugfts7esetjodx|/)

[[HTML5.Canvas(2nd,2013.4)].Steve.Fulton.文字版.epub](ed2k://|file|[HTML5.Canvas(2nd,2013.4)].Steve.Fulton.文字版.epub|9202004|6daca6deaa91337152747777888d9a2c|h=23jutj6imhqma2ygv7dwpnr5hrcxyqyd|/)

[[Computer.Science.Programming.Basics.in.Ruby(2013.4)].Ophir.Frieder.文字版.pdf](ed2k://|file|[Computer.Science.Programming.Basics.in.Ruby(2013.4)].Ophir.Frieder.文字版.pdf|8013002|9372509046fee30c0ece1e88cb3bcedf|h=ibpjdllpumdgt2pt4irgi46c5zykuemr|/)

[[Computer.Science.Programming.Basics.in.Ruby(2013.4)].Ophir.Frieder.文字版.epub](ed2k://|file|[Computer.Science.Programming.Basics.in.Ruby(2013.4)].Ophir.Frieder.文字版.epub|2659288|fb2848f4191cdd7fd5babbf36245950b|h=yzianveonyogwgvuyv63ndlypkkes2ts|/)

[[AngularJS(2013.4)].Brad.Green.文字版.pdf](ed2k://|file|[AngularJS(2013.4)].Brad.Green.文字版.pdf|8264080|1a4c2932413864867915d05c8ce511ed|h=tpxsmcs7x5qgirveibvk4k5kygbc4xlg|/)

[[AngularJS(2013.4)].Brad.Green.文字版.epub](ed2k://|file|[AngularJS(2013.4)].Brad.Green.文字版.epub|1768226|d26a61f4d658bbaba9cacbb6eb1ceab5|h=ircdmuqnb42pb5cfia6q4fp6tudocsxd|/)

[[Windows.8：The.Missing.Manual(2013.2)].David.Pogue.文字版.pdf](ed2k://|file|[Windows.8：The.Missing.Manual(2013.2)].David.Pogue.文字版.pdf|65507785|09e3002eda8c9571a50a3e8dbd70eabd|h=ni2kf44dadhwcbpval7pdtt2h3562blr|/)

[[Windows.8：The.Missing.Manual(2013.2)].David.Pogue.文字版.epub](ed2k://|file|[Windows.8：The.Missing.Manual(2013.2)].David.Pogue.文字版.epub|30543909|4bdc434cebcc716ff62cd73c5d0b0648|h=ze22gx6jgm6nfl3q2jyi2tompwctwgmz|/)

[[NOOK.HD：The.Missing.Manual(2nd,2013.2)].Preston.Gralla.文字版.pdf](ed2k://|file|[NOOK.HD：The.Missing.Manual(2nd,2013.2)].Preston.Gralla.文字版.pdf|48916145|2a8be301cd5b00ddc69955cb690f1b33|h=k35qlkmfwxvp7d7btciijx6pdoml2myv|/)

[[NOOK.HD：The.Missing.Manual(2nd,2013.2)].Preston.Gralla.文字版.epub](ed2k://|file|[NOOK.HD：The.Missing.Manual(2nd,2013.2)].Preston.Gralla.文字版.epub|35549134|30ae44b90abd10a14ca9d98ea8984341|h=2p72zl7beroxrsjidbsg77lso7lgo6no|/)

[[Kindle.Fire.HD：The.Missing.Manual(2nd,2013.1)].Peter.Meyers.文字版.pdf](ed2k://|file|[Kindle.Fire.HD：The.Missing.Manual(2nd,2013.1)].Peter.Meyers.文字版.pdf|24316468|69f5f9272dc63b7adac74da9a5e460d8|h=ieed7k2jryfpyczfsqrvwfjvfeiwofvy|/)

[[Kindle.Fire.HD：The.Missing.Manual(2nd,2013.1)].Peter.Meyers.文字版.epub](ed2k://|file|[Kindle.Fire.HD：The.Missing.Manual(2nd,2013.1)].Peter.Meyers.文字版.epub|18397103|9a8ca979d2e51bcb6101b154d774ae2a|h=4cq6ftwobtwgsozudup4n6peyttb2jqu|/)

[[Microsoft.Project.2013：The.Missing.Manual(2013.4)].Bonnie.Biafore.文字版.pdf](ed2k://|file|[Microsoft.Project.2013：The.Missing.Manual(2013.4)].Bonnie.Biafore.文字版.pdf|31390950|13e5c175aa7fa49547b07b121eb6a575|h=mmpeeczdpr7az3sd6ge6szyyjo22i2ts|/)

[[Microsoft.Project.2013：The.Missing.Manual(2013.4)].Bonnie.Biafore.文字版.epub](ed2k://|file|[Microsoft.Project.2013：The.Missing.Manual(2013.4)].Bonnie.Biafore.文字版.epub|19835926|2e53c4cea8ac587de2f21d9464ce8fca|h=uufleutc3nv62zlvlxejv4f2bjntua5i|/)

[[Excel.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.pdf](ed2k://|file|[Excel.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.pdf|33332766|b6f164b139ad8ed261d0f7b9d4c052ca|h=qlxwltof5wx3jjhnnwaa7clwe5hzwc47|/)

[[Excel.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.epub](ed2k://|file|[Excel.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.epub|30173646|9c139ffea932ec9819fd20c30f9234a5|h=gbiftqvn4qzei56fqclxzochzwnwsrxp|/)

[[Access.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.pdf](ed2k://|file|[Access.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.pdf|33411953|41756ca19df859404490744274c593da|h=bhtf44blox53vegjkg4nknjcycqzmto2|/)

[[Access.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.epub](ed2k://|file|[Access.2013：The.Missing.Manual(2013.4)].Matthew.MacDonald.文字版.epub|23990622|1ecc0ce820a4710ac545bf6055c7033c|h=the4hjj3zrxugqvmshp4qulvcp3c3pzj|/)

[[Active.Directory(5th,2013.4)].Brian.Desmond.文字版.pdf](ed2k://|file|[Active.Directory(5th,2013.4)].Brian.Desmond.文字版.pdf|24880009|22EB34453D45AE50B3031A9E5ADF4F61|h=4IRGNZM3KO2U7UZRQSDUKKGJPPNIFLY7|/)

[[Active.Directory(5th,2013.4)].Brian.Desmond.文字版.epub](ed2k://|file|[Active.Directory(5th,2013.4)].Brian.Desmond.文字版.epub|18229691|BD0E9C61EB3B36796B2C95D1A4900313|h=2GLJZ7STO54E326WTKLFMEX5LLCHZHUE|/)

[[Programming.Grails(2013.4)].Burt.Beckwith.文字版.pdf](ed2k://|file|[Programming.Grails(2013.4)].Burt.Beckwith.文字版.pdf|9495146|BDA8D995767F30E2243B1069669ED55C|h=T5BQ7ZAB64Z2WCFAKWPTDD4TSGINRQFQ|/)

[[Programming.Grails(2013.4)].Burt.Beckwith.文字版.epub](ed2k://|file|[Programming.Grails(2013.4)].Burt.Beckwith.文字版.epub|2491734|9163FB7D717F43B06E5156D6E9996A12|h=T652F2CPC7MRVIGHWSIPWXIHMLCUUIKP|/)

[[Understanding.and.Using.C.Pointers(2013.5)].Richard.Reese.文字版.pdf](ed2k://|file|[Understanding.and.Using.C.Pointers(2013.5)].Richard.Reese.文字版.pdf|7700942|551AC4B2B6DEAED091BDB05208268FFF|h=YJXD3M7COHYJW5ZT3EFYRK65JJISBPR7|/)

[[Understanding.and.Using.C.Pointers(2013.5)].Richard.Reese.文字版.epub](ed2k://|file|[Understanding.and.Using.C.Pointers(2013.5)].Richard.Reese.文字版.epub|3919242|7EBEB318A68C916104527AEED21A84C1|h=AZ5CKYFEJZAE2F3ID2USSRBPMGLFXHB3|/)

[[Physics.for.Game.Developers(2nd,2013.4)].David.M.Bourg.文字版.pdf](ed2k://|file|[Physics.for.Game.Developers(2nd,2013.4)].David.M.Bourg.文字版.pdf|16637365|0EA05A5618D6B7893FBACF4E59B4EB4B|h=ZARSAXTYHRAFQ25YAUJ5YGIPI6EH2ACP|/)

[[Physics.for.Game.Developers(2nd,2013.4)].David.M.Bourg.文字版.epub](ed2k://|file|[Physics.for.Game.Developers(2nd,2013.4)].David.M.Bourg.文字版.epub|8836243|055998AAF2C741D88E5A522037EFC95A|h=YZZRLXC4S7RXKXA5QNJDSRZYR4VYKZZG|/)

[[Microinteractions(2013.4)].Dan.Saffer.文字版.pdf](ed2k://|file|[Microinteractions(2013.4)].Dan.Saffer.文字版.pdf|25760782|1043EE0E226407EFB523910037A548F8|h=JSFCMDMYDV3BJSURRI2GKJPWIBOFX437|/)

[[Microinteractions(2013.4)].Dan.Saffer.文字版.epub](ed2k://|file|[Microinteractions(2013.4)].Dan.Saffer.文字版.epub|7805376|62CB41594064506E7BA2275A4BF2C5A5|h=IMDP77KZ3MDRBHPQCL46KEVJLJRCTPLU|/)

[[Managing.Startups：Best.Blog.Posts(2013.5)].Thomas.Eisenmann.文字版.pdf](ed2k://|file|[Managing.Startups：Best.Blog.Posts(2013.5)].Thomas.Eisenmann.文字版.pdf|24020605|8E84F866FDD345ABAC82501694C39954|h=6UPMGNI5OI5ZHWPEDDGXRDRQ6PPTWJAV|/)

[[Managing.Startups：Best.Blog.Posts(2013.5)].Thomas.Eisenmann.文字版.epub](ed2k://|file|[Managing.Startups：Best.Blog.Posts(2013.5)].Thomas.Eisenmann.文字版.epub|8881125|F0A527F3827137D55CE7D4486CBC8E1C|h=UB3WNRODZU6HOJJK7DHDZKYYKLF4R3MJ|/)

[[Building.a.DevOps.Culture(2013.4)].Mandi.Walls.文字版.pdf](ed2k://|file|[Building.a.DevOps.Culture(2013.4)].Mandi.Walls.文字版.pdf|5291428|CC96508E03BFBC08FD22D19100FB6499|h=BQKUNWR4DEIQUDMRTKYSBFFNYKGA34ER|/)

[[Building.a.DevOps.Culture(2013.4)].Mandi.Walls.文字版.epub](ed2k://|file|[Building.a.DevOps.Culture(2013.4)].Mandi.Walls.文字版.epub|3307298|CE450F09E9678D3161F5B659B52D7709|h=UNPRMZWJIHKJHY6VIZ3ZQ5PTVXGGYCCY|/)

[[Learning.Ext.JS.3.2(2010.10)].Shea.Frederick.文字版.pdf](ed2k://|file|[Learning.Ext.JS.3.2(2010.10)].Shea.Frederick.文字版.pdf|11787818|6B707F2DA858E272F9DFE01761A8A835|h=E3EUFEOF35WNYPFCNHUSXTV7KQVNUOGG|/)

[[Ext.JS.3.0.Cookbook(2009.10)].Jorge.Ramon.文字版.pdf](ed2k://|file|[Ext.JS.3.0.Cookbook(2009.10)].Jorge.Ramon.文字版.pdf|4437352|A103782BDFC6D3E49250FC1B1F8B2258|h=TG7CYXJ7VCLXRB3VEELYVV46JW7CBCA7|/)

[[Learning.Ext.JS(2008.11)].Colin.Ramsay.文字版.pdf](ed2k://|file|[Learning.Ext.JS(2008.11)].Colin.Ramsay.文字版.pdf|8353593|6FD3731F909216C5A502FEC2FD504690|h=T4FWUAZ65WMQ2FBD2XC7AMXRBHCDOLFL|/)

[[Learning.Ext.JS.4(2013.1)].Crysfel.Villa.文字版.pdf](ed2k://|file|[Learning.Ext.JS.4(2013.1)].Crysfel.Villa.文字版.pdf|9302247|7264D63C7954EBDC4611C3A1DB3DBDA2|h=SKYBTYP7QQSRZYGTXBVDU2UDQTBGMVLN|/)

[[Learning.Ext.JS.4(2013.1)].Crysfel.Villa.文字版.epub](ed2k://|file|[Learning.Ext.JS.4(2013.1)].Crysfel.Villa.文字版.epub|5394936|A81435243E78ACC4DEAAE893F3E0B057|h=44Y32XB5RJX3GT6NF6GYWQOI45GDMEDB|/)

[[Ext.JS.4.Web.Application.Development.Cookbook(2012.8)].Andrew.Duncan.文字版.pdf](ed2k://|file|[Ext.JS.4.Web.Application.Development.Cookbook(2012.8)].Andrew.Duncan.文字版.pdf|4216369|512F8619955B840CD0DA0BAAE7F78797|h=ZJL7XBWMSZAV3I3A2ZS3KPDCQICUWAYV|/)

[[Ext.JS.4.Web.Application.Development.Cookbook(2012.8)].Andrew.Duncan.文字版.epub](ed2k://|file|[Ext.JS.4.Web.Application.Development.Cookbook(2012.8)].Andrew.Duncan.文字版.epub|5774518|08514A2598D1B6380862FB794CD7DFFC|h=5F2KIL4M6MUXA3AKSLWC4H4GJGYAJGUL|/)

[[Ext.JS.4.First.Look(2012.1)].Loiane.Groner.文字版.pdf](ed2k://|file|[Ext.JS.4.First.Look(2012.1)].Loiane.Groner.文字版.pdf|5179336|A94CACBDD541FAA6D217808DE14DA1F5|h=CVTVN6FZVFIILVYG6MPG4ARSAC67BQGA|/)

[[Ext.JS.4.First.Look(2012.1)].Loiane.Groner.文字版.epub](ed2k://|file|[Ext.JS.4.First.Look(2012.1)].Loiane.Groner.文字版.epub|7690532|8DF319B986B669F32FE8CFED5D9BECD1|h=PPQAURZ3PULYGMAMNTYIHB7RTB3PMLZC|/)

[[The.Art.of.Unit.Testing(2009.5)].Roy.Osherove.文字版.pdf](ed2k://|file|[The.Art.of.Unit.Testing(2009.5)].Roy.Osherove.文字版.pdf|8286298|6550157909A8349D796158BF0A182B79|h=XAWXC6HCCOBNSLLBVVJVS7J2VSEBJEI3|/)

[[Learning.JavaScript(2nd,2008.12)].Shelley.Powers.文字版.pdf](ed2k://|file|[Learning.JavaScript(2nd,2008.12)].Shelley.Powers.文字版.pdf|7159491|B024E36CFB8F1BF7CE239D2BC7245B4D|h=SF7LNQJ5MFRREUF6W63NEXBWC2FM4DTR|/)

[[ZeroMQ(2013.3)].Pieter.Hintjens.文字版.pdf](ed2k://|file|[ZeroMQ(2013.3)].Pieter.Hintjens.文字版.pdf|11441436|3958dd52f37e418eba47ad2942ec2a75|h=thihgwtx7fo4dhi6pmbklj7ci45r4mtr|/)

[[ZeroMQ(2013.3)].Pieter.Hintjens.文字版.epub](ed2k://|file|[ZeroMQ(2013.3)].Pieter.Hintjens.文字版.epub|4866639|72ce331c2e5801c053a037ec5157d741|h=cg6bf5bpl4uzduq3ve7fg4aofbvc7z45|/)

[[User-Centered.Design(2013.3)].Travis.Lowdermilk.文字版.pdf](ed2k://|file|[User-Centered.Design(2013.3)].Travis.Lowdermilk.文字版.pdf|18067030|881e794eecc937093c7d264dc7606523|h=xvojxdpcpuk6mza3be3ppzy55hrnziql|/)

[[User-Centered.Design(2013.3)].Travis.Lowdermilk.文字版.epub](ed2k://|file|[User-Centered.Design(2013.3)].Travis.Lowdermilk.文字版.epub|3131424|2440bea7a8c8aa967ebac1312f1464f5|h=xvvfpa5l5wpiqomt3xzvloqph2zqgnzg|/)

[[Programming.the.Mobile.Web(2nd,2013.3)].Maximiliano.Firtman.文字版.pdf](ed2k://|file|[Programming.the.Mobile.Web(2nd,2013.3)].Maximiliano.Firtman.文字版.pdf|59110630|ed22c0e1014d939c5692d4729fec00fe|h=y3kwzkatpcsud2cup4bzjc5hvqvzlniu|/)

[[Programming.the.Mobile.Web(2nd,2013.3)].Maximiliano.Firtman.文字版.epub](ed2k://|file|[Programming.the.Mobile.Web(2nd,2013.3)].Maximiliano.Firtman.文字版.epub|23488667|ebd8601d47c7232b8fde4b9790eec7d0|h=neyyrjvonzosjro5xrp4m6dopbr546ts|/)

[[JavaScript.Testing.with.Jasmine(2013.3)].Evan.Hahn.文字版.pdf](ed2k://|file|[JavaScript.Testing.with.Jasmine(2013.3)].Evan.Hahn.文字版.pdf|1534780|31e4393f32721651b2d966f22018c36c|h=jtamnq7rqas2zwvw5ikhgl3ctacqganh|/)

[[JavaScript.Testing.with.Jasmine(2013.3)].Evan.Hahn.文字版.epub](ed2k://|file|[JavaScript.Testing.with.Jasmine(2013.3)].Evan.Hahn.文字版.epub|1427036|93f60ac4db285010739a66db79ccf03a|h=jlzcvbo6tbz2a4mcb6kftkxuwvhz35im|/)

[[Industrial.Internet(2013.3)].Jon.Bruner.文字版.pdf](ed2k://|file|[Industrial.Internet(2013.3)].Jon.Bruner.文字版.pdf|3121534|5e544b3303521ae04986ac708b4d2955|h=wzo643jidtwmutvxc7uv5gtbxxkusgvq|/)

[[Industrial.Internet(2013.3)].Jon.Bruner.文字版.epub](ed2k://|file|[Industrial.Internet(2013.3)].Jon.Bruner.文字版.epub|2522968|6b2dfd095106a3784a92473820397703|h=inll3f74vemfli2c3gq6bzvv2ivejfoj|/)

[[Études.for.Erlang(2013.3)].J.David.Eisenberg.文字版.pdf](ed2k://|file|[Études.for.Erlang(2013.3)].J.David.Eisenberg.文字版.pdf|4030983|7a59c35f5b04ee33fb03a46e551ecde3|h=2y7bysuocltvb52k33am2emihh37oopl|/)

[[Études.for.Erlang(2013.3)].J.David.Eisenberg.文字版.epub](ed2k://|file|[Études.for.Erlang(2013.3)].J.David.Eisenberg.文字版.epub|1883653|89e49347dc3b748a125f54c8dd8987b8|h=knwjpiumi3dbdbr6a5fx37ucr53gd5oh|/)

[[Ethernet.Switches(2013.4)].Charles.E.Spurgeon.文字版.pdf](ed2k://|file|[Ethernet.Switches(2013.4)].Charles.E.Spurgeon.文字版.pdf|7377476|1a3cf06069ea3d3580ccf2ea567bfe14|h=cje27tvqym2neynklgska7ozow3hunet|/)

[[Ethernet.Switches(2013.4)].Charles.E.Spurgeon.文字版.epub](ed2k://|file|[Ethernet.Switches(2013.4)].Charles.E.Spurgeon.文字版.epub|3038286|dc57a152feb9ed37212e534dc4fc512d|h=sxeaszn5fsj37jiwzh56qhxgibr4wqfm|/)

[[Embedded.Android(2013.3)].Karim.Yaghmour.文字版.pdf](ed2k://|file|[Embedded.Android(2013.3)].Karim.Yaghmour.文字版.pdf|12062338|00acc1afce7e6e4c5a5477ddf84f8854|h=ocdophqnjxorvxvkdghs6hz6ao6h7de5|/)

[[Embedded.Android(2013.3)].Karim.Yaghmour.文字版.epub](ed2k://|file|[Embedded.Android(2013.3)].Karim.Yaghmour.文字版.epub|5079645|fa399124ce2b576ccf2382973ec56b23|h=bswxxwlauaxv5sey44c6kknu4zbszv2t|/)

[[DIY.Instruments.for.Amateur.Space(2013.3)].Sandy.Antunes.文字版.pdf](ed2k://|file|[DIY.Instruments.for.Amateur.Space(2013.3)].Sandy.Antunes.文字版.pdf|19405113|4c1aa38829056625ae97cf79056309c7|h=kd2lingmstwu2ivtemmxqgojhraw74bv|/)

[[DIY.Instruments.for.Amateur.Space(2013.3)].Sandy.Antunes.文字版.epub](ed2k://|file|[DIY.Instruments.for.Amateur.Space(2013.3)].Sandy.Antunes.文字版.epub|4400679|6c376aaa47e7f1a997e409c168cbb529|h=7zvybcdu5ip3cdhx7qy23jbpghyxzyoj|/)

[[Twisted.Network.Programming.Essentials(2nd,2013.3)].Jessica.McKellar.文字版.pdf](ed2k://|file|[Twisted.Network.Programming.Essentials(2nd,2013.3)].Jessica.McKellar.文字版.pdf|10175977|a96a57275663959efe9fa8f2ef7bc57f|h=dnnwa5batewk5lihvtoxf6gwol3ctnaw|/)

[[Twisted.Network.Programming.Essentials(2nd,2013.3)].Jessica.McKellar.文字版.epub](ed2k://|file|[Twisted.Network.Programming.Essentials(2nd,2013.3)].Jessica.McKellar.文字版.epub|2290223|9fa92b94e17c2d00223067cd51d4e1ff|h=n5zk74ubgpfyxieansw2ryxbfgjul4fe|/)

[[Appcelerator.Titanium：Up.and.Running(2013.3)].John.Anderson.文字版.pdf](ed2k://|file|[Appcelerator.Titanium：Up.and.Running(2013.3)].John.Anderson.文字版.pdf|11198103|7eeb84132ccb951a20b686141e4c5eb7|h=lttyepsre5rsns642yv6wlfyaz4qz4di|/)

[[Appcelerator.Titanium：Up.and.Running(2013.3)].John.Anderson.文字版.epub](ed2k://|file|[Appcelerator.Titanium：Up.and.Running(2013.3)].John.Anderson.文字版.epub|3450823|8cb12f4e790891b0160aa96b128faf1e|h=jpten4evblpvhf5mguhanmqythd2l4by|/)

[[Interactive.Data.Visualization.for.the.Web(2013.3)].Scott.Murray.文字版.pdf](ed2k://|file|[Interactive.Data.Visualization.for.the.Web(2013.3)].Scott.Murray.文字版.pdf|11027241|cb429e4636e54b965bcf5fc9c899d70b|h=wqvgyxyovbf3gfeikmp3rlcrahgxj4np|/)

[[Interactive.Data.Visualization.for.the.Web(2013.3)].Scott.Murray.文字版.epub](ed2k://|file|[Interactive.Data.Visualization.for.the.Web(2013.3)].Scott.Murray.文字版.epub|5052679|f4598649fe41877eff27980f70f8385c|h=a4dg6wguiadp7d6sskdvqm3tvcdcand4|/)

[[Web.Audio.API(2013.3)].Boris.Smus.文字版.pdf](ed2k://|file|[Web.Audio.API(2013.3)].Boris.Smus.文字版.pdf|10101291|0f3a1a3239274548aefcced131456633|h=hizbzbls2kuttgpawlbiqgd4cmwhjwsl|/)

[[Web.Audio.API(2013.3)].Boris.Smus.文字版.epub](ed2k://|file|[Web.Audio.API(2013.3)].Boris.Smus.文字版.epub|2492196|3ea438b0e28bfdd1f40e5fe7c868157f|h=deysnwyj5g6hllqyl5mhyeo4c522kz5l|/)

[[The.New.Kingmakers(2013.3)].Stephen.O'Grady.文字版.pdf](ed2k://|file|[The.New.Kingmakers(2013.3)].Stephen.O'Grady.文字版.pdf|1590067|a74c82b11be9b4433c4754349203636f|h=cqug5xtvkdfhtmoal6rs3vb2wfz2lftf|/)

[[The.New.Kingmakers(2013.3)].Stephen.O'Grady.文字版.epub](ed2k://|file|[The.New.Kingmakers(2013.3)].Stephen.O'Grady.文字版.epub|2343837|f8b5f98383e98aa1dd12663e1af68d89|h=ijicqzxciaee7njacdkfz3domot3vtls|/)

[[MongoDB.Applied.Design.Patterns(2013.3)].Rick.Copeland.文字版.pdf](ed2k://|file|[MongoDB.Applied.Design.Patterns(2013.3)].Rick.Copeland.文字版.pdf|9729582|a6892b02de846fa5a0e0b5f1d5aed2c3|h=qaeezfmwtadln2k3nqlyaaytdbnuvso4|/)

[[MongoDB.Applied.Design.Patterns(2013.3)].Rick.Copeland.文字版.epub](ed2k://|file|[MongoDB.Applied.Design.Patterns(2013.3)].Rick.Copeland.文字版.epub|2413967|bdffb10f15dbd5c1c48ff431818cdde1|h=dcw2lqweyakpokv7r5w5eotfwvxlawt3|/)

[[Learning.iOS.Programming(3rd,2013.3)].Alasdair.Allan.文字版.pdf](ed2k://|file|[Learning.iOS.Programming(3rd,2013.3)].Alasdair.Allan.文字版.pdf|99850733|9ea3af440a3c9f3730e672122e0aa147|h=43l4rj3jhbx27cc4jasnlfcwcwdza5ol|/)

[[Learning.iOS.Programming(3rd,2013.3)].Alasdair.Allan.文字版.epub](ed2k://|file|[Learning.iOS.Programming(3rd,2013.3)].Alasdair.Allan.文字版.epub|31429229|ec0782964d3dddb65637020c416e625b|h=xebdx3bdlertwngvaty7ykikrcrs3ack|/)

[[Lean.Analytics(2013.3)].Alistair.Croll.文字版.pdf](ed2k://|file|[Lean.Analytics(2013.3)].Alistair.Croll.文字版.pdf|12193620|9aeb83a8172ded9d78a5c6d1006172b4|h=buksa7ass76bs3gvg3xoqzujvs2hhdzl|/)

[[Lean.Analytics(2013.3)].Alistair.Croll.文字版.epub](ed2k://|file|[Lean.Analytics(2013.3)].Alistair.Croll.文字版.epub|7626340|a834bace5c72151676e909373216eb43|h=ha4sfbx4t5pqh3s4kk7wsfujgt4pnb2v|/)

[[Thoughts.on.Interaction.Design(2007.3)].Jon.Kolko.文字版.pdf](ed2k://|file|[Thoughts.on.Interaction.Design(2007.3)].Jon.Kolko.文字版.pdf|2504417|6AAA1737DB9626F254AB8DB07A856B4B|h=BLALGZ4WC2EBJRE3SQ5NGD7JCOVCOTSG|/)

[[Third-Party.JavaScript(2013.3)].Ben.Vinegar.文字版.pdf](ed2k://|file|[Third-Party.JavaScript(2013.3)].Ben.Vinegar.文字版.pdf|7079183|990cf8112a8cb2585099a680bc54f0d1|h=x5rrzhgr2xugcc6iekar7wykugaqtvm6|/)

[[Third-Party.JavaScript(2013.3)].Ben.Vinegar.文字版.epub](ed2k://|file|[Third-Party.JavaScript(2013.3)].Ben.Vinegar.文字版.epub|10350029|281a6b823dbb2fd39446085f172cd29f|h=v3vqojmmc7x5b55ycbwtwmjjktqqktmd|/)

[[PowerShell.in.Depth(2013.2)].Don.Jones.文字版.pdf](ed2k://|file|[PowerShell.in.Depth(2013.2)].Don.Jones.文字版.pdf|16252513|37b1878915996703c2f81b9133dd996a|h=6bzcnh3jdyjpuybbdecbrwp6twa6pkcz|/)

[[PowerShell.in.Depth(2013.2)].Don.Jones.文字版.epub](ed2k://|file|[PowerShell.in.Depth(2013.2)].Don.Jones.文字版.epub|6250749|1c82aa39e1cc20f1900c785b071d117b|h=jbhfismhjl3wctwyqwqorzyenxaemorw|/)

[[GWT.in.Action(2nd,2013.1)].Adam.Tacy.文字版.pdf](ed2k://|file|[GWT.in.Action(2nd,2013.1)].Adam.Tacy.文字版.pdf|31950974|83b0f7beecf9a70ad1ffeaf8ae2eeb8e|h=mdwxh75yqzuho5ntaxbc4ktxsrs6gj4s|/)

[[GWT.in.Action(2nd,2013.1)].Adam.Tacy.文字版.epub](ed2k://|file|[GWT.in.Action(2nd,2013.1)].Adam.Tacy.文字版.epub|36674250|870ac41ab93a9d5d728b0880ab371e07|h=crkx6uu67imjhc7qetozrsat2eqpcns5|/)

[[Enterprise.OSGi.in.Action(2013.3)].Holly.Cummins.文字版.pdf](ed2k://|file|[Enterprise.OSGi.in.Action(2013.3)].Holly.Cummins.文字版.pdf|22822254|2d7b48edca22443ac007b1ff6db8c676|h=5sf6iui2tme2ettcmxyfw6ipemkdxtmj|/)

[[Effective.Unit.Testing(2013.2)].Lasse.Koskela.文字版.pdf](ed2k://|file|[Effective.Unit.Testing(2013.2)].Lasse.Koskela.文字版.pdf|4216001|c104bdeefc9f90c9709fab4865f135ae|h=qnjpql32x7cp5oaanbasjjqdpablv24u|/)

[[Effective.Unit.Testing(2013.2)].Lasse.Koskela.文字版.epub](ed2k://|file|[Effective.Unit.Testing(2013.2)].Lasse.Koskela.文字版.epub|7322416|38d2ab3f2eded045c55d6a06f16e37c6|h=zh6u2bclrkvzwc5y5goau6roqyidy3zg|/)

[[The.Lean.Startup(2011.9)].Eric.Ries.文字版.epub](ed2k://|file|[The.Lean.Startup(2011.9)].Eric.Ries.文字版.epub|2606924|FB4EFC410E71612D4892772FF66778DB|h=ADOFGXYCWCLRKIKGY2ZZXYJQ2WVNWWZM|/)

[[The.Lean.Startup(2011.9)].Eric.Ries.文字版.mobi](ed2k://|file|[The.Lean.Startup(2011.9)].Eric.Ries.文字版.mobi|1703779|6C61F90DE1B06102BA6A6F1C72497163|h=UJCJIQH6XENCZXOJO7DE5R37PDQQFFJT|/)

[[In-Memory.Data.Management(2011.6)].Hasso.Plattner.文字版.pdf](ed2k://|file|[In-Memory.Data.Management(2011.6)].Hasso.Plattner.文字版.pdf|2398546|FBC979692715AE70F49AD74811D82F67|h=5YLWL4Q7V2POBN4WWOH5JFB2GPO2R6SH|/)

[[The.Clean.Coder(2011.5)].Robert.C.Martin.文字版.pdf](ed2k://|file|[The.Clean.Coder(2011.5)].Robert.C.Martin.文字版.pdf|6349234|3FD94F795C8199D7967303E9F67C2EA5|h=POIWU437OXRT5NWOHXOVOKDOEFW474KQ|/)

[[Seven.Languages.in.Seven.Weeks(2010.11)].Bruce.A.Tate.文字版.pdf](ed2k://|file|[Seven.Languages.in.Seven.Weeks(2010.11)].Bruce.A.Tate.文字版.pdf|4731018|F06DB067A3F2AF108C4BD0E6695AC4E0|h=QEMI2QRKIPNNOKKP326JBNCZTBBMLRSD|/)

[[Windows.8.Hacks(2012.11)].Preston.Gralla.文字版.pdf](ed2k://|file|[Windows.8.Hacks(2012.11)].Preston.Gralla.文字版.pdf|44730288|1eeaecb4208b5d63786fc33f3f28c63d|h=qvacw3qcig3fx2hjw5j4usjm3k6kisa4|/)

[[Windows.8.Hacks(2012.11)].Preston.Gralla.文字版.epub](ed2k://|file|[Windows.8.Hacks(2012.11)].Preston.Gralla.文字版.epub|19093283|29afebe83f1041728bd752844d8bca0b|h=d2te43qapok37vddf6p2gwetfw5dkteh|/)

[[Network.Security.Hacks(2nd,2006.10)].Andrew.Lockhart.文字版.pdf](ed2k://|file|[Network.Security.Hacks(2nd,2006.10)].Andrew.Lockhart.文字版.pdf|7540834|cfd911781c2906a83b0632df7b2a49ea|h=dgnom3djn6ackbc4q3c6fbxq4xgpkif5|/)

[[Network.Security.Hacks(2nd,2006.10)].Andrew.Lockhart.文字版.epub](ed2k://|file|[Network.Security.Hacks(2nd,2006.10)].Andrew.Lockhart.文字版.epub|8160728|88b2df6550822b89d5fd8e714c84abd0|h=v7chwqnaxfjqzadjmsklkbwxngrwbm32|/)

[[Mac.Hacks(2013.3)].Chris.Seibold.文字版.pdf](ed2k://|file|[Mac.Hacks(2013.3)].Chris.Seibold.文字版.pdf|60645497|1fd824ae47a9811e4858d00b0d702269|h=6ceu3dxegsm4zqjtdyw4qtsrjj6s54nn|/)

[[Mac.Hacks(2013.3)].Chris.Seibold.文字版.epub](ed2k://|file|[Mac.Hacks(2013.3)].Chris.Seibold.文字版.epub|19632678|b20e1c765a9ceae92bdc034f047b6fba|h=2jdws3bughbklt7qngjufy3xemyl5gvx|/)

[[Big.Book.of.Windows.Hacks(2007.10)].Preston.Gralla.文字版.pdf](ed2k://|file|[Big.Book.of.Windows.Hacks(2007.10)].Preston.Gralla.文字版.pdf|77352001|584b8c339cd9b26f81c9c718f4ed1390|h=hckmczoa3q2tusb5sryn2ntsti42zck6|/)

[[Big.Book.of.Apple.Hacks(2008.4)].Chris.Seibold.文字版.pdf](ed2k://|file|[Big.Book.of.Apple.Hacks(2008.4)].Chris.Seibold.文字版.pdf|59876983|8d39bc43332526eaa351dbb9443c9a73|h=sn6bqexga4elz2cwl6b5jabn5i5ql3zl|/)

[[Big.Book.of.Apple.Hacks(2008.4)].Chris.Seibold.文字版.epub](ed2k://|file|[Big.Book.of.Apple.Hacks(2008.4)].Chris.Seibold.文字版.epub|40314140|7dc94df355a42aefd53d25e9bf2a9f33|h=zqxzyh4bg64sv3avpn62cwhmpns36nrm|/)

[[Windows编程(第6版,Final.Edition)].(Programming.Windows).Charles.Petzold.文字版.pdf](ed2k://|file|[Windows编程(第6版,Final.Edition)].(Programming.Windows).Charles.Petzold.文字版.pdf|23899860|018ab2058f59348f6ad4940eb3cde78e|h=7jx2jjm5veahkvspj4iilqnszushkpsi|/)

[[Windows编程(第6版,Final.Edition)].(Programming.Windows).Charles.Petzold.文字版.epub](ed2k://|file|[Windows编程(第6版,Final.Edition)].(Programming.Windows).Charles.Petzold.文字版.epub|9279550|1b09ee95ce50ef4528ae6651b1b15059|h=nonicdq7a65eml6rfpgeq3mjmlfcvchg|/)

[[Programming.iOS.6(3rd,2013.2)].Matt.Neuburg.文字版.pdf](ed2k://|file|[Programming.iOS.6(3rd,2013.2)].Matt.Neuburg.文字版.pdf|20804333|92c9ffcbbf6c6a6f3c03458e5a2da4b2|h=z5inrsrbyfqnwdzblgkwbokugx5eynli|/)

[[Programming.iOS.6(3rd,2013.2)].Matt.Neuburg.文字版.epub](ed2k://|file|[Programming.iOS.6(3rd,2013.2)].Matt.Neuburg.文字版.epub|8918173|50795660a91309b03e63f3e31866c1a4|h=z6pdyjdwgnenjsqtp5vjh34yz2oxywvq|/)

[[Packet.Guide.to.Voice.over.IP(2013.2)].Bruce.Hartpence.文字版.pdf](ed2k://|file|[Packet.Guide.to.Voice.over.IP(2013.2)].Bruce.Hartpence.文字版.pdf|25911039|e1583a620d5401c12024d035c90b75bc|h=35s6yinksqkljin7lxwhyi3y7s5vweld|/)

[[Packet.Guide.to.Voice.over.IP(2013.2)].Bruce.Hartpence.文字版.epub](ed2k://|file|[Packet.Guide.to.Voice.over.IP(2013.2)].Bruce.Hartpence.文字版.epub|14569398|b66b728a6d282264de578205f1d16ed0|h=ryvcbrvwiyora3k6y7gxvxkaiizvbfen|/)

[[Distributed.Network.Data(2013.2)].Alasdair.Allan.文字版.pdf](ed2k://|file|[Distributed.Network.Data(2013.2)].Alasdair.Allan.文字版.pdf|65536081|0e584889485250537bf9c3cf2a7096be|h=4gaquj2npccdnmhmwoczazihrtxb24al|/)

[[Distributed.Network.Data(2013.2)].Alasdair.Allan.文字版.epub](ed2k://|file|[Distributed.Network.Data(2013.2)].Alasdair.Allan.文字版.epub|10780673|bc508f53bc9a9f282b5ed7082fa6f61c|h=arz6wse5adpqdsr3yqr5uz4ud4zazx6w|/)

[[Sencha.Touch.2：Up.and.Running(2013.2)].Adrian.Kosmaczewski.文字版.pdf](ed2k://|file|[Sencha.Touch.2：Up.and.Running(2013.2)].Adrian.Kosmaczewski.文字版.pdf|17211217|1dba3479a69bce3429d9b0f41ac55e3c|h=xbitumwzvqmmg3ddsqit3pha2frmwj2s|/)

[[Sencha.Touch.2：Up.and.Running(2013.2)].Adrian.Kosmaczewski.文字版.epub](ed2k://|file|[Sencha.Touch.2：Up.and.Running(2013.2)].Adrian.Kosmaczewski.文字版.epub|10755724|09c62c6efa07c7cd24812a59812db157|h=r4up6unimvo46bapmkprsjlpb77acwhp|/)

[[Programming.PHP(3rd,2013.2)].Kevin.Tatroe.文字版.pdf](ed2k://|file|[Programming.PHP(3rd,2013.2)].Kevin.Tatroe.文字版.pdf|7902211|bdc05f040b938553965a9c7f7a2eb2c2|h=pc5huw52x4letdbpyvep4rmku4efoszf|/)

[[Programming.PHP(3rd,2013.2)].Kevin.Tatroe.文字版.epub](ed2k://|file|[Programming.PHP(3rd,2013.2)].Kevin.Tatroe.文字版.epub|3918880|c9ac1310f8e1648b903dd4250f28c016|h=7kxrarbrri33x277xk6ikdfms6quux6z|/)

[[Opa：Up.and.Running(2013.2)].Henri.Binsztok.文字版.pdf](ed2k://|file|[Opa：Up.and.Running(2013.2)].Henri.Binsztok.文字版.pdf|9068309|e16838f6908ce8f5ea305d37522162a2|h=soiv3mqasjnzymiqccxl3k24rm6hzgem|/)

[[Opa：Up.and.Running(2013.2)].Henri.Binsztok.文字版.epub](ed2k://|file|[Opa：Up.and.Running(2013.2)].Henri.Binsztok.文字版.epub|3091724|5256b1bc73ee698f2dc32e544c777b86|h=6vrazbudkbymtr72bem3d7lzevgqsisr|/)

[[Learning.PHP.Design.Patterns(2013.2)].William.Sanders.文字版.pdf](ed2k://|file|[Learning.PHP.Design.Patterns(2013.2)].William.Sanders.文字版.pdf|20331441|df81ee9e5c41f544c2a99d2c01ac0b42|h=c4oyjs42geihbhxlzq545pf5vcnewdga|/)

[[Learning.PHP.Design.Patterns(2013.2)].William.Sanders.文字版.epub](ed2k://|file|[Learning.PHP.Design.Patterns(2013.2)].William.Sanders.文字版.epub|9328346|8eb88492ba3a09d5ede0bbb10e1262aa|h=kztfwlj2u5hb7m3waposk6uqq2htqvtl|/)

[[Lean.UX(2013.2)].Jeff.Gothelf.文字版.pdf](ed2k://|file|[Lean.UX(2013.2)].Jeff.Gothelf.文字版.pdf|7860571|da9744267967eb67c5c53beec24ec106|h=4y4ixu7zhbl7hicceuvlbs23z3vt646w|/)

[[Lean.UX(2013.2)].Jeff.Gothelf.文字版.epub](ed2k://|file|[Lean.UX(2013.2)].Jeff.Gothelf.文字版.epub|4390199|300295cb33a22c947eafe281626e4b57|h=v3k2ov7zk7v3n4ldgrvp6hicdyohysje|/)

[[DOM.Enlightenment(2013.2)].Cody.Lindley.文字版.pdf](ed2k://|file|[DOM.Enlightenment(2013.2)].Cody.Lindley.文字版.pdf|7209332|0144baa942d51eabcc43e972474d5249|h=36g6t3fpmpusxjx5d5talpv74mdrfbjn|/)

[[DOM.Enlightenment(2013.2)].Cody.Lindley.文字版.epub](ed2k://|file|[DOM.Enlightenment(2013.2)].Cody.Lindley.文字版.epub|1338496|ef5731a8b061f98de4f77a7fd75c510b|h=usegzdbldtuo4memwdgku4tq5h2mxdv3|/)

[[Developing.with.Couchbase.Server(2013.2)].MC.Brown.文字版.pdf](ed2k://|file|[Developing.with.Couchbase.Server(2013.2)].MC.Brown.文字版.pdf|8769708|2961638725749debd977e8fa3e21394e|h=haimclyenumwwmqrbcafvaiyomtmfhyi|/)

[[Developing.with.Couchbase.Server(2013.2)].MC.Brown.文字版.epub](ed2k://|file|[Developing.with.Couchbase.Server(2013.2)].MC.Brown.文字版.epub|2073512|55bc56a2e989ae4f03cca3b769329054|h=dxtcevvagv35mqiyknoqzpmp42k3gjxm|/)

[[Operating.System.Concepts(8th,2008.7)].Abraham.Silberschatz.文字版.epub](ed2k://|file|[Operating.System.Concepts(8th,2008.7)].Abraham.Silberschatz.文字版.epub|8212221|cdb63c81859b3e98048276274a23bcb5|h=frciz7oythzwpxkvwuxcgdelmz3bcszt|/)

[[Operating.System.Concepts(8th,2008.7)].Abraham.Silberschatz.文字版.mobi](ed2k://|file|[Operating.System.Concepts(8th,2008.7)].Abraham.Silberschatz.文字版.mobi|8266012|73318c74bf223f3c85fe3f21db3a7b78|h=q5kptn7dyb2wwekrdvrq4ne2fk64buw7|/)

[[Operating.System.Concepts(8th,Update.Edition.2011.7)].Abraham.Silberschatz.文字版.pdf](ed2k://|file|[Operating.System.Concepts(8th,Update.Edition.2011.7)].Abraham.Silberschatz.文字版.pdf|7488718|2c4b4addc9be1f566473020c8d17f7fe|h=wgtt6sda2uhszvmdiph6roxfyae6g2a7|/)

[[Operating.System.Concepts(9th,2012.12)].Abraham.Silberschatz.文字版.pdf](ed2k://|file|[Operating.System.Concepts(9th,2012.12)].Abraham.Silberschatz.文字版.pdf|7038104|537ecc11fe5bac4392954fe77b7c3971|h=urs5pkshwi24ev6w7ejgyhqmg3biaata|/)

[[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.pdf](ed2k://|file|[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.pdf|10237989|16C2EFD2C2A13B9C748BBF7FE322647D|h=BLSQZJPVTBNNJZKFW42KWNUX7LTES7IQ|/)

[[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.epub](ed2k://|file|[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.epub|11474336|7b8fc0ba9a1a3805b60e29ab0427bb2e|h=a7ppy2zbagjp5wxgkxirq67gzqjqfxqo|/)

[[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.mobi](ed2k://|file|[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.mobi|4126516|ab38f36ae4fd0bc650addff71d51b8e2|h=cur67jrmxdzzcaxwwdiasmzlancyqyqs|/)

[[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.chm](ed2k://|file|[代码大全(第2版)].(Code.Complete,2nd).Steve.McConnell.英文文字版.chm|6813257|853cb55ac62b3cd5d35263d54baab2b3|h=o72havpf55orw233qhxybljdfdsgxjad|/)

[[Free.as.in.Freedom(2.0)].Sam.Williams.文字版.epub](ed2k://|file|[Free.as.in.Freedom(2.0)].Sam.Williams.文字版.epub|379033|bca107beb186851c4b4ef9ba46cabbf4|h=envkknkmiugw5rqvliivk5sz7r2p3xci|/)

[[Free.as.in.Freedom(2.0)].Sam.Williams.文字版.mobi](ed2k://|file|[Free.as.in.Freedom(2.0)].Sam.Williams.文字版.mobi|478281|0fa61a9295e54b22eaf968c0c5dd9f6d|h=iuv7t5j5d7yekgpjkorvrmzh53c7xg73|/)

[[Python应用核心编程(第3版)].(Core.Python.Applications.Programming).Wesley.Chun.文字版.pdf](ed2k://|file|[Python应用核心编程(第3版)].(Core.Python.Applications.Programming).Wesley.Chun.文字版.pdf|9799663|499d4364d5ae0b9ff24f38484b8a71f8|h=pnvg3qntw72wptfzsq63k4svciydiqfn|/)

[[Python应用核心编程(第3版)].(Core.Python.Applications.Programming).Wesley.Chun.文字版.epub](ed2k://|file|[Python应用核心编程(第3版)].(Core.Python.Applications.Programming).Wesley.Chun.文字版.epub|17544123|ab39449b612fd996811900a99aca87b0|h=lmpp3fq3k2k2ol54fv2u5hlv7wtbbyga|/)

[[Python核心编程(第2版)].(Core.Python.Programming).Wesley.Chun.文字版.pdf](ed2k://|file|[Python核心编程(第2版)].(Core.Python.Programming).Wesley.Chun.文字版.pdf|16576559|09bfd0ba82c45635600578d43369d190|h=2x3ju7lv3lb7h3xv72smb4uhd5fbqlq5|/)

[[Python核心编程(第2版)].(Core.Python.Programming).Wesley.Chun.文字版.epub](ed2k://|file|[Python核心编程(第2版)].(Core.Python.Programming).Wesley.Chun.文字版.epub|42057674|79c44297e95a4de21b51062a491f8366|h=covl2asv2ivsq34kjubaoswthumtlmus|/)

[[Java设计模式(第2版)].(Design.Patterns.in.Java).Steven.John.Metsker.文字版.pdf](ed2k://|file|[Java设计模式(第2版)].(Design.Patterns.in.Java).Steven.John.Metsker.文字版.pdf|4909982|4af2c5fa42a871e612f4352b9fb8dc36|h=bgfmcoz3hpelgtxcs2xlo37db5olhq7f|/)

[[Java设计模式(第2版)].(Design.Patterns.in.Java).Steven.John.Metsker.文字版.epub](ed2k://|file|[Java设计模式(第2版)].(Design.Patterns.in.Java).Steven.John.Metsker.文字版.epub|5435494|2b3d8fcf32bab45f7cb0c5b0d17d9d52|h=rr3kzed5rnyb66dvoq2fha43rchvt3xu|/)

[[How.Google.Tests.Software].James.Whittaker.文字版.pdf](ed2k://|file|[How.Google.Tests.Software].James.Whittaker.文字版.pdf|8562777|445c8b032ff17046f2a6db525c5f7704|h=lemdvgk47kju2aacxqsmcvtk36iojnhg|/)

[[How.Google.Tests.Software].James.Whittaker.文字版.epub](ed2k://|file|[How.Google.Tests.Software].James.Whittaker.文字版.epub|19033077|daaca5a1461e0de3ca56b30dff358928|h=r5rz22gsy2mxkml6kv2ybdot3o4fmzoo|/)

[[Mathematical.Foundations.of.Computer.Networking(2012.4)].Srinivasan.Keshav.文字版.pdf](ed2k://|file|[Mathematical.Foundations.of.Computer.Networking(2012.4)].Srinivasan.Keshav.文字版.pdf|7721980|e37096e7379238760c2c5c6a88dd579b|h=lenawqzpltqx7ufdffwq2t4y7xmu2kkq|/)

[[Mathematical.Foundations.of.Computer.Networking(2012.4)].Srinivasan.Keshav.文字版.epub](ed2k://|file|[Mathematical.Foundations.of.Computer.Networking(2012.4)].Srinivasan.Keshav.文字版.epub|30045505|df9b00001b6d80c0fbc0584194a8fe2b|h=vdcjta4mbnjhjq2femy24b6lnoeqblhi|/)

[[Learning.MySQL(2006.11)].Hugh.E.Williams.文字版.pdf](ed2k://|file|[Learning.MySQL(2006.11)].Hugh.E.Williams.文字版.pdf|4423656|1b96b104b363506b2bbfd8dc9a15e42a|h=3w4iz6c7s6eif6ptq54n7rlnbxqmalqf|/)

[[Learning.MySQL(2006.11)].Hugh.E.Williams.文字版.epub](ed2k://|file|[Learning.MySQL(2006.11)].Hugh.E.Williams.文字版.epub|5942273|c90526bef856facadc2100a231aa4054|h=2uaqrn2vkzaxkx232r32bkb4mkmpkt2b|/)

[[Essential.PHP.Security(2005.10)].Chris.Shiflett.文字版.pdf](ed2k://|file|[Essential.PHP.Security(2005.10)].Chris.Shiflett.文字版.pdf|1397867|f6915c603cc1cbdc5c73d7eae12b30f6|h=ainqzdu7jpz7v5odpi73trmsj5moiy7a|/)

[[Essential.PHP.Security(2005.10)].Chris.Shiflett.文字版.epub](ed2k://|file|[Essential.PHP.Security(2005.10)].Chris.Shiflett.文字版.epub|2882959|6810d5107699320e2c3f070191863809|h=zijk277suq3sumbmw2vrp46ik7enj3aw|/)

[[Designing.Interfaces(2nd,2010.12)].Jenifer.Tidwell.文字版.pdf](ed2k://|file|[Designing.Interfaces(2nd,2010.12)].Jenifer.Tidwell.文字版.pdf|60446156|d852bc631f53c3e18c0530a4dbdb0682|h=yxet5xvkido75cdmbhw45aqzza5jstod|/)

[[Designing.Interfaces(2nd,2010.12)].Jenifer.Tidwell.文字版.epub](ed2k://|file|[Designing.Interfaces(2nd,2010.12)].Jenifer.Tidwell.文字版.epub|50936754|71914d933b5f26c3c32982e03b64563a|h=yhvbz6hkc6p7yzf2je22ftl435vtovew|/)

[[Coding.with.Coda(2013.1)].Eric.J.Gruber.文字版.pdf](ed2k://|file|[Coding.with.Coda(2013.1)].Eric.J.Gruber.文字版.pdf|9141398|6868614c15261e6c8492b40b68832b35|h=xpqyxpvevm6bintx6pqba2ofikuw227n|/)

[[Coding.with.Coda(2013.1)].Eric.J.Gruber.文字版.epub](ed2k://|file|[Coding.with.Coda(2013.1)].Eric.J.Gruber.文字版.epub|3023361|9be7b6166343e0ee1f622f95aec908d5|h=45tmbmaivc3qcws2iy3pgk6uk4cj4awb|/)

[[Designing.Games(2013.1)].Tynan.Sylvester.文字版.pdf](ed2k://|file|[Designing.Games(2013.1)].Tynan.Sylvester.文字版.pdf|5063174|62f8507ace869e23c6176ba80eb75ad9|h=gb62dait5pt6pzofwrdbbxnifyounixk|/)

[[Designing.Games(2013.1)].Tynan.Sylvester.文字版.epub](ed2k://|file|[Designing.Games(2013.1)].Tynan.Sylvester.文字版.epub|5067918|2f47112cf072330e890eddb859bad691|h=bxvbxal3isjkwom4io5acdppvx6gpayn|/)

[[EPUB.3.Best.Practices(2013.1)].Matt.Garrish.文字版.pdf](ed2k://|file|[EPUB.3.Best.Practices(2013.1)].Matt.Garrish.文字版.pdf|12826292|e04662c519f6ade93ac4575dafb063ce|h=x2xze2dygcpmeozyp2bh4r4cofw7lpgh|/)

[[EPUB.3.Best.Practices(2013.1)].Matt.Garrish.文字版.epub](ed2k://|file|[EPUB.3.Best.Practices(2013.1)].Matt.Garrish.文字版.epub|4495863|101e9c3001830a8f165e7f5d0f59f6da|h=2nfiqbp6aiz67ya2w64rurx4avtt663s|/)

[[Introducing.Erlang(2013.1)].Simon.St.Laurent.文字版.pdf](ed2k://|file|[Introducing.Erlang(2013.1)].Simon.St.Laurent.文字版.pdf|8119082|f3c44e22a31c871ea7480cfd62ac53fa|h=t5443bzkhbyfomovrcddogwak3qmehsb|/)

[[Introducing.Erlang(2013.1)].Simon.St.Laurent.文字版.epub](ed2k://|file|[Introducing.Erlang(2013.1)].Simon.St.Laurent.文字版.epub|3246562|f5b7f63ad1a7e4cba69a4ece6e0e9013|h=wxujgrhvidmkznf6qldvktj2ulsmflh3|/)

[[Learning.from.jQuery(2013.1)].Callum.Macrae.文字版.pdf](ed2k://|file|[Learning.from.jQuery(2013.1)].Callum.Macrae.文字版.pdf|6218555|a859bcc1c0a224844e62785ab5dd7e74|h=xbbdmkycklh5lzqzra4v2fxejszjksmc|/)

[[Learning.from.jQuery(2013.1)].Callum.Macrae.文字版.epub](ed2k://|file|[Learning.from.jQuery(2013.1)].Callum.Macrae.文字版.epub|2013832|950ea3e459dc04e3f08f840fc639e55d|h=cristycujitnyvdsiwo6r3pcttjjatil|/)

[[XML.and.InDesign(2013.1)].Dorothy.J.Hoskins.文字版.pdf](ed2k://|file|[XML.and.InDesign(2013.1)].Dorothy.J.Hoskins.文字版.pdf|16858790|4a93e0b06f859d173025e7d0951a7ce6|h=y7lslygetwmixif2m42edwaoepokpe3l|/)

[[XML.and.InDesign(2013.1)].Dorothy.J.Hoskins.文字版.epub](ed2k://|file|[XML.and.InDesign(2013.1)].Dorothy.J.Hoskins.文字版.epub|5163645|9eed508793646ef77d3b81bd7b7a7565|h=wintvjybsjh5wa3fiv6hg35uarewj3bf|/)

[[Testing.in.Scala(2013.1)].Daniel.Hinojosa.文字版.pdf](ed2k://|file|[Testing.in.Scala(2013.1)].Daniel.Hinojosa.文字版.pdf|6729436|383a1c4ca994b31bda1aa27790e62fe9|h=vvjmx4hh5ixfneoz6bgqd5qvxpuiyhc7|/)

[[Testing.in.Scala(2013.1)].Daniel.Hinojosa.文字版.epub](ed2k://|file|[Testing.in.Scala(2013.1)].Daniel.Hinojosa.文字版.epub|1388027|1462566731f6a293b9d8638a74194734|h=g5c27k6fmlfwyzeemogm3z553a5ndicr|/)

[[Testable.JavaScript(2013.1)].Mark.Ethan.Trostler.文字版.pdf](ed2k://|file|[Testable.JavaScript(2013.1)].Mark.Ethan.Trostler.文字版.pdf|19105178|1539dc70658797f2bc554d011fff26b7|h=waeezopofz2o44wpx46vgmy4n4yus6by|/)

[[Testable.JavaScript(2013.1)].Mark.Ethan.Trostler.文字版.epub](ed2k://|file|[Testable.JavaScript(2013.1)].Mark.Ethan.Trostler.文字版.epub|7946183|61f64db832b947c54da1dc79b1ad854e|h=y75eusac2m6hvti74ex2ko5ypcczmhpu|/)

[[Resilience.and.Reliability.on.AWS(2013.1)].Jurg.van.Vliet.文字版.pdf](ed2k://|file|[Resilience.and.Reliability.on.AWS(2013.1)].Jurg.van.Vliet.文字版.pdf|8798027|c4dcf89f6830be79b6114adf1b0ef524|h=i5lowwojiiiua3z6qun7t4agprb6opfd|/)

[[Resilience.and.Reliability.on.AWS(2013.1)].Jurg.van.Vliet.文字版.epub](ed2k://|file|[Resilience.and.Reliability.on.AWS(2013.1)].Jurg.van.Vliet.文字版.epub|3811165|01c728f62aa94240b09f3b70c24ba1bf|h=g3lrhl4i2uafzuedfsayrubzz24yhlxj|/)

[[Enyo：Up.and.Running(2013.1)].Roy.Sutton.文字版.pdf](ed2k://|file|[Enyo：Up.and.Running(2013.1)].Roy.Sutton.文字版.pdf|6101242|7f9d13380a2f85b00a27a90ca22a4409|h=owmraodtoncepsmsiz2kc4n3vmvig47r|/)

[[Enyo：Up.and.Running(2013.1)].Roy.Sutton.文字版.epub](ed2k://|file|[Enyo：Up.and.Running(2013.1)].Roy.Sutton.文字版.epub|1500470|d7f4e00193bb2346f2426a6959ce8acb|h=hsvn5kilvclpwzt2nymfd5uecorqnfnu|/)

[[Secrets.of.the.JavaScript.Ninja(2012.12)].John.Resig.文字版.pdf](ed2k://|file|[Secrets.of.the.JavaScript.Ninja(2012.12)].John.Resig.文字版.pdf|21416079|50dc97fc1e3c49faf32e4d4149649e4d|h=gaxb4wvezrdtxznahn4hww7t7xerjoxo|/)

[[Secrets.of.the.JavaScript.Ninja(2012.12)].John.Resig.文字版.epub](ed2k://|file|[Secrets.of.the.JavaScript.Ninja(2012.12)].John.Resig.文字版.epub|22044820|a51a09ff3bef32d1a5e4c66e9aa2ec13|h=lp4yhitjxzwjkytfcbzcl6o5cm2fysr6|/)

[[Taming.Text(2013.1)].Grant.S.Ingersoll.文字版.pdf](ed2k://|file|[Taming.Text(2013.1)].Grant.S.Ingersoll.文字版.pdf|8760768|9eeb252a94b6ec8c56cf9ab3ab7a6db8|h=x2kn57axnpsdr634gblh3ohgxptltcm4|/)

[[Taming.Text(2013.1)].Grant.S.Ingersoll.文字版.epub](ed2k://|file|[Taming.Text(2013.1)].Grant.S.Ingersoll.文字版.epub|10416905|c6ec57e783c5993649edf35c87481c9a|h=5a5wtlbhgtbcrwhvto5i6g3ffsqfr2n3|/)

[[Metaprogramming.in.NET(2012.12)].Kevin.Hazzard.文字版.pdf](ed2k://|file|[Metaprogramming.in.NET(2012.12)].Kevin.Hazzard.文字版.pdf|12458967|90b17babd320fccdd8fd1234e1713cb5|h=ixaongqvltiq6aofhf5hrq6twvhdj2uh|/)

[[Metaprogramming.in.NET(2012.12)].Kevin.Hazzard.文字版.epub](ed2k://|file|[Metaprogramming.in.NET(2012.12)].Kevin.Hazzard.文字版.epub|8383345|b1885fc5e4425f1d3255dc2537337bfd|h=a5jeymauzj766c7dljbloolwgtqj3pdc|/)

[[Learn.Windows.PowerShell.3.in.a.Month.of.Lunches(2nd,2012.11)].Don.Jones.文字版.pdf](ed2k://|file|[Learn.Windows.PowerShell.3.in.a.Month.of.Lunches(2nd,2012.11)].Don.Jones.文字版.pdf|8133332|0b3d3f072dc0082d055e833792db0f4c|h=jhgbhjiahwj3sg7vugq2qlz6texjp6vv|/)

[[Learn.Windows.PowerShell.3.in.a.Month.of.Lunches(2nd,2012.11)].Don.Jones.文字版.epub](ed2k://|file|[Learn.Windows.PowerShell.3.in.a.Month.of.Lunches(2nd,2012.11)].Don.Jones.文字版.epub|10920268|8134672408ea25431aec8027e802ec17|h=3zyndmuzt5wd7nddgmqr3ropmz2jemik|/)

[[Learn.PowerShell.Toolmaking.in.a.Month.of.Lunches(2012.12)].Don.Jones.文字版.pdf](ed2k://|file|[Learn.PowerShell.Toolmaking.in.a.Month.of.Lunches(2012.12)].Don.Jones.文字版.pdf|14085922|6e95680870dd7039a9b6bd157ef7bb25|h=yqzd23gagzpkjuosgkyrhbpmx5tj2e3z|/)

[[Learn.PowerShell.Toolmaking.in.a.Month.of.Lunches(2012.12)].Don.Jones.文字版.epub](ed2k://|file|[Learn.PowerShell.Toolmaking.in.a.Month.of.Lunches(2012.12)].Don.Jones.文字版.epub|10210483|eb4646f127355ce0d99a6ddf5546c349|h=yrcmb2x6ynrphoda5fyc2vjgc7kjcblt|/)

[[HTML5.for.NET.Developers(2012.11)].Jim.Jackson.II.文字版.pdf](ed2k://|file|[HTML5.for.NET.Developers(2012.11)].Jim.Jackson.II.文字版.pdf|13735858|9aec64b1839d22d818832373d6f715cc|h=xyht6ouxasrk5z6astbedb7zqfvs24o2|/)

[[HTML5.for.NET.Developers(2012.11)].Jim.Jackson.II.文字版.epub](ed2k://|file|[HTML5.for.NET.Developers(2012.11)].Jim.Jackson.II.文字版.epub|25141701|79496b8b917dd47825476d2970b5ac7d|h=xugzbdksngl6eunkdyzcp3w6xuocmrmp|/)

[[Dart.in.Action(2013.1)].Chris.Buckett.文字版.pdf](ed2k://|file|[Dart.in.Action(2013.1)].Chris.Buckett.文字版.pdf|14820777|7fad9e3c4e2da46c7598c0a3ebeee5b3|h=h4n6vndxh7wnxoaqecsqzpmg6mqvzbre|/)

[[Dart.in.Action(2013.1)].Chris.Buckett.文字版.epub](ed2k://|file|[Dart.in.Action(2013.1)].Chris.Buckett.文字版.epub|31944053|52f60cc26df65a51f9c3f459c08f7fe4|h=lun6t3hzxqsmd6ivrgs2bt5vnumjr4g4|/)

[[WordPress：The.Missing.Manual(2012.10)].Matthew.MacDonald.文字版.pdf](ed2k://|file|[WordPress：The.Missing.Manual(2012.10)].Matthew.MacDonald.文字版.pdf|33206239|f9b43a2acb40be16a35cb72668691714|h=vdwaps7spsnfvdxypdt6n6x5wze4q26a|/)

[[WordPress：The.Missing.Manual(2012.10)].Matthew.MacDonald.文字版.epub](ed2k://|file|[WordPress：The.Missing.Manual(2012.10)].Matthew.MacDonald.文字版.epub|34770845|98fc5af5866a0a6a501061e5c716517b|h=ppsdm35v2llojqtwpsqjgjhwobl4kjoj|/)

[[QuickBooks.2013：The.Missing.Manual(2012.10)].Bonnie.Biafore.文字版.pdf](ed2k://|file|[QuickBooks.2013：The.Missing.Manual(2012.10)].Bonnie.Biafore.文字版.pdf|62546856|c75275672be5493e2de571ccb8d57749|h=45phis6dtikwnlyirvfzs4gc2ybstbko|/)

[[QuickBooks.2013：The.Missing.Manual(2012.10)].Bonnie.Biafore.文字版.epub](ed2k://|file|[QuickBooks.2013：The.Missing.Manual(2012.10)].Bonnie.Biafore.文字版.epub|16240925|cb914dd88b1147f215ea64b734b4ab52|h=4zafxjiq5d3s4cetc72egdrdwfllcup4|/)

[[PHP&MySQL：The.Missing.Manual(2nd,2012.11)].Brett.McLaughlin.文字版.pdf](ed2k://|file|[PHP&MySQL：The.Missing.Manual(2nd,2012.11)].Brett.McLaughlin.文字版.pdf|26143474|01c9d6f27569786ec5fd4aeea0a83b4e|h=shitdy55qn22fs7gyhpnx7mtdmssnkux|/)

[[PHP&MySQL：The.Missing.Manual(2nd,2012.11)].Brett.McLaughlin.文字版.epub](ed2k://|file|[PHP&MySQL：The.Missing.Manual(2nd,2012.11)].Brett.McLaughlin.文字版.epub|20159113|520ba70d0c2e53cae6513386f04dcdfd|h=e7hhgs5ibzmv3fpya3762jjaazjwasnc|/)

[[iPod：The.Missing.Manual(11st,2012.12)].J.D.Biersdorfer.文字版.pdf](ed2k://|file|[iPod：The.Missing.Manual(11st,2012.12)].J.D.Biersdorfer.文字版.pdf|44637885|ec78d58e21bc96bc4f7d30e3fd32e99d|h=3pwtnx3m2rxfmt4ltygzb5fc7vqz7e63|/)

[[iPod：The.Missing.Manual(11st,2012.12)].J.D.Biersdorfer.文字版.epub](ed2k://|file|[iPod：The.Missing.Manual(11st,2012.12)].J.D.Biersdorfer.文字版.epub|22874355|72d8c545a8ed28ac7821546ee49504eb|h=tbwwzitlmlptfl46d2r5iahy65nt7ham|/)

[[iPhone：The.Missing.Manual(6th,2012.10)].David.Pogue.文字版.pdf](ed2k://|file|[iPhone：The.Missing.Manual(6th,2012.10)].David.Pogue.文字版.pdf|48176988|4471049e9d5fcbd86ebaf148ffded8b7|h=kvfcvlopmmnu3igzns6vdobdubj6cihq|/)

[[iPhone：The.Missing.Manual(6th,2012.10)].David.Pogue.文字版.epub](ed2k://|file|[iPhone：The.Missing.Manual(6th,2012.10)].David.Pogue.文字版.epub|20243293|1c272c3e42579491943893f05f2c03b8|h=z6xllwungpupxlnz72tu5jjfu46nkfhb|/)

[[iPad：The.Missing.Manual(5th,2012.11)].J.D.Biersdorfer.文字版.pdf](ed2k://|file|[iPad：The.Missing.Manual(5th,2012.11)].J.D.Biersdorfer.文字版.pdf|53397044|88945afa8b70631eceafdbc197a8b17e|h=ykwexuzcsnccswmtpumg3k2w6r754cgd|/)

[[iPad：The.Missing.Manual(5th,2012.11)].J.D.Biersdorfer.文字版.epub](ed2k://|file|[iPad：The.Missing.Manual(5th,2012.11)].J.D.Biersdorfer.文字版.epub|24556325|0dceb1c8d2269d72e4404e95dfe79ebf|h=ehuz7lxef3sl5kenwate35xez3r37yt6|/)

[[CSS3：The.Missing.Manual(3rd,2012.12)].David.Sawyer.McFarland.文字版.pdf](ed2k://|file|[CSS3：The.Missing.Manual(3rd,2012.12)].David.Sawyer.McFarland.文字版.pdf|27834398|0ba1da1279cafbc7f66047959d55532b|h=sj3zb3h7low6vqci245ahwvdrod76hd7|/)

[[CSS3：The.Missing.Manual(3rd,2012.12)].David.Sawyer.McFarland.文字版.epub](ed2k://|file|[CSS3：The.Missing.Manual(3rd,2012.12)].David.Sawyer.McFarland.文字版.epub|36364296|80610362258d16fa6d28bd47c4681235|h=znsxhmskp3fag7mrerkoxjw4h2rvg4qq|/)

[[Adobe.Edge.Animate：The.Missing.Manual(2012.11)].Chris.Grover.文字版.pdf](ed2k://|file|[Adobe.Edge.Animate：The.Missing.Manual(2012.11)].Chris.Grover.文字版.pdf|20500392|218ec79d2a4fa7956b3dba9cb958c08c|h=2gp4ybc4ymu24s2kncynkcg6jf6w2v4l|/)

[[Adobe.Edge.Animate：The.Missing.Manual(2012.11)].Chris.Grover.文字版.epub](ed2k://|file|[Adobe.Edge.Animate：The.Missing.Manual(2012.11)].Chris.Grover.文字版.epub|10428608|48908eee18d1c87519959cbb6cf6c28b|h=7ht6gtnupkbu3xur6fmjrljfnhbq52av|/)

[[Linux.in.a.Nutshell(6th,2009.9)].Ellen.Siever.文字版.pdf](ed2k://|file|[Linux.in.a.Nutshell(6th,2009.9)].Ellen.Siever.文字版.pdf|12512070|A8EF20B3FFF86AA6572F3305791D09E6|h=INX6QMMTGQHBRTUHSJH7CQIINYSIST3F|/)

[[Java.Message.Service(2nd,2009.5)].Mark.Richards.文字版.pdf](ed2k://|file|[Java.Message.Service(2nd,2009.5)].Mark.Richards.文字版.pdf|5226253|8A4C8F51D19EF23162E09C4A67A88538|h=CSHAB6B5MEDSNKD3XYM4WMJBRPB2TIKS|/)

[[iPhone.3D.Programming(2010.5)].Philip.Rideout.文字版.pdf](ed2k://|file|[iPhone.3D.Programming(2010.5)].Philip.Rideout.文字版.pdf|6025409|6B4753BB5457EC1D1132AFC35602175E|h=MEYY5CBD5BB245XUZXRQXAJPUJJ7IUH7|/)

[[JavaScript.Cookbook(2010.7)].Shelley.Powers.文字版.pdf](ed2k://|file|[JavaScript.Cookbook(2010.7)].Shelley.Powers.文字版.pdf|9669201|D956E5808D1BE29F5075814AB1A3AB77|h=AKPRW34KOLOS654OMWKX63QFXZT5KVZF|/)

[[Effective.UI(2010.1)].Jonathan.Anderson.文字版.pdf](ed2k://|file|[Effective.UI(2010.1)].Jonathan.Anderson.文字版.pdf|5445549|3A41EF08AB06C55EAC076F44BE31A33D|h=IBCP6LSNPAA46UAKOBHUY5QGDAEKOXST|/)

[[Windows.PowerShell.Pocket.Reference(2nd,2012.12)].Lee.Holmes.文字版.pdf](ed2k://|file|[Windows.PowerShell.Pocket.Reference(2nd,2012.12)].Lee.Holmes.文字版.pdf|3342276|13e3b285fc59aa3584dd8a35fc8c13dd|h=3q2rzm4x4veu7hi4gnf4nqwygajehsrx|/)

[[Windows.PowerShell.Pocket.Reference(2nd,2012.12)].Lee.Holmes.文字版.epub](ed2k://|file|[Windows.PowerShell.Pocket.Reference(2nd,2012.12)].Lee.Holmes.文字版.epub|2228799|5bdbb5ca261e66534eba7eaaa3b06e8c|h=24m2h3vafj6vzvsfust4bk7qkvpwhr7t|/)

[[Windows.PowerShell.Cookbook(3rd,2012.12)].Lee.Holmes.文字版.pdf](ed2k://|file|[Windows.PowerShell.Cookbook(3rd,2012.12)].Lee.Holmes.文字版.pdf|15982437|25189f50c2da2a06b535815250ebb5a4|h=p2mketd4a6jbnk4uqcr4y77piokr3pvs|/)

[[Windows.PowerShell.Cookbook(3rd,2012.12)].Lee.Holmes.文字版.epub](ed2k://|file|[Windows.PowerShell.Cookbook(3rd,2012.12)].Lee.Holmes.文字版.epub|3791675|7432ca9688c5b3a7dd4619af03a53db7|h=lhvoatxjiatomgmgvp457sss3ffsrccg|/)

[[View.Updating.and.Relational.Theory(2012.12)].C.J.Date.文字版.pdf](ed2k://|file|[View.Updating.and.Relational.Theory(2012.12)].C.J.Date.文字版.pdf|19638519|6537ac35a2a5c34361ecddbcd335fff7|h=vtqdblyyb43wnvpdg2jzrat47bzquxjk|/)

[[View.Updating.and.Relational.Theory(2012.12)].C.J.Date.文字版.epub](ed2k://|file|[View.Updating.and.Relational.Theory(2012.12)].C.J.Date.文字版.epub|2976328|a52e1e70fa7edf1eabedd1aad8eedbfc|h=ge756lgymhkpdackk4yfgrk73enn5zyf|/)

[[Getting.Started.with.Raspberry.Pi(2012.12)].Matt.Richardson.文字版.pdf](ed2k://|file|[Getting.Started.with.Raspberry.Pi(2012.12)].Matt.Richardson.文字版.pdf|14903794|d2c251c59f2417fa075824e51d0b66c9|h=yyatethbmmwrupnvs7f5bahlschw6hqh|/)

[[Getting.Started.with.Raspberry.Pi(2012.12)].Matt.Richardson.文字版.epub](ed2k://|file|[Getting.Started.with.Raspberry.Pi(2012.12)].Matt.Richardson.文字版.epub|9501056|c233dc684b40c64eab56f8e1acd1d8ce|h=x2yrc3dtairfyqyfvhsivaqoxbb6sn2i|/)

[[JavaScript.Enlightenment(2012.12)].Cody.Lindley.文字版.pdf](ed2k://|file|[JavaScript.Enlightenment(2012.12)].Cody.Lindley.文字版.pdf|7487336|bf82ce7c552c886e9078263255860dc8|h=refwzgvyyqq56pfwfvvsund3fbl6donx|/)

[[JavaScript.Enlightenment(2012.12)].Cody.Lindley.文字版.epub](ed2k://|file|[JavaScript.Enlightenment(2012.12)].Cody.Lindley.文字版.epub|2161713|bd64a536024385d505d2081b9de9ceac|h=hnbnzw7vlrdakicdhglscrlmulggwvkm|/)

[[Getting.Started.with.Mule.Cloud.Connect(2012.12)].Ryan.Carter.文字版.pdf](ed2k://|file|[Getting.Started.with.Mule.Cloud.Connect(2012.12)].Ryan.Carter.文字版.pdf|10370183|0b5969fd27276edc4d1a3c4dacb8ff81|h=luve7srnprvo6ogyqyslcuix63j56cbe|/)

[[Getting.Started.with.Mule.Cloud.Connect(2012.12)].Ryan.Carter.文字版.epub](ed2k://|file|[Getting.Started.with.Mule.Cloud.Connect(2012.12)].Ryan.Carter.文字版.epub|3086842|16377d3a035f865fe97901f9cc53fbd9|h=g6uo22y34v75lfbod2dcfgnm5747hmmp|/)

[[Getting.Started.with.MakerBot(2012.12)].Bre.Pettis.文字版.pdf](ed2k://|file|[Getting.Started.with.MakerBot(2012.12)].Bre.Pettis.文字版.pdf|28530424|2d1c6e1f8a89da858fccb83bea610ff2|h=rv3ugiazdqcdpdmrvowkoya5s57jeu5w|/)

[[Getting.Started.with.MakerBot(2012.12)].Bre.Pettis.文字版.epub](ed2k://|file|[Getting.Started.with.MakerBot(2012.12)].Bre.Pettis.文字版.epub|18525522|174bd9a892bd6123db719c964d6020e2|h=eres2e52hrufkughg2iqqdkpbqyofet3|/)

[[eBay.Commerce.Cookbook(2012.12)].Chuck.Hudson.文字版.pdf](ed2k://|file|[eBay.Commerce.Cookbook(2012.12)].Chuck.Hudson.文字版.pdf|11297598|94dc99c5e59876c57ccf602126648af9|h=sr5htaylpqbkw6xraq4rckk5gwfap5oj|/)

[[eBay.Commerce.Cookbook(2012.12)].Chuck.Hudson.文字版.epub](ed2k://|file|[eBay.Commerce.Cookbook(2012.12)].Chuck.Hudson.文字版.epub|7237791|baacaef54709d7f5ecacae8467f4f9c2|h=xl3yl7zawrhtglbgpj44mloibahmegah|/)

[[Building.Node.Applications.with.MongoDB.and.Backbone(2012.12)].Mike.Wilson.文字版.pdf](ed2k://|file|[Building.Node.Applications.with.MongoDB.and.Backbone(2012.12)].Mike.Wilson.文字版.pdf|5356318|4918aab1c97f494bc59944654aec3fd6|h=iiaiqlem3kfektyxnvighwcziaeqckho|/)

[[Building.Node.Applications.with.MongoDB.and.Backbone(2012.12)].Mike.Wilson.文字版.epub](ed2k://|file|[Building.Node.Applications.with.MongoDB.and.Backbone(2012.12)].Mike.Wilson.文字版.epub|2310722|e0d516f2b0df83810543efc82a6d271e|h=djn65yaafdagymbgspnykaeqppztciws|/)

[[R.Graphics.Cookbook(2012.12)].Winston.Chang.文字版.pdf](ed2k://|file|[R.Graphics.Cookbook(2012.12)].Winston.Chang.文字版.pdf|35290906|903c72773ba4a1957c1da508ccd4c0cb|h=wlhg6dcnhi7jafzcazmb7zl53ct4gfki|/)

[[R.Graphics.Cookbook(2012.12)].Winston.Chang.文字版.epub](ed2k://|file|[R.Graphics.Cookbook(2012.12)].Winston.Chang.文字版.epub|21516045|904dda32c04d960e082d78e27df0575d|h=pqq6yvk6miqtisegtvfp4mloymv4eacr|/)

[[Puppet.Types.and.Providers(2012.12)].Dan.Bode.文字版.pdf](ed2k://|file|[Puppet.Types.and.Providers(2012.12)].Dan.Bode.文字版.pdf|5079163|4810eef2cbb1416b1bb70352e32a104c|h=l7trmnw5ausdnqfju4fpya5yailpqfrd|/)

[[Puppet.Types.and.Providers(2012.12)].Dan.Bode.文字版.epub](ed2k://|file|[Puppet.Types.and.Providers(2012.12)].Dan.Bode.文字版.epub|2845265|5a4f5923296cfacc3c874c02548e37ac|h=nvfcbmzwvxnd4wdb2nttkq5swmwrvmhk|/)

[[Programmers.Guide.to.Drupal(2012.12)].Jennifer.Hodgdon.文字版.pdf](ed2k://|file|[Programmers.Guide.to.Drupal(2012.12)].Jennifer.Hodgdon.文字版.pdf|4738210|531ab934e5f61e8e5e8f401623b98882|h=ibi7q5cptxndwernbkphg56negv7vxan|/)

[[Programmers.Guide.to.Drupal(2012.12)].Jennifer.Hodgdon.文字版.epub](ed2k://|file|[Programmers.Guide.to.Drupal(2012.12)].Jennifer.Hodgdon.文字版.epub|2734483|8233a0c94c1fe503b3ec22aba73d143c|h=vqlfdklthokobmtrpwdq3z7tykmth3ec|/)

[[Node.js.for.PHP.Developers(2012.11)].Daniel.Howard.文字版.pdf](ed2k://|file|[Node.js.for.PHP.Developers(2012.11)].Daniel.Howard.文字版.pdf|5635876|6a327bbd748ecb5dd53f4282722d7826|h=hyx7wzfgwsrznzf4u2njpkswxeoa6nz2|/)

[[Node.js.for.PHP.Developers(2012.11)].Daniel.Howard.文字版.epub](ed2k://|file|[Node.js.for.PHP.Developers(2012.11)].Daniel.Howard.文字版.epub|2495481|67375ca8b816c54b9cc4c42914cb3e38|h=uvqeh36qmpbmuuurh2uivqkxzsg4i2i5|/)

[[Learning.Cocoa.with.Objective-C(3rd,2012.12)].Paris.Buttfield-Addison.文字版.pdf](ed2k://|file|[Learning.Cocoa.with.Objective-C(3rd,2012.12)].Paris.Buttfield-Addison.文字版.pdf|11610331|4ce66a2ad644e030d2031d64c3163ea5|h=qikwzs5ag2bl7tyft44mzcw4gbs6h2qq|/)

[[Learning.Cocoa.with.Objective-C(3rd,2012.12)].Paris.Buttfield-Addison.文字版.epub](ed2k://|file|[Learning.Cocoa.with.Objective-C(3rd,2012.12)].Paris.Buttfield-Addison.文字版.epub|6933751|4d914398cd404b344667b16014f0312f|h=fxnpsdpgfbmajlipt443mocpj6p5mkrc|/)

[[HTTP：The.Definitive.Guide(2002.9)].David.Gourley.文字版.pdf](ed2k://|file|[HTTP：The.Definitive.Guide(2002.9)].David.Gourley.文字版.pdf|9603510|6ac030ff242bbbb49ca976b55108d973|h=ujxlca4y2sietkf44dyiw47qqi33b26y|/)

[[HTTP：The.Definitive.Guide(2002.9)].David.Gourley.文字版.epub](ed2k://|file|[HTTP：The.Definitive.Guide(2002.9)].David.Gourley.文字版.epub|5655909|bd937190af600fdf2303d963c9facb89|h=cnyqhx7epsnda4jtnotephsvvstr4klb|/)

[[Bandit.Algorithms.for.Website.Optimization(2012.12)].John.Myles.White.文字版.pdf](ed2k://|file|[Bandit.Algorithms.for.Website.Optimization(2012.12)].John.Myles.White.文字版.pdf|7586478|72271ace72134cd65f5851c50ecb8645|h=cnxy533hhrszzv3uened4ltdj3qvdb4o|/)

[[Bandit.Algorithms.for.Website.Optimization(2012.12)].John.Myles.White.文字版.epub](ed2k://|file|[Bandit.Algorithms.for.Website.Optimization(2012.12)].John.Myles.White.文字版.epub|4083011|50a1e1a8f15221b699eb24cdb9657e39|h=gt66guvwlnc3vvkdcm3tywk6hbfwxuql|/)

[[MySQL.High.Availability(2010.6)].Charles.Bell.文字版.pdf](ed2k://|file|[MySQL.High.Availability(2010.6)].Charles.Bell.文字版.pdf|9878301|7F60692576C792F21F919D7B80881525|h=F4OSB4DLTLX5W675YJ7J7HI33M3PYNSL|/)

[[Windows.Internals.Part.2(6th,2012.9)].Mark.E.Russinovich.文字版.pdf](ed2k://|file|[Windows.Internals.Part.2(6th,2012.9)].Mark.E.Russinovich.文字版.pdf|22738659|1a0e27fb2221ff55c7ced45fb9d14d2b|h=nlj32oaur7jqyltnb5psloppmzvwo64i|/)

[[Windows.Internals.Part.2(6th,2012.9)].Mark.E.Russinovich.文字版.epub](ed2k://|file|[Windows.Internals.Part.2(6th,2012.9)].Mark.E.Russinovich.文字版.epub|14776191|0a147e9316b5e035e2ac94ebea727146|h=5umbdeojdvve5jvj5jodfgpendk5tpe5|/)

[[Windows.Internals.Part.1(6th,2012.3)].Mark.E.Russinovich.文字版.pdf](ed2k://|file|[Windows.Internals.Part.1(6th,2012.3)].Mark.E.Russinovich.文字版.pdf|26217201|918d6710a19b4467ab2f2f45627e061d|h=gwcz7bfbhncri2nl3cqxhlsi23jrm4jk|/)

[[Windows.Internals.Part.1(6th,2012.3)].Mark.E.Russinovich.文字版.epub](ed2k://|file|[Windows.Internals.Part.1(6th,2012.3)].Mark.E.Russinovich.文字版.epub|18997549|171851ffc8481b7ac3638f9a5c4bf3e6|h=t5oyoyq6f44tooz7gocvv5fhfcmsrcp3|/)

[[More.Effective.CPP(1995.12)].Scott.Meyers.文字版.pdf](ed2k://|file|[More.Effective.CPP(1995.12)].Scott.Meyers.文字版.pdf|9089757|9cdaba210eac20c15d5bfacff910b61c|h=ysxzbxpld2p54zrzlziclnyrzxb7das7|/)

[[More.Effective.CPP(1995.12)].Scott.Meyers.文字版.epub](ed2k://|file|[More.Effective.CPP(1995.12)].Scott.Meyers.文字版.epub|1810425|6b068ae72c5bf95e2bd4a4771533e13c|h=4hugyquu6ahlugjomaworhgfobxicsm6|/)

[[Effective.CPP(3rd,2005)].Scott.Meyers.文字版.pdf](ed2k://|file|[Effective.CPP(3rd,2005)].Scott.Meyers.文字版.pdf|7903538|8f8fc41d5ad1528a22881ec57ad09255|h=fexiciazflt5ku5r6zecde64dty6kf65|/)

[[Effective.CPP(3rd,2005)].Scott.Meyers.文字版.epub](ed2k://|file|[Effective.CPP(3rd,2005)].Scott.Meyers.文字版.epub|1350840|07334017875eaa8664c3f64f84b94af2|h=cry5kvfwvzfkwaznuixwgrecc5wtsnf6|/)

[[Effective.STL(2001.6)].Scott.Meyers.文字版.pdf](ed2k://|file|[Effective.STL(2001.6)].Scott.Meyers.文字版.pdf|3285389|70fbac3f4b9d675b00f853b0e8744136|h=vlozsvko5fg2gpr3uteonqvn4g222nyo|/)

[[Effective.STL(2001.6)].Scott.Meyers.文字版.epub](ed2k://|file|[Effective.STL(2001.6)].Scott.Meyers.文字版.epub|1517168|04b100879fa964ac130fb198942c2a10|h=2t2wnuzpm5cxk5l2lzozhh4gfzsaxupd|/)

[[Tomcat：The.Definitive.Guide(2nd,2007.10)].Jason.Brittain.文字版.pdf](ed2k://|file|[Tomcat：The.Definitive.Guide(2nd,2007.10)].Jason.Brittain.文字版.pdf|4449730|a494ca5fb2e9cb6018f0a1d99a5c6af2|h=ho6mnzutzs2q6s7ec5kg36ws2jjqi2ao|/)

[[Tomcat：The.Definitive.Guide(2nd,2007.10)].Jason.Brittain.文字版.epub](ed2k://|file|[Tomcat：The.Definitive.Guide(2nd,2007.10)].Jason.Brittain.文字版.epub|4280336|b3129dec2a7d7c37c3f5483eba34fb08|h=5icawp2yxneyfjmilidy6adrlyagrwr2|/)

[[Programming.Csharp.5.0(2012.10)].Ian.Griffiths.文字版.pdf](ed2k://|file|[Programming.Csharp.5.0(2012.10)].Ian.Griffiths.文字版.pdf|20314511|5db117727251a26430515a55583d67b2|h=bhvofur3gnclu4l3oabqo763lvhwnpmx|/)

[[Programming.Csharp.5.0(2012.10)].Ian.Griffiths.文字版.epub](ed2k://|file|[Programming.Csharp.5.0(2012.10)].Ian.Griffiths.文字版.epub|4402420|8d28bfff7a29547c380e049f926ec067|h=2b3rczj2w26fqpxha57pi75vhtp5ezn5|/)

[[JRuby.Cookbook(2008.11)].Justin.Edelson.文字版.pdf](ed2k://|file|[JRuby.Cookbook(2008.11)].Justin.Edelson.文字版.pdf|2928605|fb78575ed7278377f8bf51200fd536db|h=iblazx2tziplhcrtbq2ybtwmaw2e3mj5|/)

[[JRuby.Cookbook(2008.11)].Justin.Edelson.文字版.epub](ed2k://|file|[JRuby.Cookbook(2008.11)].Justin.Edelson.文字版.epub|3518154|342df8daa387114ea97ddc64a2e3277f|h=s2gum2embfpnuznkp4wedwrd6wx456n7|/)

[[Java.Cookbook(2nd,2004.6)].Ian.F.Darwin.文字版.pdf](ed2k://|file|[Java.Cookbook(2nd,2004.6)].Ian.F.Darwin.文字版.pdf|5233088|2e672892998eb21027be274291cb3b6d|h=6mxqmoj6c6fq2zvonn5njc7ezppu4tqk|/)

[[Java.Cookbook(2nd,2004.6)].Ian.F.Darwin.文字版.epub](ed2k://|file|[Java.Cookbook(2nd,2004.6)].Ian.F.Darwin.文字版.epub|4257301|af32072f1470c9688b58eb27f1732f8c|h=ooias7aldpuo35o7dp26lszk6g6shkc3|/)

[[Eclipse.IDE.Pocket.Guide(2005.8)].Ed.Burnette.文字版.pdf](ed2k://|file|[Eclipse.IDE.Pocket.Guide(2005.8)].Ed.Burnette.文字版.pdf|1046317|ca60b6a145376e5150ab5551fbbc9f50|h=yxkvnzdx3aacfq3ib4wne73acbltx2oj|/)

[[Eclipse.IDE.Pocket.Guide(2005.8)].Ed.Burnette.文字版.epub](ed2k://|file|[Eclipse.IDE.Pocket.Guide(2005.8)].Ed.Burnette.文字版.epub|2526386|0b470b9d7340c7edf118a2fbfd078fbd|h=tbuqryjwuh4jbyi5lmn6hh6yp3lh5354|/)

[[Eclipse(2004.4)].Steve.Holzner.文字版.pdf](ed2k://|file|[Eclipse(2004.4)].Steve.Holzner.文字版.pdf|9388097|6b45e6fb5fea463b14221abb55262842|h=7iphbz2xsiaki5gou4kuqtijdykpndph|/)

[[Eclipse(2004.4)].Steve.Holzner.文字版.epub](ed2k://|file|[Eclipse(2004.4)].Steve.Holzner.文字版.epub|8171277|3221f4dbc1ecd1a765d189c4cfed781f|h=filxtvqjf3ksdyres2lvgvg4kwper23r|/)

[[Windows.Server.2012：Up.and.Running(2012.11)].Samara.Lynn.文字版.pdf](ed2k://|file|[Windows.Server.2012：Up.and.Running(2012.11)].Samara.Lynn.文字版.pdf|15616147|87d8c4ab40f50db8edeeeef59ad4b79f|h=kmfbc5ypqgsl44hb57ganykd3r3tbvy6|/)

[[Windows.Server.2012：Up.and.Running(2012.11)].Samara.Lynn.文字版.epub](ed2k://|file|[Windows.Server.2012：Up.and.Running(2012.11)].Samara.Lynn.文字版.epub|21228892|2cff8bf28702a9645f80f3d0548c22d3|h=yq5y2tsfycrxtcefpglzqbrbypvwnnsw|/)

[[Monitoring.with.Ganglia(2012.11)].Matt.Massie.文字版.pdf](ed2k://|file|[Monitoring.with.Ganglia(2012.11)].Matt.Massie.文字版.pdf|12142203|77739765b52eac87e09b32c016e08d7e|h=pgux6gnf7oy6qvafozpijshkivntzz4r|/)

[[Monitoring.with.Ganglia(2012.11)].Matt.Massie.文字版.epub](ed2k://|file|[Monitoring.with.Ganglia(2012.11)].Matt.Massie.文字版.epub|7521752|c6e863548501b5c95d64ab196d079bec|h=s6ado7j6lixg56rqxdwosl7ttg5x2jnp|/)

[[MapReduce.Design.Patterns(2012.11)].Donald.Miner.文字版.pdf](ed2k://|file|[MapReduce.Design.Patterns(2012.11)].Donald.Miner.文字版.pdf|9711511|2634984abfe1b6b9eb71608c88fd30a6|h=7yisizx5gpxqnoxsoy45ucqk5spr6ntv|/)

[[MapReduce.Design.Patterns(2012.11)].Donald.Miner.文字版.epub](ed2k://|file|[MapReduce.Design.Patterns(2012.11)].Donald.Miner.文字版.epub|5858534|549e1a76a39ff7026012db010bb17ad2|h=2gnwptav3ma7cbtnzxi2z7r75ptc4fan|/)

[[Juniper.Networks.Warrior(2012.11)].Peter.Southwick.文字版.pdf](ed2k://|file|[Juniper.Networks.Warrior(2012.11)].Peter.Southwick.文字版.pdf|11772313|68c3c0fe641b831f3ea0246c6422fd98|h=ndobyuocvtlwxa7lrzxwrik2uoekrfam|/)

[[Juniper.Networks.Warrior(2012.11)].Peter.Southwick.文字版.epub](ed2k://|file|[Juniper.Networks.Warrior(2012.11)].Peter.Southwick.文字版.epub|8378848|1e9faf58de05979747368299e10d9312|h=344t7dh2zilvwvonis2oaqveho3kci7z|/)

[[iOS.6.Programming.Cookbook(2012.11)].Vandad.Nahavandipoor.文字版.pdf](ed2k://|file|[iOS.6.Programming.Cookbook(2012.11)].Vandad.Nahavandipoor.文字版.pdf|40119317|dfd097eb29781bd7fbbfd7dd6bbbd68e|h=j6gvn46ci3i5c2vbiri6axgtbv4veiof|/)

[[iOS.6.Programming.Cookbook(2012.11)].Vandad.Nahavandipoor.文字版.epub](ed2k://|file|[iOS.6.Programming.Cookbook(2012.11)].Vandad.Nahavandipoor.文字版.epub|23006847|1c0e32ff57bf8fb6c9a3f60911ef4917|h=vzdmuilcxbytc3eii2yuz5anbuyzdgcp|/)

[[Building.Web.Cloud.and.Mobile.Solutions.with.Fsharp(2012.11)].Daniel.Mohl.文字版.pdf](ed2k://|file|[Building.Web.Cloud.and.Mobile.Solutions.with.Fsharp(2012.11)].Daniel.Mohl.文字版.pdf|5973547|31b0a161c2f965456fbaa1ed5e74e1a9|h=r2spwz62rtm54vhus7ji5ggif3u66atn|/)

[[Building.Web.Cloud.and.Mobile.Solutions.with.Fsharp(2012.11)].Daniel.Mohl.文字版.epub](ed2k://|file|[Building.Web.Cloud.and.Mobile.Solutions.with.Fsharp(2012.11)].Daniel.Mohl.文字版.epub|2766854|35b1f25dbde15fea446ceae7716fb776|h=3hnygya7erlktcxzjqseaqt36xosxbzk|/)

[[Bad.Data.Handbook(2012.11)].Q.Ethan.McCallum.文字版.pdf](ed2k://|file|[Bad.Data.Handbook(2012.11)].Q.Ethan.McCallum.文字版.pdf|10996061|9894f6547ae728cfb4b6a513002d4013|h=uhetklytn2lbey42bh32gozybp2aiked|/)

[[Bad.Data.Handbook(2012.11)].Q.Ethan.McCallum.文字版.epub](ed2k://|file|[Bad.Data.Handbook(2012.11)].Q.Ethan.McCallum.文字版.epub|5315311|e6055773511fdf3bc1d5ca6e4ea054e3|h=5lyyhjdzbq64ie2rkgeaims3of2vhm2x|/)

[[SciPy.and.NumPy(2012.11)].Eli.Bressert.文字版.pdf](ed2k://|file|[SciPy.and.NumPy(2012.11)].Eli.Bressert.文字版.pdf|6431406|9efe828a44f495aa9bc2c7de78db8d68|h=ndguw4nu377k6g4cwe3w4citmwsbmnr3|/)

[[SciPy.and.NumPy(2012.11)].Eli.Bressert.文字版.epub](ed2k://|file|[SciPy.and.NumPy(2012.11)].Eli.Bressert.文字版.epub|3031668|89e9f541a74cb193212f0d32cdd9046e|h=g3p3qagbzokndbukhjvf7e5x5x5v6yl7|/)

[[Statistics.in.a.Nutshell(2nd,2012.11)].Sarah.Boslaugh.文字版.pdf](ed2k://|file|[Statistics.in.a.Nutshell(2nd,2012.11)].Sarah.Boslaugh.文字版.pdf|21681680|1b2cb5362221ed96462d460e6a78b722|h=6fsvno6xcxzgkpx2zkgokmrp5oo7mzsb|/)

[[Statistics.in.a.Nutshell(2nd,2012.11)].Sarah.Boslaugh.文字版.epub](ed2k://|file|[Statistics.in.a.Nutshell(2nd,2012.11)].Sarah.Boslaugh.文字版.epub|16084740|e8a5e52cdef59b29826cc1f03babdbc0|h=pauiizrn6fqqjg2fkrrza3gskcqmva2v|/)

[[Make：Ultimate.Guide.to.3D.Printing(2012.11)].The.Editors.of.MAKE.文字版.pdf](ed2k://|file|[Make：Ultimate.Guide.to.3D.Printing(2012.11)].The.Editors.of.MAKE.文字版.pdf|48449281|a29dd0b6861672cbf931424ccd6f2dff|h=okyjfiwwrwzd6exn5v6rnobld2xddeng|/)

[[Kinect.Hacks(2012.11)].Jared.St.Jean.文字版.pdf](ed2k://|file|[Kinect.Hacks(2012.11)].Jared.St.Jean.文字版.pdf|22619918|b592eeab1ec46bd0f508e3c567a6c1b3|h=7ho5rom2fpzkdzfjuykosjd5c3o2x6el|/)

[[Kinect.Hacks(2012.11)].Jared.St.Jean.文字版.epub](ed2k://|file|[Kinect.Hacks(2012.11)].Jared.St.Jean.文字版.epub|14037481|193efa8ca9f2691b09c59a9d08339a3d|h=ebhowtnrnrfqcczormttnbjudowmld2a|/)

[[HTML5.Hacks(2012.11)].Jesse.Cravens.文字版.pdf](ed2k://|file|[HTML5.Hacks(2012.11)].Jesse.Cravens.文字版.pdf|26561259|43d8a66cee3ab19d07949764a5d4519f|h=2n6y7pckxcpodf64bhp3tqdiqbnjr7wv|/)

[[HTML5.Hacks(2012.11)].Jesse.Cravens.文字版.epub](ed2k://|file|[HTML5.Hacks(2012.11)].Jesse.Cravens.文字版.epub|18025596|fa4a253fbddd8252fbced767affc48a7|h=4taiujmeje4hpooswv7fohued3ymso3b|/)

[[Atmospheric.Monitoring.with.Arduino(2012.11)].Patrick.Di.Justo.文字版.pdf](ed2k://|file|[Atmospheric.Monitoring.with.Arduino(2012.11)].Patrick.Di.Justo.文字版.pdf|18377441|1a3b44c8d4be677635080bcbd76ccdc8|h=ubbac7jeoz7sqesfqfnexkzdmukwdn7i|/)

[[Atmospheric.Monitoring.with.Arduino(2012.11)].Patrick.Di.Justo.文字版.epub](ed2k://|file|[Atmospheric.Monitoring.with.Arduino(2012.11)].Patrick.Di.Justo.文字版.epub|4687072|a80b98e905819ec42f288e8bab9ddd1f|h=mg6tbb6soikecbd7pspnfnm4vfhzfaev|/)

[[Spring.Data(2012.10)].Mark.Pollack.文字版.pdf](ed2k://|file|[Spring.Data(2012.10)].Mark.Pollack.文字版.pdf|12399133|b2b336863c621e1687f16c5868a054ba|h=gbk2pfjwpxk7ttr6jrybepkn4alw2bhr|/)

[[Spring.Data(2012.10)].Mark.Pollack.文字版.epub](ed2k://|file|[Spring.Data(2012.10)].Mark.Pollack.文字版.epub|6531919|e549545038afa9e00238e14635991cc9|h=wc7j5hh5gq4xqoskwpmtsdpvswfo6c6d|/)

[[Make.an.Arduino-Controlled.Robot(2012.10)].Michael.Margolis.文字版.pdf](ed2k://|file|[Make.an.Arduino-Controlled.Robot(2012.10)].Michael.Margolis.文字版.pdf|20979518|875474b0833df01469ccb77663aa9dd4|h=qbf65ypditbgisq4gvyyhg6g6ti5t6ci|/)

[[Make.an.Arduino-Controlled.Robot(2012.10)].Michael.Margolis.文字版.epub](ed2k://|file|[Make.an.Arduino-Controlled.Robot(2012.10)].Michael.Margolis.文字版.epub|13036098|2795165cbdeedc024c31eb910bd87d30|h=6adkbju6ktx37273qnrsci4euketjswl|/)

[[Encyclopedia.of.Electronic.Components.Volume.1(2012.10)].Charles.Platt.文字版.pdf](ed2k://|file|[Encyclopedia.of.Electronic.Components.Volume.1(2012.10)].Charles.Platt.文字版.pdf|32289841|8a0fc10eae256013c7f6edb5497023e4|h=ylsq37povpjtpcg6aedpqqfog4r4khjw|/)

[[Encyclopedia.of.Electronic.Components.Volume.1(2012.10)].Charles.Platt.文字版.epub](ed2k://|file|[Encyclopedia.of.Electronic.Components.Volume.1(2012.10)].Charles.Platt.文字版.epub|25967737|1fc9c881d1e484535e45e66b676f8d34|h=cpxpmxjj4fpzgidea5dwufvdneien4ra|/)

[[Dart：Up.and.Running(2012.10)].Kathy.Walrath.文字版.pdf](ed2k://|file|[Dart：Up.and.Running(2012.10)].Kathy.Walrath.文字版.pdf|6722948|49097c52d559a0edd8b34461abc5160a|h=22b6kojgeqk6p2lnyusc2htdiwbtfdkc|/)

[[Dart：Up.and.Running(2012.10)].Kathy.Walrath.文字版.epub](ed2k://|file|[Dart：Up.and.Running(2012.10)].Kathy.Walrath.文字版.epub|3362305|a0e9e5f10650bd526cedf0328517a953|h=ptxr6bl6gk2rcg6ym65rk5djye73n4u3|/)

[[ClojureScript：Up.and.Running(2012.10)].Stuart.Sierra.文字版.pdf](ed2k://|file|[ClojureScript：Up.and.Running(2012.10)].Stuart.Sierra.文字版.pdf|8168740|f8ffabe9e400b81ce4b567bf3728d9ae|h=szbwqfm3vij5b5a2gpa2xdkchkgjb4vs|/)

[[ClojureScript：Up.and.Running(2012.10)].Stuart.Sierra.文字版.epub](ed2k://|file|[ClojureScript：Up.and.Running(2012.10)].Stuart.Sierra.文字版.epub|2460137|fd9cbd8c992be1a27b3ee44aed9178d7|h=pb2mlhoyddvrjqtoiosedfurqshfyuvo|/)

[[21st.Century.C(2012.10)].Ben.Klemens.文字版.pdf](ed2k://|file|[21st.Century.C(2012.10)].Ben.Klemens.文字版.pdf|8230832|1168557a9a43040d1021a9d1be2d12e0|h=pb2mnqfspus5oqyet4pr3j5e5gqtpeok|/)

[[21st.Century.C(2012.10)].Ben.Klemens.文字版.epub](ed2k://|file|[21st.Century.C(2012.10)].Ben.Klemens.文字版.epub|2576955|912d60796e9a00553aff97b7b8c9101b|h=h73qttibe2yuoh2ott6ehfheieutpiys|/)

[[The.Art.of.Agile.Development(2007.10)].James.Shore.文字版.pdf](ed2k://|file|[The.Art.of.Agile.Development(2007.10)].James.Shore.文字版.pdf|4079782|c557feeefc2df086693f6fd1dc5ce059|h=mpwf7ipycyqarasdibfpolouu25pr563|/)

[[The.Art.of.Agile.Development(2007.10)].James.Shore.文字版.epub](ed2k://|file|[The.Art.of.Agile.Development(2007.10)].James.Shore.文字版.epub|3033308|48c444f7f16bff0811e12f9eb34fee56|h=l53vbqr73e3kpaqp2sxvcwlmrijgrzjc|/)

[[Real.World.Haskell(2008.11)].Bryan.O'Sullivan.文字版.pdf](ed2k://|file|[Real.World.Haskell(2008.11)].Bryan.O'Sullivan.文字版.pdf|5806018|9aaa8819e089a094609922ee1e2398a3|h=x6yyicwfcnerzqhiluid5tje7yyhgm7q|/)

[[Real.World.Haskell(2008.11)].Bryan.O'Sullivan.文字版.epub](ed2k://|file|[Real.World.Haskell(2008.11)].Bryan.O'Sullivan.文字版.epub|3429880|0656b8b400540c3c7494cf8e8601940d|h=pqj34fz2twq7q3ordf5pswixs4q3ttl6|/)

[[Python.for.Data.Analysis(2012.10)].Wes.McKinney.文字版.pdf](ed2k://|file|[Python.for.Data.Analysis(2012.10)].Wes.McKinney.文字版.pdf|16221970|317366c367e5e0d9d34ffadd42c61c2f|h=fijotiqudfhzcddzqfiwfx6afagtfu4m|/)

[[Python.for.Data.Analysis(2012.10)].Wes.McKinney.文字版.epub](ed2k://|file|[Python.for.Data.Analysis(2012.10)].Wes.McKinney.文字版.epub|7661848|c9e7a3aba460f68831b666361f62b977|h=xyventlqto67hpmhaobifodtbzm2k7xp|/)

[[Information.Architecture.for.the.World.Wide.Web(3rd,2006.11)].Peter.Morville.文字版.pdf](ed2k://|file|[Information.Architecture.for.the.World.Wide.Web(3rd,2006.11)].Peter.Morville.文字版.pdf|15536445|573630a41045f199335136add7f3faff|h=liiusvqwzpb5lbmyjcigwrzdm7oh4ovo|/)

[[Information.Architecture.for.the.World.Wide.Web(3rd,2006.11)].Peter.Morville.文字版.epub](ed2k://|file|[Information.Architecture.for.the.World.Wide.Web(3rd,2006.11)].Peter.Morville.文字版.epub|21269119|4af3c57692e702a9491ce4d25290a69e|h=6pyblh23bv7egmmslhej2zmhm6w6nhzs|/)

[[Windows.8.Out.of.the.Box(2012.10)].Mike.Halsey.文字版.pdf](ed2k://|file|[Windows.8.Out.of.the.Box(2012.10)].Mike.Halsey.文字版.pdf|14386506|b14852d68770e0f279ca9d5f0e11b84f|h=6toolmqlcgeokyxerlwnv5jdy6i4vfcm|/)

[[Windows.8.Out.of.the.Box(2012.10)].Mike.Halsey.文字版.epub](ed2k://|file|[Windows.8.Out.of.the.Box(2012.10)].Mike.Halsey.文字版.epub|7520015|23ca5d0736ce4184e00353e66c54f0a3|h=xwulgwo33wtelz4aq3f34expup7vhbzh|/)

[[R.in.a.Nutshell(2nd,2012.9)].Joseph.Adler.文字版.pdf](ed2k://|file|[R.in.a.Nutshell(2nd,2012.9)].Joseph.Adler.文字版.pdf|14077099|d17c28dc477cfa09652dcd00b16fdbf9|h=z3skjkgudh6beazld7dtgaw2yehvi6cq|/)

[[R.in.a.Nutshell(2nd,2012.9)].Joseph.Adler.文字版.epub](ed2k://|file|[R.in.a.Nutshell(2nd,2012.9)].Joseph.Adler.文字版.epub|6668202|c7cae05bfb9f8906e55013223e0a318a|h=iueif66n2qu6sbbmwgrlnucljejmde5c|/)

[[Programming.Google.App.Engine(2nd,2012.10)].Dan.Sanderson.文字版.pdf](ed2k://|file|[Programming.Google.App.Engine(2nd,2012.10)].Dan.Sanderson.文字版.pdf|12642705|1303c692e2a364d0389afb4582c3391f|h=zb2ar2ycjfhyqsx3kwipbs2ifzlfa3fl|/)

[[Programming.Google.App.Engine(2nd,2012.10)].Dan.Sanderson.文字版.epub](ed2k://|file|[Programming.Google.App.Engine(2nd,2012.10)].Dan.Sanderson.文字版.epub|6643709|247f64bf43d438e8f57d8874e493fe0e|h=klcdpcflugfuuliw4r4szvteiiuc7ip4|/)

[[Programming.Fsharp.3.0(2nd,2012.10)].Chris.Smith.文字版.pdf](ed2k://|file|[Programming.Fsharp.3.0(2nd,2012.10)].Chris.Smith.文字版.pdf|15971455|2dc3078d3754ffec38861762ac915b2b|h=ti6bu6eeq2sjdal42igf2hmucqszpl3g|/)

[[Programming.Fsharp.3.0(2nd,2012.10)].Chris.Smith.文字版.epub](ed2k://|file|[Programming.Fsharp.3.0(2nd,2012.10)].Chris.Smith.文字版.epub|3296025|f9ea714dd8edd8fd51ae69ec91d66c7d|h=txzdjyvsp2sfqibk62trz33ojgap52sv|/)

[[Practical.Zendesk.Administration(2012.10)].Stafford.Vaughan.文字版.pdf](ed2k://|file|[Practical.Zendesk.Administration(2012.10)].Stafford.Vaughan.文字版.pdf|12096381|459a4844ff076c8e96adbf00e4fb3a94|h=4quc4ysrntoopges7o3nilvvilan6nlf|/)

[[Practical.Zendesk.Administration(2012.10)].Stafford.Vaughan.文字版.epub](ed2k://|file|[Practical.Zendesk.Administration(2012.10)].Stafford.Vaughan.文字版.epub|8485753|7e4955cce646a7e73cdd4f726c79865c|h=767hp5k5t6jxj54ehkfgiyvf2wgk5uhw|/)

[[Natural.Language.Annotation.for.Machine.Learning(2012.10)].James.Pustejovsky.文字版.pdf](ed2k://|file|[Natural.Language.Annotation.for.Machine.Learning(2012.10)].James.Pustejovsky.文字版.pdf|13479260|3ab1b493fba83338e2b0359cdd1de0b0|h=5epp4ddppqp7bva2gz7dijck63zzs6ap|/)

[[Natural.Language.Annotation.for.Machine.Learning(2012.10)].James.Pustejovsky.文字版.epub](ed2k://|file|[Natural.Language.Annotation.for.Machine.Learning(2012.10)].James.Pustejovsky.文字版.epub|5035060|86aad8ee696bd10cced01dfced6ebf7c|h=glxvnelfy7bxmd44wfnilxm52p5cbavm|/)

[[MintDuino(2011.9)].James.Floyd.Kelly.文字版.pdf](ed2k://|file|[MintDuino(2011.9)].James.Floyd.Kelly.文字版.pdf|40756772|4906655ca115b63fcb0a507692c09af2|h=3fqmrypv2o7utgeifbr75u53vyx3swzd|/)

[[MintDuino(2011.9)].James.Floyd.Kelly.文字版.epub](ed2k://|file|[MintDuino(2011.9)].James.Floyd.Kelly.文字版.epub|5677840|3fcc90c48f8829aab79c3cd4bf75dfb4|h=efzfh4nydiksk7ggme5aojzakpgble3a|/)

[[Hadoop.Operations(2012.9)].Eric.Sammer.文字版.pdf](ed2k://|file|[Hadoop.Operations(2012.9)].Eric.Sammer.文字版.pdf|8411873|712701c4fd9db31b7ee0a170ff43dabf|h=d7wg752n6gyparx4e5t3vhlqu4yh6uph|/)

[[Hadoop.Operations(2012.9)].Eric.Sammer.文字版.epub](ed2k://|file|[Hadoop.Operations(2012.9)].Eric.Sammer.文字版.epub|3285784|6d02f6b27305441591edf4718d4a8a84|h=wyc5jdyagjjcmmzfixycjne5ahhqxjwp|/)

[[Geolocation.in.iOS(2012.10)].Alasdair.Allan.文字版.pdf](ed2k://|file|[Geolocation.in.iOS(2012.10)].Alasdair.Allan.文字版.pdf|15700683|f78dbae6f6836ffbb4a11e7e7d7f7b0e|h=3zbop2stxqiy6jxjaecdgxbgb6dk3buc|/)

[[Geolocation.in.iOS(2012.10)].Alasdair.Allan.文字版.epub](ed2k://|file|[Geolocation.in.iOS(2012.10)].Alasdair.Allan.文字版.epub|9487818|9bf72ff19b678d57efecc686b3fd9e1b|h=6ixot3ckwdebhtcjctll6byfiqd55pyg|/)

[[Arista.Warrior(2012.10)].Gary.A.Donahue.文字版.pdf](ed2k://|file|[Arista.Warrior(2012.10)].Gary.A.Donahue.文字版.pdf|35095255|32f30c291cac9142a870ea7ce08631f4|h=ycy73zyklcin53l3wlaunymiw6reyafr|/)

[[Arista.Warrior(2012.10)].Gary.A.Donahue.文字版.epub](ed2k://|file|[Arista.Warrior(2012.10)].Gary.A.Donahue.文字版.epub|10680783|7f03af4e4e4ce818a0c7e13fb5560ac1|h=6mnwtx5jyrny75ojxa33lq54ddif5lot|/)

[[HBase.in.Action(2012.11)].Nick.Dimiduk.文字版.pdf](ed2k://|file|[HBase.in.Action(2012.11)].Nick.Dimiduk.文字版.pdf|8245859|cb33e747049cbb36be3b217be4e1ceb5|h=cxuoypnj23tmkk34g5l37pae4iwf5zbg|/)

[[iOS.in.Practice(2012.11)].Bear.Cahill.文字版.pdf](ed2k://|file|[iOS.in.Practice(2012.11)].Bear.Cahill.文字版.pdf|14015344|c8450a4af48297c4373ebf242a09ee88|h=k4pgpnrudpirviiezf2o4br7tx6qvpzi|/)

[[Hello.HTML5.and.CSS3(2012.10)].Rob.Crowther.文字版.pdf](ed2k://|file|[Hello.HTML5.and.CSS3(2012.10)].Rob.Crowther.文字版.pdf|28704341|00434bf635afa01c0fe3b9cfa16a1007|h=y7u2aeoq24uxnq6ymiwgavxxnc2bx2h4|/)

[[Hadoop.in.Practice(2012.10)].Alex.Holmes.文字版.pdf](ed2k://|file|[Hadoop.in.Practice(2012.10)].Alex.Holmes.文字版.pdf|16023771|7b42418750835b79b270d5d63ea95542|h=v2iw62vdxbzd3tjsusx4dsv477faze7f|/)

[[Hadoop.in.Practice(2012.10)].Alex.Holmes.文字版.epub](ed2k://|file|[Hadoop.in.Practice(2012.10)].Alex.Holmes.文字版.epub|38920078|7437b86c50f9ce3918beeb20140093f2|h=mxsyj6nohecpwpsqomzfcl3uavav32rq|/)

[[Restlet.in.Action(2012.9)].Jerome.Louvel.文字版.pdf](ed2k://|file|[Restlet.in.Action(2012.9)].Jerome.Louvel.文字版.pdf|22241681|9c4ec481f91170404716804d08b03638|h=c653gjngql4c74fzfhfiudiyybjmgffz|/)

[[Restlet.in.Action(2012.9)].Jerome.Louvel.文字版.epub](ed2k://|file|[Restlet.in.Action(2012.9)].Jerome.Louvel.文字版.epub|12891595|ba47c7d432ed5ae3e1786ae2301ae982|h=sjgvzd2jzn3cy2khkdqk7ixodzm22nls|/)

[[Getting.Started.with.RFID(2012.3)].Tom.Igoe.文字版.pdf](ed2k://|file|[Getting.Started.with.RFID(2012.3)].Tom.Igoe.文字版.pdf|4868524|2c43a399e055a5ceab0674f3529eabde|h=zeds7oh4iyaj72flbg2cg5kxazzmecjv|/)

[[Getting.Started.with.RFID(2012.3)].Tom.Igoe.文字版.epub](ed2k://|file|[Getting.Started.with.RFID(2012.3)].Tom.Igoe.文字版.epub|2278066|51f00be9d1cf12717fcd648d1d30d42a|h=ncycbanqillgx2m3wkbmovj7dwpd2yfm|/)

[[Web.Design.in.a.Nutshell(3rd,2006.2)].Jennifer.Niederst.Robbins.文字版.pdf](ed2k://|file|[Web.Design.in.a.Nutshell(3rd,2006.2)].Jennifer.Niederst.Robbins.文字版.pdf|7815628|649149aac37411ca4f15e450941ea73d|h=n66wrnk4dxoqpxmvekm5zbjkumzt6hoz|/)

[[Web.Design.in.a.Nutshell(3rd,2006.2)].Jennifer.Niederst.Robbins.文字版.epub](ed2k://|file|[Web.Design.in.a.Nutshell(3rd,2006.2)].Jennifer.Niederst.Robbins.文字版.epub|14414451|1c2fc59762f5a37314f1bdbb09fbf75c|h=j4dtdikkcnvynqzajvc7ckijb5roydkw|/)

[[Tcl.Tk.in.a.Nutshell(1999.3)].Paul.Raines.文字版.pdf](ed2k://|file|[Tcl.Tk.in.a.Nutshell(1999.3)].Paul.Raines.文字版.pdf|1592356|3ef3c580c255f8f879de350bb99eefd1|h=7pwulvb4c3kzbf2njrelextb6o5knibw|/)

[[Tcl.Tk.in.a.Nutshell(1999.3)].Paul.Raines.文字版.epub](ed2k://|file|[Tcl.Tk.in.a.Nutshell(1999.3)].Paul.Raines.文字版.epub|2777257|74bddb483e90480f14f0fedb05ac4e91|h=lvahkj4mjpthixhay6qimw2npz7tmu5w|/)

[[MySQL.in.a.Nutshell(2nd,2008.4)].Russell.Dyer.文字版.pdf](ed2k://|file|[MySQL.in.a.Nutshell(2nd,2008.4)].Russell.Dyer.文字版.pdf|2955428|235dda94fc0c807c02122b7d30581899|h=zhjimjbbj65jkocrtmb4jvphzfoqjan4|/)

[[MySQL.in.a.Nutshell(2nd,2008.4)].Russell.Dyer.文字版.epub](ed2k://|file|[MySQL.in.a.Nutshell(2nd,2008.4)].Russell.Dyer.文字版.epub|3069670|ee32f9d712e962f36bf0a38ebbf95c9b|h=t2sej7wlvetn5gre7cbimbqev4gov5vc|/)

[[Linux.Kernel.in.a.Nutshell(2006.12)].Greg.Kroah-Hartman.文字版.pdf](ed2k://|file|[Linux.Kernel.in.a.Nutshell(2006.12)].Greg.Kroah-Hartman.文字版.pdf|2366397|2c1447782d94b8070c2f0545fe96f6c5|h=a3frpcxcsot4huzza4cbuu5zpo3kdpsx|/)

[[Linux.Kernel.in.a.Nutshell(2006.12)].Greg.Kroah-Hartman.文字版.epub](ed2k://|file|[Linux.Kernel.in.a.Nutshell(2006.12)].Greg.Kroah-Hartman.文字版.epub|3750440|c43816be8cf079396b62b68bbfd2831f|h=7squqva25afahn5j3ssnfm4w2f3wcepm|/)

[[CPP.In.a.Nutshell(2003.5)].Ray.Lischner.文字版.pdf](ed2k://|file|[CPP.In.a.Nutshell(2003.5)].Ray.Lischner.文字版.pdf|4670463|b781aa89a54b3ef3ea53a7c13989cbca|h=zpncsn4hlm4ql6i7xwgmczthjyyaknke|/)

[[CPP.In.a.Nutshell(2003.5)].Ray.Lischner.文字版.epub](ed2k://|file|[CPP.In.a.Nutshell(2003.5)].Ray.Lischner.文字版.epub|3527807|4290d323251b3934684dd84fce3015be|h=4v2u3m2n2yaxocnfcjskyjrmnnuhnobc|/)

[[C.in.a.Nutshell(2005.12)].Peter.Prinz.文字版.pdf](ed2k://|file|[C.in.a.Nutshell(2005.12)].Peter.Prinz.文字版.pdf|7429843|4d9ffc6279301512f5c7ce64fef97dea|h=6dehbkjkxrde2vb7ktkqmumfhcw5hcpb|/)

[[C.in.a.Nutshell(2005.12)].Peter.Prinz.文字版.epub](ed2k://|file|[C.in.a.Nutshell(2005.12)].Peter.Prinz.文字版.epub|2974831|ee7bdddd33cc0f5e35d33ae64e1bc0ab|h=iz6hudo3ztz75dfwn76h6anrp4uv4pip|/)

[[Switching.to.the.Mac：The.Missing.Manual(Mountain.Lion.Edition,2012.9)].David.Pogue.文字版.pdf](ed2k://|file|[Switching.to.the.Mac：The.Missing.Manual(Mountain.Lion.Edition,2012.9)].David.Pogue.文字版.pdf|35238416|c1409d4f71790418a73206d40935fb2e|h=zztkmuo555xroz3c5zbs7wbb5jvl54ik|/)

[[Switching.to.the.Mac：The.Missing.Manual(Mountain.Lion.Edition,2012.9)].David.Pogue.文字版.epub](ed2k://|file|[Switching.to.the.Mac：The.Missing.Manual(Mountain.Lion.Edition,2012.9)].David.Pogue.文字版.epub|23788668|b1e430a9ef82e4a05a0f37b1e4ff7086|h=gkugscamankclqpyjx7jfck3zz2j4iio|/)

[[Photoshop.Elements.11：The.Missing.Manual(2012.9)].Barbara.Brundage.文字版.pdf](ed2k://|file|[Photoshop.Elements.11：The.Missing.Manual(2012.9)].Barbara.Brundage.文字版.pdf|57247377|a474c483da60818e0644cee99d1d69f2|h=b6ykdk7zpeehpo7tgeb6j2hhorifj4ee|/)

[[Photoshop.Elements.11：The.Missing.Manual(2012.9)].Barbara.Brundage.文字版.epub](ed2k://|file|[Photoshop.Elements.11：The.Missing.Manual(2012.9)].Barbara.Brundage.文字版.epub|20542355|1bbd2e31bbdad69c34492352cb809d19|h=dmah7xq6ayiu22mu4qovfqorsaao2mh5|/)

[[Adobe.Edge.Animate.Preview.7：The.Missing.Manual(2012.9)].Chris.Grover.文字版.pdf](ed2k://|file|[Adobe.Edge.Animate.Preview.7：The.Missing.Manual(2012.9)].Chris.Grover.文字版.pdf|19775962|924ee06db116ec06f848145412682d9e|h=zo5s7mffla24njknfjtnckbssv5ahd6h|/)

[[Adobe.Edge.Animate.Preview.7：The.Missing.Manual(2012.9)].Chris.Grover.文字版.epub](ed2k://|file|[Adobe.Edge.Animate.Preview.7：The.Missing.Manual(2012.9)].Chris.Grover.文字版.epub|8159068|50aa8c13883b6f1852842c66d4707892|h=vcex6ym4uohm6xmngsu6je2d25zmdjyv|/)

[[Selectors.Specificity.and.the.Cascade(2012.9)].Eric.A.Meyer.文字版.pdf](ed2k://|file|[Selectors.Specificity.and.the.Cascade(2012.9)].Eric.A.Meyer.文字版.pdf|8596563|f07be35bffb8b298b2f69f8aa89b526e|h=bj2jm5rkd7tro42ah6twxofpexvntsos|/)

[[Selectors.Specificity.and.the.Cascade(2012.9)].Eric.A.Meyer.文字版.epub](ed2k://|file|[Selectors.Specificity.and.the.Cascade(2012.9)].Eric.A.Meyer.文字版.epub|3329182|1f5104347cfca04b09c1b958a174702c|h=7xmclcmhxq5xhg674e4aksuk4gsgs7wr|/)

[[Programming.ASP.NET.MVC.4(2012.9)].Jess.Chadwick.文字版.pdf](ed2k://|file|[Programming.ASP.NET.MVC.4(2012.9)].Jess.Chadwick.文字版.pdf|13096454|901c8f104d9302e7e9db29054cd22d30|h=tmgl3phgoi4pbwkvonko3irutx32l7yo|/)

[[Programming.ASP.NET.MVC.4(2012.9)].Jess.Chadwick.文字版.epub](ed2k://|file|[Programming.ASP.NET.MVC.4(2012.9)].Jess.Chadwick.文字版.epub|6036904|1aaa9550ac1cd10cea9b929e28fc9343|h=vt3yegyjfbl3e32ixodl6autdpjnlwzj|/)

[[Juniper.MX.Series(2012.9)].Douglas.Richard.Hanks.Jr.文字版.pdf](ed2k://|file|[Juniper.MX.Series(2012.9)].Douglas.Richard.Hanks.Jr.文字版.pdf|47723408|2da46bf3e7bf23645dae1ff6ecb0a6d2|h=nlpd3tidaelhjdvhl7squ4umsby2uizk|/)

[[Juniper.MX.Series(2012.9)].Douglas.Richard.Hanks.Jr.文字版.epub](ed2k://|file|[Juniper.MX.Series(2012.9)].Douglas.Richard.Hanks.Jr.文字版.epub|20823946|53d8e56af4eb9722c658dfe5d3fdadea|h=6hrhcqtxb4kfchxhaeflm7a3mcawydtt|/)

[[Cloud.Architecture.Patterns(2012.9)].Bill.Wilder.文字版.pdf](ed2k://|file|[Cloud.Architecture.Patterns(2012.9)].Bill.Wilder.文字版.pdf|8564142|b9e1e22bed0e0662f7571449ebb37a27|h=fqjzjiiytozykokvh2x7auk23r5yhcfm|/)

[[Cloud.Architecture.Patterns(2012.9)].Bill.Wilder.文字版.epub](ed2k://|file|[Cloud.Architecture.Patterns(2012.9)].Bill.Wilder.文字版.epub|2524988|eddf5d7fe217a9fffee219c8e119cf8b|h=z4u6rk2maghpxld7scbi6tu4f4x245my|/)

[[Building.a.Windows.IT.Infrastructure.in.the.Cloud(2012.9)].David.K.Rensin.文字版.pdf](ed2k://|file|[Building.a.Windows.IT.Infrastructure.in.the.Cloud(2012.9)].David.K.Rensin.文字版.pdf|11653432|f10bf2f18f74d79921d904838dcdaff2|h=3lumpt2bomr5pvftxgdvkji66cd3qsl7|/)

[[Building.a.Windows.IT.Infrastructure.in.the.Cloud(2012.9)].David.K.Rensin.文字版.epub](ed2k://|file|[Building.a.Windows.IT.Infrastructure.in.the.Cloud(2012.9)].David.K.Rensin.文字版.epub|9279902|b8daf8503418f9d585281619527d538e|h=ixikpwd5eboe5efthoavr4yw74bwy7om|/)

[[Programming.Hive(2012.9)].Edward.Capriolo.文字版.pdf](ed2k://|file|[Programming.Hive(2012.9)].Edward.Capriolo.文字版.pdf|8192362|692d33d5dc795322b6b2a9ecb7d5fea6|h=snnmaipkc4mdujp5i4n72maswwofywjr|/)

[[Programming.Hive(2012.9)].Edward.Capriolo.文字版.epub](ed2k://|file|[Programming.Hive(2012.9)].Edward.Capriolo.文字版.epub|3266584|a143d57db7b083accdeabd6191e1a86b|h=avs5fgpfmplyxvy77rmwp4xmw7sld6jn|/)

[[Programming.Android(2nd,2012.9)].Zigurd.Mednieks.文字版.pdf](ed2k://|file|[Programming.Android(2nd,2012.9)].Zigurd.Mednieks.文字版.pdf|13885604|e583d3666cce7ee599719913ab9de05c|h=ddl33ao4sxohfvjvvmtzy6yk6uawhksh|/)

[[Programming.Android(2nd,2012.9)].Zigurd.Mednieks.文字版.epub](ed2k://|file|[Programming.Android(2nd,2012.9)].Zigurd.Mednieks.文字版.epub|6883388|e59df3f5941ab1164d9bce8301b12d51|h=567xa333dhnikeoinzbmjsgbneerknck|/)

[[Learning.Unix.for.OS.X.Mountain.Lion(2012.9)].Dave.Taylor.文字版.pdf](ed2k://|file|[Learning.Unix.for.OS.X.Mountain.Lion(2012.9)].Dave.Taylor.文字版.pdf|14096062|03bbed04c0e84392f8a1c89f8430a5f1|h=nqpsvofdngzhjiddbhfvhiglgs4xthiz|/)

[[Learning.Unix.for.OS.X.Mountain.Lion(2012.9)].Dave.Taylor.文字版.epub](ed2k://|file|[Learning.Unix.for.OS.X.Mountain.Lion(2012.9)].Dave.Taylor.文字版.epub|5496094|83fd9d8f6cd33e2b797aa652fc89ceaa|h=53lbbkejucstf6trwgf3roarduobx6tz|/)

[[Learning.Node(2012.8)].Shelley.Powers.文字版.pdf](ed2k://|file|[Learning.Node(2012.8)].Shelley.Powers.文字版.pdf|9737333|a6a44de1816f80a8d3695d44437768fd|h=vddkd4gmdst6ktqet3nnptmfuxyslweb|/)

[[Learning.Node(2012.8)].Shelley.Powers.文字版.epub](ed2k://|file|[Learning.Node(2012.8)].Shelley.Powers.文字版.epub|5064311|39d8a62bee4e8f1a7eeb0bd34b8144eb|h=iwlbswcahxagscs7pfmryzpyjqgxvhjq|/)

[[Ethics.of.Big.Data(2012.9)].Kord.Davis.文字版.pdf](ed2k://|file|[Ethics.of.Big.Data(2012.9)].Kord.Davis.文字版.pdf|9414978|0f81aa51e556d6780b5bd988738a164f|h=6tuqdnu7mafekgtfejqjhufkkqwgkui5|/)

[[Ethics.of.Big.Data(2012.9)].Kord.Davis.文字版.epub](ed2k://|file|[Ethics.of.Big.Data(2012.9)].Kord.Davis.文字版.epub|2971750|4a54aedce804f66df5b5265381112c1f|h=7y5d77gfnsq5q3p36lebmhhmxmtmabfd|/)

[[Enterprise.Games(2012.9)].Michael.Hugos.文字版.pdf](ed2k://|file|[Enterprise.Games(2012.9)].Michael.Hugos.文字版.pdf|19301494|b4ef037e3e797dada7f8cb25bdefa855|h=l23vuvwxwxidx6s45qgji4d22ohmiii5|/)

[[Enterprise.Games(2012.9)].Michael.Hugos.文字版.epub](ed2k://|file|[Enterprise.Games(2012.9)].Michael.Hugos.文字版.epub|8813253|81de1cd09778f8cd3760a6e0d67625c0|h=2sogbbm3fmte44la5mbzzn6jdjxb52tb|/)

[[Developing.with.Googleplus(2012.9)].Jennifer.Murphy.文字版.pdf](ed2k://|file|[Developing.with.Googleplus(2012.9)].Jennifer.Murphy.文字版.pdf|12107749|3603c54d76cea3e90e98aa2b9669f0ba|h=pqkzvd734jrqxgqkf6rfz3ryteffulls|/)

[[Developing.with.Googleplus(2012.9)].Jennifer.Murphy.文字版.epub](ed2k://|file|[Developing.with.Googleplus(2012.9)].Jennifer.Murphy.文字版.epub|9198761|1c2ca4203bf7d7ef6a8822f5f603ae05|h=shheoih7vzo5t5a2o2fdsixlcwkhunrw|/)

[[Async.in.CSharp.5.0(2012.9)].Alex.Davies.文字版.pdf](ed2k://|file|[Async.in.CSharp.5.0(2012.9)].Alex.Davies.文字版.pdf|5749107|fc51d7a0f32e0398376a869e3d0c8d8e|h=4peawcxdes63evlh2qqity6jb75xg4b5|/)

[[Async.in.CSharp.5.0(2012.9)].Alex.Davies.文字版.epub](ed2k://|file|[Async.in.CSharp.5.0(2012.9)].Alex.Davies.文字版.epub|2213927|e6504b206ddc4b3fdb69f2a0dd557ab8|h=5jlizvvvd272x5qq76zd25ndzrt3gyq6|/)

[[Civic.Apps.Competition.Handbook(2012.9)].Kate.Eyler-Werve.文字版.pdf](ed2k://|file|[Civic.Apps.Competition.Handbook(2012.9)].Kate.Eyler-Werve.文字版.pdf|6696837|941cdb5f080b3bbe854294028d24aa32|h=lrxnxbvbxseadrg2bah7nrykgci4s456|/)

[[Civic.Apps.Competition.Handbook(2012.9)].Kate.Eyler-Werve.文字版.epub](ed2k://|file|[Civic.Apps.Competition.Handbook(2012.9)].Kate.Eyler-Werve.文字版.epub|2264081|8fd5c2a9ce787f746e411d59a246be9b|h=xsvwobfalmhxfl6fcrn66rezch7e5m4a|/)

[[CSS.and.Documents(2012.9)].Eric.A.Meyer.文字版.pdf](ed2k://|file|[CSS.and.Documents(2012.9)].Eric.A.Meyer.文字版.pdf|1218856|d90dd978a6af9221df5ef16612418910|h=kfhshrr5jao4qo6liccxluuvrdkpk7lg|/)

[[CSS.and.Documents(2012.9)].Eric.A.Meyer.文字版.epub](ed2k://|file|[CSS.and.Documents(2012.9)].Eric.A.Meyer.文字版.epub|2369331|134df7a4c26c34d3688808d670ee6eb8|h=p4eustuoa6w7xfvsxcyvj7xgtm665nnn|/)

[[Java.EE.6.Pocket.Guide(2012.9)].Arun.Gupta.文字版.pdf](ed2k://|file|[Java.EE.6.Pocket.Guide(2012.9)].Arun.Gupta.文字版.pdf|4248509|92eeef6a0a85da575ea79d78c4bb9327|h=fgxshr2yuzz4mrzzvvio5ll7fqll2gsu|/)

[[Java.EE.6.Pocket.Guide(2012.9)].Arun.Gupta.文字版.epub](ed2k://|file|[Java.EE.6.Pocket.Guide(2012.9)].Arun.Gupta.文字版.epub|2241507|c15c1a67fd3e371dd431fe33ce5104ad|h=ab5llbfx656ipsm2cyh26zbkj7ptglfw|/)

[[Values.Units.and.Colors(2012.9)].Eric.A.Meyer.文字版.pdf](ed2k://|file|[Values.Units.and.Colors(2012.9)].Eric.A.Meyer.文字版.pdf|7651393|fcf57d6a9cb5b8f86775db5ee53592d7|h=3cebw6kpt6kmtli47av3b4b33bxmtgnz|/)

[[Values.Units.and.Colors(2012.9)].Eric.A.Meyer.文字版.epub](ed2k://|file|[Values.Units.and.Colors(2012.9)].Eric.A.Meyer.文字版.epub|2397658|68e47955b42dd0cc74860fff5fc153c8|h=bctootadenont6dnyioacugvggpcihut|/)

[[Unix.in.a.Nutshell(4th,2005.10)].Arnold.Robbins.文字版.pdf](ed2k://|file|[Unix.in.a.Nutshell(4th,2005.10)].Arnold.Robbins.文字版.pdf|5315164|bf388915a7ea897bcc5b8491ec63beba|h=wf5frnjevd2yhufr4pu6r6idfy4ppgo4|/)

[[Unix.in.a.Nutshell(4th,2005.10)].Arnold.Robbins.文字版.epub](ed2k://|file|[Unix.in.a.Nutshell(4th,2005.10)].Arnold.Robbins.文字版.epub|2383740|3f4fe306bbe7cd482b7e583f5aa5d195|h=tr6dz776iajuurs425ocssbos3u32gpx|/)

[[LPI.Linux.Certification.in.a.Nutshell(3rd,2010.6)].Adam.Haeder.文字版.pdf](ed2k://|file|[LPI.Linux.Certification.in.a.Nutshell(3rd,2010.6)].Adam.Haeder.文字版.pdf|9078638|b698782b19aa8926c1f2be757420c389|h=hsdmeykrz5pfgmdy32ffltm63lzjj2qa|/)

[[LPI.Linux.Certification.in.a.Nutshell(3rd,2010.6)].Adam.Haeder.文字版.epub](ed2k://|file|[LPI.Linux.Certification.in.a.Nutshell(3rd,2010.6)].Adam.Haeder.文字版.epub|3149203|587034e5fa704a4bcf8afd4b7aba253e|h=rjh2fo675rc7bo7yzuthwjogxelbjo73|/)

[[Exploring.Expect(1994.12)].Don.Libes.文字版.pdf](ed2k://|file|[Exploring.Expect(1994.12)].Don.Libes.文字版.pdf|18840178|249ee78bb972682c102cf73205e6fc45|h=57zwnqtr2fbyraaawc54sk3ij2q7jzys|/)

[[Exploring.Expect(1994.12)].Don.Libes.文字版.epub](ed2k://|file|[Exploring.Expect(1994.12)].Don.Libes.文字版.epub|3032527|6bb99c11de68caa7c6b34effd5a57acc|h=7bxm7v5xktthcxa7a4wdgkmjjdssvwim|/)

[[Essential.System.Administration(3rd,2002.8)].Æleen.Frisch.文字版.pdf](ed2k://|file|[Essential.System.Administration(3rd,2002.8)].Æleen.Frisch.文字版.pdf|10471488|b5e82d6e16d9ea1095919becc4b89eca|h=bmlg4g2nk4fin4nigc2sdxvkgs63aj6i|/)

[[Essential.System.Administration(3rd,2002.8)].Æleen.Frisch.文字版.epub](ed2k://|file|[Essential.System.Administration(3rd,2002.8)].Æleen.Frisch.文字版.epub|5280451|39ddc07ba384c724faf54e834096115c|h=77z5kjdnkrhpargfoqpyyklyngjcqyv2|/)

[[Building.Embedded.Linux.Systems(2nd,2008.8)].Karim.Yaghmour.文字版.pdf](ed2k://|file|[Building.Embedded.Linux.Systems(2nd,2008.8)].Karim.Yaghmour.文字版.pdf|4671320|50b2f3cd3c4805abccdd658ae8ea25ca|h=ixvfefglqyl7m6ss3mdtp4a4abnlr2m7|/)

[[Building.Embedded.Linux.Systems(2nd,2008.8)].Karim.Yaghmour.文字版.epub](ed2k://|file|[Building.Embedded.Linux.Systems(2nd,2008.8)].Karim.Yaghmour.文字版.epub|5156686|04d4120b88d49038378c30b514b331e4|h=7xou6d5j5uc34aog5mmfb2k2qhgychlu|/)

[[Using.Samba(3rd,2007.1)].Gerald.Carter.文字版.pdf](ed2k://|file|[Using.Samba(3rd,2007.1)].Gerald.Carter.文字版.pdf|4710633|744612e74a557249d8d61c5a948f220e|h=q3cisbhm7m4zhfe2a6t52lhijkodyaat|/)

[[Understanding.the.Linux.Kernel(3rd,2005.11)].Daniel.P.Bovet.文字版.pdf](ed2k://|file|[Understanding.the.Linux.Kernel(3rd,2005.11)].Daniel.P.Bovet.文字版.pdf|5816835|3a7a487e2ade072f6c8bd264b6d37f77|h=y6dcofzfjbwbu4hwt4ewhrlvhewv3din|/)

[[Understanding.the.Linux.Kernel(3rd,2005.11)].Daniel.P.Bovet.文字版.epub](ed2k://|file|[Understanding.the.Linux.Kernel(3rd,2005.11)].Daniel.P.Bovet.文字版.epub|4436804|0207ead9c59e64f86ad68c5e5d8f8355|h=3lq2ex5dehdt5hnlewnopzags7pvimhh|/)

[[Understanding.Linux.Network.Internals(2005.12)].Christian.Benvenuti.文字版.pdf](ed2k://|file|[Understanding.Linux.Network.Internals(2005.12)].Christian.Benvenuti.文字版.pdf|10365350|4916e1fe273e17566cf91700c86171bb|h=re7xht6skeebuwo5tc6cgml4hsyezqot|/)

[[Understanding.Linux.Network.Internals(2005.12)].Christian.Benvenuti.文字版.epub](ed2k://|file|[Understanding.Linux.Network.Internals(2005.12)].Christian.Benvenuti.文字版.epub|15891961|36765173d3d360a83ae2554c6903a1da|h=5bzygjlnmpthlseqrlu3xeuhease5fqy|/)

[[Running.Linux(5th,2005.12)].Matthias.Kalle.Dalheimer.文字版.pdf](ed2k://|file|[Running.Linux(5th,2005.12)].Matthias.Kalle.Dalheimer.文字版.pdf|12155836|e74ac8a95e220796f4804c11a7e37314|h=verqroagdqukpje43zzmkxtvxzwyhzpv|/)

[[Running.Linux(5th,2005.12)].Matthias.Kalle.Dalheimer.文字版.epub](ed2k://|file|[Running.Linux(5th,2005.12)].Matthias.Kalle.Dalheimer.文字版.epub|13619895|95cc7b9aa1f5bd94f953a05de2304c51|h=2x6f45bavzoyqwdnsuitv5c2i6bbt42v|/)

[[Python.in.a.Nutshell(2nd,2006.7)].Alex.Martelli.文字版.pdf](ed2k://|file|[Python.in.a.Nutshell(2nd,2006.7)].Alex.Martelli.文字版.pdf|10276535|227d984113794ddc97e57a11cfea630c|h=sexwizv3rdhww4zizo2fm3eftyyqnhle|/)

[[Python.in.a.Nutshell(2nd,2006.7)].Alex.Martelli.文字版.epub](ed2k://|file|[Python.in.a.Nutshell(2nd,2006.7)].Alex.Martelli.文字版.epub|4147364|9a721adb8c3938155c7a009a7fed064d|h=w763nulyr4dv3bfdbud4fbqb4qclvkhb|/)

[[Python.Cookbook(2nd,2005.5)].Alex.Martelli.文字版.pdf](ed2k://|file|[Python.Cookbook(2nd,2005.5)].Alex.Martelli.文字版.pdf|4399300|f9d370b6071c82492bbc9a1f84db065e|h=c7qqz5bpwbeuknz6i3iu3vj4vxqlyfy4|/)

[[Python.Cookbook(2nd,2005.5)].Alex.Martelli.文字版.epub](ed2k://|file|[Python.Cookbook(2nd,2005.5)].Alex.Martelli.文字版.epub|2863712|f319b5d1f0390fdbf28cc490768a5e36|h=3imo537hr5bnyebkcicszkpvik6a4erf|/)

[[Postfix：The.Definitive.Guide(2003.12)].Kyle.D.Dent.文字版.pdf](ed2k://|file|[Postfix：The.Definitive.Guide(2003.12)].Kyle.D.Dent.文字版.pdf|8324401|dcc7780283574ff2c22bfa48d77f25c5|h=jeffcvxtkdo4lpgdjsrut3tebuk5wjr7|/)

[[Postfix：The.Definitive.Guide(2003.12)].Kyle.D.Dent.文字版.epub](ed2k://|file|[Postfix：The.Definitive.Guide(2003.12)].Kyle.D.Dent.文字版.epub|2614203|170714122fae28683c28a2757e52f81f|h=m4ohe5i7go3o3p6fysgg7fw4d5d6fpaj|/)

[[Linux.System.Programming(2007.9)].Robert.Love.文字版.pdf](ed2k://|file|[Linux.System.Programming(2007.9)].Robert.Love.文字版.pdf|2619048|d14688d785e40dd498c76ac310716750|h=dnx6nokrwkm7lfbbwzznboc53wmf7imd|/)

[[Linux.System.Programming(2007.9)].Robert.Love.文字版.epub](ed2k://|file|[Linux.System.Programming(2007.9)].Robert.Love.文字版.epub|2454700|9a9cc3be14aa4ae7744151f91d825fc9|h=vr2mdog6vfa3duk4p62qdzakl5f3ettu|/)

[[Linux.Networking.Cookbook(2007.11)].Carla.Schroder.文字版.pdf](ed2k://|file|[Linux.Networking.Cookbook(2007.11)].Carla.Schroder.文字版.pdf|4905317|10454c2da12befcab95d064c1dc820e1|h=y4ggvr4qesepjwqa6j3be3wse4kkyp4a|/)

[[Linux.Networking.Cookbook(2007.11)].Carla.Schroder.文字版.epub](ed2k://|file|[Linux.Networking.Cookbook(2007.11)].Carla.Schroder.文字版.epub|5871973|35fb2e5ce73aef6a2e75d3e36e8ae110|h=lwek5ncxukzyzjflca5ev5wj77o6ngxb|/)

[[Linux.Network.Administrators.Guide(3rd,2005.2)].Tony.Bautts.文字版.pdf](ed2k://|file|[Linux.Network.Administrators.Guide(3rd,2005.2)].Tony.Bautts.文字版.pdf|3562070|87fa01ff6ae929f61253007ad97be19c|h=ckycqz7gzdz7b76ktrlmxeedtnw767w2|/)

[[Linux.Device.Drivers(3rd,2005.2)].Jonathan.Corbet.文字版.pdf](ed2k://|file|[Linux.Device.Drivers(3rd,2005.2)].Jonathan.Corbet.文字版.pdf|5979404|1069d21d449c9371f49bc4e6500051f5|h=zyowtcs5ytxxhoqahycorte33f2nnzzm|/)

[[Linux.Device.Drivers(3rd,2005.2)].Jonathan.Corbet.文字版.epub](ed2k://|file|[Linux.Device.Drivers(3rd,2005.2)].Jonathan.Corbet.文字版.epub|2908018|84dcb2582fb6f7dfbcd2c717173c16ee|h=pelzgox22ozcn2oalz56n32vrxtahb3d|/)

[[Spring.Integration.in.Action(2012.9)].Mark.Fisher.文字版.pdf](ed2k://|file|[Spring.Integration.in.Action(2012.9)].Mark.Fisher.文字版.pdf|12649232|b1491ed017f03ab5ee4a734995d9ca4e|h=pp4qhzeccoqvkeyx6mbtwkk2fuyjcauf|/)

[[Programming.the.TI-83.Plus.TI-84.Plus(2012.9)].Christopher.R.Mitchell.文字版.pdf](ed2k://|file|[Programming.the.TI-83.Plus.TI-84.Plus(2012.9)].Christopher.R.Mitchell.文字版.pdf|13489370|fe925f1d186c24dc881590f0f625758a|h=5wmxqudyxeua7t2yzuehl2ggnds3ydtf|/)

[[SOA.Patterns(2012.9)].Arnon.Rotem-Gal-Oz.文字版.pdf](ed2k://|file|[SOA.Patterns(2012.9)].Arnon.Rotem-Gal-Oz.文字版.pdf|13268406|a15d6f91e8780bcb5d4415145f016b8a|h=miul2esoipwyx2wnarvzrsnsd5c4lnin|/)

[[Windows.Phone.7.in.Action(2012.9)].Timothy.Binkley-Jones.文字版.pdf](ed2k://|file|[Windows.Phone.7.in.Action(2012.9)].Timothy.Binkley-Jones.文字版.pdf|20394564|d4e8e2e13f485b24efb6661b3476ad09|h=mtoanwfxi7sfyjxn2kfajhzuskcateod|/)

[[Start.Here!Learn.the.Kinect.API(2012.6)].Rob.Miles.文字版.pdf](ed2k://|file|[Start.Here!Learn.the.Kinect.API(2012.6)].Rob.Miles.文字版.pdf|19029631|b73daae6302c383b600b944c10ae8b0e|h=ycbaflstqnijvvhofmpvyvsoxt2oexgh|/)

[[Start.Here!Learn.the.Kinect.API(2012.6)].Rob.Miles.文字版.epub](ed2k://|file|[Start.Here!Learn.the.Kinect.API(2012.6)].Rob.Miles.文字版.epub|8083855|d3f204fbd24c01b27afc6d7d6dcde9e5|h=znom4n5d3kpy5ijc5ncnk5m5mhepfigy|/)

[[Visual.Models.for.Software.Requirements(2012.7)].Joy.Beatty.文字版.pdf](ed2k://|file|[Visual.Models.for.Software.Requirements(2012.7)].Joy.Beatty.文字版.pdf|47611618|e68d9c7dd3e9661e05bbda74ea4e5fbc|h=vg6lvx4fnimq3fe5iiodkced6uhrunbg|/)

[[Visual.Models.for.Software.Requirements(2012.7)].Joy.Beatty.文字版.epub](ed2k://|file|[Visual.Models.for.Software.Requirements(2012.7)].Joy.Beatty.文字版.epub|13397029|d127bfbe8a7c3effb183d0e966ca34bf|h=uaazh64iznrpxqc6qo7bt52v5kef65vv|/)

[[Windows编程(第6版,Release.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.pdf](ed2k://|file|[Windows编程(第6版,Release.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.pdf|11279373|708738ab3212f2346c98b0e1670eb2f5|h=ukzcujfajf6q2qcvjkh3ryurpa372qs4|/)

[[Windows编程(第6版,Release.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.epub](ed2k://|file|[Windows编程(第6版,Release.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.epub|3654926|17a44ae7f3fd26d92a138cf0000e59dd|h=guiamzace37pk6jbj3d3x3gpttf7uor6|/)

[[Windows编程(第6版,Consumer.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.pdf](ed2k://|file|[Windows编程(第6版,Consumer.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.pdf|5271160|2854518758eb12352ceba534bb02b57e|h=zbleawiae32twtozlzwqnjia4w3my4jm|/)

[[Windows编程(第6版,Consumer.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.epub](ed2k://|file|[Windows编程(第6版,Consumer.Preview.eBook)].(Programming.Windows).Charles.Petzold.文字版.epub|3663029|21c4f8cc905cab75415daeb094943b1a|h=qemjps6chvxossqxdwmhkfv3ysivbp6b|/)

[[Start.Here!Learn.JavaScript(2012.8)].Steve.Suehring.文字版.pdf](ed2k://|file|[Start.Here!Learn.JavaScript(2012.8)].Steve.Suehring.文字版.pdf|20159415|a6ad765631e1df3f02ad9e4ba7722863|h=acejt7yltxw4zp6j2at44zafpjt7chmf|/)

[[Start.Here!Learn.JavaScript(2012.8)].Steve.Suehring.文字版.epub](ed2k://|file|[Start.Here!Learn.JavaScript(2012.8)].Steve.Suehring.文字版.epub|4493155|54f544a4068d924fdce1d5dd83ab84db|h=li7yki66wpiktmoygvfwjvdcarfqssev|/)

[[The.Connected.Company(2012.8)].Dave.Gray.文字版.pdf](ed2k://|file|[The.Connected.Company(2012.8)].Dave.Gray.文字版.pdf|7886727|c7219ebe06abb57bbe267efdc99e58d5|h=soqjes2tddw56fsagt7vbn4rn34tdt6o|/)

[[The.Connected.Company(2012.8)].Dave.Gray.文字版.epub](ed2k://|file|[The.Connected.Company(2012.8)].Dave.Gray.文字版.epub|5491018|c97ba9e6a42d1f9e9bec7ddd428119f7|h=y5saubjymt42hfsp6rkqtidqxwacoj7z|/)

[[Surviving.Orbit.the.DIY.Way(2012.8)].Sandy.Antunes.文字版.pdf](ed2k://|file|[Surviving.Orbit.the.DIY.Way(2012.8)].Sandy.Antunes.文字版.pdf|43479779|e19a0aa92789d7cf131e8ca8c84e2a7e|h=6coa7jy7lzrvpuwnnh5xmd7hgeasszoc|/)

[[Surviving.Orbit.the.DIY.Way(2012.8)].Sandy.Antunes.文字版.epub](ed2k://|file|[Surviving.Orbit.the.DIY.Way(2012.8)].Sandy.Antunes.文字版.epub|5134763|4e89dfc8642527f00b393f7db79d05c8|h=xqvnjtimsyombnxicz6dc7b2u6xeoqrb|/)

[[Shipping.Greatness(2012.8)].Chris.Vander.Mey.文字版.pdf](ed2k://|file|[Shipping.Greatness(2012.8)].Chris.Vander.Mey.文字版.pdf|14179563|68b191e5bc833e7a1ca0c607e3be6fb2|h=iaqpeodz644uepwyetyzbdd4kukr7w7y|/)

[[Shipping.Greatness(2012.8)].Chris.Vander.Mey.文字版.epub](ed2k://|file|[Shipping.Greatness(2012.8)].Chris.Vander.Mey.文字版.epub|4250882|229fac42aef929b59ba2d8c7e5c8dfe5|h=35df3wvd7rajzpgg6yvdoth4pvx7xqlz|/)

[[Illustrated.Guide.to.Home.Forensic.Science.Experiments(2012.8)].Robert.Bruce.Thompson.文字版.pdf](ed2k://|file|[Illustrated.Guide.to.Home.Forensic.Science.Experiments(2012.8)].Robert.Bruce.Thompson.文字版.pdf|19766181|11e37b76032cb4b45cf47fb820a22e24|h=zt7octtd5x3u6itdwh3peigp7pyuwqhb|/)

[[Illustrated.Guide.to.Home.Forensic.Science.Experiments(2012.8)].Robert.Bruce.Thompson.文字版.epub](ed2k://|file|[Illustrated.Guide.to.Home.Forensic.Science.Experiments(2012.8)].Robert.Bruce.Thompson.文字版.epub|10550291|8980395b92c35cd66beaa77f9b3aa22d|h=xiyffjmlek654ubihxxlaiviwalmncgp|/)

[[Getting.Started.with.Storm(2012.8)].Jonathan.Leibiusky.文字版.pdf](ed2k://|file|[Getting.Started.with.Storm(2012.8)].Jonathan.Leibiusky.文字版.pdf|5764942|d974cb6409ad6868029907500bd37d03|h=w4ymhdvp52q5rdgdfel75cc5zmbcuhbr|/)

[[Getting.Started.with.Storm(2012.8)].Jonathan.Leibiusky.文字版.epub](ed2k://|file|[Getting.Started.with.Storm(2012.8)].Jonathan.Leibiusky.文字版.epub|2476037|4d98b1d3a41e0b1a3f6baa502635eb2f|h=m6ogvwm7gga2syreenltq3a5gajsjta2|/)

[[Accessibility.Handbook(2012.8)].Katie.Cunningham.文字版.pdf](ed2k://|file|[Accessibility.Handbook(2012.8)].Katie.Cunningham.文字版.pdf|11953467|50ada45a9b6ceb7603479230aabf1f63|h=rwzpqgvpztnd7ltnuaz4p64ln3zjw2bx|/)

[[Accessibility.Handbook(2012.8)].Katie.Cunningham.文字版.epub](ed2k://|file|[Accessibility.Handbook(2012.8)].Katie.Cunningham.文字版.epub|3940240|268dc3e6b3a27d9c56b6c9be4a024dbe|h=dl2kezen7toeyl74vmnloguswkn7o6lt|/)

[[Head.First.HTML.and.CSS(2nd,2012.8)].Elisabeth.Robson.文字版.pdf](ed2k://|file|[Head.First.HTML.and.CSS(2nd,2012.8)].Elisabeth.Robson.文字版.pdf|103077459|b34475e2e15ed773ebf4d85bc44e7832|h=nv7bzv72wgwbjtsrvjqy3ovx5dsga2b6|/)

[[Version.Control.with.Git(2nd,2012.8)].Jon.Loeliger.文字版.pdf](ed2k://|file|[Version.Control.with.Git(2nd,2012.8)].Jon.Loeliger.文字版.pdf|16234058|7bdfde460c096728c3b3e322aa4509a4|h=uqoy2me2et4jdzytf755gti7ukwetx4k|/)

[[Version.Control.with.Git(2nd,2012.8)].Jon.Loeliger.文字版.epub](ed2k://|file|[Version.Control.with.Git(2nd,2012.8)].Jon.Loeliger.文字版.epub|7776056|9d6f4d8725ce023f517044cd8e05a259|h=5i26bao23nhuelocmftavdx7qfjmqwpp|/)

[[Learning.Web.Design(4th,2012.8)].Jennifer.Niederst.Robbins.文字版.pdf](ed2k://|file|[Learning.Web.Design(4th,2012.8)].Jennifer.Niederst.Robbins.文字版.pdf|58309317|793b6c893119bc26f2d71e9f524a86ef|h=enmnncy7ybgrexivdlr466lwk4iyrnh3|/)

[[Learning.Web.Design(4th,2012.8)].Jennifer.Niederst.Robbins.文字版.epub](ed2k://|file|[Learning.Web.Design(4th,2012.8)].Jennifer.Niederst.Robbins.文字版.epub|30023525|12b33ab22e93e7c210087bb78775dc79|h=5luunmz7ioenrk43ixzc7gefcx2dgca4|/)

[[Think.Python(2012.8)].Allen.B.Downey.文字版.pdf](ed2k://|file|[Think.Python(2012.8)].Allen.B.Downey.文字版.pdf|11376331|88652c8ca5e048ee3e41cbda6b48325d|h=ushuzhkuynbq7x7i5olfsrzkeo2i2co4|/)

[[Think.Python(2012.8)].Allen.B.Downey.文字版.epub](ed2k://|file|[Think.Python(2012.8)].Allen.B.Downey.文字版.epub|2898450|1c7e79bea000c63fdfd62b888a7b31e7|h=lyqeos252rbjhxb6446dsy554ylxn76g|/)

[[Learning.PHP.MySQL.JavaScript.and.CSS(2nd,2012.8)].Robin.Nixon.文字版.pdf](ed2k://|file|[Learning.PHP.MySQL.JavaScript.and.CSS(2nd,2012.8)].Robin.Nixon.文字版.pdf|17475607|6d816e7c96373329ff2962ee9842dca7|h=pzl2aprj67jgpywxnqxmpo2xjauxy4ju|/)

[[Learning.PHP.MySQL.JavaScript.and.CSS(2nd,2012.8)].Robin.Nixon.文字版.epub](ed2k://|file|[Learning.PHP.MySQL.JavaScript.and.CSS(2nd,2012.8)].Robin.Nixon.文字版.epub|9572773|6d3f2be35168c30fa1fa12c67674d99f|h=u7buipdq32vviupyo7j5dzczxc55rc2m|/)

[[WebGL：Up.and.Running(2012.8)].Tony.Parisi.文字版.pdf](ed2k://|file|[WebGL：Up.and.Running(2012.8)].Tony.Parisi.文字版.pdf|26293644|5912bf316fb5e774c30e2aeb838e8cc2|h=6nabq3w3gw7y52wd7kcc2rcics3d77v7|/)

[[WebGL：Up.and.Running(2012.8)].Tony.Parisi.文字版.epub](ed2k://|file|[WebGL：Up.and.Running(2012.8)].Tony.Parisi.文字版.epub|5640129|a1e87b9c376767c85184b04d35c29013|h=wwsu76eqnhmvattabc4uikvmnkafe2i6|/)

[[Regular.Expressions.Cookbook(2nd,2012,8)].Jan.Goyvaerts.文字版.pdf](ed2k://|file|[Regular.Expressions.Cookbook(2nd,2012,8)].Jan.Goyvaerts.文字版.pdf|10973566|a77e75223123ce9cdb5f0c867aa62bb9|h=kc33pvqerxcf4gwz76uqubrpvddqjfgl|/)

[[Regular.Expressions.Cookbook(2nd,2012,8)].Jan.Goyvaerts.文字版.epub](ed2k://|file|[Regular.Expressions.Cookbook(2nd,2012,8)].Jan.Goyvaerts.文字版.epub|5297571|ac1257b3df0f5224e67a23a4e78fa5dd|h=4l2mm25h2pfakt5r23zzypxiijnxqri5|/)

[[The.Well-Grounded.Java.Developer(2012.7)].Benjamin.J.Evans.文字版.pdf](ed2k://|file|[The.Well-Grounded.Java.Developer(2012.7)].Benjamin.J.Evans.文字版.pdf|18441126|216512629bd53a072931998525d34cca|h=ma2x2tyggir3w7cvozmm4oe4rkj6ueiq|/)

[[The.Well-Grounded.Java.Developer(2012.7)].Benjamin.J.Evans.文字版.epub](ed2k://|file|[The.Well-Grounded.Java.Developer(2012.7)].Benjamin.J.Evans.文字版.epub|6867433|a77d08557946c091cb22a84373ee9b86|h=3k2h3y32rrqrry5drvsoq6d7da2ajfwi|/)

[[SOA.Governance.in.Action(2012.7)].Jos.Dirksen.文字版.pdf](ed2k://|file|[SOA.Governance.in.Action(2012.7)].Jos.Dirksen.文字版.pdf|22951960|516209798d9ee509232ed3aea3ad72fd|h=pzjnbrj4yxhr6pqwa5wuvp5bq4whjv6z|/)

[[SOA.Governance.in.Action(2012.7)].Jos.Dirksen.文字版.epub](ed2k://|file|[SOA.Governance.in.Action(2012.7)].Jos.Dirksen.文字版.epub|10539417|428b918ac08886514bd58a92df358d35|h=7vyx35d4ywpghv7ppyknq7ua5hukfu2h|/)

[[Silverlight.5.in.Action(2012.6)].Pete.Brown.文字版.pdf](ed2k://|file|[Silverlight.5.in.Action(2012.6)].Pete.Brown.文字版.pdf|35506546|21d720d00c835b4a20cc1806a3bf9e76|h=bj2uyl6mqscboxsfwakomw77whxvwbnt|/)

[[Silverlight.5.in.Action(2012.6)].Pete.Brown.文字版.epub](ed2k://|file|[Silverlight.5.in.Action(2012.6)].Pete.Brown.文字版.epub|28843964|932e54d16ea14248ea6040909bfd4b51|h=v4vuzfeg2znlhve7jn3ohq3ybwks2j3e|/)

[[Activiti.in.Action(2012.7)].Tijs.Rademakers.文字版.pdf](ed2k://|file|[Activiti.in.Action(2012.7)].Tijs.Rademakers.文字版.pdf|18008269|34d4c587cddc5bb15547dc1b13bedd88|h=4lisjjgm4edqmkscy2yjseebgklqxznm|/)

[[Activiti.in.Action(2012.7)].Tijs.Rademakers.文字版.epub](ed2k://|file|[Activiti.in.Action(2012.7)].Tijs.Rademakers.文字版.epub|15008291|ec6390b2e242bd718cd453caac80754d|h=fmnya4r35d33e7r37yrrjbrzzleqbd4v|/)

[[Brownfield.Application.Development.in.NET(2010.4)].Kyle.Baley.文字版.pdf](ed2k://|file|[Brownfield.Application.Development.in.NET(2010.4)].Kyle.Baley.文字版.pdf|8873692|b06b06ecb4696aca0d5683d4d6c177ea|h=fpmzmdtlmpvar4i6fbr7schoxm4fetjs|/)

[[Brownfield.Application.Development.in.NET(2010.4)].Kyle.Baley.文字版.epub](ed2k://|file|[Brownfield.Application.Development.in.NET(2010.4)].Kyle.Baley.文字版.epub|3529169|498477f087a5269f41909514a9e7179e|h=mdyjhc6on4p2gpbnxlyyt2z7s2u2zyg2|/)

[[Scala.in.Depth(2012.5)].Joshua.D.Suereth.文字版.pdf](ed2k://|file|[Scala.in.Depth(2012.5)].Joshua.D.Suereth.文字版.pdf|7635381|85c78da9aa3bc1484c442f4c48244637|h=b75pb42rc7kwdlehkk7krpusbqc6o4ch|/)

[[Scala.in.Depth(2012.5)].Joshua.D.Suereth.文字版.epub](ed2k://|file|[Scala.in.Depth(2012.5)].Joshua.D.Suereth.文字版.epub|3750489|8c46a058a8138da9d94798358fc04409|h=3bnpzjnlho2zgznx7awcfteczy6isqua|/)

[[Griffon.in.Action(2012.6)].Andres.Almiray.文字版.pdf](ed2k://|file|[Griffon.in.Action(2012.6)].Andres.Almiray.文字版.pdf|17582953|8296ad0cab5e90eecc1e09352485c6ca|h=bw7bxcp7xusxqpo67jvddkwpdpz2vkug|/)

[[Flex.Mobile.in.Action(2012.5)].Jonathan.Campos.文字版.pdf](ed2k://|file|[Flex.Mobile.in.Action(2012.5)].Jonathan.Campos.文字版.pdf|16918309|b0cd25cb5b6267367f8304eaa675f910|h=fwabjd3bpq3sl2nlgmaabbgdxbqqj65k|/)

[[ASP.NET.MVC.4.in.Action(2012.6)].Jeffrey.Palermo.文字版.pdf](ed2k://|file|[ASP.NET.MVC.4.in.Action(2012.6)].Jeffrey.Palermo.文字版.pdf|11408307|9c455b7061eecc883faab6ba42c69956|h=m6gknmfjp5lzi5vgtlmeoto3rvv225ns|/)

[[RabbitMQ.in.Action(2012.4)].Alvaro.Videla.文字版.pdf](ed2k://|file|[RabbitMQ.in.Action(2012.4)].Alvaro.Videla.文字版.pdf|8362849|a884657c61d19c2abe68059e6703657f|h=i5y64zs5kai73prabckzeqbsd27utron|/)

[[RabbitMQ.in.Action(2012.4)].Alvaro.Videla.文字版.epub](ed2k://|file|[RabbitMQ.in.Action(2012.4)].Alvaro.Videla.文字版.epub|6647058|008b8935c0d75a78734bedc563145048|h=56tv6ulzhj4jqq5yp6yhsgkxeenvlalt|/)

[[PowerShell.and.WMI(2012.4)].Richard.Siddaway.文字版.pdf](ed2k://|file|[PowerShell.and.WMI(2012.4)].Richard.Siddaway.文字版.pdf|15584861|8ce4df81f7cf96da404967129b2dab28|h=fr7ufnhlmshgbxd7hgojgu7nr5lrzn62|/)

[[PowerShell.and.WMI(2012.4)].Richard.Siddaway.文字版.epub](ed2k://|file|[PowerShell.and.WMI(2012.4)].Richard.Siddaway.文字版.epub|9007349|79072a73a23812ee9be1e71003bffe15|h=lbpxkgtfa6rhilcou6iiujrxdjwn4z5t|/)

[[MacRuby.in.Action(2012.4)].Brendan.G.Lim.文字版.pdf](ed2k://|file|[MacRuby.in.Action(2012.4)].Brendan.G.Lim.文字版.pdf|19643746|89b766d13d88920802829c65cdb2659e|h=qkmifq3f4et3c6tsqwziehahz47bzsgu|/)

[[MacRuby.in.Action(2012.4)].Brendan.G.Lim.文字版.epub](ed2k://|file|[MacRuby.in.Action(2012.4)].Brendan.G.Lim.文字版.epub|7286924|ec9f8db13a888380016c7a413bd5c2df|h=sbsisy4a6a5wrnyhmqrrxueihcjnfqxu|/)

[[Spring.Roo.in.Action(2012.4)].Ken.Rimple.文字版.pdf](ed2k://|file|[Spring.Roo.in.Action(2012.4)].Ken.Rimple.文字版.pdf|16578273|5e8eff2c612827c9a283759112e8d71f|h=m7a5yggvpypzikucaoy7v6rchisyetbl|/)

[[SharePoint.2010.Site.Owner's.Manual(2012.2)].Yvonne.M.Harryman.文字版.pdf](ed2k://|file|[SharePoint.2010.Site.Owner's.Manual(2012.2)].Yvonne.M.Harryman.文字版.pdf|26941568|77201edb6e9917286b5e358680b72290|h=caqncbaiz3hv5fpmxqk56zhwnlfrrgzz|/)

[[Hello!Python(2012.2)].Anthony.Briggs.文字版.pdf](ed2k://|file|[Hello!Python(2012.2)].Anthony.Briggs.文字版.pdf|29499367|4c51fb76a6f658ba1d5b12f6a956bc19|h=klp45em5psuxcbwsciiojx7b6mgxswag|/)

[[Hello!Python(2012.2)].Anthony.Briggs.文字版.epub](ed2k://|file|[Hello!Python(2012.2)].Anthony.Briggs.文字版.epub|18206053|cb2391b00ae3ba8eee76a618b38a3ee8|h=4iqa2ipl6waximucc4zqsyf5oaev54x6|/)

[[CPP.Concurrency.in.Action(2012.2)].Anthony.Williams.文字版.pdf](ed2k://|file|[CPP.Concurrency.in.Action(2012.2)].Anthony.Williams.文字版.pdf|6490945|bb2bdfdb8c527d40168f60760db01339|h=qqy2cdj5ojcwcubxjbch4qhjc2frfumt|/)

[[CPP.Concurrency.in.Action(2012.2)].Anthony.Williams.文字版.epub](ed2k://|file|[CPP.Concurrency.in.Action(2012.2)].Anthony.Williams.文字版.epub|6906578|4372a0183bd0c3ee0e2f3069180cad67|h=o32rok3ad7zgyyqljym6helqqm3pkelh|/)

[[Machine.Learning.in.Action(2012.3)].Peter.Harrington.文字版.pdf](ed2k://|file|[Machine.Learning.in.Action(2012.3)].Peter.Harrington.文字版.pdf|8920138|aab4fc8267ba3f480deddca29eebdbb8|h=cm3ipp2rp6ryl4n57jtwuvhl5eml6wm7|/)

[[MongoDB.in.Action(2011.12)].Kyle.Banker.文字版.pdf](ed2k://|file|[MongoDB.in.Action(2011.12)].Kyle.Banker.文字版.pdf|5549332|30dee48bedcc7cb5730d90b3f7af0436|h=bvaxm5xpqruzkk3nhdxkzvljxjydtyl7|/)

[[OSGi.in.Depth(2011.12)].Alexandre.文字版.pdf](ed2k://|file|[OSGi.in.Depth(2011.12)].Alexandre.文字版.pdf|11679351|7364ce5faf2dc13e08d77003662b87d6|h=yz55k5sbvvhwg5nxkomekawlflv5o6mf|/)

[[Tika.in.Action(2011.11)].Chris.A.Mattmann.文字版.pdf](ed2k://|file|[Tika.in.Action(2011.11)].Chris.A.Mattmann.文字版.pdf|18028504|1da4c1b6430959c4f6522f93a32a3e8c|h=rrkct53djuavjr74h3pzran6oaqag3ko|/)

[[Lift.in.Action(2011.11)].Timothy.Perrett.文字版.pdf](ed2k://|file|[Lift.in.Action(2011.11)].Timothy.Perrett.文字版.pdf|12376694|988e14f19ad18ef608e3bc0da7dbb624|h=sa2na45nro5ffrqxotb35wpi63xt6rem|/)

[[Android.in.Action(3rd,2011.11)].W.Frank.Ableson.文字版.pdf](ed2k://|file|[Android.in.Action(3rd,2011.11)].W.Frank.Ableson.文字版.pdf|15609285|d2ec1d78e27f032c7da2c5a8db6b2815|h=bzud7jv47twxzt7sxffvqljhp72acx7e|/)

[[OpenCL.in.Action(2011.11)].Matthew.Scarpino.文字版.pdf](ed2k://|file|[OpenCL.in.Action(2011.11)].Matthew.Scarpino.文字版.pdf|17691507|267e4674be80b9c27e0d07ae13a33d6c|h=uvfhffguq3yst2ng5ffwvrsv5dgy6yo6|/)

[[Clojure.in.Action(2011.11)].Amit.Rathore.文字版.pdf](ed2k://|file|[Clojure.in.Action(2011.11)].Amit.Rathore.文字版.pdf|7955307|57889e83c5648b237570564428801a4f|h=5hw4awgqluyrzrg2sygzxykwa44i3cgl|/)

[[SQL.Server.MVP.Deep.Dives(Vol2.2011)].Kalen.Delaney.文字版.pdf](ed2k://|file|[SQL.Server.MVP.Deep.Dives(Vol2.2011)].Kalen.Delaney.文字版.pdf|21678420|2731e2da84ca9c0333847df1ad01b4a4|h=eo3w2yqcaq4jptfs3k2k4czt4jkm7d6f|/)

[[SQL.Server.MVP.Deep.Dives(Vol2.2011)].Kalen.Delaney.文字版.epub](ed2k://|file|[SQL.Server.MVP.Deep.Dives(Vol2.2011)].Kalen.Delaney.文字版.epub|15302547|3dec23931379b55be86ad9e02eae89ba|h=srjks2nzmcbkufgo2mzfvemed5xmzgxb|/)

[[SQL.Server.MVP.Deep.Dives(Vol1.2009)].Kalen.Delaney.文字版.pdf](ed2k://|file|[SQL.Server.MVP.Deep.Dives(Vol1.2009)].Kalen.Delaney.文字版.pdf|13010842|ae1b689b5a838b1d29882003ebd4d235|h=spxgtg4qtgumfwadxhxczbajy7i3nead|/)

[[Spring.Batch.in.Action(2011)].Arnaud.Cogoluegnes.文字版.pdf](ed2k://|file|[Spring.Batch.in.Action(2011)].Arnaud.Cogoluegnes.文字版.pdf|11740593|06ff11d48973db4f29e370ae96ba771d|h=jmwzvibh4ncr45sepgc2d7wsluzm6lky|/)

[[Mahout.in.Action(2011)].Sean.Owen.文字版.pdf](ed2k://|file|[Mahout.in.Action(2011)].Sean.Owen.文字版.pdf|14050962|d0b410413cc5ed390cadd21ddc7d6ff5|h=hmc3qurg6sccmxucqxg6wp67ubcab74o|/)

[[Android.in.Practice(2011)].Charlie.Collins.文字版.pdf](ed2k://|file|[Android.in.Practice(2011)].Charlie.Collins.文字版.pdf|12516139|2534f436cd7124a46809a02bf241f54e|h=nuj2asbhi3cjz4qbaql3qqku3iswyc43|/)

[[Rails.3.in.Action].Ryan.Bigg.文字版.pdf](ed2k://|file|[Rails.3.in.Action].Ryan.Bigg.文字版.pdf|8291837|967aadbd2b68558bbeb10e06af18ba3f|h=wcx2e3zw7mgmtlk6xy5dohsybipkrtqw|/)

[[Portlets.in.Action].Ashish.Sarin.文字版.pdf](ed2k://|file|[Portlets.in.Action].Ashish.Sarin.文字版.pdf|19036218|9962e140715e92964f9726b4fe206c37|h=qcfpl3xl24d22fkrb3yv25cvskuwn4y2|/)

[[Objective-C.Fundamentals].Christopher.K.Fairbairn.文字版.pdf](ed2k://|file|[Objective-C.Fundamentals].Christopher.K.Fairbairn.文字版.pdf|13702601|b1b2dd600587cd959057720bc8e6e922|h=q7kfrlbobdzw44ih63ugtrp3cr6xenbz|/)

[[Liferay.in.Action].Richard.Sezov.文字版.pdf](ed2k://|file|[Liferay.in.Action].Richard.Sezov.文字版.pdf|20076944|07fd7d4cc37fdfe96e332196605a7de2|h=kwvh3xm33fldlkv2dljawujej2nfyuvy|/)

[[OS.X.Mountain.Lion：The.Missing.Manual(2012.7)].David.Pogue.文字版.pdf](ed2k://|file|[OS.X.Mountain.Lion：The.Missing.Manual(2012.7)].David.Pogue.文字版.pdf|37795294|de4e7218236c800fd2da17ba858f377c|h=mvchavy3mhnj3vnoatq5xhetu5blcsf6|/)

[[OS.X.Mountain.Lion：The.Missing.Manual(2012.7)].David.Pogue.文字版.epub](ed2k://|file|[OS.X.Mountain.Lion：The.Missing.Manual(2012.7)].David.Pogue.文字版.epub|23943285|13d65deb2e3a26421e9bde9ad9ad21f6|h=wx633ftvccoh7mgi6fqmxhj5aunupv2j|/)

[[FileMaker.Pro.12：The.Missing.Manual(2012.7)].Susan.Prosser.文字版.pdf](ed2k://|file|[FileMaker.Pro.12：The.Missing.Manual(2012.7)].Susan.Prosser.文字版.pdf|77845198|e52c0bc09ac408adc59ae6ceabd8725a|h=kynmlct3wer3e3w4qr3bnj3cgzufnrlc|/)

[[FileMaker.Pro.12：The.Missing.Manual(2012.7)].Susan.Prosser.文字版.epub](ed2k://|file|[FileMaker.Pro.12：The.Missing.Manual(2012.7)].Susan.Prosser.文字版.epub|26451639|57a5f455ca2f3e1a96c4d1567918ebcf|h=5q6w2zlqnza2gyw6r5q56gy2icvlhxwt|/)

[[Intermediate.Perl(2nd,2012.7)].brian.d.foy.文字版.pdf](ed2k://|file|[Intermediate.Perl(2nd,2012.7)].brian.d.foy.文字版.pdf|8821991|1005419338688c1631bf03ba6cf13a8c|h=haips6sf6oaygqqrueq6grcjvo6zw4ao|/)

[[Intermediate.Perl(2nd,2012.7)].brian.d.foy.文字版.epub](ed2k://|file|[Intermediate.Perl(2nd,2012.7)].brian.d.foy.文字版.epub|2795318|e25faaf3866ea8163a2baa23252a598d|h=7yt7ndmmzrqxf2c3ezejfjjbb6gdjanf|/)

[[OS.X.Mountain.Lion.Pocket.Guide(2012.7)].Chris.Seibold.文字版.pdf](ed2k://|file|[OS.X.Mountain.Lion.Pocket.Guide(2012.7)].Chris.Seibold.文字版.pdf|16039454|a4615c5c5da252187257fbf27200be9e|h=6d77xg53gushgo3vaf4qn3fltjmtb2t3|/)

[[OS.X.Mountain.Lion.Pocket.Guide(2012.7)].Chris.Seibold.文字版.epub](ed2k://|file|[OS.X.Mountain.Lion.Pocket.Guide(2012.7)].Chris.Seibold.文字版.epub|7963691|1722f25eca5e0b05386e91b018ccbf89|h=hv6u6dcrkzyr3d7c6mqtynoa4skwzu6r|/)

[[Practical.Computer.Vision.with.SimpleCV(2012.7)].Kurt.Demaagd.文字版.pdf](ed2k://|file|[Practical.Computer.Vision.with.SimpleCV(2012.7)].Kurt.Demaagd.文字版.pdf|44222155|567b32505734e9b528c0f9ed38731018|h=zxdibhznnf3aaeurhghpnvyzdnjhyndb|/)

[[Practical.Computer.Vision.with.SimpleCV(2012.7)].Kurt.Demaagd.文字版.epub](ed2k://|file|[Practical.Computer.Vision.with.SimpleCV(2012.7)].Kurt.Demaagd.文字版.epub|11547917|c6dfdb8104aa352dc05f9e49764e5ca0|h=aztaijyf3lpgo7nrqrycwjyv7n6e2dam|/)

[[Ext.JS.in.Action(第1版)].(Ext.JS.in.Action).Jesus.Garcia.文字版.pdf](ed2k://|file|[Ext.JS.in.Action(第1版)].(Ext.JS.in.Action).Jesus.Garcia.文字版.pdf|19013936|a12cb2bbd246e2c018e22c221e1c109f|h=hgbavo5xiasl64alghtu337dcxqufr36|/)

[[iBatis实战].(iBatis.in.Action).C.Begin&B.Goodin&L.Meadors.文字版.pdf](ed2k://|file|[iBatis实战].(iBatis.in.Action).C.Begin&B.Goodin&L.Meadors.文字版.pdf|3914276|4849e0dd5f2eb78ebcde5e507f6ffd53|h=76bwr3xefd4je3ugeyiditxauzc3plaz|/)

[[ASP.NET.4.0实战].(ASP.NET.4.0.in.Practice).D.Bochicchio&S.Mostarda&M.D.Sanctis.文字版.pdf](ed2k://|file|[ASP.NET.4.0实战].(ASP.NET.4.0.in.Practice).D.Bochicchio&S.Mostarda&M.D.Sanctis.文字版.pdf|7071345|f2b2a62fd6fea652aee4cd6400d1c2d6|h=adeuhjed7fz4f7oeolglkpcxt2at775r|/)

[[Entity.Framework.4.实战].(Entity.Framework.4.in.Action).S.Mostarda&M.D.Sanctis&D.Bochicchio.文字版.pdf](ed2k://|file|[Entity.Framework.4.实战].(Entity.Framework.4.in.Action).S.Mostarda&M.D.Sanctis&D.Bochicchio.文字版.pdf|12586299|239b79156f40da4feed917d7dd0401b2|h=442sptmtt4bs47qe7cl4xdtf6rwab4wu|/)

[[Windows.Powershell.实战(第2版)].(Windows.Powershell.in.Action).Bruce.Payette.文字版.pdf](ed2k://|file|[Windows.Powershell.实战(第2版)].(Windows.Powershell.in.Action).Bruce.Payette.文字版.pdf|11647740|e8673dd8138dda83029f03dde5437d37|h=aoh3bs4lozq6cmpuhptt23ec6tcx6tpn|/)

[[SQL.Server.DMVs.实战].(SQL.Server.DMVs.in.Action).Ian.W.Stirk.文字版.pdf](ed2k://|file|[SQL.Server.DMVs.实战].(SQL.Server.DMVs.in.Action).Ian.W.Stirk.文字版.pdf|10607276|69470dff5f0892ef8338d4b115f74129|h=6uriwupazo3pqmdqnp6puyukoceilr3f|/)

[[SharePoint.2010.Web.Parts.实战].(SharePoint.2010.Web.Parts.in.Action).Wictor.Wilén.文字版.pdf](ed2k://|file|[SharePoint.2010.Web.Parts.实战].(SharePoint.2010.Web.Parts.in.Action).Wictor.Wilén.文字版.pdf|4895545|5c15801f5fb94fe4478bffbc4d2b1afc|h=pe2hlmvboqtnchgqebxtbqz5mxwjsdmw|/)

[[SharePoint.2010.Workflows.实战].(SharePoint.2010.Workflows.in.Action).Phil.Wicklund.文字版.pdf](ed2k://|file|[SharePoint.2010.Workflows.实战].(SharePoint.2010.Workflows.in.Action).Phil.Wicklund.文字版.pdf|6482920|eb63288427f67b591ae23d06627d9344|h=7ehccfj4hxnkixcvgacjobtaldrcll4s|/)

[[PostGIS实战].(PostGIS.in.Action).Regina.O.Obe&Leo.S.Hsu.文字版.pdf](ed2k://|file|[PostGIS实战].(PostGIS.in.Action).Regina.O.Obe&Leo.S.Hsu.文字版.pdf|4686782|705e500039e0dcaebf36879798712d40|h=ykiibsibtb5mizxrtd3nl6alqrcvhna2|/)

[[OSGi实战].(OSGi.in.Action).R.S.Hall&K.Pauls&S.McCulloch&D.Savage.文字版.pdf](ed2k://|file|[OSGi实战].(OSGi.in.Action).R.S.Hall&K.Pauls&S.McCulloch&D.Savage.文字版.pdf|4202812|a3065c905a67f8beae01f2771c407ae5|h=hdxc3bpv2dyffkuspbtjc7h752tuxecn|/)

[[Clojure编程乐趣].(The.Joy.of.Clojure).Michael.Fogus&Chris.Houser.文字版.pdf](ed2k://|file|[Clojure编程乐趣].(The.Joy.of.Clojure).Michael.Fogus&Chris.Houser.文字版.pdf|4788006|4a60a915549b9e3e8afa925cfb2a0bd3|h=jafdvjtamn744ouz3atlmuvru7grdmqt|/)

[[Clojure编程乐趣].(The.Joy.of.Clojure).Michael.Fogus&Chris.Houser.文字版.epub](ed2k://|file|[Clojure编程乐趣].(The.Joy.of.Clojure).Michael.Fogus&Chris.Houser.文字版.epub|3847050|a19c70fe9ddac7eaa700bb728115a70e|h=yg76zswazyivvs3lwcxcpdn5zzxsnxzs|/)

[[ActiveMQ实战].(ActiveMQ.in.Action).Bruce.Snyder&Dejan.Bosanac&Rob.Davies.文字版.pdf](ed2k://|file|[ActiveMQ实战].(ActiveMQ.in.Action).Bruce.Snyder&Dejan.Bosanac&Rob.Davies.文字版.pdf|14852456|37c1cebb65ffa58bbb08abcdb822f631|h=6i3qv5dymk5i3odat24srmhfqu7fgd2l|/)

[[.NET中的持续集成].(Continuous.Integration.in.NET).M.Kawalerowicz&C.Berntson.文字版.pdf](ed2k://|file|[.NET中的持续集成].(Continuous.Integration.in.NET).M.Kawalerowicz&C.Berntson.文字版.pdf|16985872|9767a928855178fb1548cf4974531653|h=mretpykpmbzsjyo36wrpscgpxatizp6n|/)

[[Tuscany.SCA实战].(Tuscany.SCA.in.Action).S.Laws&M.Combellack&R.Feng&H.Mahbod&S.Nash.文字版.pdf](ed2k://|file|[Tuscany.SCA实战].(Tuscany.SCA.in.Action).S.Laws&M.Combellack&R.Feng&H.Mahbod&S.Nash.文字版.pdf|9219812|92e71ad8473bf2b71278d46194cbfb7f|h=33hfxgplnu7j4xnbaajcni7hn3c4einr|/)

[[DSLs实战].(DSLs.in.Action).Debasish.Ghosh.文字版.pdf](ed2k://|file|[DSLs实战].(DSLs.in.Action).Debasish.Ghosh.文字版.pdf|6689681|439b4a5bccbde1f1a416827c633c813b|h=7dexbmlvvkz7kjhzwqshxwszrusv6clp|/)

[[Camel实战].(Camel.in.Action).Claus.Ibsen&Jonathan.Anstey.文字版.pdf](ed2k://|file|[Camel实战].(Camel.in.Action).Claus.Ibsen&Jonathan.Anstey.文字版.pdf|4079951|03dc1c491919ed39b841cd612f99312c|h=nn2rtjx6ynqdb4vl33s6iaozy563q4f7|/)

[[Hadoop实战].(Hadoop.in.Action).Chuck.Lam.文字版.pdf](ed2k://|file|[Hadoop实战].(Hadoop.in.Action).Chuck.Lam.文字版.pdf|2795826|5abcae09fa8615dbf12d2407c6874041|h=otlcd6a537d66qtyjkgjyztuxktbwrux|/)

[[Learning.SQL(2nd,2009.4)].Alan.Beaulieu.文字版.pdf](ed2k://|file|[Learning.SQL(2nd,2009.4)].Alan.Beaulieu.文字版.pdf|2849507|FEDB1E84CF33D218F52DFF324EA97BE6|h=NGM6N5DD6WXXL7HEAKQG3PBTFMH42PRY|/)

[[Erlang.and.OTP.实战].(Erlang.and.OTP.in.Action).M.Logan&E.Merritt&R.Carlsson.文字版.pdf](ed2k://|file|[Erlang.and.OTP.实战].(Erlang.and.OTP.in.Action).M.Logan&E.Merritt&R.Carlsson.文字版.pdf|3151503|e201fbb924fe897cc20de0bc4434f591|h=d4zjbowds6majdkmk5xxro5hrglfsyxd|/)

[[Flex.4.实战].(Flex.4.in.Action).T.Ahmed&D.Orlando&J.C.Bland&J.Hooks.文字版.pdf](ed2k://|file|[Flex.4.实战].(Flex.4.in.Action).T.Ahmed&D.Orlando&J.C.Bland&J.Hooks.文字版.pdf|6318664|0f3791e5107af1c235fa743c0660185b|h=ms5s65opnywgm6fgj4z4xzxiuxenurcv|/)

[[The.Cloud.at.Your.Service(第1版)].(The.Cloud.at.Your.Service).J.Rosenberg&A.Mateos.文字版.pdf](ed2k://|file|[The.Cloud.at.Your.Service(第1版)].(The.Cloud.at.Your.Service).J.Rosenberg&A.Mateos.文字版.pdf|16393886|b8855a9462deb48d39e9f1d5904108f3|h=ksurm2u4dam5uadkyuu5zekx5qpqkaya|/)

[[深入解析C#(第2版)].(C#.in.Depth).Jon.Skeet.文字版.pdf](ed2k://|file|[深入解析C#(第2版)].(C#.in.Depth).Jon.Skeet.文字版.pdf|5756582|f78410f0f203c8fe983d256c92777496|h=4oismqqdtixxto2mzrckzvkfeblhh3z2|/)

[[iText实战(第2版)].(iText.in.Action).Bruno.Lowagie.文字版.pdf](ed2k://|file|[iText实战(第2版)].(iText.in.Action).Bruno.Lowagie.文字版.pdf|21242804|f880f082bf059406663f97691c67be0a|h=inakeeqpj2akazxj5zfdlc5f4x6rg5s3|/)

[[Flex.on.Java(第1版)].(Flex.on.Java).Bernerd.Allmon&Jeremy.Anderson.文字版.pdf](ed2k://|file|[Flex.on.Java(第1版)].(Flex.on.Java).Bernerd.Allmon&Jeremy.Anderson.文字版.pdf|2374045|14167a40336b3a52616a4300f31efd7a|h=yamw42vv3hztmclnocxxzjydk6jl6qvb|/)

[[Azure实战].(Azure.in.Action).Chris.Hay&Brian.H.Prince.文字版.pdf](ed2k://|file|[Azure实战].(Azure.in.Action).Chris.Hay&Brian.H.Prince.文字版.pdf|12820916|fbb1a3c443b110ae69bd60895f0c1ecb|h=uxohgzeu2ady7tir7o2wr2filhnvg5pw|/)

[[Silverlight.4.实战].(Silverlight.4.in.Action).Pete.Brown.文字版.pdf](ed2k://|file|[Silverlight.4.实战].(Silverlight.4.in.Action).Pete.Brown.文字版.pdf|17392369|69ab85fb4ba193af6a91e1db04899d9b|h=4qre7n3xnkskxudtv6eojam5zcxwpvrm|/)

[[Spring.Dynamic.Modules实战].(Spring.Dynamic.Modules.in.Action).A.Cogoluègnes&T.Templier&A.Piper.文字版.pdf](ed2k://|file|[Spring.Dynamic.Modules实战].(Spring.Dynamic.Modules.in.Action).A.Cogoluègnes&T.Templier&A.Piper.文字版.pdf|17992461|d8219a90e377eae6fddf24f738fd27c7|h=jdqdpgamrsvuuadj274quocan46voedz|/)

[[Agile.ALM(第1版)].(Agile.ALM).Michael.Hüttermann.文字版.pdf](ed2k://|file|[Agile.ALM(第1版)].(Agile.ALM).Michael.Hüttermann.文字版.pdf|14539907|549d0b627fba7c644e99bcd7ac02d58a|h=3pr22pigysmyme2lucwqolhrlnw3peh3|/)

[[Generative.Art(第1版)].(Generative.Art).Matt.Pearson.文字版.pdf](ed2k://|file|[Generative.Art(第1版)].(Generative.Art).Matt.Pearson.文字版.pdf|29320498|1fe04edec9a6201603a4865f2dc462e9|h=5f4tgzmbm2rnviogyqianwhl6i3h6pal|/)

[[Location.Aware.Applications(第1版)].(Location.Aware.Applications).Richard.Ferraro.文字版.pdf](ed2k://|file|[Location.Aware.Applications(第1版)].(Location.Aware.Applications).Richard.Ferraro.文字版.pdf|17892239|785dbd920bfa84f9e12615d3a1005312|h=ei4hraduoqruak23xg535bikfnytyqrz|/)

[[R语言实战].(R.in.Action).Robert.I.Kabacoff.文字版.pdf](ed2k://|file|[R语言实战].(R.in.Action).Robert.I.Kabacoff.文字版.pdf|24067996|3b4b0e0878857389ddd09a10328ae001|h=k67m6ras2c4ha5fvnvviiymgsh6wguec|/)

[[iOS.4.实战].(iOS.4.in.Action).J.Harrington&B.Trebitowski&C.Allen&S.Appelcline.文字版.pdf](ed2k://|file|[iOS.4.实战].(iOS.4.in.Action).J.Harrington&B.Trebitowski&C.Allen&S.Appelcline.文字版.pdf|10680823|72cf0f9584bae5ad40a7252a295a125b|h=yjslqry3ycoqmp2gtm3gap7bwddf4vqq|/)

[[Spring实战(第3版)].(Spring.in.Action.3rd.Edition).Craig.Walls.文字版.pdf](ed2k://|file|[Spring实战(第3版)].(Spring.in.Action.3rd.Edition).Craig.Walls.文字版.pdf|10521957|1c9d08484c08a1bdd98c46f1510a2fb0|h=jxk47lxrujlziz3pnixbqkwkfbizwcfh|/)

[[Dreamweaver.CS6：The.Missing.Manual(2012.7)].David.Sawyer.McFarland.文字版.pdf](ed2k://|file|[Dreamweaver.CS6：The.Missing.Manual(2012.7)].David.Sawyer.McFarland.文字版.pdf|70160046|0072ba73cb05e68c4407a8eef1c44070|h=i3pbviwszseqad4moeab2onv3eu3ghgx|/)

[[Dreamweaver.CS6：The.Missing.Manual(2012.7)].David.Sawyer.McFarland.文字版.epub](ed2k://|file|[Dreamweaver.CS6：The.Missing.Manual(2012.7)].David.Sawyer.McFarland.文字版.epub|20478460|ee610dc5d2ccd7e217a74980650ab37e|h=yoc2n55ssqv673t7ovhvhlbbpey3kjol|/)

[[Flash.CS6：The.Missing.Manual(2012.6)].Chris.Grover.文字版.pdf](ed2k://|file|[Flash.CS6：The.Missing.Manual(2012.6)].Chris.Grover.文字版.pdf|66386376|1daf8b9e810e6b484e18f1f4deffa4a6|h=3iu7g56ieef7dqt6vrezxdorhw23djim|/)

[[Flash.CS6：The.Missing.Manual(2012.6)].Chris.Grover.文字版.epub](ed2k://|file|[Flash.CS6：The.Missing.Manual(2012.6)].Chris.Grover.文字版.epub|24734561|239301383244ed6180ab616cae35bb0d|h=rn2gsybmkbhemlsl7krghylyqmzcl62m|/)

[[Adobe.Edge.Preview.5：The.Missing.Manual(2012.5)].Chris.Grover.文字版.pdf](ed2k://|file|[Adobe.Edge.Preview.5：The.Missing.Manual(2012.5)].Chris.Grover.文字版.pdf|16902190|26f37d35f02807b1559c77502e8784d8|h=wc3bcjp62p27dnmwquiwgu76ktxge4qm|/)

[[Adobe.Edge.Preview.5：The.Missing.Manual(2012.5)].Chris.Grover.文字版.epub](ed2k://|file|[Adobe.Edge.Preview.5：The.Missing.Manual(2012.5)].Chris.Grover.文字版.epub|4805451|f42ae5a495b83af8f71104066379da5a|h=st542nzsx5bmbcqw377h5ot556m5w6mk|/)

[[Photoshop.CS6：The.Missing.Manual(2012.5)].Lesa.Snider.文字版.pdf](ed2k://|file|[Photoshop.CS6：The.Missing.Manual(2012.5)].Lesa.Snider.文字版.pdf|101987498|ed1cfe86f3a671a94e731759cb7442cb|h=xwmz2rnezs3cvwceuxtd3hqouuw26jmm|/)

[[Photoshop.CS6：The.Missing.Manual(2012.5)].Lesa.Snider.文字版.epub](ed2k://|file|[Photoshop.CS6：The.Missing.Manual(2012.5)].Lesa.Snider.文字版.epub|30578095|9a14c7e4b48731ddc8d0fff02a9086f8|h=k4v2ucgggerw7vbzilb6xjokim3mrtvw|/)

[[iPad：The.Missing.Manual(4th,2012.4)].J.D.Biersdorfer.文字版.pdf](ed2k://|file|[iPad：The.Missing.Manual(4th,2012.4)].J.D.Biersdorfer.文字版.pdf|56799501|91179bbb96b90973a7c724e24fe67edb|h=neufwluzcig3c63el7dcmclxz7zmmn6j|/)

[[iPad：The.Missing.Manual(4th,2012.4)].J.D.Biersdorfer.文字版.epub](ed2k://|file|[iPad：The.Missing.Manual(4th,2012.4)].J.D.Biersdorfer.文字版.epub|32955161|6b543ee54bfd99dfe124941fd95956ca|h=3u27oo6fbuxe6ip6qdgpojvlzupxvism|/)

[[NOOK.Tablet：The.Missing.Manual(2012.3)].Preston.Gralla.文字版.pdf](ed2k://|file|[NOOK.Tablet：The.Missing.Manual(2012.3)].Preston.Gralla.文字版.pdf|64798021|598642cb4920d3378423a0704d15c6b3|h=h62chapehedbwg4vwpffmxkhtgy6rbys|/)

[[NOOK.Tablet：The.Missing.Manual(2012.3)].Preston.Gralla.文字版.epub](ed2k://|file|[NOOK.Tablet：The.Missing.Manual(2012.3)].Preston.Gralla.文字版.epub|52722785|e58c491ec0201f97ede1ce3b6e8a9953|h=b2jamjcayjx2ro6zipyne4tbbgqwxkix|/)

[[Switching.to.the.Mac：The.Missing.Manual(Lion.Edition,2012.3)].David.Pogue.文字版.pdf](ed2k://|file|[Switching.to.the.Mac：The.Missing.Manual(Lion.Edition,2012.3)].David.Pogue.文字版.pdf|40380935|d866ec949c5e6b54aa834d649cb496cd|h=7ausl3bidwcnajggzu5qu4ta2sqgmphu|/)

[[Switching.to.the.Mac：The.Missing.Manual(Lion.Edition,2012.3)].David.Pogue.文字版.epub](ed2k://|file|[Switching.to.the.Mac：The.Missing.Manual(Lion.Edition,2012.3)].David.Pogue.文字版.epub|27077600|e401c38440c2e4df17ef42cd6c1dddfa|h=qcc2vr5knlp3ys2bhgkzjtrbikh6zmru|/)

[[Learning.Rails.3(2012.7)].Simon.St.Laurent.文字版.pdf](ed2k://|file|[Learning.Rails.3(2012.7)].Simon.St.Laurent.文字版.pdf|12741109|eef89acbceacda16abde1775a4fea479|h=5chivew4s75wce424k22svtjav77qauu|/)

[[Learning.Rails.3(2012.7)].Simon.St.Laurent.文字版.epub](ed2k://|file|[Learning.Rails.3(2012.7)].Simon.St.Laurent.文字版.epub|6837860|7fd68f8ca2b33b5062b183db84300fb1|h=7h6wrqdnz6mggfkncb4sckq2tpadxven|/)

[[Introducing.Regular.Expressions(2012.7)].Michael.Fitzgerald.文字版.pdf](ed2k://|file|[Introducing.Regular.Expressions(2012.7)].Michael.Fitzgerald.文字版.pdf|9147546|c568f37409130249013927bea3879273|h=sfmg5i2b3bzuqtlczrzoy2rath4nrlzk|/)

[[Introducing.Regular.Expressions(2012.7)].Michael.Fitzgerald.文字版.epub](ed2k://|file|[Introducing.Regular.Expressions(2012.7)].Michael.Fitzgerald.文字版.epub|5456437|40a5f9213e8f0f1c70580bd932191b97|h=ilbviofr2nwhg6wwp37xapmvp7jfhkmy|/)

[[Getting.Started.with.Metro.Style.Apps(2012.7)].Ben.Dewey.文字版.pdf](ed2k://|file|[Getting.Started.with.Metro.Style.Apps(2012.7)].Ben.Dewey.文字版.pdf|12092714|94f7e4d441fc0858657fa29b753c8cde|h=pijtiewm22i7f75nzrf2j3ogeegmor5o|/)

[[Getting.Started.with.Metro.Style.Apps(2012.7)].Ben.Dewey.文字版.epub](ed2k://|file|[Getting.Started.with.Metro.Style.Apps(2012.7)].Ben.Dewey.文字版.epub|4142991|c3c671d430b2004cdc6977f281ecda47|h=dhqlwa5rlhneophhczujjxqqenibwty3|/)

[[The.Data.Journalism.Handbook(2012.7)].Jonathan.Gray.文字版.pdf](ed2k://|file|[The.Data.Journalism.Handbook(2012.7)].Jonathan.Gray.文字版.pdf|55302686|dc7646e058ae63eb905eee726493ae79|h=bgws5tycr4zufanmbxhkuuflq4b26enp|/)

[[The.Data.Journalism.Handbook(2012.7)].Jonathan.Gray.文字版.epub](ed2k://|file|[The.Data.Journalism.Handbook(2012.7)].Jonathan.Gray.文字版.epub|19880633|7180ae16077590e35fdf696bc495e846|h=fcxxo544ijqv34fk7paygghya5erzscb|/)

[[LED.Lighting(2012.7)].Sal.Cangeloso.文字版.pdf](ed2k://|file|[LED.Lighting(2012.7)].Sal.Cangeloso.文字版.pdf|35928019|c6c12bc18add779f3b2d062edfd01629|h=wu4d4etb74ndl5yd3txjo2xgmbfgmcdt|/)

[[LED.Lighting(2012.7)].Sal.Cangeloso.文字版.epub](ed2k://|file|[LED.Lighting(2012.7)].Sal.Cangeloso.文字版.epub|3258802|ee1490acc56371128e1971ba91ee15a5|h=ppxkllyly5xswseytg3du74k67o3hg25|/)

[[Drupal.for.Designers(2012.7)].Dani.Nordin.文字版.pdf](ed2k://|file|[Drupal.for.Designers(2012.7)].Dani.Nordin.文字版.pdf|25513960|7613ea89e7592245e454a3c32f044ecc|h=b4j3zx4xarxsyj3yclu7e3plvugnt462|/)

[[Drupal.for.Designers(2012.7)].Dani.Nordin.文字版.epub](ed2k://|file|[Drupal.for.Designers(2012.7)].Dani.Nordin.文字版.epub|18531009|35df2c62f5cc162732009681cffaa34a|h=7dzqyrnjlqufrznof2hbsisfczys2yy2|/)

[[HTML5.The.Missing.Manual].Matthew.MacDonald.文字版.pdf](ed2k://|file|[HTML5.The.Missing.Manual].Matthew.MacDonald.文字版.pdf|20971178|47f4c187518f423edeceaabb9eae24ef|h=bjx3x6v2niy5thlsfeq5t4b7fditvhzc|/)

[[HTML5.The.Missing.Manual].Matthew.MacDonald.文字版.epub](ed2k://|file|[HTML5.The.Missing.Manual].Matthew.MacDonald.文字版.epub|8651356|1aa8d4e4fc5f3c57bfa38df9ac969ee3|h=mm6bsky5yh7ftm7s2k3n5rb2wzd4khhi|/)

[[Kindle.Fire：The.Missing.Manual(2012.2)].Peter.Meyers.文字版.pdf](ed2k://|file|[Kindle.Fire：The.Missing.Manual(2012.2)].Peter.Meyers.文字版.pdf|26124840|1b65c38941769b62ac668cea77299dab|h=jndrsvd5jp2m2poz6g2wwdvymeswqtqd|/)

[[Kindle.Fire：The.Missing.Manual(2012.2)].Peter.Meyers.文字版.epub](ed2k://|file|[Kindle.Fire：The.Missing.Manual(2012.2)].Peter.Meyers.文字版.epub|25000615|66acaf515d89f01f05a0651af8ddfa91|h=3wu5kxosvf6gifoyvnabd3slqscz3nis|/)

[[Creating.a.Website：The.Missing.Manual(3rd,2011.04)].Matthew.MacDonald.文字版.pdf](ed2k://|file|[Creating.a.Website：The.Missing.Manual(3rd,2011.04)].Matthew.MacDonald.文字版.pdf|35714357|abf38e0fa56e5a70d2650e0e4bd8a822|h=lmofm6fl3wqvp33gn57oxwzyo5usp6hl|/)

[[Creating.a.Website：The.Missing.Manual(3rd,2011.04)].Matthew.MacDonald.文字版.epub](ed2k://|file|[Creating.a.Website：The.Missing.Manual(3rd,2011.04)].Matthew.MacDonald.文字版.epub|20660114|9f0012ad8d6d65989b0ae698fdc5bca4|h=d7ftlyqkrnmp6brai2mtuq3zteqlc2kx|/)

[[Galaxy.S.II：The.Missing.Manual(2011.12)].Preston.Gralla.文字版.pdf](ed2k://|file|[Galaxy.S.II：The.Missing.Manual(2011.12)].Preston.Gralla.文字版.pdf|36272246|926420e911a1e164f95f22062ce3feff|h=4agpu752sqya63dvr7hmeuvqiij4orgq|/)

[[Galaxy.S.II：The.Missing.Manual(2011.12)].Preston.Gralla.文字版.epub](ed2k://|file|[Galaxy.S.II：The.Missing.Manual(2011.12)].Preston.Gralla.文字版.epub|35739188|031256cbe5038eab399c3d08c381cf37|h=cyp4iprj3lzwdsbduerfc2m6pzl3hh6p|/)

[[Team.Geek(2012.7)].Brian.W.Fitzpatrick.文字版.pdf](ed2k://|file|[Team.Geek(2012.7)].Brian.W.Fitzpatrick.文字版.pdf|31378202|442530f7a14f3d273cd751862e14b34c|h=mqllticy5b3skbgkn4pifq4ehtx7ov4h|/)

[[Team.Geek(2012.7)].Brian.W.Fitzpatrick.文字版.epub](ed2k://|file|[Team.Geek(2012.7)].Brian.W.Fitzpatrick.文字版.epub|5334284|42ee7afe112ebc15831616f2e5e1e71b|h=bq4rqwuqa6jtwrahy23v3n2qjmbvsalj|/)

[[Windows.PowerShell.for.Developers(2012.7)].Douglas.Finke.文字版.pdf](ed2k://|file|[Windows.PowerShell.for.Developers(2012.7)].Douglas.Finke.文字版.pdf|9195465|bd6e4bbd13fb76201705671375623c09|h=hfkvn7sgwzqg7eii4xoepfpkit2prbdd|/)

[[Windows.PowerShell.for.Developers(2012.7)].Douglas.Finke.文字版.epub](ed2k://|file|[Windows.PowerShell.for.Developers(2012.7)].Douglas.Finke.文字版.epub|5909494|4358511e4b00978450c6963eef961b72|h=32sky7d4mnocrxsjhlmmja3ntwruw7bm|/)

[[PostgreSQL：Up.and.Running(2012.7)].Regina.Obe.文字版.pdf](ed2k://|file|[PostgreSQL：Up.and.Running(2012.7)].Regina.Obe.文字版.pdf|6532437|630be1b5166af643bffc4514cd0371a8|h=tv47twuepbxrstjwxmitomzrwgkaa5jk|/)

[[PostgreSQL：Up.and.Running(2012.7)].Regina.Obe.文字版.epub](ed2k://|file|[PostgreSQL：Up.and.Running(2012.7)].Regina.Obe.文字版.epub|2943003|82a7ff387864dad247a5237596a40c34|h=f7ule5xbarxpi4z746fvo2dtbm2ugcvu|/)

[[HLSL.and.Pixel.Shaders.for.XAML.Developers(2012.7)].Walt.Ritscher.文字版.pdf](ed2k://|file|[HLSL.and.Pixel.Shaders.for.XAML.Developers(2012.7)].Walt.Ritscher.文字版.pdf|26443491|480d0f38a87db6b111b9db57abea5faf|h=nhfiizbwdvgnd5vnunpfpe4booblnm3t|/)

[[HLSL.and.Pixel.Shaders.for.XAML.Developers(2012.7)].Walt.Ritscher.文字版.epub](ed2k://|file|[HLSL.and.Pixel.Shaders.for.XAML.Developers(2012.7)].Walt.Ritscher.文字版.epub|9124827|d40afb5313194d09569af60995c1cb79|h=j4hvjmstzynffssdu63i5i7zusflo7sp|/)

[[Google.：The.Missing.Manual(2011.12)].Kevin.Purdy.文字版.pdf](ed2k://|file|[Google.：The.Missing.Manual(2011.12)].Kevin.Purdy.文字版.pdf|18349104|2b27f0fc5fe6f66b5dc11bb83a525631|h=3ff2lcdhwh42djq4twuuaz4wvj7r2a54|/)

[[Google.：The.Missing.Manual(2011.12)].Kevin.Purdy.文字版.epub](ed2k://|file|[Google.：The.Missing.Manual(2011.12)].Kevin.Purdy.文字版.epub|12783596|f41ccf39935fb29fa0e8017c4ca2d796|h=jgqlmihu6jqrgr2hcadlaocdbvqtijy5|/)

[[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.pdf](ed2k://|file|[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.pdf|44960247|7c8cff7dc6b9eae32ebfce299f8d22c7|h=zy7t3ghhqzjazjjnmkdqla62u3o3cpt4|/)

[[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.epub](ed2k://|file|[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.epub|15122067|5fb70e6e7cd1830dcac19c278d84c531|h=ruxteh2c6wn5grktdkic7acvehtrvl6e|/)

[[QuickBooks.2012：The.Missing.Manual(2011.10)].Bonnie.Biafore.文字版.pdf](ed2k://|file|[QuickBooks.2012：The.Missing.Manual(2011.10)].Bonnie.Biafore.文字版.pdf|29008002|8b938a69ba7f392cde8bf85eb15640d4|h=z6d2ky7upwuxzs44gpctvbfebeodpslt|/)

[[QuickBooks.2012：The.Missing.Manual(2011.10)].Bonnie.Biafore.文字版.epub](ed2k://|file|[QuickBooks.2012：The.Missing.Manual(2011.10)].Bonnie.Biafore.文字版.epub|14306579|cff1c3dfe6b71429f0275b76e6db45d6|h=ceuk5vqzgsjq7ldautfe24zmo27fbo3m|/)

[[iPod：The.Missing.Manual(10th,2011.11)].J.D.Biersdorfer.文字版.pdf](ed2k://|file|[iPod：The.Missing.Manual(10th,2011.11)].J.D.Biersdorfer.文字版.pdf|46251513|810bbd0cbfdcf8218ba4ee9c6569fed3|h=bvlpkvyl5jjrmhq6ccofmjddgwoo2lsh|/)

[[iPod：The.Missing.Manual(10th,2011.11)].J.D.Biersdorfer.文字版.epub](ed2k://|file|[iPod：The.Missing.Manual(10th,2011.11)].J.D.Biersdorfer.文字版.epub|42108634|97646a746db45ba2f73810b1bd2521d8|h=fjmlzpbhvv4w7d3rz77lv2sov3xnxm2g|/)

[[JavaScript：The.Missing.Manual(2008)].David.Sawyer.McFarland.文字版.pdf](ed2k://|file|[JavaScript：The.Missing.Manual(2008)].David.Sawyer.McFarland.文字版.pdf|22432604|01b2ec40f7eff9ae24070fee73c77117|h=qclbcqvjf67hfgfwuxsc5coft24af52c|/)

[[JavaScript：The.Missing.Manual(2008)].David.Sawyer.McFarland.文字版.epub](ed2k://|file|[JavaScript：The.Missing.Manual(2008)].David.Sawyer.McFarland.文字版.epub|15019250|7644a2f580db36e05f70627daa5e9104|h=m3rfbmq6xtosuolv253r5g3h3pwgxyqc|/)

[[iPad2：The.Missing.Manual(3rd,2011.11)].J.D.Biersdorfer.文字版.pdf](ed2k://|file|[iPad2：The.Missing.Manual(3rd,2011.11)].J.D.Biersdorfer.文字版.pdf|53004262|61869a3a3d23116b5a0ef2fd62292bc3|h=jkob2uhng7aab2jbuunb62v7faqcexwb|/)

[[iPad2：The.Missing.Manual(3rd,2011.11)].J.D.Biersdorfer.文字版.epub](ed2k://|file|[iPad2：The.Missing.Manual(3rd,2011.11)].J.D.Biersdorfer.文字版.epub|29610387|b3a53fadafa963c182d737d47bfc6452|h=46j3jsqeekjy3tqb76zuoircwo42yvz7|/)

[[PHP.and.MySQL：The.Missing.Manual(2011.11)].Brett.McLaughlin.文字版.pdf](ed2k://|file|[PHP.and.MySQL：The.Missing.Manual(2011.11)].Brett.McLaughlin.文字版.pdf|29631771|ac95f9d5bbc463e77cb21a2e1206500a|h=5nefp7fose2upwrduyucxoisdy3yixao|/)

[[PHP.and.MySQL：The.Missing.Manual(2011.11)].Brett.McLaughlin.文字版.epub](ed2k://|file|[PHP.and.MySQL：The.Missing.Manual(2011.11)].Brett.McLaughlin.文字版.epub|18308803|430cc381321932b43e04addf37f069ab|h=ao2t7mbikd4okbctedqnqpj7464mg5hj|/)

[[Mac.OS.X.Lion：The.Missing.Manual(2011.10)].David.Pogue.文字版.pdf](ed2k://|file|[Mac.OS.X.Lion：The.Missing.Manual(2011.10)].David.Pogue.文字版.pdf|32867250|0c5404be04508e9d8528d28d42c22adf|h=b2slkvriatiy3dxf45xvbo3cnck2sykq|/)

[[Mac.OS.X.Lion：The.Missing.Manual(2011.10)].David.Pogue.文字版.epub](ed2k://|file|[Mac.OS.X.Lion：The.Missing.Manual(2011.10)].David.Pogue.文字版.epub|20363784|1241a8b638eb709831cac534c44f1efb|h=3txjs3tzum3fqn3vlpdfaqyfa27ln36b|/)

[[JavaScript.&.jQuery：The.Missing.Manual(2nd.2011)].David.Sawyer.McFarland.文字版.pdf](ed2k://|file|[JavaScript.&.jQuery：The.Missing.Manual(2nd.2011)].David.Sawyer.McFarland.文字版.pdf|16380453|78ba4004a33a50bfd5c8aa3394194572|h=wyqftd4fza2w5rn5wxmtjpnifaw5brdm|/)

[[JavaScript.&.jQuery：The.Missing.Manual(2nd.2011)].David.Sawyer.McFarland.文字版.epub](ed2k://|file|[JavaScript.&.jQuery：The.Missing.Manual(2nd.2011)].David.Sawyer.McFarland.文字版.epub|10236054|0230f869030de5c3daf5dbd77a087f43|h=zjs7mjsdclvqyut2moj3rbmjyfa42cpj|/)

[[CSS：The.Missing.Manual(2nd.2009)].David.Sawyer.文字版.pdf](ed2k://|file|[CSS：The.Missing.Manual(2nd.2009)].David.Sawyer.文字版.pdf|16861781|b74fb54441a6aaf8c0cf61ec82a12990|h=5yym4anlr32oriw5mbc4oawwbpaqwxyz|/)

[[Programming.Computer.Vision.with.Python(2012.6)].Jan.Erik.Solem.文字版.pdf](ed2k://|file|[Programming.Computer.Vision.with.Python(2012.6)].Jan.Erik.Solem.文字版.pdf|72604555|f22d25b93b1bbb31fe1d23e9b7900266|h=a5vn7cpgb4gvhtdd3arsskk7qb2ti5qt|/)

[[Programming.Computer.Vision.with.Python(2012.6)].Jan.Erik.Solem.文字版.epub](ed2k://|file|[Programming.Computer.Vision.with.Python(2012.6)].Jan.Erik.Solem.文字版.epub|9369414|d2693a4f8bc84df06df6da3ff55ad735|h=3t77agvxu35qlbz4plpyoi4javtbth4b|/)

[[Java.in.a.Nutshell(5th,2005.3)].David.Flanagan.文字版.pdf](ed2k://|file|[Java.in.a.Nutshell(5th,2005.3)].David.Flanagan.文字版.pdf|21561997|e9650dbe673c984b3c220979a6febe1a|h=hxfttk4up3y42zpk6difnri2k4gas23q|/)

[[Java.in.a.Nutshell(5th,2005.3)].David.Flanagan.文字版.epub](ed2k://|file|[Java.in.a.Nutshell(5th,2005.3)].David.Flanagan.文字版.epub|4054432|9e741e2f7b679eea53990fb291c0b22d|h=2cgufzlokxrdf2fb73mnw7jerw67ubf5|/)

[[Getting.Started.with.D3(2012.6)].Mike.Dewar.文字版.pdf](ed2k://|file|[Getting.Started.with.D3(2012.6)].Mike.Dewar.文字版.pdf|6591325|b0f94cff29ac3e37812c8a137d497ab3|h=xntmkxnbem3p2hjaz4pgjvdd7dhv35qo|/)

[[Getting.Started.with.D3(2012.6)].Mike.Dewar.文字版.epub](ed2k://|file|[Getting.Started.with.D3(2012.6)].Mike.Dewar.文字版.epub|3326499|aaa4538ad8fcd2c0947c5e36b5be68d2|h=m5pb42avziiqv4e3df7sihtuujfcs5df|/)

[[Exploring.Everyday.Things.with.R.and.Ruby(2012.6)].Sau.Sheong.Chang.文字版.pdf](ed2k://|file|[Exploring.Everyday.Things.with.R.and.Ruby(2012.6)].Sau.Sheong.Chang.文字版.pdf|14650670|8b9f41ff9a39678cb4c4fd1457d43413|h=n6vub6byzxz7gdf2ok7ot2nwibwinp3m|/)

[[Exploring.Everyday.Things.with.R.and.Ruby(2012.6)].Sau.Sheong.Chang.文字版.epub](ed2k://|file|[Exploring.Everyday.Things.with.R.and.Ruby(2012.6)].Sau.Sheong.Chang.文字版.epub|5196938|91fd50d20a4b2eff1c99e6c2a05c8c48|h=agmk3xasoroq6nh24nzybriu42iuu47o|/)

[[Web.Performance.Daybook.Volume.2(2012.6)].Stoyan.Stefanov.文字版.pdf](ed2k://|file|[Web.Performance.Daybook.Volume.2(2012.6)].Stoyan.Stefanov.文字版.pdf|15777240|5aadcc86ca0cfdc751f0a79ad913df9b|h=6wxipulyqz4zvuy3wkl4lhe7osgprepq|/)

[[Web.Performance.Daybook.Volume.2(2012.6)].Stoyan.Stefanov.文字版.epub](ed2k://|file|[Web.Performance.Daybook.Volume.2(2012.6)].Stoyan.Stefanov.文字版.epub|8573108|21be9686e82183601d6525cd679cd7ff|h=u2zmow3p7djg2idckqib4y6v4qatsw2d|/)

[[VMware.Cookbook(2nd,2012.6)].Ryan.Troy.文字版.pdf](ed2k://|file|[VMware.Cookbook(2nd,2012.6)].Ryan.Troy.文字版.pdf|13132206|d50db60164c82c6b8040086d583b810f|h=gem7tgplhuxgg4mivbnqhsskygopyzvt|/)

[[VMware.Cookbook(2nd,2012.6)].Ryan.Troy.文字版.epub](ed2k://|file|[VMware.Cookbook(2nd,2012.6)].Ryan.Troy.文字版.epub|6160919|d3559d31e71d80386b9fd43bbcfcf94f|h=dyeub7dnr22fhpypqkccpds36tmhvt6i|/)

[[Mobile.JavaScript.Application.Development(2012.6)].Adrian.Kosmaczewski.文字版.pdf](ed2k://|file|[Mobile.JavaScript.Application.Development(2012.6)].Adrian.Kosmaczewski.文字版.pdf|9446825|7a3727cf4671921774ce0d9bbef86533|h=w3ayoqief2u3jbrowfus7prx5wp7gcow|/)

[[Mobile.JavaScript.Application.Development(2012.6)].Adrian.Kosmaczewski.文字版.epub](ed2k://|file|[Mobile.JavaScript.Application.Development(2012.6)].Adrian.Kosmaczewski.文字版.epub|6373411|c8dc5d7c6fd0db18f8e9a04aa799b42f|h=yqbioowsoufdlyuuy37rf26xeudbk3hx|/)

[[Macintosh.Terminal.Pocket.Guide(2012.6)].Daniel.J.Barrett.文字版.pdf](ed2k://|file|[Macintosh.Terminal.Pocket.Guide(2012.6)].Daniel.J.Barrett.文字版.pdf|4471382|ad3449222372e6762e83d4836c9727a4|h=esgo4s74msmr5jahx2u3dh5s6vmnwvmw|/)

[[Macintosh.Terminal.Pocket.Guide(2012.6)].Daniel.J.Barrett.文字版.epub](ed2k://|file|[Macintosh.Terminal.Pocket.Guide(2012.6)].Daniel.J.Barrett.文字版.epub|2543819|0889b3159deee3ad970ef657ad95fd7d|h=6ohpkwtpwtxryy4lg7tbls2jaeug63mm|/)

[[Csharp.5.0.in.a.Nutshell(5th,2012.6)].Joseph.Albahari.文字版.pdf](ed2k://|file|[Csharp.5.0.in.a.Nutshell(5th,2012.6)].Joseph.Albahari.文字版.pdf|11706034|2bae9bad53929d3ff07ab0ee7bb028a2|h=mlw537ezw7jkxny6xxoei4gjl5dk3i3h|/)

[[Csharp.5.0.in.a.Nutshell(5th,2012.6)].Joseph.Albahari.文字版.epub](ed2k://|file|[Csharp.5.0.in.a.Nutshell(5th,2012.6)].Joseph.Albahari.文字版.epub|4546059|69141971711a2ce216240a4feeffcab3|h=ohno7dneoibfptqtxxut3fhymiuvtb4u|/)

[[Sakai.OAE.Deployment.and.Management(2012.6)].Max.Whitney.文字版.pdf](ed2k://|file|[Sakai.OAE.Deployment.and.Management(2012.6)].Max.Whitney.文字版.pdf|10662566|d6fe86bb2a8309b8eb81651e45742565|h=gbemczxo4g7yk4gll5vdkc3h6axbpzit|/)

[[Sakai.OAE.Deployment.and.Management(2012.6)].Max.Whitney.文字版.epub](ed2k://|file|[Sakai.OAE.Deployment.and.Management(2012.6)].Max.Whitney.文字版.epub|5819133|7d6c090bef084ba34f04653492ed02bf|h=2o25wnr5k6h5juuh5n4ush6dz3bqxbt7|/)

[[Just.Spring.Data.Access(2012.6)].Madhusudhan.Konda.文字版.pdf](ed2k://|file|[Just.Spring.Data.Access(2012.6)].Madhusudhan.Konda.文字版.pdf|6773837|c783cdd865ae46a1d45dfcda84ebb40b|h=4ekgalbobxmqildpgcfu5sxipszpffdf|/)

[[Just.Spring.Data.Access(2012.6)].Madhusudhan.Konda.文字版.epub](ed2k://|file|[Just.Spring.Data.Access(2012.6)].Madhusudhan.Konda.文字版.epub|2101307|7f384e4cff5dd30f774997de2fc96cdb|h=6gfeotahf4jsuqiasxseu5l6xitose5n|/)

[[Getting.Started.with.Couchbase.Server(2012.6)].MC.Brown.文字版.pdf](ed2k://|file|[Getting.Started.with.Couchbase.Server(2012.6)].MC.Brown.文字版.pdf|6955501|733b2a2b1a9c9c021e0c54f1023a44ee|h=khzkk3ibc3mkvsvqcjjb4hfyh3vaszpl|/)

[[Getting.Started.with.Couchbase.Server(2012.6)].MC.Brown.文字版.epub](ed2k://|file|[Getting.Started.with.Couchbase.Server(2012.6)].MC.Brown.文字版.epub|3617321|fef131317abe48105306ef1c4c9e3afa|h=enggxnltsiir6nzuggxt6cx4co32vzb7|/)

[[Building.Web.Applications.with.Erlang(2012.6)].Zachary.Kessin.文字版.pdf](ed2k://|file|[Building.Web.Applications.with.Erlang(2012.6)].Zachary.Kessin.文字版.pdf|8336530|7e2ad4cea12fd99bbfc1443b26fcf8c6|h=xludtj6ik2icsbryv46txtjd6ioxkivk|/)

[[Building.Web.Applications.with.Erlang(2012.6)].Zachary.Kessin.文字版.epub](ed2k://|file|[Building.Web.Applications.with.Erlang(2012.6)].Zachary.Kessin.文字版.epub|2517413|01d60f52148b39d01cf0455729b00c59|h=zrcuepknhtinochyf3qdxio2pclehgd3|/)

[[Learning.Node(Early.Release,2012.6)].Shelley.Powers.文字版.pdf](ed2k://|file|[Learning.Node(Early.Release,2012.6)].Shelley.Powers.文字版.pdf|5864254|a19661c3e48dbe1271d57ff7d3c514e7|h=u5tieqsii36inqzprfc7j7qtm5jnpubc|/)

[[Linux.Pocket.Guide(1st,2004)].Daniel.J.Barrett.文字版.pdf](ed2k://|file|[Linux.Pocket.Guide(1st,2004)].Daniel.J.Barrett.文字版.pdf|2044561|c709cc850b3af9677dbac21b3f06120f|h=qr2ci37nlfx3s76prhwwrqk3c7kjyzf3|/)

[[Linux.Pocket.Guide(1st,2004)].Daniel.J.Barrett.文字版.epub](ed2k://|file|[Linux.Pocket.Guide(1st,2004)].Daniel.J.Barrett.文字版.epub|929656|f9b01b597762fdca6b0b3334f666e9ba|h=j33hc5t6n36kh3ygdv3o73phrk2b4zxv|/)

[[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.pdf](ed2k://|file|[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.pdf|44960247|7c8cff7dc6b9eae32ebfce299f8d22c7|h=zy7t3ghhqzjazjjnmkdqla62u3o3cpt4|/)

[[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.epub](ed2k://|file|[iPhone：The.Missing.Manual(5th,2011.12)].David.Pogue.文字版.epub|15122067|5fb70e6e7cd1830dcac19c278d84c531|h=ruxteh2c6wn5grktdkic7acvehtrvl6e|/)

[[Packet.Guide.to.Routing.and.Switching].Bruce.Hartpence.文字版.pdf](ed2k://|file|[Packet.Guide.to.Routing.and.Switching].Bruce.Hartpence.文字版.pdf|7745497|8f47b1fcd39e5b213e42ddf4dd5ee4c4|h=2vzd4kbw5tll5hf4436jb7kdtxgnojyy|/)

[[Packet.Guide.to.Routing.and.Switching].Bruce.Hartpence.文字版.epub](ed2k://|file|[Packet.Guide.to.Routing.and.Switching].Bruce.Hartpence.文字版.epub|4005057|b166c826e442ab837d803b1164eb5531|h=r75a5vo4zgzwx6i5rp2hnxxeus7bdcci|/)

[[Mac.OS.X.Lion.Pocket.Guide].Chris.Seibold.文字版.pdf](ed2k://|file|[Mac.OS.X.Lion.Pocket.Guide].Chris.Seibold.文字版.pdf|18995115|c4ce5e980b7bc97b85e163470dba0c55|h=ez6h72ui2f7b2xkjj775yn4nkwnvvfhe|/)

[[Mac.OS.X.Lion.Pocket.Guide].Chris.Seibold.文字版.epub](ed2k://|file|[Mac.OS.X.Lion.Pocket.Guide].Chris.Seibold.文字版.epub|4780099|0dc2f3d98c3e6fb769a52543349b8c2d|h=dzidqimmpuvwqe6m2hmf2sr6lrcukc7m|/)

[[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.pdf](ed2k://|file|[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.pdf|2228023|c29ae4ad819a1ee5355f7247ed861e38|h=rydpleybvojkyu7pun4esqu5a7ccu3lg|/)

[[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.epub](ed2k://|file|[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.epub|679117|dca3c7a1de0ac8647c6fa6f24a8f54a8|h=q6qlt4va7c25mvktysxal73pblqjtqay|/)

[[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.mobi](ed2k://|file|[Perl参考手册(第5版,涵盖Perl5.14)].(Perl.Pocket.Reference.5th.Edition).Johan.Vromans.文字版.mobi|2620615|8e814402509f6c1ba92db4ff921b2858|h=v625wv6zhkj4bmgwuzfrynv7knegu4p4|/)

[[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.pdf](ed2k://|file|[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.pdf|4303244|0e4e6b215ec10258709799fd1d4d1b27|h=aown6xmgwypc2xvoal5rm72dtinpcr2r|/)

[[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.epub](ed2k://|file|[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.epub|1090627|225d3973a8b319232a9702b1d2ad3bc3|h=udr6srmwxthro3ggv3vvhk3qzkvszulu|/)

[[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.mobi](ed2k://|file|[CSS参考手册(第4版,涵盖CSS3)].(CSS.Pocket.Reference.4th.Edition).Eric.A.Meyer.文字版.mobi|3039408|645d0e4b5b4d77577a45f34586436cce|h=koqiv6gycz2yr26qypvjw5mujc5cd4iq|/)

[[CSS参考手册(第3版)].(CSS.Pocket.Reference.3rd.Edition).Eric.A.Meyer.文字版.pdf](ed2k://|file|[CSS参考手册(第3版)].(CSS.Pocket.Reference.3rd.Edition).Eric.A.Meyer.文字版.pdf|2000136|180e0aa5c45aa57b28dd523161f80369|h=julowcbsckcpwblulp37xlwut7mar5bd|/)

[[Ruby袖珍参考手册].(Ruby.Pocket.Reference).Michael.Fitzgerald.文字版.pdf](ed2k://|file|[Ruby袖珍参考手册].(Ruby.Pocket.Reference).Michael.Fitzgerald.文字版.pdf|3736491|01a0cc25b86a4d5b239f5dfd82d7bb52|h=cy4eas5nskfzq6rcnduz5nod6cejt34h|/)

[[MySQL.Pocket.Reference(2nd,2007.7)].George.Reese.文字版.pdf](ed2k://|file|[MySQL.Pocket.Reference(2nd,2007.7)].George.Reese.文字版.pdf|910315|c96f38bfa86aa14b36dcfe923b7c070a|h=qc5cwyn772b6rf4kikrpdvxwf2skekid|/)

[[MySQL.Pocket.Reference(2nd,2007.7)].George.Reese.文字版.epub](ed2k://|file|[MySQL.Pocket.Reference(2nd,2007.7)].George.Reese.文字版.epub|2137485|b676548e49af77791a5a40d777f4c009|h=ttfv7xudquf24sftkyt3kwwqx32r4dvv|/)

[[SQL袖珍参考手册(第3版)].(SQL.Pocket.Guide).Jonathan.Gennick.文字版.pdf](ed2k://|file|[SQL袖珍参考手册(第3版)].(SQL.Pocket.Guide).Jonathan.Gennick.文字版.pdf|1057372|746ec13e6cb709b99950ad2e821aa96d|h=wmfmoohbeyt4ro6klekjayfqyr5had2v|/)

[[成为极客].(Being.Geek：The.Software.Develope's.Career.Handbook).Michael.Lopp.文字版.pdf](ed2k://|file|[成为极客].(Being.Geek：The.Software.Develope's.Career.Handbook).Michael.Lopp.文字版.pdf|7264726|936a4bcedb5e7a714d48c432bcc04eb6|h=ptzl3ifqczhmj3z2x3wk2dsgxrkgq3hz|/)

[[成为极客].(Being.Geek：The.Software.Develope's.Career.Handbook).Michael.Lopp.文字版.epub](ed2k://|file|[成为极客].(Being.Geek：The.Software.Develope's.Career.Handbook).Michael.Lopp.文字版.epub|3183262|a8ef19001d4ca1f2c5328e6f390859fe|h=jpb3ko2k736m4uo6cj3s5irbckbx3qzt|/)

[[Inside.Cyber.Warfare(2nd,2011.12)].Jeffrey.Carr.文字版.pdf](ed2k://|file|[Inside.Cyber.Warfare(2nd,2011.12)].Jeffrey.Carr.文字版.pdf|14403492|c2c78dde3ab958fcce291bf517a3ad95|h=htvtmsn7cngnv2ifbjtlj43rgmtdpvwn|/)

[[Inside.Cyber.Warfare(2nd,2011.12)].Jeffrey.Carr.文字版.pdf](ed2k://|file|[Inside.Cyber.Warfare(2nd,2011.12)].Jeffrey.Carr.文字版.pdf|14403492|c2c78dde3ab958fcce291bf517a3ad95|h=htvtmsn7cngnv2ifbjtlj43rgmtdpvwn|/)

[[Inside.Cyber.Warfare(1st,2009.12)].Jeffrey.Carr.文字版.pdf](ed2k://|file|[Inside.Cyber.Warfare(1st,2009.12)].Jeffrey.Carr.文字版.pdf|2996213|877536491fbb771d62b9adc5c1424ac4|h=pwzkqchpjok3oiw2alrac4lnetcmlxxo|/)

[[XSLT(2nd,2008.7)].Doug.Tidwell.文字版.pdf](ed2k://|file|[XSLT(2nd,2008.7)].Doug.Tidwell.文字版.pdf|6498151|34EB7EA76D47301EC1FC7609B805A745|h=JMFDOJL2C2L5SQBQVEHOXJMUQHEDTYZ3|/)

[[Making.Things.See(2012.01)].Greg.Borenstein.文字版.pdf](ed2k://|file|[Making.Things.See(2012.01)].Greg.Borenstein.文字版.pdf|20645840|3b5eba784337f85d1241502c767af7d4|h=6c2krzknyfhgjw5l3hxloj2jzkkwysun|/)

[[Making.Things.See(2012.01)].Greg.Borenstein.文字版.epub](ed2k://|file|[Making.Things.See(2012.01)].Greg.Borenstein.文字版.epub|12140491|1de8b97b2c27c1a49049c80dd940910f|h=odzkyclnypgpj6cfo7upu2da3uctpzhk|/)

[[Environmental.Monitoring.with.Arduino(2012.01)].Emily.Gertz.文字版.pdf](ed2k://|file|[Environmental.Monitoring.with.Arduino(2012.01)].Emily.Gertz.文字版.pdf|17347805|5c0eb036bc832bc1d0ac793e611f4d32|h=t4nkiu53uyvzoo5hlgbfo3ghou56h7q3|/)

[[Environmental.Monitoring.with.Arduino(2012.01)].Emily.Gertz.文字版.epub](ed2k://|file|[Environmental.Monitoring.with.Arduino(2012.01)].Emily.Gertz.文字版.epub|4115901|74ec22c314be96a5a004b709f35390cd|h=o7qo5zivvllw4ofyv2agl6zimygepece|/)

[[Safe.CPP(2012.5)].Vladimir.Kushnir.文字版.pdf](ed2k://|file|[Safe.CPP(2012.5)].Vladimir.Kushnir.文字版.pdf|5746355|606e698ab3e6c5184b6c8658396e4cae|h=fgepy2vda7kxiylk7ugujerc6qvqvyz3|/)

[[Safe.CPP(2012.5)].Vladimir.Kushnir.文字版.epub](ed2k://|file|[Safe.CPP(2012.5)].Vladimir.Kushnir.文字版.epub|2835848|be180707ed27c2f717c696261d7f07fa|h=vaz47rxybyurscvw4lnnun2ctj2dggx6|/)

[[C#.5.0.Pocket.Reference(2012.5)].Joseph.Albahari.文字版.pdf](ed2k://|file|[C#.5.0.Pocket.Reference(2012.5)].Joseph.Albahari.文字版.pdf|3833160|9a4f236e542dacd18b0d8300ad70db93|h=m4whks7e4zhcx7nl2mombbefnpy52uzo|/)

[[C#.5.0.Pocket.Reference(2012.5)].Joseph.Albahari.文字版.epub](ed2k://|file|[C#.5.0.Pocket.Reference(2012.5)].Joseph.Albahari.文字版.epub|2339415|4e1e472e33b04f618c815c90da92f68b|h=ume34dlv2ba6tz3h7l6zmhczjl6xeqcg|/)

[[Learning.iPhone.Programming(2010.3)].Alasdair.Allan.文字版.pdf](ed2k://|file|[Learning.iPhone.Programming(2010.3)].Alasdair.Allan.文字版.pdf|7816358|10dd3ee51d5fafa272903ad438e0dcbe|h=eu2f44evzzg2ycq57g2xudkcnvj7vs4h|/)

[[Learning.iPhone.Programming(2010.3)].Alasdair.Allan.文字版.epub](ed2k://|file|[Learning.iPhone.Programming(2010.3)].Alasdair.Allan.文字版.epub|6406853|29fc55600baf4f7584f9e0b69da18224|h=ybg5g3bddceb4mhjsvcsqql75ehrc3cg|/)

[[Make.a.Mind-Controlled.Arduino.Robot(2011.12)].Tero.Karvinen.文字版.pdf](ed2k://|file|[Make.a.Mind-Controlled.Arduino.Robot(2011.12)].Tero.Karvinen.文字版.pdf|47932372|8e0519ed09ec354eb4789f7f95ff3d19|h=6wp6m2jsnlz3haedhpjnzeb3xmj3m3ia|/)

[[Make.a.Mind-Controlled.Arduino.Robot(2011.12)].Tero.Karvinen.文字版.epub](ed2k://|file|[Make.a.Mind-Controlled.Arduino.Robot(2011.12)].Tero.Karvinen.文字版.epub|7456219|8c7c206a4276e186c0313bd036f54ff0|h=2l6f3wun4fkdj7azo5fxyo72f3dfpuvx|/)

[[Arduino.Cookbook(2nd,2011.12)].Michael.Margolis.文字版.pdf](ed2k://|file|[Arduino.Cookbook(2nd,2011.12)].Michael.Margolis.文字版.pdf|20132016|0e6a1dc8654da3b6b0c852a4373b4239|h=alsx72hczhmttifnufampevf2fysf5v4|/)

[[Arduino.Cookbook(2nd,2011.12)].Michael.Margolis.文字版.epub](ed2k://|file|[Arduino.Cookbook(2nd,2011.12)].Michael.Margolis.文字版.epub|14180901|cab70eb032ec47b26ae58cbec6320282|h=gt3ue2gfmrxseh4lq4qbeebfmbeyuu5x|/)

[[Make：Arduino.Bots.and.Gadgets].T.Karvinen&K.Karvinen.文字版.pdf](ed2k://|file|[Make：Arduino.Bots.and.Gadgets].T.Karvinen&K.Karvinen.文字版.pdf|7225875|7dd6b65500cc6cc5c9bf7b0c03087203|h=itgfumpltmq2ycrnwahfrphipqfkoubp|/)

[[Getting.Started.with.Arduino].Massimo.Banzi.文字版.pdf](ed2k://|file|[Getting.Started.with.Arduino].Massimo.Banzi.文字版.pdf|3921595|4f93cec1505f78b260b52b39ed2b7781|h=4ap2u5vsjxltlohkylysuwqigtzbqe4g|/)

[[Arduino.Cookbook(1st,2011.03)].Michael.Margolis.文字版.pdf](ed2k://|file|[Arduino.Cookbook(1st,2011.03)].Michael.Margolis.文字版.pdf|12922709|3035a4b3fa696c7b9fa0e59d5f50b781|h=mlbanadx57whj7q7xviglws6hy2louxl|/)

[[Arduino.Cookbook(1st,2011.03)].Michael.Margolis.文字版.epub](ed2k://|file|[Arduino.Cookbook(1st,2011.03)].Michael.Margolis.文字版.epub|6691131|afc40c0cf7d2afd3036b49f599319815|h=lpb3cll72cncz7t4fwgp7e5ig7bjhb7z|/)

[[Head.First.C(2012)].David.Griffiths.文字版.pdf](ed2k://|file|[Head.First.C(2012)].David.Griffiths.文字版.pdf|55975961|ccbf3734b7b2961483cb72c01437206f|h=qtzwf3lqyjyd2gnrccqskldgz6lnfsuo|/)

[[Head.First.Android.Development(Early.Release,2011.07)].Jonathan.Simon.文字版.pdf](ed2k://|file|[Head.First.Android.Development(Early.Release,2011.07)].Jonathan.Simon.文字版.pdf|48480921|1e15c6ca4ff82b3af19e31e32d701acc|h=c2d7ydy2dmemmc3qsb5pvatinrqvoqkh|/)

[[Head.First.Mobile.Web(2011.12)].Lyza.Danger.Gardner.文字版.pdf](ed2k://|file|[Head.First.Mobile.Web(2011.12)].Lyza.Danger.Gardner.文字版.pdf|81697741|001df5546b08199912eef7f6ff41706b|h=fmyjwqohq7he55xd6k2pr5nepspwzlyr|/)

[[Head.First.HTML5.Programming(2011)].Eric.Freeman.文字版.pdf](ed2k://|file|[Head.First.HTML5.Programming(2011)].Eric.Freeman.文字版.pdf|88961705|17b78eb0cd23eb03569647f814e6d334|h=2m2rnncumwir4luuogxcb3urzfuw36bs|/)

[[Head.First.jQuery.2011].Ryan.Benedetti.文字版.pdf](ed2k://|file|[Head.First.jQuery.2011].Ryan.Benedetti.文字版.pdf|72237494|14efccaf9c2e42fecd6c3a893d90322b|h=htkee4zsufdp4b6apjrc7dhdn4d6xuua|/)

[[Head.Rush.Ajax.2006].Brett.Mclaughlin.文字版.pdf](ed2k://|file|[Head.Rush.Ajax.2006].Brett.Mclaughlin.文字版.pdf|73224287|60bbaebb193d677f7085655588c97d20|h=3nn6ghxuaaddipubuc3bcpe3hf4zqneh|/)

[[Head.First.Wordpress.2010].Jeff.Siarto.文字版.pdf](ed2k://|file|[Head.First.Wordpress.2010].Jeff.Siarto.文字版.pdf|22055900|597d0c14e126e58e4902342ead8a465b|h=eqieg2wll6zfvmqlbr3n3qyqpb2kts7h|/)

[[Head.First.Web.Design.2008].Ethan.Watrall.文字版.pdf](ed2k://|file|[Head.First.Web.Design.2008].Ethan.Watrall.文字版.pdf|29147952|b9893288ea05bd8f80b83f7bb4bdd14c|h=krmotfaglwb5d7cmgcy7llifqvimo5bv|/)

[[Head.First.SQL.2007].Lynn.Beighley.文字版.pdf](ed2k://|file|[Head.First.SQL.2007].Lynn.Beighley.文字版.pdf|24568559|e742d4889f20d2b46e967b935a5016ce|h=dzgjqddhok2axvwd24cybxhthxzewt5w|/)

[[Head.First.Statistics.2008].Dawn.Griffiths.文字版.pdf](ed2k://|file|[Head.First.Statistics.2008].Dawn.Griffiths.文字版.pdf|24143386|0e087769d3313a4f22caf48491c0f332|h=j4ljfmrmwb3ehd6q74h3onz4l6vwtoec|/)

[[Head.First.Software.Development.2008].Dan.Pilone.文字版.pdf](ed2k://|file|[Head.First.Software.Development.2008].Dan.Pilone.文字版.pdf|49272020|9f64e2790cb925d18e681a705e16391c|h=yrlxkbnlx7w2xyf2w3ykca7zjrviupw5|/)

[[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.pdf](ed2k://|file|[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.pdf|12943410|02963fcc9deda9f538dd559d700784e2|h=5xqx3hprnxfdk63bfozwre5wvuozn5fe|/)

[[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.epub](ed2k://|file|[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.epub|3000096|3cad97f6ac7d5bd2e0f3641e0ed2102d|h=hwnbe4i2xwpoehq6j4s7wioe72qjdjuf|/)

[[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.mobi](ed2k://|file|[Python学习手册(第4版)].(Learning.Python.4th.Edition).Mark.Lutz.文字版.mobi|14095584|1a09197c4c2f4d6333b7bf483807889e|h=466ssnpv4pk5iovfs22odzk6wu3pprd3|/)

[[Head.First.Servlets.and.JSP.2nd.Edition.2008].Bryan.Basham.文字版.pdf](ed2k://|file|[Head.First.Servlets.and.JSP.2nd.Edition.2008].Bryan.Basham.文字版.pdf|41788332|5114a366ab26d2b42dd69b7e6f92563c|h=b4jgpmzfelf5snypcgredokvu4ehboi3|/)

[[Head.First.Rails.2009].David.Griffiths.高清扫描版.pdf](ed2k://|file|[Head.First.Rails.2009].David.Griffiths.高清扫描版.pdf|38314681|4817bacc796dc9ceaf11218ac7f8eb23|h=246ldadqnegtrbtthmj634iy5o6cuqdh|/)

[[Head.First.Python.2010].Paul.Barry.文字版.pdf](ed2k://|file|[Head.First.Python.2010].Paul.Barry.文字版.pdf|39356136|7d10daed9d83cf11be674daa2b7983a0|h=7kqtothsvri5havq4t4mo4om7p66gvha|/)

[[Getting.Started.with.Dwarf.Fortress(2012.5)].Peter.Tyson.文字版.pdf](ed2k://|file|[Getting.Started.with.Dwarf.Fortress(2012.5)].Peter.Tyson.文字版.pdf|33319350|6021bb16ca8dc769dd73e255afd933e6|h=35g44nd7pmx7m4e224goo32na7ci4o3t|/)

[[Getting.Started.with.Dwarf.Fortress(2012.5)].Peter.Tyson.文字版.epub](ed2k://|file|[Getting.Started.with.Dwarf.Fortress(2012.5)].Peter.Tyson.文字版.epub|4252243|c082c8ae683020b2ac22cae1b6d54ccc|h=ibvr4frpw25pdyqovcdc55fgesti3n7n|/)

[[Orchard.CMS：Up.and.Running(2012.5)].John.Zablocki.文字版.pdf](ed2k://|file|[Orchard.CMS：Up.and.Running(2012.5)].John.Zablocki.文字版.pdf|9309581|314ed9abd3ad8667b3e4691e7c83759b|h=7d7ju3uxfqegx7athpuls57jcznhhunw|/)

[[Orchard.CMS：Up.and.Running(2012.5)].John.Zablocki.文字版.epub](ed2k://|file|[Orchard.CMS：Up.and.Running(2012.5)].John.Zablocki.文字版.epub|5314404|4fd6e22d29d0634ce530ee4456be93fc|h=fbpkupilvjxn276422wjxvbsu52wotwz|/)

[[Resource-Oriented.Computing.with.NetKernel(2012.5)].Tom.Geudens.文字版.pdf](ed2k://|file|[Resource-Oriented.Computing.with.NetKernel(2012.5)].Tom.Geudens.文字版.pdf|11705522|f5bee45bd843ed6baf4699299c2b9b26|h=ckiuba3alfflku3gmnrgb5pu2qtykmt4|/)

[[Resource-Oriented.Computing.with.NetKernel(2012.5)].Tom.Geudens.文字版.epub](ed2k://|file|[Resource-Oriented.Computing.with.NetKernel(2012.5)].Tom.Geudens.文字版.epub|7159618|950e88d880aee8136a738e8818a353fe|h=kh6yqo4v2lwh5djhyemejd3wtn37n6sw|/)

[[Web.Workers(2012.5)].Ido.Green.文字版.pdf](ed2k://|file|[Web.Workers(2012.5)].Ido.Green.文字版.pdf|6631078|8ccc742372b6966a83d1c091da6a61f6|h=aap6lebsybxy3nxwse46bbzgcyd5iogv|/)

[[Web.Workers(2012.5)].Ido.Green.文字版.epub](ed2k://|file|[Web.Workers(2012.5)].Ido.Green.文字版.epub|3177600|dd287209a3a1ca7459531d6ed36b2a49|h=mt6aw5yr2wtjyaqt73tdjxydemioutqz|/)

[[YUI.3.Cookbook(2012.5)].Evan.Goer.文字版.pdf](ed2k://|file|[YUI.3.Cookbook(2012.5)].Evan.Goer.文字版.pdf|11097637|5acca8213b99d1f41a6463e86db638d8|h=n4umt4oemffsb435s2c6ptntba7l3z3v|/)

[[YUI.3.Cookbook(2012.5)].Evan.Goer.文字版.epub](ed2k://|file|[YUI.3.Cookbook(2012.5)].Evan.Goer.文字版.epub|2894938|6c4199bdffd4c7cad0d8b26179d6e583|h=fawhr2gdbtiq67xhxjz7efqzt5jff4fc|/)

[[Head.First.Programming(Python).2009].David.Griffiths.文字版.pdf](ed2k://|file|[Head.First.Programming(Python).2009].David.Griffiths.文字版.pdf|17646876|fa5b79c882aa0b4f98b7ac89e0ed2b83|h=mbllgsjmowb5qlrgaora26zk4ywtiqh7|/)

[[The.Art.of.Community(2nd,2012.5)].Jono.Bacon.文字版.pdf](ed2k://|file|[The.Art.of.Community(2nd,2012.5)].Jono.Bacon.文字版.pdf|21482454|6f88444d5d609808a667afd18dd582f3|h=c5vbfckkkx3xryj3znrh4d3xkg5ybrca|/)

[[The.Art.of.Community(2nd,2012.5)].Jono.Bacon.文字版.epub](ed2k://|file|[The.Art.of.Community(2nd,2012.5)].Jono.Bacon.文字版.epub|3353753|2918360242d80e2a1a1d792b91475d2b|h=myhvfoke3bjeeiunbc7o23om7wchcczu|/)

[[Maintainable.JavaScript(2012.5)].Nicholas.C.Zakas.文字版.pdf](ed2k://|file|[Maintainable.JavaScript(2012.5)].Nicholas.C.Zakas.文字版.pdf|9202186|bb196b04a7e03a58bc7118aedae23b67|h=jletprykjdkg67hm44nqkxfi3znm72se|/)

[[Maintainable.JavaScript(2012.5)].Nicholas.C.Zakas.文字版.epub](ed2k://|file|[Maintainable.JavaScript(2012.5)].Nicholas.C.Zakas.文字版.epub|2389760|a44c60d0099d50a6952397e360e93009|h=cavj3s5sk3lepr5vzhgh5thrwxdayqsu|/)

[[Hadoop：The.Definitive.Guide(3rd,2012.5)].Tom.White.文字版.pdf](ed2k://|file|[Hadoop：The.Definitive.Guide(3rd,2012.5)].Tom.White.文字版.pdf|16701598|21f4d8e69966f367334cc5a0aaad73a6|h=p2stgqs6cavzi53xbsoytyne3d6rygne|/)

[[Hadoop：The.Definitive.Guide(3rd,2012.5)].Tom.White.文字版.epub](ed2k://|file|[Hadoop：The.Definitive.Guide(3rd,2012.5)].Tom.White.文字版.epub|5612627|1be76df7e90470b77a113a7817aacc70|h=mhvrto3z5og2bim4wk2plsdtfvrm2obv|/)

[[Head.First.PMP.2nd.Edition.2009].Jennifer.Greene.文字版.pdf](ed2k://|file|[Head.First.PMP.2nd.Edition.2009].Jennifer.Greene.文字版.pdf|49641071|ceb81fef77fd985786d24554c782f088|h=xabss4kmej5hjnr2t4yaupx66xgzng2y|/)

[[Head.First.Physics.2008].Heather.Lang.文字版.pdf](ed2k://|file|[Head.First.Physics.2008].Heather.Lang.文字版.pdf|56032463|2b789827d7c177a531201b3cdf826315|h=kttp5m4qtsgzqxmeptjlkmnnf3ppm3mx|/)

[[Head.First.PHP.and.MySQL.2008].Lynn.Beighley.文字版.pdf](ed2k://|file|[Head.First.PHP.and.MySQL.2008].Lynn.Beighley.文字版.pdf|33859018|e425a40f4541a107019cbf4b9601337a|h=zf2ysndf3ayfpe4swcx76g4e6m3ndu2h|/)

[[Head.First.Object.Oriented.Analysis.and.Design.2006].Brett.D.McLaughlin.文字版.pdf](ed2k://|file|[Head.First.Object.Oriented.Analysis.and.Design.2006].Brett.D.McLaughlin.文字版.pdf|32023841|43fed73ccfc7e5e21956d32697ec29ad|h=5pi2veszrnkp52xiu2347dtdxi3fl4is|/)

[[Cocoa.and.Objective-C：Up.and.Running(2010.3)].Scott.Stevenson.文字版.pdf](ed2k://|file|[Cocoa.and.Objective-C：Up.and.Running(2010.3)].Scott.Stevenson.文字版.pdf|10947810|334a0a2c2c0ede440f9c5eb58a7e532a|h=7rtdmdlcwcjqkpwwzqmdkaq4cgm43gyw|/)

[[Cocoa.and.Objective-C：Up.and.Running(2010.3)].Scott.Stevenson.文字版.epub](ed2k://|file|[Cocoa.and.Objective-C：Up.and.Running(2010.3)].Scott.Stevenson.文字版.epub|9577822|93f5720d6b6c8a34ec8d2ea43e84a886|h=rk3iwgedvgxfia5inkeh2qxge7osixhi|/)

[[Mac.OS.X.for.Unix.Geeks(4th,2008.9)].Ernest.E.Rothman.文字版.pdf](ed2k://|file|[Mac.OS.X.for.Unix.Geeks(4th,2008.9)].Ernest.E.Rothman.文字版.pdf|12003387|50ed69464fbb7b87b744c9011374e299|h=3b3dg46q6sldbn2tj7s2msnfco74hqdl|/)

[[Mac.OS.X.for.Unix.Geeks(4th,2008.9)].Ernest.E.Rothman.文字版.epub](ed2k://|file|[Mac.OS.X.for.Unix.Geeks(4th,2008.9)].Ernest.E.Rothman.文字版.epub|10535290|1c0c3a4dfa9024f3de8210c8bb6c629f|h=qeegfzptn4iya5xkjalypu5ggorgtp4k|/)

[[Make：Technology.on.Your.Time.Volume.30(2012.4)].Mark.Frauenfelder.pdf](ed2k://|file|[Make：Technology.on.Your.Time.Volume.30(2012.4)].Mark.Frauenfelder.pdf|23271479|d1cf5259eb990ffc18d1ebfd8ce96794|h=a4br4c4buo26fk7rbje62kjd7ldh5rt5|/)

[[Learn.to.Solder(2012.5)].Brian.Jepson.文字版.pdf](ed2k://|file|[Learn.to.Solder(2012.5)].Brian.Jepson.文字版.pdf|59423676|4b8191a3554ad1ced414fb743a7f8f5b|h=qfur4jvlrvm5i75vonyqxfr54r4gx5hh|/)

[[Learn.to.Solder(2012.5)].Brian.Jepson.文字版.epub](ed2k://|file|[Learn.to.Solder(2012.5)].Brian.Jepson.文字版.epub|5070596|ca9191680000efaee9a0b329a3d6be6d|h=zocp5nodec3wwnsdkjvkdcw2vcfvocbl|/)

[[Illustrated.Guide.to.Home.Biology.Experiments(2012.4)].Robert.Bruce.Thompson.文字版.pdf](ed2k://|file|[Illustrated.Guide.to.Home.Biology.Experiments(2012.4)].Robert.Bruce.Thompson.文字版.pdf|58196092|e67d26535d8d57809d919b9e650bd67d|h=ksk7nvfuc7imtwrp6725g5zs5sw33yy4|/)

[[Illustrated.Guide.to.Home.Biology.Experiments(2012.4)].Robert.Bruce.Thompson.文字版.epub](ed2k://|file|[Illustrated.Guide.to.Home.Biology.Experiments(2012.4)].Robert.Bruce.Thompson.文字版.epub|25642963|de7ada940386d599da67eaa52f8ff8a5|h=6r24n3ambsndqt3mpynwzkhafsbn23vg|/)

[[Getting.Started.with.NET.Gadgeteer(2012.5)].Simon.Monk.文字版.pdf](ed2k://|file|[Getting.Started.with.NET.Gadgeteer(2012.5)].Simon.Monk.文字版.pdf|11964072|3f9144d0a1842b343ac0b820c641b131|h=fesoq37ehe5oumjiyseoodg5yoq4wj3r|/)

[[Getting.Started.with.NET.Gadgeteer(2012.5)].Simon.Monk.文字版.epub](ed2k://|file|[Getting.Started.with.NET.Gadgeteer(2012.5)].Simon.Monk.文字版.epub|4801853|034b6fed100c23c4fdc3c54da8ec2824|h=sk4hfqwz7e3yoaa3trw5wu3mvau3qcsw|/)

[[Fitness.for.Geeks(2012.4)].Bruce.W.Perry.文字版.pdf](ed2k://|file|[Fitness.for.Geeks(2012.4)].Bruce.W.Perry.文字版.pdf|63955373|dd01cc7cf4dc0c9ff949b9552855b7dc|h=thgs54zbjelfejl24dnpaot4p3xhvdvs|/)

[[Fitness.for.Geeks(2012.4)].Bruce.W.Perry.文字版.epub](ed2k://|file|[Fitness.for.Geeks(2012.4)].Bruce.W.Perry.文字版.epub|20653646|b3aaf42e005196b261c06f4c22c28d04|h=7k4qidv3ilumxx5vqxeyjetysbb4ssrp|/)

[[Practical.Computer.Vision.with.SimpleCV(2012.5)].Kurt.Demaagd.文字版.pdf](ed2k://|file|[Practical.Computer.Vision.with.SimpleCV(2012.5)].Kurt.Demaagd.文字版.pdf|39625880|cca3d412372b7b3eb98f5751762f3d80|h=ig7lhqnbgeyqpqyu2cwjlhxa5cnbveop|/)

[[Practical.Computer.Vision.with.SimpleCV(2012.5)].Kurt.Demaagd.文字版.epub](ed2k://|file|[Practical.Computer.Vision.with.SimpleCV(2012.5)].Kurt.Demaagd.文字版.epub|9867313|b6d42c6e5ecb0af5b2671218f1b570a8|h=g3vfocwy5gogu2tx33vdvbqbp35toyft|/)

[[Head.First.Networking.2009].Al.Anderson.文字版.pdf](ed2k://|file|[Head.First.Networking.2009].Al.Anderson.文字版.pdf|38862062|a0dcb7e55f6a08feb00064217b2923b1|h=gwa6vmatemdyahx6kn4wpepmqhdceban|/)

[[Head.First.JavaScript.2008].Michael.Morrison.文字版.pdf](ed2k://|file|[Head.First.JavaScript.2008].Michael.Morrison.文字版.pdf|40374051|6eaae20dc3665368fb676229a52ff267|h=q642hdda7nqi3qlp77i4om466rle5wgg|/)

[[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.pdf](ed2k://|file|[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.pdf|5760115|821a29d968d4b96044501cb588a401ce|h=f3tfm5d2q6kbm6mjimzuj7ibyewrgqmf|/)

[[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.epub](ed2k://|file|[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.epub|10729122|bdf87f023457155f64d26d12597e5bad|h=7jridoecqdczc3fvge3lhez6rtfmqwpc|/)

[[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.mobi](ed2k://|file|[YouTube：An.Insider's.Guide.to.Climbing.the.Charts(2008.11)].Alan.Lastufka.文字版.mobi|10564135|aa915fafd341afd061238bde9e3860ba|h=3pq5ndb6776xy7kntdkhxy7lhyeiqnnz|/)

[[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.pdf](ed2k://|file|[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.pdf|10031609|5cc2c27c67b28fb778d4a3b2221f5292|h=lzkfoouwhdcjxdlcjf7c6uagbmmeje3k|/)

[[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.epub](ed2k://|file|[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.epub|5205321|9ca160ec8cbb68c41d15622b2d6d149b|h=y2xio2qnzaf2l43h7qnam7urgubodlj7|/)

[[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.mobi](ed2k://|file|[Mobile.Development.with.C#(2012.5)].Greg.Shackles.文字版.mobi|3918616|1bb849a9a2d28479d61014adecdc0853|h=6ejpk2xhdr7bczots6ztqxgxxpgan7r3|/)

[[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.pdf](ed2k://|file|[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.pdf|125533722|34581045949bdb30a154f3bcdb32c996|h=uwpne6uzueyx55cpl7ik6ymcopbb2jou|/)

[[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.epub](ed2k://|file|[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.epub|27212590|b9bb7407ebdb3a7680070f3fe621917a|h=ijxpsejaicsce7phbziumr6ch2wxipzs|/)

[[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.mobi](ed2k://|file|[Mobile.Design.Pattern.Gallery(Color.Edition,2012.5)].Theresa.Neil.文字版.mobi|24211548|a88e02adac886054ce500459560519f2|h=pexnrw2x5pedvi27wau4jffieva56sdn|/)

[[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.pdf](ed2k://|file|[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.pdf|8789226|1ee99a3303d737af151bdf072d6d1489|h=ubt3v7dfzhliyl73f2uueo3xl4w6ecyi|/)

[[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.epub](ed2k://|file|[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.epub|2546274|d4ed7fad841140a458b76f4c32628ba1|h=ljnts7ynzpm4jodl25rek6ljuoqdoppk|/)

[[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.mobi](ed2k://|file|[Developing.Web.Applications.with.Haskell.and.Yesod(2012.4)].Michael.Snoyman.文字版.mobi|1100586|2028a17d4a7ca0c7951050cfe3e01b07|h=rgouvl4ujlfhfeprgg7mrts6hs77ht7f|/)

[[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.pdf](ed2k://|file|[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.pdf|32558146|8ef7343c96cb6cfa53610c73f86967d5|h=ai6gtysyouduiddqa7aqetcj6ozdomgi|/)

[[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.epub](ed2k://|file|[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.epub|3102167|05d7860b4fed10152b4d3e4bef205100|h=6p6mrangv4wlmnmnfxv662ikploc4rio|/)

[[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.mobi](ed2k://|file|[Database.Design.and.Relational.Theory(2012.4)].C.J.Date.文字版.mobi|1461467|84c6cd1f847632f26ece488ccc0524cd|h=bmmvslcwvcwsbhvl76qc7qd3de7d27rp|/)

[[Head.First.Java.2nd.Edition.2005].Kathy.Sierra.文字版.pdf](ed2k://|file|[Head.First.Java.2nd.Edition.2005].Kathy.Sierra.文字版.pdf|35076843|d37b74f3f237f61ee6e1e28d8d82ac84|h=whr2p55eyi2dixo5lz7fl6iwobnpv4hy|/)

[[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.pdf](ed2k://|file|[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.pdf|8262125|008716696abc5c143cc6b4fadfaa6081|h=7yci3stdb4s5h7bmrs3sfxz6pz7pl6ux|/)

[[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.epub](ed2k://|file|[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.epub|2305727|8bf45c3e79ca9e6e6f6395eed1b877e9|h=v5zkuqphiwif3m4c7bcsfea6fqhrmcz5|/)

[[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.mobi](ed2k://|file|[Node：Up.and.Running(2012.4)].Tom.Hughes-Croucher.文字版.mobi|533119|a0bbcf7e0302d6b1a3ecdff16cea6916|h=uiukkoyu6bcoqxzrt3zfbbttigys3hui|/)

[[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.pdf](ed2k://|file|[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.pdf|2429733|d59eb5414d35285902d8d5eb69cc6f4f|h=hkz5vzwgextrqjaxfbbuyojlc2vwrymy|/)

[[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.epub](ed2k://|file|[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.epub|2311588|16bc324711a4458b97119df81b7a7a76|h=5kkkfifxbjfjcqc37lrui6264t36fkjm|/)

[[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.mobi](ed2k://|file|[Learning.JavaScript.Design.Patterns(Early.Release,2012.4)].Addy.Osmani.文字版.mobi|748625|fca13d0bb3567efc4954bc53a064c853|h=x3ntjavpqkrv74ypngipgof3x4wgiwno|/)

[[HLSL.and.Pixel.Shaders.for.XAML.Developers(Early.Release,2012.4)].Walt.Ritscher.文字版.pdf](ed2k://|file|[HLSL.and.Pixel.Shaders.for.XAML.Developers(Early.Release,2012.4)].Walt.Ritscher.文字版.pdf|5736485|d1db7c80d289ea89646414038896959c|h=sk5fvs7fwoeoni22xe5jum4tajooqfnl|/)

[[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.pdf](ed2k://|file|[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.pdf|3944474|5a35d8363d8fa3fb673336a2aca17006|h=2qsrc5qp4di5qxpzc43zoxedugzjsnm3|/)

[[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.epub](ed2k://|file|[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.epub|2860151|4dc44baf8d216ea64e9339d3784d3e0e|h=xmfkqfl6ceu5z4xg6tfrzqtlt2qriezt|/)

[[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.mobi](ed2k://|file|[Getting.Started.with.Metro.Apps(Early.Release,2012.4)].Ben.Dewey.文字版.mobi|1118465|7a216a234ec43eebb973197e117fa4f6|h=gj7kwwk2kkugorft7vu2bwkem6yc4g6q|/)

[[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.pdf](ed2k://|file|[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.pdf|2659011|2c52ffd95d04d342a58978110fb9e17a|h=y6zy5vroxestqm7u5of56bpwbduve7fw|/)

[[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.epub](ed2k://|file|[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.epub|2520280|aee18ec8ae189a6260eae2bc80800dba|h=crvw5cwufzafnywitqn3e5lbyasli6zu|/)

[[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.mobi](ed2k://|file|[Developing.Backbone.js.Applications(Early.Release,2012.4)].Addy.Osmani.文字版.mobi|997383|d45308932ada380ddc84c55aa9e2839f|h=2d7lce5sefmdfn45yem2cmndts2x7mvs|/)

[[Head.First.iPhone.Development.2009].Dan.Pilone.文字版.pdf](ed2k://|file|[Head.First.iPhone.Development.2009].Dan.Pilone.文字版.pdf|17321892|bf97706e969a120823da5a82d7ebe3aa|h=5obgkiyla6vzoirlveybyqg36mnhnpoj|/)

[[Head.First.iPhone.and.iPad.Development.2nd.Edition.2011].Dan.Pilone.文字版.pdf](ed2k://|file|[Head.First.iPhone.and.iPad.Development.2nd.Edition.2011].Dan.Pilone.文字版.pdf|64169795|a3988b84723333466a02f6598747f997|h=j5bwwoz5grtqokvj2vu5lsoo3aeguyhk|/)

[[Head.First.HTML.with.CSS.XHTML.2005].Eric.T.Freeman.文字版.pdf](ed2k://|file|[Head.First.HTML.with.CSS.XHTML.2005].Eric.T.Freeman.文字版.pdf|21821155|736bb68d39293cf67b5541314c825589|h=uy4oqy5w736m4jflnrvdbrnvpauzzbsr|/)

[[Head.First.Excel.2010].Michael.Milton.文字版.pdf](ed2k://|file|[Head.First.Excel.2010].Michael.Milton.文字版.pdf|22034717|98a9afe76b12f419e15ea0b6d827b457|h=nu3pmvdn4n4irqvvh7ntp3x7m6laleyn|/)

[[Head.First.EJB.2003].Kathy.Sierra.文字版.pdf](ed2k://|file|[Head.First.EJB.2003].Kathy.Sierra.文字版.pdf|23104811|e55afaea1f243255d1356ea63467df50|h=xvlrqpb5wgfo4sizcnk3fifabowjvzdg|/)

[[Head.First.Design.Patterns.2004].Elisabeth.Freeman.文字版.pdf](ed2k://|file|[Head.First.Design.Patterns.2004].Elisabeth.Freeman.文字版.pdf|32096353|e2b5ee581fcdb5583b6bc94642967cc0|h=lvdtpvyyf4jg35wdqma2n6akslfcsxbh|/)

[[Head.First.Data.Analysis.2009].Michael.Milton.文字版.pdf](ed2k://|file|[Head.First.Data.Analysis.2009].Michael.Milton.文字版.pdf|36540614|9b84c7c98596b9ec14d1e14d85967e19|h=qyyalhsf633v7vmkvnrgnw3jtugiz3mu|/)

[[Head.First.C#.2nd.Edition.2010].Andrew.Stellman.文字版.pdf](ed2k://|file|[Head.First.C#.2nd.Edition.2010].Andrew.Stellman.文字版.pdf|30186745|87ac4be5e0734a0e3f2a70b3662aab08|h=ntytgroqlcyj62lwy2tmrdzlhfumprxp|/)

[[Head.First.Algebra.2009].Dan.Pilone.文字版.pdf](ed2k://|file|[Head.First.Algebra.2009].Dan.Pilone.文字版.pdf|48137619|c3dc6f7a80979dd3d600a8079089458c|h=7rcauakqzxz7pi4imzervadvnw3www6o|/)

[[Head.First.Ajax.2008].Rebecca.M.Riordan.文字版.pdf](ed2k://|file|[Head.First.Ajax.2008].Rebecca.M.Riordan.文字版.pdf|34735830|e9aad8a15d3673270489092a66fc763c|h=ktldtww475p4jhh237c4cxyiardmxnwt|/)

[[Head.First.2D.Geometry.2009].Stray.文字版.pdf](ed2k://|file|[Head.First.2D.Geometry.2009].Stray.文字版.pdf|23009065|56f47c4ab9e85dc1848cd964c3aad3d4|h=hk6vzbmh5wskhbte2tnjxswrckzao66e|/)

[[Learning.Oracle.PLSQL(2001)].Bill.Pribyl.文字版.pdf](ed2k://|file|[Learning.Oracle.PLSQL(2001)].Bill.Pribyl.文字版.pdf|5307791|5b86d6b5a2042c82505e5fa7bd60026d|h=rv5or5lleunennuv7xasjkyxojw7d3w7|/)

[[Oracle.SQL.Loader：The.Definitive.Guide(2001)].Jonathan.Gennick.文字版.pdf](ed2k://|file|[Oracle.SQL.Loader：The.Definitive.Guide(2001)].Jonathan.Gennick.文字版.pdf|2171474|5839602c2825795dee1ad61249e36f76|h=uv2mgi2xawulzb4ej2bxpfiswvaqypeo|/)

[[Oracle.PLSQL.Programming：A.Developer's.Workbook(2000.05,Covers.Oracle8i)].Steven.Feuerstein.文字版.pdf](ed2k://|file|[Oracle.PLSQL.Programming：A.Developer's.Workbook(2000.05,Covers.Oracle8i)].Steven.Feuerstein.文字版.pdf|4273914|b0f679f6229f2d15f6dfb1bcf318a85b|h=wetwsj7kl35owxjo6gpczc3d4lpmx7vi|/)

[[Oracle.PLSQL.Programming：A.Developer's.Workbook(2000.05,Covers.Oracle8i)].Steven.Feuerstein.文字版.epub](ed2k://|file|[Oracle.PLSQL.Programming：A.Developer's.Workbook(2000.05,Covers.Oracle8i)].Steven.Feuerstein.文字版.epub|1327288|d924fe6af23f8ebd61e612551656dfb1|h=255tjtq4pa45asemyxpe4sphpqsu6ntx|/)

[[Perl.for.Oracle.DBAs(2002)].Andy.Duncan.文字版.pdf](ed2k://|file|[Perl.for.Oracle.DBAs(2002)].Andy.Duncan.文字版.pdf|9729996|730df97337813b2649ac0488cd8921f6|h=i4hxnfkdswslqahhejp6myf5se5axrwy|/)

[[Perl.for.Oracle.DBAs(2002)].Andy.Duncan.文字版.epub](ed2k://|file|[Perl.for.Oracle.DBAs(2002)].Andy.Duncan.文字版.epub|4353469|be0c65e044587f20c96b1257a73a2741|h=ln5mittaxa45wvohsuzbfwjbija6kgf2|/)

[[Oracle.Regular.Expressions.Pocket.Reference(2003)].Jonathan.Gennick.文字版.pdf](ed2k://|file|[Oracle.Regular.Expressions.Pocket.Reference(2003)].Jonathan.Gennick.文字版.pdf|928867|4bb7e9d3f70b58d6450b704130fed924|h=jumjkmpkjnczjcoz7okayqcao6nsnlik|/)

[[Oracle.Regular.Expressions.Pocket.Reference(2003)].Jonathan.Gennick.文字版.epub](ed2k://|file|[Oracle.Regular.Expressions.Pocket.Reference(2003)].Jonathan.Gennick.文字版.epub|2500885|c312fc9ca01bc75602f2171176f6c49e|h=y4k56syu3i7hglntp2bnigj4uvthjdc6|/)

[[Oracle.Essentials(4th,2007,11g)].Rick.Greenwald.文字版.pdf](ed2k://|file|[Oracle.Essentials(4th,2007,11g)].Rick.Greenwald.文字版.pdf|3796880|c97ec0b03de03282fb943f6080e15c53|h=evxhwyvohp4pvxfkqmx3yxt6r7z4rdqe|/)

[[Oracle.Essentials(4th,2007,11g)].Rick.Greenwald.文字版.epub](ed2k://|file|[Oracle.Essentials(4th,2007,11g)].Rick.Greenwald.文字版.epub|5176390|8f7a209529d3fe3105f0b670da737f20|h=u7ed6jtd7wx2zcsrzzuimiplm33wlyr7|/)

[[Oracle.PLSQL.Language.Pocket.Reference(4th,2007)].S.Feuerstein.文字版.pdf](ed2k://|file|[Oracle.PLSQL.Language.Pocket.Reference(4th,2007)].S.Feuerstein.文字版.pdf|869419|ef63f98ea443bcfc4dc667014b1b0aaa|h=syasxixz7kozfreamsylg4d3iprgaqu6|/)

[[Oracle.SQL.Plus.Pocket.Reference(3rd,2004)].Jonathan.Gennick.文字版.pdf](ed2k://|file|[Oracle.SQL.Plus.Pocket.Reference(3rd,2004)].Jonathan.Gennick.文字版.pdf|1742894|62750a75710761519830ad409826b7a1|h=i6nkhhbx52jb664emrzigreyj5p67yvh|/)

[[Toad.Pocket.Reference.for.Oracle(2nd,2005)].Jeff.Smith.文字版.pdf](ed2k://|file|[Toad.Pocket.Reference.for.Oracle(2nd,2005)].Jeff.Smith.文字版.pdf|1630079|d0761188b7e8ebe71ef43b5e431aec9d|h=mxyd66bzqu33sugf5v2r2beyi3amrj7m|/)

[[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.pdf](ed2k://|file|[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.pdf|6782504|71646806231ac9fa44dd807bb8bbcfa2|h=ajlbazelja2k4lnyxvg6ptx4ad4mosbq|/)

[[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.epub](ed2k://|file|[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.epub|9461812|80b1a61448a6c119e20183deaf349f41|h=v3qc6rb7kqz3mk7tvatrcogpjm6aqfco|/)

[[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.mobi](ed2k://|file|[Programming.the.Mobile.Web(2010.7)].Maximiliano.Firtman.文字版.mobi|10636079|7e8dc51fc3c99748d2b17bede7074629|h=xjlfcdsqkcb37bqwtd66vko6ezssnd3y|/)

[[Oracle.DBA.Pocket.Guide(2005)].David.C.Kreines.文字版.pdf](ed2k://|file|[Oracle.DBA.Pocket.Guide(2005)].David.C.Kreines.文字版.pdf|1247586|16ed363eba226198ba9ec7a24713bc3b|h=jm5y26zngdbteobtxqmfl5zictknr2xq|/)

[[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.pdf](ed2k://|file|[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.pdf|1696202|4b13c5f35c8207d6a0fe4436877f1232|h=syilugahw2lkqsczi2hmr3k2w2tuohla|/)

[[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.epub](ed2k://|file|[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.epub|1000835|db7e837877a48f8dee9b75f615ab2cc2|h=hzhywc4545a7nyjdq57v2elzrmyf63cb|/)

[[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.mobi](ed2k://|file|[Scaling.MongoDB(2011.1)].Kristina.Chodorow.文字版.mobi|1037079|8b99872b6bbb15544ce9b00e99a7d0ff|h=o2lv2c2h4e4goiis2su3sbpda2twgugn|/)

[[Programming.F#.3.0(2nd,2012.4,Early.Release)].Chris.Smith.文字版.pdf](ed2k://|file|[Programming.F#.3.0(2nd,2012.4,Early.Release)].Chris.Smith.文字版.pdf|6895017|d34e6e674a24d77a2aa670298f881bf4|h=pbmzdpne2anesrjbtwbsxorqvivpxzsv|/)

[[C#.5.0.Pocket.Reference(2012.4,Early.Release)].Joseph.Albahari.文字版.pdf](ed2k://|file|[C#.5.0.Pocket.Reference(2012.4,Early.Release)].Joseph.Albahari.文字版.pdf|2110363|9dadde7b99b6fdf3f2f4720e35d138ee|h=77aruvixy3fwezvid6463atd4dhgwn3u|/)

[[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.pdf](ed2k://|file|[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.pdf|21718955|9e9828fed2ee38a7463dd0cde8d5f0df|h=3mea3gfrr3jkaczcdrcaugqzrgr2mnmh|/)

[[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.epub](ed2k://|file|[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.epub|11675985|584118529132e91e232bb9ebf6c63bbf|h=7nhk6qdgc6irvamo2puq3m2r4rr7l3fq|/)

[[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.mobi](ed2k://|file|[Android.Cookbook(2012.4)].Ian.F.Darwin.文字版.mobi|12315791|8528cb3723f83ecb632e477eaf3315ba|h=wttkv3zailqiq6an5xlnuqqcgptuuwfj|/)

[[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.pdf](ed2k://|file|[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.pdf|33547221|b21e42ae315f5aca9b8fff60c1634b99|h=lpr6ar3th4iofvqr3icdbeobkovnsbpu|/)

[[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.epub](ed2k://|file|[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.epub|28449409|b5acd1717297e67b380f23c92d653c69|h=7hlifqpiy42rq6tzacqb4olku46ned3e|/)

[[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.mobi](ed2k://|file|[Using.Drupal(2nd,2012.4)].Angela.Byron.文字版.mobi|30485664|aeba4cf70aab4c94ab139af1ab9fe209|h=cejgyxtznbatmng6inaz4g22x4tzukmh|/)

[[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.pdf](ed2k://|file|[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.pdf|11130657|2678d2cb2ad94e3abdb4acf0c5a44420|h=lylz5dducnaqvkyfegjs6suf4g7naxmi|/)

[[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.epub](ed2k://|file|[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.epub|6408535|d3a5bcf7b72c1c096c256795971b35e1|h=zhgwchhouwykmwfsobdgkzza3ea32jz6|/)

[[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.mobi](ed2k://|file|[PayPal.APIs：Up.and.Running(2nd,2012.4)].Matthew.A.Russell.文字版.mobi|4828355|51edaad091e56249c4bd8cc5b793b4cc|h=rvuh6kcarzqrit6pehu4yhv6wksckqou|/)

[[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.pdf](ed2k://|file|[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.pdf|6486219|7e656323a2c62f99faeb0390f4e2d34b|h=rtxayjrtw7j34to3nuu6isex3w4uajou|/)

[[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.epub](ed2k://|file|[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.epub|2454815|a0a6e8f4aa5c7a3ffe61177349d1e057|h=6mc7kqtvuoivichmgmirzyr62a3ektaj|/)

[[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.mobi](ed2k://|file|[JavaScript.Pocket.Reference(3rd,2012.4)].David.Flanagan.文字版.mobi|729924|b80114d1354e38958b367ed3798008fe|h=zfuctsqlc2s23aqmk7vr23xeikomo7kf|/)

[[25.Recipes.for.Getting.Started.with.R(2011.1)].Paul.Teetor.文字版.pdf](ed2k://|file|[25.Recipes.for.Getting.Started.with.R(2011.1)].Paul.Teetor.文字版.pdf|1167687|f812c6be975e2cfbccdfd644305f3f34|h=fdv3eayr2e7fpli45y74ndcxvfk5f57y|/)

[[谷歌无人驾驶汽车编程].(Week1.Basics.of.probability).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week1.Basics.of.probability).rar|314383612|66070b86ba41a6bfce4c4685a57dc4a0|h=52tvguchieraofwuzbdwu5lcv6nbbf4i|/)

[[谷歌无人驾驶汽车编程].(Week2.Gaussians.and.continuous.probability).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week2.Gaussians.and.continuous.probability).rar|377003346|3EDA9273CD90D86C07066670C505ADB9|h=CT2LQ62W2K7AUXDYRLFWGLKX2Z3XPUB3|/)

[[谷歌无人驾驶汽车编程].(Week3.Image.Processing.and.Machine.Learning).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week3.Image.Processing.and.Machine.Learning).rar|370416821|65574C888F2FA1233E8FD31DFF101DC5|h=6GBKWEN6BGNOWVZI4TPKL3FLQERASRQK|/)

[[谷歌无人驾驶汽车编程].(Week4.Planning.and.search).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week4.Planning.and.search).rar|363639262|3DD8253FFB2D68B913B8B330D7005013|h=CQYNGMJL7AXK2XDCDHU7YTPVZ7FF262L|/)

[[谷歌无人驾驶汽车编程].(Week5.Controls).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week5.Controls).rar|314907047|40B1A08E31BAE722121AC1C30DC629CF|h=3QL47OKUAQYKHRMTAUCFYU7FPEB6EBQY|/)

[[谷歌无人驾驶汽车编程].(Week6.Putting.it.all.together).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week6.Putting.it.all.together).rar|330457445|609919E23EFDB5EBD777017CEA455F1C|h=7IDD72SUUN55PD255ZHEFEOMS6WL3TKO|/)

[[谷歌无人驾驶汽车编程].(Week7.Final.Exam).rar](ed2k://|file|[谷歌无人驾驶汽车编程].(Week7.Final.Exam).rar|120195325|1683481135CA8E2FE29C5C995FCBC11F|h=RM7UF2JIW5TQDWGENJL7AXVIJGYG6NUC|/)

[[21.Recipes.for.Mining.Twitter(2011.1)].Matthew.A.Russell.文字版.pdf](ed2k://|file|[21.Recipes.for.Mining.Twitter(2011.1)].Matthew.A.Russell.文字版.pdf|1016061|f673bbe229b4df572c7ca80e45c13105|h=idd7hnr6r2mthbrm6kylgghofqixyhon|/)

[[Windows.PowerShell.for.Developers(Early.Release,2012.3)].Douglas.Finke.文字版.pdf](ed2k://|file|[Windows.PowerShell.for.Developers(Early.Release,2012.3)].Douglas.Finke.文字版.pdf|3714634|a7092564d2a6d4143440bf71d8fa2c42|h=xkkgdyoes4uhyvwo64sokxzpcd5d3k7q|/)

[[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.pdf](ed2k://|file|[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.pdf|11934920|2d01ed8cc2ccfd88db1ac210a6b793f8|h=jrlwxlo6icbuj3gihgk7jb2cgjtjtwkj|/)

[[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.epub](ed2k://|file|[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.epub|4618839|7f58184c4f2d0242afef5dab73178e2a|h=ossrcp74gr3mnmkx4jz7faqne6uqn25z|/)

[[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.mobi](ed2k://|file|[802.11n：A.Survival.Guide(2012.4)].Matthew.Gast.文字版.mobi|2564844|e5b625008d1ab1b5413c705136e3ab99|h=kfm4tunbws3tviq2nou5a5si4yy65i2y|/)

[[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.pdf](ed2k://|file|[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.pdf|9297797|fe5fd96c4f6b7463fae7cf2baae07dca|h=74pp5tejfzuiqtqv5d6nzex7i3izez32|/)

[[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.epub](ed2k://|file|[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.epub|5406030|f8286f37a1a80e8bb024bff848eb4b1e|h=foqfbuai2ra5k4c2rcee2dgu7rjlgpud|/)

[[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.mobi](ed2k://|file|[SharePoint.Apps.with.LightSwitch(2012.4)].Paul.Ferrill.文字版.mobi|5926780|0011ce8c3406056641bf176528fa012f|h=v7yjzeb5bsij7rzcvcmmualqz5xgd2hd|/)

[[Clojure.Programming(2012.3)].Chas.Emerick.文字版.pdf](ed2k://|file|[Clojure.Programming(2012.3)].Chas.Emerick.文字版.pdf|9781594|19c552e0c7a03c109b214002e0d52a3f|h=sdskzbpkjljuheqwhqzk5hiwbplffsn3|/)

[[Clojure.Programming(2012.3)].Chas.Emerick.文字版.epub](ed2k://|file|[Clojure.Programming(2012.3)].Chas.Emerick.文字版.epub|3837293|04fee31bd45eea265ff078856f6f7f1c|h=27ej5g3ehaf4ty5lon5rwoywh7jgds5f|/)

[[Clojure.Programming(2012.3)].Chas.Emerick.文字版.mobi](ed2k://|file|[Clojure.Programming(2012.3)].Chas.Emerick.文字版.mobi|3019938|5a6da22164e81f13015811ec6eb2711f|h=7wytd7ejnc2lyy4e6soqjzozsdpyw6kd|/)

[[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.pdf](ed2k://|file|[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.pdf|4497409|79bc31d37bc2f3798cbf2f9e055b17da|h=eo6isviz3jzyly62orp6qr4z5wletejo|/)

[[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.epub](ed2k://|file|[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.epub|2211772|4fa014e69454da76820aca8714ba6ff7|h=pdl6bdpp6757ectnib3jcync3tte4fye|/)

[[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.mobi](ed2k://|file|[Just.Spring.Integration(2012.4)].Madhusudhan.Konda.文字版.mobi|368527|08f90082e1c16992e2d2e57755e09236|h=znojegq7gte6cehqpwwqvrtglyc27rxx|/)

[[Learning.Android(2011.3)].Marko.Gargenta.文字版.pdf](ed2k://|file|[Learning.Android(2011.3)].Marko.Gargenta.文字版.pdf|10013300|2d95179c4b8006a7662b31b32df59501|h=xxdk5h6wfr2522ibomfvsezfwvv7gfld|/)

[[Learning.Android(2011.3)].Marko.Gargenta.文字版.epub](ed2k://|file|[Learning.Android(2011.3)].Marko.Gargenta.文字版.epub|5169416|6d0969726499f1f0dbce5b7fcfd6c23b|h=mijeg3bw2idoqwziheyqchvhebbm6ltt|/)

[[Learning.Android(2011.3)].Marko.Gargenta.文字版.mobi](ed2k://|file|[Learning.Android(2011.3)].Marko.Gargenta.文字版.mobi|4294073|6097915e44164934c374099cac63c085|h=olpmh5dwomrrhgv2iap3rbgbtthlqznf|/)

[[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.pdf](ed2k://|file|[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.pdf|34800745|6842700537b3336665904605ec59921d|h=7kay7yhudy32kyz4vrtqtymtisihpfz2|/)

[[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.epub](ed2k://|file|[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.epub|36713140|ef58e608a7252c8e030e6bbf2827565b|h=6rwanbnbeycu6jfszjcxgd6uhrwtsbii|/)

[[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.mobi](ed2k://|file|[Learning.iOS.Programming(2nd,2012.3)].Alasdair.Allan.文字版.mobi|23312869|8f5ccd59a2a7c7f94befef43e6f7033e|h=wyn3gzbe3g4vfhiwegjxli2lqx3mmvwa|/)

[[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.pdf](ed2k://|file|[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.pdf|2937163|456b9c13a5bc7a8ef1e15345fe2edeb0|h=u2nj53jqvdrhpab4rv62ecmn5frblvvt|/)

[[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.epub](ed2k://|file|[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.epub|2875829|78a7859fa1d6c9c93d863d6cf14dc57c|h=e5vik32gxra33u6paacloiqkngpip6zr|/)

[[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.mobi](ed2k://|file|[Designing.Great.Data.Products(2012.3)].Jeremy.Howard.文字版.mobi|745244|d46b58df7e62241ce57c48ae1d541739|h=5323qc3kry65vkewqz3w2evrcw3tqei2|/)

[[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.pdf](ed2k://|file|[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.pdf|6030276|83b8ac436d5ca7f4b757ec3d9a9f41db|h=mt22zfguh2m2i3olzcsdtbaddyujmveg|/)

[[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.epub](ed2k://|file|[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.epub|2176918|a5c368b580013f49f8c8afaf6b138a24|h=fsyvclj6j4jmoecys2f2jprrb2gkuyfc|/)

[[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.mobi](ed2k://|file|[Code.Simplicity(2012.3)].Max.Kanat-Alexander.文字版.mobi|292610|e649cac85ad7b5f6bcafa2cf85bfad45|h=r5b52nk7atbmnljj74arzluy56hdpyqo|/)

[[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.pdf](ed2k://|file|[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.pdf|23894403|e7ea31472551585a288dc3855594b764|h=zitjxazbobwry7ekjre332gg3cebqxax|/)

[[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.epub](ed2k://|file|[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.epub|14673214|49dffa163e37c5bb3ff73e0c763ea358|h=sdoqdp7oqhg3kea2i66sdd4crwao2c7s|/)

[[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.mobi](ed2k://|file|[Using.Mac.OS.X.Lion.Server(2012.3)].Charles.Edge.文字版.mobi|12420247|aee5a111ab81b206164e33345acfa35f|h=sdg3gylk2qyzqbrvplnrt4ei2ca2wlyy|/)

[[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.pdf](ed2k://|file|[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.pdf|7568391|9d778a6de44afb0aec749f110f4d43dd|h=fwkqswredcaaz5pbbj4gbwuixac75kyy|/)

[[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.epub](ed2k://|file|[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.epub|2438112|a13fbd2d9379b060520d33c0983796bc|h=fzppmwpy6tgs2j43hn4auxzqb7e2mygj|/)

[[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.mobi](ed2k://|file|[Learning.CFEngine.3(2012.3)].Diego.Zamboni.文字版.mobi|705532|154ee77a3d21c6def8f03b39e8f255a5|h=75cozrijrwd463ocb3h2tgzb4we4zhbl|/)

[[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.pdf](ed2k://|file|[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.pdf|6708843|9f505f9265da6031975b111b6eed9b73|h=frbdtlzbllpnyisvnmrmmjosxa5lf326|/)

[[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.epub](ed2k://|file|[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.epub|2235832|a7b26e3aa400ba268bab896cc582a8bc|h=wkb2fimvl665fgm54qnsfta5vhuhoqxa|/)

[[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.mobi](ed2k://|file|[20.Recipes.for.Programming.PhoneGap(2012.3)].Jamie.Munro.文字版.mobi|283909|b9ce1354ca1833c0c83fd08da5291a81|h=ysgme7vl4zt5w6mztut2svlvsnr7n7q3|/)

[[Programming.C#.5.0(Early.Release)].Ian.Griffiths.文字版.pdf](ed2k://|file|[Programming.C#.5.0(Early.Release)].Ian.Griffiths.文字版.pdf|4429468|61a01baf27207c77e10c8558d9a85ab7|h=v5pl3b4x6m5zbktvcpaunnsowesp4i6l|/)

[[HTML5.Architecture(Early.Release)].Wesley.Hales.文字版.pdf](ed2k://|file|[HTML5.Architecture(Early.Release)].Wesley.Hales.文字版.pdf|2602370|0ec2f82b59b8d8e6a451d5a5b2cf459a|h=hfiirybs35j3y6ixjm5fnpcdpg37h246|/)

[[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.pdf](ed2k://|file|[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.pdf|2229966|b6d89082519c89a1fae6b492a40305c9|h=6hdcmp7lssrjlh4qrst3rcxnj5yjwynh|/)

[[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.epub](ed2k://|file|[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.epub|2780715|f36b70af3536abbdb82d0da5d9dfee5e|h=xj2hfocmyftrk7r5qeyec646fq7owonq|/)

[[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.mobi](ed2k://|file|[Natural.Language.Annotation.for.Machine.Learning(Early.Release)].James.Pustejovsky.文字版.mobi|1227184|aeefc2efa216783b7a3f84136c95d41f|h=54imxvybblx7wkckckcwrzjahrv7gawv|/)

[[jQuery.UI(2012.3)].Eric.Sarrion.文字版.pdf](ed2k://|file|[jQuery.UI(2012.3)].Eric.Sarrion.文字版.pdf|10983996|b547cf89a744d3f2def1b1a22705fa3b|h=b3kk65upxtntut6g3qnxhnsxlouqixcy|/)

[[jQuery.UI(2012.3)].Eric.Sarrion.文字版.epub](ed2k://|file|[jQuery.UI(2012.3)].Eric.Sarrion.文字版.epub|7858403|5d28e506d803ac927d2657413289c2e4|h=5r6attuklhamzy5zw2wvnblx4i7w75h6|/)

[[jQuery.UI(2012.3)].Eric.Sarrion.文字版.mobi](ed2k://|file|[jQuery.UI(2012.3)].Eric.Sarrion.文字版.mobi|7141483|25319459fe56f9123fbd5e1d2ce79a3f|h=66i3vlr6rnkrmgfxizfljpymkvmejle6|/)

[[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.pdf](ed2k://|file|[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.pdf|10145264|7f610fc16f2c8cd7559edd115dbfb83a|h=at3p3wouq4luviely66qq76wubwvcwwo|/)

[[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.epub](ed2k://|file|[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.epub|7027668|4d8fa3dbcec8ff5878ed03fd065d3a3c|h=vc5axjejkm2ycryq6rj4yl7sj4x4x7w3|/)

[[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.mobi](ed2k://|file|[Enabling.Programmable.Self.with.HealthVault(2012.3)].Vaibhav.Bhandari.文字版.mobi|5565054|76f66e64ede7a317bdc80b47aa453cb6|h=yxyxfsqqqt65tnlqqtqbat6momd2r25x|/)

[[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.pdf](ed2k://|file|[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.pdf|17466484|a1dcf44371b18eb6ac60ef4c0bd3ec46|h=jqxf7lu2yrhds7s73lck53jljdu6vybv|/)

[[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.epub](ed2k://|file|[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.epub|8913841|7857b2532249adf8073dd37cf8546418|h=heuba67n4bylv7wcvulkx2olhwwttoz3|/)

[[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.mobi](ed2k://|file|[Programming.iOS.5(2nd,2012.3)].Matt.Neuburg.文字版.mobi|7750088|3ce38397b5650c7208e2befd70fe2d36|h=42saxl3aaa2ryg6acp5yj7iwhyfwfnqz|/)

[[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.pdf](ed2k://|file|[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.pdf|12124669|bde0809de19e6ecb93b8d619c0b21783|h=hw6j52wpc42onlotf5u6kmlrzfouvwrv|/)

[[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.epub](ed2k://|file|[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.epub|3988747|5e6df07b5f14812a7ea22485f146207d|h=4au6eajlmwsfvsfsbtifzhyllur55f6c|/)

[[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.mobi](ed2k://|file|[Introduction.to.Tornado(2012.3)].Michael.Dory.文字版.mobi|2678662|07e87938e521948a5572eae0bf31f0dd|h=p6ypkom63l7l3jsphzpkayb4lcspw3l7|/)

[[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.pdf](ed2k://|file|[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.pdf|7796465|3a486696f0982a2cf547eb66f2454b3a|h=vklpp3ctc63iqufoxi6zx4i4lgodqfsg|/)

[[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.epub](ed2k://|file|[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.epub|4158861|c46ffa77f791d2b461257c3c571a93bf|h=cglaoyzxci67k256cv6wnufvqg6rgebn|/)

[[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.mobi](ed2k://|file|[Drupal.Development.Tricks.for.Designers(2012.3)].Dani.Nordin.文字版.mobi|2976669|0d0678f01235b6cbf49102843c16bcdd|h=pxvgcsqdeftuv2ewthzdbnvda6dzxk5c|/)

[[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.pdf](ed2k://|file|[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.pdf|1788493|2ebdbf1a9b10088349bbb4adef358a06|h=hnnqzgxekqunox4p4jaact5jo22pdcig|/)

[[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.epub](ed2k://|file|[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.epub|5551025|6a13094d44eebd89d01d1d2d4547d5f4|h=fikwuahhyybf7m27yzm5zx2g5bnsjzox|/)

[[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.mobi](ed2k://|file|[What.is.Dart？(2012.3)].Kathy.Walrath.文字版.mobi|761491|95f156d0ba9095d8f8a79b3a264d8130|h=qv6ce6oggdm2vum4ys7yfr5ai573daus|/)

[[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.pdf](ed2k://|file|[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.pdf|6021092|028f3a94820792821603c8ce2d2874e0|h=xxss3mhgavihevchwjb7samgdulgpuao|/)

[[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.epub](ed2k://|file|[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.epub|2521620|e17d6f903c2bfffd34f2f1ad088b9da1|h=ez7j3ha5fa2ogbnz4awjba6aub6fgqhj|/)

[[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.mobi](ed2k://|file|[Linux.Pocket.Guide(2nd,2012.3)].Daniel.J.Barrett.文字版.mobi|702121|38897134eb407ed27e669be8da4e32ce|h=j4lkkgpvq7jnpxshjepamcmwstv4lpux|/)

[[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.pdf](ed2k://|file|[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.pdf|8619703|22b88573ebf67f6f6bc9c4cfef461a96|h=x447q5ldl4fxcytkjdpiwvgtwqbynn6v|/)

[[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.epub](ed2k://|file|[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.epub|6314582|27944b40032a4a843f1a9dd9e2ed5291|h=2wk6qhldhvlzcfjb7unynhuywebl3b4m|/)

[[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.mobi](ed2k://|file|[Programming.Android(2nd,Early.Release,2012.3)].Zigurd.Mednieks.文字版.mobi|6524775|24d4b928b6591b3c7d7e7eca2546addb|h=5xchhzyneop45dejibs76f3x3jxqqppd|/)

[[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.pdf](ed2k://|file|[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.pdf|8375887|94504304285ae6a979597006c9944408|h=nc6yrpr6askzmwcrvsr7qe5yqs3a5vwd|/)

[[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.epub](ed2k://|file|[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.epub|4013280|5789e09416fb4d4942d91fed3d9188a0|h=z7avj4ho3un7tc55lsxqmyexbhsoumkp|/)

[[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.mobi](ed2k://|file|[Building.Mobile.Applications.with.Java(2012.3)].Joshua.Marinacci.文字版.mobi|2181264|5c80208cf8d6438620728761bdad9553|h=rzmu53rrncymtxm75vc5r4z6twnkznlp|/)

[[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.pdf](ed2k://|file|[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.pdf|72157871|299e5806c5f33b28adaad97bef7c7e6e|h=xejnedmdxtl2hn252gb6yw32qtxeeiut|/)

[[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.epub](ed2k://|file|[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.epub|32414633|176cf99af12f15a580987caa2cb98ca3|h=sw3s2wlxj2lvkjbmfj4r43amvt7kzdmy|/)

[[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.mobi](ed2k://|file|[The.Art.of.SEO(2nd,2012.3)].Eric.Enge.文字版.mobi|32203151|b411b6c73f3cc7d827f8b77052e730de|h=vpptn67p3emmd3yqchvp5gbgdhci72sd|/)

[[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.pdf](ed2k://|file|[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.pdf|30115324|4ce914fa4516fec1aa22f3da89374621|h=oepidlkepgpywhxk66nbzapaoaav7gvd|/)

[[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.epub](ed2k://|file|[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.epub|27338160|e3ab9f9091b0bbfc01a381a057cf98c4|h=qbwm4cin73yrq2soiilec5jy2djqf4dx|/)

[[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.mobi](ed2k://|file|[Mobile.Design.Pattern.Gallery(2012.3)].Theresa.Neil.文字版.mobi|24413949|450bc3dd94f6a5c6eb8844a9761a167d|h=2yw4jwjkk7kb5gjsik53zbpzlzlh2sc7|/)

[[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.pdf](ed2k://|file|[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.pdf|16631334|f0e722135bd4e5e3fa466c736a1eedc3|h=22qywqqy5rf5iiz7yuc5i654tp6et6fu|/)

[[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.epub](ed2k://|file|[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.epub|4808659|03c0a83ffde8d4ed262da98a1090a669|h=bxd7tkwoghqnp6pgpxit6b3u5rhmngqh|/)

[[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.mobi](ed2k://|file|[High.Performance.MySQL(3rd,2012.3)].Baron.Schwartz.文字版.mobi|3909949|6e197df3f9815007e96c07be87e8b22f|h=4rm2h6nd6drqij3joigfsvknc2bjbd6n|/)

[[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.pdf](ed2k://|file|[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.pdf|12938229|0ba85ae9baea32b9800f40cef37d6bd6|h=2gtu7obvozv6mrrwa7robz2ti44prov4|/)

[[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.epub](ed2k://|file|[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.epub|5251504|eef0c9c4b326be2490c9685e5f4d8876|h=j2qqdub2jyeisjxxt7slaq6ko57gwu5j|/)

[[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.mobi](ed2k://|file|[Algorithms.in.a.Nutshell(2008.10)].George.T.Heineman.文字版.mobi|8016797|a9c465e515d862c022084f978f007383|h=mfgxrjfv64fplynff4halsfu6nj62l6u|/)

[[Think.Complexity(2012.2)].Allen.B.Downey.文字版.pdf](ed2k://|file|[Think.Complexity(2012.2)].Allen.B.Downey.文字版.pdf|5851091|8bf2492dc6d983df8288d468ef03b117|h=sccnwyujllqwarvezya5inn7og3qwlb7|/)

[[Think.Complexity(2012.2)].Allen.B.Downey.文字版.epub](ed2k://|file|[Think.Complexity(2012.2)].Allen.B.Downey.文字版.epub|4263204|1b876c0bd1767dccc52966d7c90c7817|h=owyrzwuqzadjjuo3qrkchpfv7sushakn|/)

[[Think.Complexity(2012.2)].Allen.B.Downey.文字版.mobi](ed2k://|file|[Think.Complexity(2012.2)].Allen.B.Downey.文字版.mobi|2687633|2f126110f0345aa6dc0cccfefa165541|h=fktebtjjrwvlfxti3mlxvxcjw4wzkqy4|/)

[[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.pdf](ed2k://|file|[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.pdf|7554414|8318406dbb45b60df4cea1669bd46bf1|h=wngpk4qsaobcxidbjk7bb77h6qcd2mnn|/)

[[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.epub](ed2k://|file|[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.epub|3364478|263693846158fc638c3d9a86b926c335|h=2btlr53ixo7dcu3wnmiazyj3bqpzgusf|/)

[[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.mobi](ed2k://|file|[Programming.Entity.Framework：DbContext(2012.2)].Julia.Lerman.文字版.mobi|1945668|eb34e81ad0e1f0a5361bb8e410543b7a|h=7trirwu5axjn3gkf6qlfxcpgaz36lhq7|/)

[[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.pdf](ed2k://|file|[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.pdf|24204134|4a80d87890fe90be12a62c4e3281cfcc|h=vr4zh2j4nlejazvxbgdso6df7tyedxgw|/)

[[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.epub](ed2k://|file|[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.epub|16791025|b28daa12c4be74199d210677f6f7c8aa|h=oico7dqjf2xgy3jdgr5k4xpvqaalhtrb|/)

[[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.mobi](ed2k://|file|[Machine.Learning.for.Hackers(2012.2)].Drew.Conway.文字版.mobi|10979541|94c199549ec69b880428d47abbc6e779|h=2x73kgwot7x36ful3pvwbmzhj4ep2zem|/)

[[Programming.Perl(4th,2012.2)].Tom.Christiansen.文字版.pdf](ed2k://|file|[Programming.Perl(4th,2012.2)].Tom.Christiansen.文字版.pdf|21146356|10216e5dc0636743985b1ecea2b51446|h=vlxa2wmmzzg65sbysup5cog4tdmzdwws|/)

[[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.pdf](ed2k://|file|[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.pdf|22770991|6820b4e4cfc8e92c3e47467e5eff2126|h=64bivzts5cn43cmcsloqrtunxsp3mxys|/)

[[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.epub](ed2k://|file|[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.epub|20069779|d6f7309d7b3160180887db27b7bc8b32|h=jyhakwoh2vmo47z76xphqgllklcx3vic|/)

[[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.mobi](ed2k://|file|[SharePoint.2010.at.Work(2012.2)].Mark.Miller.文字版.mobi|18849975|a6c622be20bb3b7a2fad843b60f53e55|h=a6orqngrohqt7hfecdaezuhy62nixhqc|/)

[[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.pdf](ed2k://|file|[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.pdf|11713046|7e599161ccc5a7ce00a3db770645271f|h=pqeznmhwfzhu22pfgzd6753qhru65qag|/)

[[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.epub](ed2k://|file|[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.epub|11381560|46c3fde50aa428face188cac76f5b7fe|h=7nkl5jfg44s3sowcyoftntk55hmyasio|/)

[[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.mobi](ed2k://|file|[Running.Lean(2nd,2012.2)].Ash.Maurya.文字版.mobi|6493128|5acec7a584951dd66ebdb4434a0fe897|h=p3yybi3gikdv4owzod2fidudo4t6emwf|/)

[[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.pdf](ed2k://|file|[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.pdf|7786482|33be3c31beb038c892e292a1367c4aaa|h=bt2ybrn45doty2vkeiblq25faqmcqs2x|/)

[[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.epub](ed2k://|file|[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.epub|4684249|54f6e045f04a75f96da46347abaac25a|h=pcamuj4mozm7no3xc4wqkchfttk36ord|/)

[[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.mobi](ed2k://|file|[Making.Musical.Apps(2012.2)].Peter.Brinkmann.文字版.mobi|3349502|0e216f9724b812baa07534ed56f6185d|h=tof2b2msckgr7wglt7mjtt2qseg4tddn|/)

[[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.pdf](ed2k://|file|[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.pdf|55672233|bc5088a0d96edf133c0c898e050a9e1e|h=u7bsavleqgdj7x5npmy7bkzt7o7owgfg|/)

[[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.epub](ed2k://|file|[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.epub|5982668|07dbc54cceb3aff68afe035f13cddb06|h=esljwgawf7xgnqqmth7osj3cygm5otlf|/)

[[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.mobi](ed2k://|file|[Making.Android.Accessories.with.IOIO(2012.2)].Simon.Monk.文字版.mobi|4137544|46fa0b84c0a5bee79da258ae9ef8ed11|h=rtwfs6g42czkpkrnvbgwyf5cl2zj4y3h|/)

[[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.pdf](ed2k://|file|[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.pdf|6866435|47a9b35d775cefd93060f7e636ba7664|h=sq3adrp4kc3lqx66x7ivi4ugtlqimroq|/)

[[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.epub](ed2k://|file|[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.epub|4386047|c418e1963024254180a168ed587f6ef6|h=ohtay643ee75vc5orpckmhhwkqaf5avm|/)

[[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.mobi](ed2k://|file|[Introducing.HTML5.Game.Development(2012.2)].Jesse.Freeman.文字版.mobi|3183316|37b0141751bc5d8853f22f407b80b292|h=hrgrv52ay46gjuv4s3ymcl6h4gel6v5y|/)

[[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.pdf](ed2k://|file|[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.pdf|6305847|ce19661e8ac09db9a7500977c1af13e1|h=ypyy5ljjbk44o7tvq5466ag2lwhbjvwu|/)

[[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.epub](ed2k://|file|[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.epub|2924085|611291752528a46e91ea0f01e1b31ff7|h=ylss7blgjzcj2l44riky2evas3sug4aa|/)

[[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.mobi](ed2k://|file|[Getting.Started.with.OAuth.2.0(2012.2)].Ryan.Boyd.文字版.mobi|1161182|2c67902f59f57cc36d01b3d60413c868|h=52726oye4e5a4d2iroijlijha6raujve|/)

[[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.pdf](ed2k://|file|[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.pdf|8814175|3f95e829b0c7e848ea7dffbff1a04409|h=3yhpyey2m7v7uxinfbjvvczlyxjx7b54|/)

[[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.epub](ed2k://|file|[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.epub|6274529|c11f0f025fa1d19cb65daed04dc5f1dc|h=hnzjiem2vnqp4d657fzkxxucazmlm7db|/)

[[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.mobi](ed2k://|file|[Getting.Started.with.Netduino(2012.2)].Chris.Walker.文字版.mobi|5147424|b8451eedf29af85e137deed7205cb04b|h=szr2bdelp3idve4jld7zjmyjqukqxqez|/)

[[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.pdf](ed2k://|file|[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.pdf|10693432|2671ed5ec12d323d1a1013165d107e54|h=najq4plkbqj2kvddbt32qf52hrglbecl|/)

[[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.epub](ed2k://|file|[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.epub|3555764|30294dd4ae91d0ce7b57c24a0a1599a9|h=ldr6fhaodzxiohl5nibpvizs4wgi7zyg|/)

[[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.mobi](ed2k://|file|[Getting.Started.with.Fluidinfo(2012.2)].Nicholas.J.Radcliffe.文字版.mobi|1837889|5e2f0e8de860bfa56fb57d70e22da12c|h=fnqakdmwrrdrwk5lf3ps2owrlsp3dnv7|/)

[[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.pdf](ed2k://|file|[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.pdf|1961571|6ee61f75b4e7fee3c943b92983c21d97|h=by3sh3grcalb7c4diaxuzuldvvwoxjqk|/)

[[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.epub](ed2k://|file|[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.epub|2992778|fcfee89274c91f8a87d6f61bb44821a1|h=jgkytlbwicghw5vpiy56quiryzxjkrer|/)

[[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.mobi](ed2k://|file|[Data.for.the.Public.Good(2012.2)].Alex.Howard.文字版.mobi|393096|a40c3dc621418d43b4a842c83a84b66b|h=cjt2k2yzmvownmk4s7k7dzjrykdge446|/)

[[#tweetsmart(2012.2)].J.S.McDougall.文字版.pdf](ed2k://|file|[#tweetsmart(2012.2)].J.S.McDougall.文字版.pdf|3936382|c11eee9d387bb778a4811068a5fe1240|h=tunkg6gttso3xozghwkrptdpxtezkqwx|/)

[[#tweetsmart(2012.2)].J.S.McDougall.文字版.epub](ed2k://|file|[#tweetsmart(2012.2)].J.S.McDougall.文字版.epub|2077797|2990619d079da9bf085979b59963f757|h=nvovis3rhw57rud5r6bkoc4cutv3oc6n|/)

[[#tweetsmart(2012.2)].J.S.McDougall.文字版.mobi](ed2k://|file|[#tweetsmart(2012.2)].J.S.McDougall.文字版.mobi|175376|0334c3465c7dc924d2d22a37a6b30ff3|h=eoqfxsodcehhd6iiufx4wnu6tkycacp3|/)

[[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.pdf](ed2k://|file|[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.pdf|10869135|72eebe71e2e74b6c35c4f5fdc0c17248|h=isrlbjxj54bhasz65cjs6pc3dolihqly|/)

[[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.epub](ed2k://|file|[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.epub|6884959|383d6067b845c9dabbffd058fa45d6a5|h=wqosvcyzvf5a7jqetqlvk43rttdoy4qf|/)

[[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.mobi](ed2k://|file|[Publishing.with.iBooks.Author(2012.02)].Nellie.McKesson.文字版.mobi|5545354|62f315629b07855cf6bae141b2deb188|h=bg4kz5hpqatp3dmq47qgk5ior4qgul7h|/)

[[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.pdf](ed2k://|file|[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.pdf|6493525|05f7d0895c0498939547f2bafd2b7585|h=f43u7u3tmw5mddreyhzsrfppvds3cyz3|/)

[[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.epub](ed2k://|file|[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.epub|2455170|65034b3433ff4bbd8eec1d890cb047e7|h=jn5eshieoat2fn2usj7ka6bxnhn7bydq|/)

[[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.mobi](ed2k://|file|[MySQL.Troubleshooting(2012.02)].Sveta.Smirnova.文字版.mobi|715923|5147a25ad7c505991fa7a121c6337e31|h=jtakkesoikbdohfxce3bn3iwuyk3t7ez|/)

[[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.pdf](ed2k://|file|[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.pdf|21773830|48ecaf8570e28a179e21efc07fe316ac|h=zsqsrzn2fxs3ml2an7hu35usfhwqxpp6|/)

[[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.epub](ed2k://|file|[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.epub|13082720|d6a9276717457ae7c43e55478f79910d|h=jwrtrlgv7sjivu5c2altplhrdwstmjqa|/)

[[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.mobi](ed2k://|file|[jQuery.Mobile：Up.and.Running(2012.02)].Maximiliano.Firtman.文字版.mobi|8816643|654fb87a50cc09324795a34ca509f966|h=ngswr3ekqtqyfqhnqnmmjot7w3crbmyz|/)

[[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.pdf](ed2k://|file|[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.pdf|6803588|e1431132cd5cdfee6e4a54e376c663f6|h=uqen5hxe2u6zqwbpjrg2bjtmyx64sk77|/)

[[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.epub](ed2k://|file|[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.epub|3410257|45765d2e7ce87a0d831973388a903818|h=7giltfyfrplgadohpwjg76adf4t2ii5g|/)

[[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.mobi](ed2k://|file|[C#.4.0.in.a.Nutshell(4th,2010.01)].Joseph.Albahari.文字版.mobi|4750428|6f3a1eaa3b5dee392e2823f8c0656764|h=e6uyiijj6kvi7yb7dmqxqlsghutsynbo|/)

[[The.CSS3.Anthology(4th,2012)].Rachel.Andrew.文字版.pdf](ed2k://|file|[The.CSS3.Anthology(4th,2012)].Rachel.Andrew.文字版.pdf|32953597|a8b6d47379f4499fd6733f4520bfcfe5|h=otuecute2duuaallcbdo5o4rnrrv3t3d|/)

[[The.CSS3.Anthology(4th,2012)].Rachel.Andrew.文字版.epub](ed2k://|file|[The.CSS3.Anthology(4th,2012)].Rachel.Andrew.文字版.epub|16368676|3b114758c280a5be620ddd6215174501|h=5zjvhp22vsgjv4oef4gl7d3qh4ekrayq|/)

[[jQuery.Novice.to.Ninja(2nd,2012)].Earle.Castledine.文字版.pdf](ed2k://|file|[jQuery.Novice.to.Ninja(2nd,2012)].Earle.Castledine.文字版.pdf|17942784|9c3664d14ad69ccaef54c14d36cb409f|h=rhyucm7qbf3w3uo27mnkbknxwyprnp7u|/)

[[jQuery.Novice.to.Ninja(2nd,2012)].Earle.Castledine.文字版.epub](ed2k://|file|[jQuery.Novice.to.Ninja(2nd,2012)].Earle.Castledine.文字版.epub|1807681|d9b2086b75b0dee32624968f6f2341d1|h=2335exg5r4htbe5nk5zwhu54tmgpnbi7|/)

[[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.pdf](ed2k://|file|[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.pdf|4265295|2dadd7b83f09b33240fa4b6478f81e43|h=nluvcwhhyfeterrwbyqok2ph4iagqf47|/)

[[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.epub](ed2k://|file|[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.epub|2366933|c3da0d42a189bb5648d9fb08a500a920|h=r4uqzqdcsxon7qbgnruek3uwhfmi2oyp|/)

[[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.mobi](ed2k://|file|[Best.of.TOC.2012(2012.02)].O'Reilly.Radar.Team.文字版.mobi|494796|217d3a25ee259a5843be36a669617af6|h=q4agris6hi6xlgtt42oz23b4bxzordmv|/)

[[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.pdf](ed2k://|file|[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.pdf|2172483|d56055a7911543c0d95ec07ec763240c|h=npe2vhngntjcunwsivf72ig4rjjd7wyf|/)

[[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.epub](ed2k://|file|[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.epub|3022186|95b255444880da385798224039d73674|h=32qgndze6bgz3e6avbx4eofktktezycc|/)

[[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.mobi](ed2k://|file|[Advanced.Perl.Programming(2nd,2005.06)].Simon.Cozens.文字版.mobi|3627196|b10f41635bdf007d886052731eb1b86d|h=tzconvw5of7lmncqfjegccjtfilrsy4e|/)

[[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.pdf](ed2k://|file|[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.pdf|3339508|ab6d5cd13f4b4ab57a4d9ef2259c3165|h=kg3k7gjsiueoyob7jkn3rx3lcgmcs22z|/)

[[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.epub](ed2k://|file|[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.epub|2521725|345f2dba8461395c4b97a05f3c06b8cc|h=vvemkusl3g2ahpojyhwswem66jmlmlyq|/)

[[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.mobi](ed2k://|file|[Accessible.EPUB.3(2012.02)].Matt.Garrish.文字版.mobi|485073|bc29db815657771f958a3b36d975561e|h=titedzedkk56coromgeucfhd6iq7xjf4|/)

[[Writing.and.Querying.MapReduce.Views.in.CouchDB(2011.01)].Bradley.Holt.文字版.pdf](ed2k://|file|[Writing.and.Querying.MapReduce.Views.in.CouchDB(2011.01)].Bradley.Holt.文字版.pdf|1965754|1a99ac05ac4d9c0823b4e2c5e763b47e|h=i5lo64twgbstq3mgaterytfldgak5j26|/)

[[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.pdf](ed2k://|file|[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.pdf|17761059|2a465dd67e6cac345f9d59169e415845|h=y2pqbmcu7jxqkmmm77ze7ujizyyurvec|/)

[[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.epub](ed2k://|file|[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.epub|3539381|648d650edf3986162db54baf020634f0|h=rayswe5pijutq7lrqdyz7pt5i2iszdb3|/)

[[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.mobi](ed2k://|file|[DIY.Satellite.Platforms(2012.01)].Sandy.Antunes.文字版.mobi|1637427|9b2292c7173234aaa257af39a73c2b4b|h=bviaazchbjzey2ev56edcpu3d52rai27|/)

[[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.pdf](ed2k://|file|[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.pdf|8873151|ae00139f46faa40d9cf87f93deda8574|h=e3el4cyxkw2tc23b6k2hgv7zms5lpkec|/)

[[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.epub](ed2k://|file|[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.epub|5935540|18ecf4960d6ce17a1c397d3aa5131902|h=3eiul3tvsyepkksybjyx3h4icx6pjoah|/)

[[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.mobi](ed2k://|file|[Hadoop：The.Definitive.Guide(3rd,Early.Release)].Tom.White.文字版.mobi|5315581|1214385e681d58f5e6a52e057c6cdf1b|h=by32b2h7obj6hxunfmh2rekenffmdbd5|/)

[[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.pdf](ed2k://|file|[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.pdf|7821668|2b2f74f7386aa8ee965960d07df08f58|h=dp35pkmvd27ppxujvqstgvrwqgbtitvp|/)

[[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.epub](ed2k://|file|[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.epub|5417834|a2adc4c32eedb7e985e3f24a361c54c5|h=zisvdv25qcja77kjadgvmbxd32yf4iqg|/)

[[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.mobi](ed2k://|file|[C#.Database.Basics(2012.01)].Michael.Schmalz.文字版.mobi|4526826|ddc704115579615742fbe1b5045ed42a|h=yg3dahk66ofmha6g7k6vm74qbgciagb5|/)

[[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.pdf](ed2k://|file|[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.pdf|6078168|d300a045864f3b7d141312cfaa39d1a9|h=lsdec7j2uvrst3rwcwvzuksblerw25as|/)

[[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.epub](ed2k://|file|[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.epub|2991155|0c6d4874952e1f5ba2c47566e2c3f755|h=pfn3pol3smuytxxxvd6lrtpiom7kkgcb|/)

[[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.mobi](ed2k://|file|[Getting.Started.with.CouchDB(2012.01)].MC.Brown.文字版.mobi|811154|900e0f147c6b409dfaf0f7ec87895aca|h=zfitkes4sifyebhtbitfdfitfiuln2zr|/)

[[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.pdf](ed2k://|file|[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.pdf|9825423|98e1d463fa24197b8c1bbc0901b06969|h=r7mqri4z7riuzmdrdewshs25xe52nzp3|/)

[[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.epub](ed2k://|file|[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.epub|5248032|51b50fd56f80f701b5c49bc6fcf0fdad|h=npucltlkjuzygc3bm7a3ex5affntvq2r|/)

[[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.mobi](ed2k://|file|[Google.Script：Enterprise.Application.Essentials(2012.01)].James.Ferreira.文字版.mobi|4869935|cf28d1597e7ba2ebd50650437fb26cfa|h=y7setllxrjg7drwbhkwcmi2yz4uvsasb|/)

[[Make.Technology.on.Your.Time.Volume.29(2012.01)].Mark.Frauenfelder.文字版.pdf](ed2k://|file|[Make.Technology.on.Your.Time.Volume.29(2012.01)].Mark.Frauenfelder.文字版.pdf|40694275|3a42845bfc371db830ecf828bcee814d|h=hna4oxax75oiibnzrkmwqb4p344fashg|/)

[[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.pdf](ed2k://|file|[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.pdf|3292850|75ad098167fee531d45ae61a72a77d2d|h=azug4pcvfjg4r5vs3p6izmfaqwd55mw7|/)

[[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.epub](ed2k://|file|[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.epub|5719570|d268bbd9641737ffddf5f8f2d88d712b|h=lgaf5ccrithcuhf6m4bwqdyje5msymj7|/)

[[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.mobi](ed2k://|file|[Web.2.0：A.Strategy.Guide(2008.04)].Amy.Shuen.文字版.mobi|3621599|ff03b6eb1cac9ad751450cc40fe82d48|h=bjptw47w4nq3anshvfipbzfoq3dcwade|/)

[[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.pdf](ed2k://|file|[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.pdf|5819299|c6a6a5f70f60e66f3a533369cb4d2523|h=tsstbriu2qnk6wwxpob45wwcdyc65by4|/)

[[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.epub](ed2k://|file|[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.epub|1223598|f628875e420c542cef1e86fd8756c0ed|h=n4eacqnkx47oaxm44atdopzgok2sdu7p|/)

[[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.mobi](ed2k://|file|[SQL.in.a.Nutshell(3rd,2008.11)].Kevin.Kline.文字版.mobi|1451793|b347f57eebf47a1c7bcd03f5ca6cac54|h=5baswyermayub2ha6ji7m3idb73g5lil|/)

[[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.pdf](ed2k://|file|[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.pdf|1937603|e83eb3e0dcc1c15cc98e96afd4473feb|h=ydogm5cyrarzwlqissmpg7ehdjzhpu23|/)

[[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.epub](ed2k://|file|[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.epub|2139111|ec9e925360d3fdd98265181cbf123d3a|h=jgseuawxdqgz2pllzsz2sgoxlvgsjkpq|/)

[[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.mobi](ed2k://|file|[Learning.Perl.Student.Workbook(2nd,2012.01)].brian.d.foy.文字版.mobi|274907|c514fafec4e8c68055d3264b7c537910|h=2jsn3sjdijmeio66mk3ofjo36fgvk62e|/)

[[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.pdf](ed2k://|file|[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.pdf|5435250|acb28b1c239368f37c65beaa9dd45647|h=lyhdygvqklho4ffy6rr4gi6rzpjoihqj|/)

[[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.epub](ed2k://|file|[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.epub|2133834|38bfb6f0a41bdc1015729dad2807f1ea|h=qvyyta4r7ofru7dh7bvtcqypf4ks4lcc|/)

[[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.mobi](ed2k://|file|[Node.for.Front-End.Developers(2012.01)].Garann.Means.文字版.mobi|229468|aa53c0393a58000648b6ac725e46e41b|h=4zy2k3hz622c5mfbdoerpm55u2gwhudj|/)

[[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.pdf](ed2k://|file|[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.pdf|7523382|65c0c01caad9c8de8142859aaa20ac2a|h=c7legycwvby7rh6si545iqxp6krhmcxk|/)

[[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.epub](ed2k://|file|[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.epub|2391963|e1a0f8c8c9fe4bd60a6f228ddd5128d1|h=3ifolbeu275znz6au5maswqfbxfbpjdm|/)

[[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.mobi](ed2k://|file|[MongoDB.and.PHP(2012.01)].Steve.Francia.文字版.mobi|713389|4f4f478cb04a52dc07d08ac746e2c7c0|h=x35acenqco2wyhfcw57lc3kcrt6a5dkb|/)

[[Understanding.PaaS(2012.01)].Michael.P.McGrath.文字版.pdf](ed2k://|file|[Understanding.PaaS(2012.01)].Michael.P.McGrath.文字版.pdf|5040168|c99c048699f3bb2635daf245be02ea35|h=7vydthgg6rxx4e72lnwp4rvzemkszy3z|/)

[[Understanding.PaaS(2012.01)].Michael.P.McGrath.文字版.epub](ed2k://|file|[Understanding.PaaS(2012.01)].Michael.P.McGrath.文字版.epub|2538079|d70ff17d455c2358478f8e62ce22c23d|h=7ifvd3t4sjjpdu2iebqmlnoawt65rouj|/)

[[The.Little.Book.on.CoffeeScript(2012.01)].Alex.MacCaw.文字版.pdf](ed2k://|file|[The.Little.Book.on.CoffeeScript(2012.01)].Alex.MacCaw.文字版.pdf|5906644|fdcc10c3dd350f4ebe25fdbf3393f4c8|h=gnkerjru7vhdlpghyb7powhetb4tojut|/)

[[The.Little.Book.on.CoffeeScript(2012.01)].Alex.MacCaw.文字版.epub](ed2k://|file|[The.Little.Book.on.CoffeeScript(2012.01)].Alex.MacCaw.文字版.epub|2089994|c5d371fbc867e1343330068460a8f47c|h=eahhdsffcuhmuolk2h6kmkux7tlejtur|/)

[[Hacking.and.Securing.iOS.Applications(2012.01)].Jonathan.Zdziarski.文字版.pdf](ed2k://|file|[Hacking.and.Securing.iOS.Applications(2012.01)].Jonathan.Zdziarski.文字版.pdf|10727178|1c800489bd16da92b50d4c9678b6c4df|h=65hfqgsri5f4um24yx3agcezup4yfmdi|/)

[[Hacking.and.Securing.iOS.Applications(2012.01)].Jonathan.Zdziarski.文字版.epub](ed2k://|file|[Hacking.and.Securing.iOS.Applications(2012.01)].Jonathan.Zdziarski.文字版.epub|4853417|c7d178257b0c4ed23c7545c00a1e734b|h=aez5mxq2gc6etv6rrhhs2tigseaqfnr2|/)

[[Building.Android.Apps.with.HTML,CSS,and.JavaScript(2nd,2012.01)].Jonathan.Stark.文字版.pdf](ed2k://|file|[Building.Android.Apps.with.HTML,CSS,and.JavaScript(2nd,2012.01)].Jonathan.Stark.文字版.pdf|10854983|e2cb595d24bd3d5a7f040a83055cf285|h=cmqyniolpdky2gcyliotkvuzakzohlaw|/)

[[Building.Android.Apps.with.HTML,CSS,and.JavaScript(2nd,2012.01)].Jonathan.Stark.文字版.epub](ed2k://|file|[Building.Android.Apps.with.HTML,CSS,and.JavaScript(2nd,2012.01)].Jonathan.Stark.文字版.epub|4866182|f8acc943c7646f5949617ff9412eca6d|h=s4lheuj43naybet7mo4z6jtwf2zem6ji|/)

[[SharePoint.2010.for.Project.Management(2nd,2012.01)].Dux.Raymond.Sy.文字版.pdf](ed2k://|file|[SharePoint.2010.for.Project.Management(2nd,2012.01)].Dux.Raymond.Sy.文字版.pdf|33718678|184a428b1ab4c6eefc822b2e3c51b80a|h=3jh6dbeahibc7byntybwlztmk7t3xgyx|/)

[[SharePoint.2010.for.Project.Management(2nd,2012.01)].Dux.Raymond.Sy.文字版.epub](ed2k://|file|[SharePoint.2010.for.Project.Management(2nd,2012.01)].Dux.Raymond.Sy.文字版.epub|13608314|5165a384a3c2460e4c7ef560beba62c1|h=5biq3jsnzfeoe3vy6zxoqybgpkikiuiv|/)

[[Programming.Interactivity(2nd,2012.01)].Joshua.Noble.文字版.pdf](ed2k://|file|[Programming.Interactivity(2nd,2012.01)].Joshua.Noble.文字版.pdf|36032360|ca294acafd1f991a8b1571e022dc9db3|h=cgqz4pe2x4f7injtrw3crq4nlv2f4qkh|/)

[[Programming.Interactivity(2nd,2012.01)].Joshua.Noble.文字版.epub](ed2k://|file|[Programming.Interactivity(2nd,2012.01)].Joshua.Noble.文字版.epub|16612362|2ea22fe56424a476162e3adc05680fa2|h=pq5oyhrxhtje42ja2fbvyiqruqid3p6g|/)

[[Programming.iOS.5(2nd.2012.01,Early.Release)].Matt.Neuburg.文字版.pdf](ed2k://|file|[Programming.iOS.5(2nd.2012.01,Early.Release)].Matt.Neuburg.文字版.pdf|10962972|857d67061e1bca00b33466567b3690c5|h=redjl7gdth7rjqveeikrd5pdzh6aiqz5|/)

[[Programming.iOS.5(2nd.2012.01,Early.Release)].Matt.Neuburg.文字版.epub](ed2k://|file|[Programming.iOS.5(2nd.2012.01,Early.Release)].Matt.Neuburg.文字版.epub|8557007|256cb43d64985b9b3ef087dcf2a30f07|h=scsqvehl2ssdexob4kyi7qy7fclpdq4x|/)

[[Introducing.Starling：Building.GPU.Accelerated.Applications(2012.01)].Thibault.Imbert.文字版.pdf](ed2k://|file|[Introducing.Starling：Building.GPU.Accelerated.Applications(2012.01)].Thibault.Imbert.文字版.pdf|7837524|ff9f3d732cfd08018c5d3908a350554d|h=rrixiaomczltnou6drpofsiterzu3ezl|/)

[[Introducing.Starling：Building.GPU.Accelerated.Applications(2012.01)].Thibault.Imbert.文字版.epub](ed2k://|file|[Introducing.Starling：Building.GPU.Accelerated.Applications(2012.01)].Thibault.Imbert.文字版.epub|3945628|2d702cd2cbc1b2fe628069235d643167|h=hzi5a5isiwsz6xez4kct5xqsn5cp3oj6|/)

[[PHP.in.a.Nutshell(2005)].Paul.Hudson.文字版.pdf](ed2k://|file|[PHP.in.a.Nutshell(2005)].Paul.Hudson.文字版.pdf|3365167|168e8c3f262693fa0c9aa6ad38738e62|h=znspctmumezmm3xft7afbguznkll6kdo|/)

[[PHP.in.a.Nutshell(2005)].Paul.Hudson.文字版.epub](ed2k://|file|[PHP.in.a.Nutshell(2005)].Paul.Hudson.文字版.epub|3785632|6ddfe119fa00798e7c36969e7d18f2fc|h=sar3hrvygqsuuhehtqamvpptjd7nfeon|/)

[[Building.Wireless.Sensor.Networks(2010.12)].Robert.Faludi.文字版.pdf](ed2k://|file|[Building.Wireless.Sensor.Networks(2010.12)].Robert.Faludi.文字版.pdf|42121150|c588954e08b59d0b5582d4c190b3c9f7|h=tmbvcqfj7cc46sd2pwqhnixva4c2iqoo|/)

[[Building.Wireless.Sensor.Networks(2010.12)].Robert.Faludi.文字版.epub](ed2k://|file|[Building.Wireless.Sensor.Networks(2010.12)].Robert.Faludi.文字版.epub|9504930|b882eaae82181e9890fe066613f78447|h=j5zndjvr4qeuzmynhvd54dxfiapcsy76|/)

[[What's.New.in.Flash.Player.11(2011.12)].Joseph.Labrecque.文字版.pdf](ed2k://|file|[What's.New.in.Flash.Player.11(2011.12)].Joseph.Labrecque.文字版.pdf|5533159|8685e2e3ba17e0b42499241d05548ff1|h=onpz4pmpnb5iy7oxfqqpede4xiuz2cf2|/)

[[What's.New.in.Flash.Player.11(2011.12)].Joseph.Labrecque.文字版.epub](ed2k://|file|[What's.New.in.Flash.Player.11(2011.12)].Joseph.Labrecque.文字版.epub|2225750|d3dede326e23b5f331231215c6690e1f|h=ibyht4kbtqripxossjbqaikoymmdysty|/)

[[What's.New.in.Adobe.AIR.3(2011.12)].Joseph.Labrecque.文字版.pdf](ed2k://|file|[What's.New.in.Adobe.AIR.3(2011.12)].Joseph.Labrecque.文字版.pdf|6923937|7c95ec9622ea8091eb987b096f7c3424|h=fun2ihp3xnjofgrjv7ijahmptkcn4vqc|/)

[[What's.New.in.Adobe.AIR.3(2011.12)].Joseph.Labrecque.文字版.epub](ed2k://|file|[What's.New.in.Adobe.AIR.3(2011.12)].Joseph.Labrecque.文字版.epub|2643148|9e22c3f4fde756d1ba6a645788631154|h=slhifwaja3poykufzz7dgkqffnxwmxzf|/)

[[Making.Things.Happen(2008)].Scott.Berkun.文字版.pdf](ed2k://|file|[Making.Things.Happen(2008)].Scott.Berkun.文字版.pdf|5758499|0fa15762b01a2922bf2072311586d3ba|h=bh23xkpdh7e3bjvp4vxpqtuvatfpqkwt|/)

[[Making.Things.Happen(2008)].Scott.Berkun.文字版.epub](ed2k://|file|[Making.Things.Happen(2008)].Scott.Berkun.文字版.epub|1801845|8deaf81ea7164f10e9bb5be3689e0942|h=dfwrpsfqlpxqdz2yxlxprktghhn5nygn|/)

[[R.in.a.Nutshell(1st,2009)].Joseph.Adler.文字版.pdf](ed2k://|file|[R.in.a.Nutshell(1st,2009)].Joseph.Adler.文字版.pdf|5694850|a3221816ddbbed140c9bd11efda9d3be|h=pzpe2msxojnibjtpdisanxjap4tkup4o|/)

[[R.in.a.Nutshell(1st,2009)].Joseph.Adler.文字版.epub](ed2k://|file|[R.in.a.Nutshell(1st,2009)].Joseph.Adler.文字版.epub|3970135|ace06fbec75bb044e77ddccbd1020992|h=rfnyspj6mt3dqarocj4jmtva3v7hkcvu|/)

[[Tap,Move,Shake：Turning.Your.Game.Ideas.into.iPhone&iPad.Apps(2011.12)].Todd.Moore.文字版.pdf](ed2k://|file|[Tap,Move,Shake：Turning.Your.Game.Ideas.into.iPhone&iPad.Apps(2011.12)].Todd.Moore.文字版.pdf|23879698|02130dec38db8ff570d8bba0dab46408|h=yslyg3din4vrabugnhail6dwwqnifkux|/)

[[Tap,Move,Shake：Turning.Your.Game.Ideas.into.iPhone&iPad.Apps(2011.12)].Todd.Moore.文字版.epub](ed2k://|file|[Tap,Move,Shake：Turning.Your.Game.Ideas.into.iPhone&iPad.Apps(2011.12)].Todd.Moore.文字版.epub|7073654|f1051a2da61fb4d20498dd5acbabc01d|h=sszkgupbvqidp6tb5kqj35qyyfvbx734|/)

[[APIs：A.Strategy.Guide(2011.12)].Daniel.Jacobson.文字版.pdf](ed2k://|file|[APIs：A.Strategy.Guide(2011.12)].Daniel.Jacobson.文字版.pdf|6418506|21e557a4be56e31b177dd3f2e46f434e|h=wwdtam24hac7ywgl377kezfhltr5ki6k|/)

[[APIs：A.Strategy.Guide(2011.12)].Daniel.Jacobson.文字版.epub](ed2k://|file|[APIs：A.Strategy.Guide(2011.12)].Daniel.Jacobson.文字版.epub|1840102|bb11fbe19f859c26d635bcc76197b73c|h=iht43425uvww7g6cnbbo6avrishp7rr4|/)

[[Mapping.with.Drupal(2011.12)].Alan.Palazzolo.文字版.pdf](ed2k://|file|[Mapping.with.Drupal(2011.12)].Alan.Palazzolo.文字版.pdf|30203033|f2fa11b097cf8cc8a4fbd5ff0b62cc29|h=uahsqhdvruo5qqgpiygknlf37v2qzdi7|/)

[[Mapping.with.Drupal(2011.12)].Alan.Palazzolo.文字版.epub](ed2k://|file|[Mapping.with.Drupal(2011.12)].Alan.Palazzolo.文字版.epub|5888390|dfd38eff9271268e25bb265d847deddb|h=2be3kpmja3jdag27s6xx6xrekhp3nkny|/)

[[ActionScript.Developers.Guide.to.PureMVC(2011.12)].Cliff.Hall.文字版.pdf](ed2k://|file|[ActionScript.Developers.Guide.to.PureMVC(2011.12)].Cliff.Hall.文字版.pdf|6186090|636fb345dc07751782b1a24126761579|h=4npt5xujlh6pndejqe4qbqeb7jfiqljz|/)

[[ActionScript.Developers.Guide.to.PureMVC(2011.12)].Cliff.Hall.文字版.epub](ed2k://|file|[ActionScript.Developers.Guide.to.PureMVC(2011.12)].Cliff.Hall.文字版.epub|1292192|efe54146931f7d508cbc49a343365950|h=mrrt7i5bebq6hudjamtzyxg5mtmkj7ql|/)

[[Design.and.Prototyping.for.Drupal(2011.12)].Dani.Nordin.文字版.pdf](ed2k://|file|[Design.and.Prototyping.for.Drupal(2011.12)].Dani.Nordin.文字版.pdf|17569497|61fcd6cc434e7d6577f4850aef42c5a1|h=kvxsvokjzfxsk3o3z7obufljjvwyzoct|/)

[[Design.and.Prototyping.for.Drupal(2011.12)].Dani.Nordin.文字版.epub](ed2k://|file|[Design.and.Prototyping.for.Drupal(2011.12)].Dani.Nordin.文字版.epub|11781927|e27546f1d2880b21f0209bb14f47d72e|h=3gw27dcidzmqo37vbwsdrtzplh2crcb2|/)

[[SQL.and.Relational.Theory(2nd,2011.12)].C.J.Date.文字版.pdf](ed2k://|file|[SQL.and.Relational.Theory(2nd,2011.12)].C.J.Date.文字版.pdf|21237695|a6e2c9bbaf213fcb984900d4191fb4cb|h=vkp6cm3ldpxrziclg4afuqznejs3lb2h|/)

[[SQL.and.Relational.Theory(2nd,2011.12)].C.J.Date.文字版.epub](ed2k://|file|[SQL.and.Relational.Theory(2nd,2011.12)].C.J.Date.文字版.epub|2992120|14ea4450850b2d3d2a65aed5ba0f55f5|h=miq36qlphhmh6hkoghupz5jlb66gb74x|/)

[[Advanced.Rails(2007)].Brad.Ediger.文字版.pdf](ed2k://|file|[Advanced.Rails(2007)].Brad.Ediger.文字版.pdf|2665345|4eaf4ca0ca46766605fb7c1166886f1c|h=wctxajviyl2sjv4cuddv2rjbgelei46p|/)

[[RT.Essentials(2005)].Jesse.Vincent.文字版.pdf](ed2k://|file|[RT.Essentials(2005)].Jesse.Vincent.文字版.pdf|2821293|fa0b5f864d91d8ff37f31db56870738c|h=y7xm2eyjsmjcc3nlm6pwkplutdnpznsl|/)

[[Application.Security.for.the.Android.Platform(2011.12)].Jeff.Six.文字版.pdf](ed2k://|file|[Application.Security.for.the.Android.Platform(2011.12)].Jeff.Six.文字版.pdf|5780161|bcbe40eb6131f58bc26fd76933f9810b|h=7fhpaxmrfmsku4zwj4ntperyoqmoznwd|/)

[[Application.Security.for.the.Android.Platform(2011.12)].Jeff.Six.文字版.epub](ed2k://|file|[Application.Security.for.the.Android.Platform(2011.12)].Jeff.Six.文字版.epub|729369|9fb9ab030c84acbfee760b23d8658f1f|h=r5mnaiu3wrvbvnpvdczhn5qgkjutujmw|/)

[[PDF.Explained(2011.12)].John.Whitington.文字版.pdf](ed2k://|file|[PDF.Explained(2011.12)].John.Whitington.文字版.pdf|6306982|80029da11abc98fc392853f3c64e7bf2|h=cmmyg6npekvmqyqxdlbkoiqgqr2sn2at|/)

[[PDF.Explained(2011.12)].John.Whitington.文字版.epub](ed2k://|file|[PDF.Explained(2011.12)].John.Whitington.文字版.epub|1607222|66ffcfefa97648e165bfcea8266b6a0e|h=rtmlkhmaidad5shdbf23ixzslscsxgue|/)

[[The.Information.Diet(2011.12)].Clay.A.Johnson.文字版.pdf](ed2k://|file|[The.Information.Diet(2011.12)].Clay.A.Johnson.文字版.pdf|1796869|2dc9b901e30e5792088d66667c7d2b46|h=jwzvnlmtmch4jdwlclykbwuhfq436rpv|/)

[[The.Information.Diet(2011.12)].Clay.A.Johnson.文字版.epub](ed2k://|file|[The.Information.Diet(2011.12)].Clay.A.Johnson.文字版.epub|1241692|d6ce2d54317792a68cefe6c69e19aaac|h=rfzsu7qyqugfg5dey575fapehysvbul3|/)

[[Managing.Projects.with.GNU.Make(3rd,2004)].Robert.Mecklenburg.文字版.pdf](ed2k://|file|[Managing.Projects.with.GNU.Make(3rd,2004)].Robert.Mecklenburg.文字版.pdf|3749624|7f8236a1750bbef2dd737a9ae75a06ea|h=itgp4m3226nb2w5lwmnuc73fp62f2it5|/)

[[Managing.Projects.with.GNU.Make(3rd,2004)].Robert.Mecklenburg.文字版.epub](ed2k://|file|[Managing.Projects.with.GNU.Make(3rd,2004)].Robert.Mecklenburg.文字版.epub|1067497|5521a2897b54a47768573fb06f8898a8|h=7k5jiub2kmmvvbwzuio5cvflskm7keuo|/)

[[TCP.IP.Network.Administration(3rd,2002)].Craig.Hunt.文字版.pdf](ed2k://|file|[TCP.IP.Network.Administration(3rd,2002)].Craig.Hunt.文字版.pdf|7396570|73b672355cfba8b0bc9b82132df7daa4|h=wr3xghic35swb6uwu4yz5miujvnar77i|/)

[[TCP.IP.Network.Administration(3rd,2002)].Craig.Hunt.文字版.epub](ed2k://|file|[TCP.IP.Network.Administration(3rd,2002)].Craig.Hunt.文字版.epub|4188885|d923c98610b1dbc1b7b7be12c21e4321|h=4arpb5j3rqivy6gjlybyauwi2njmxkuv|/)

[[The.Global.eBook.Market(2011.10)].Ruediger.Wischenbart.文字版.pdf](ed2k://|file|[The.Global.eBook.Market(2011.10)].Ruediger.Wischenbart.文字版.pdf|4439106|d50b9a97bd111b1152bea72f41966b4a|h=smjhrhiejhpecyfozte5npg5yz7z7rci|/)

[[Make：Ultimate.Kit.Guide(2011.11)].Mark.Frauenfelder.文字版.pdf](ed2k://|file|[Make：Ultimate.Kit.Guide(2011.11)].Mark.Frauenfelder.文字版.pdf|12339347|96c9b0fc85c936738dd1a76843071c47|h=t7zovtyxpn43osj4n34at4xz72o3bqao|/)

[[Make：Ultimate.Kit.Guide(2011.11)].Mark.Frauenfelder.文字版.epub](ed2k://|file|[Make：Ultimate.Kit.Guide(2011.11)].Mark.Frauenfelder.文字版.epub|32913172|af3d37638e029de8cc3bfa55a992ccfa|h=ja6dfejc3i6cdvofv23vnhdhxprn5w6m|/)

[[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.pdf](ed2k://|file|[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.pdf|13594366|657631cec4f1f8feaf2e19f208f6e8a6|h=orasq4mqbcheizwy43fb7yokiwixo3uq|/)

[[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.epub](ed2k://|file|[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.epub|4470754|234e2b633daf70d2c67f2e14fe284086|h=qrdle7calaozdn76z5yr6j77qvsnsu7g|/)

[[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.mobi](ed2k://|file|[Cooking.for.Geeks(2010.7)].Jeff.Potter.文字版.mobi|4208266|2683aba1527d1ac357040833ec2ff58a|h=esc2t7ycajkgxxcpyvk6vs566enasamn|/)

[[Programming.Entity.Framework：Code.First(2011.11)].Julia.Lerman.文字版.pdf](ed2k://|file|[Programming.Entity.Framework：Code.First(2011.11)].Julia.Lerman.文字版.pdf|6622819|c63d242bbf2f50a46011651d202f6a9b|h=au5vlkczzrjj6ot4jp2z7bd3jqznwlka|/)

[[Programming.Entity.Framework：Code.First(2011.11)].Julia.Lerman.文字版.epub](ed2k://|file|[Programming.Entity.Framework：Code.First(2011.11)].Julia.Lerman.文字版.epub|1564211|93d15a5dbefee860a36866696903c053|h=dbnnjtjqbiwvdxwwnr6je44vjlfcg5se|/)

[[Sinatra：Up.and.Running(2011.11)].Alan.Harris.文字版.pdf](ed2k://|file|[Sinatra：Up.and.Running(2011.11)].Alan.Harris.文字版.pdf|6892340|f5de5c830ea510fd08cdf396b1df34d9|h=euk4n4qbq7vqa4e27yxwv4mpdrydkac2|/)

[[Sinatra：Up.and.Running(2011.11)].Alan.Harris.文字版.epub](ed2k://|file|[Sinatra：Up.and.Running(2011.11)].Alan.Harris.文字版.epub|2511910|03389d9a93a4523a44fda67676fe0e02|h=4blttpveyzr32umb52h5ao2nwito5nfu|/)

[[Building.Hypermedia.APIs.with.HTML5.and.Node(2011.11)].Mike.Amundsen.文字版.pdf](ed2k://|file|[Building.Hypermedia.APIs.with.HTML5.and.Node(2011.11)].Mike.Amundsen.文字版.pdf|7226808|552ea5998119064ce448d1ab7a316404|h=dol3zjxyrxt3vm75cbjrqbwparaqlt4v|/)

[[Building.Hypermedia.APIs.with.HTML5.and.Node(2011.11)].Mike.Amundsen.文字版.epub](ed2k://|file|[Building.Hypermedia.APIs.with.HTML5.and.Node(2011.11)].Mike.Amundsen.文字版.epub|1871486|136381ac5bc08b95ff6cdfa54cca15b9|h=r75aet2amdafrwbhxwzszpnzaby7yhiy|/)

[[Kindle.Fire：Out.of.the.Box(2011.11)].Brian.Sawyer.文字版.pdf](ed2k://|file|[Kindle.Fire：Out.of.the.Box(2011.11)].Brian.Sawyer.文字版.pdf|30130880|a07f223693d8cb800527a664a015378d|h=2g4mbqgjyopc2fq23kclcqqcdp44gjha|/)

[[Google.AdWords(2011.11)].Anastasia.Holdren.文字版.pdf](ed2k://|file|[Google.AdWords(2011.11)].Anastasia.Holdren.文字版.pdf|19995522|b488d92a15eaf1493bf6bcde8e44efa4|h=tr6wxla345bjso65346vvf7haxogiifg|/)

[[Google.AdWords(2011.11)].Anastasia.Holdren.文字版.epub](ed2k://|file|[Google.AdWords(2011.11)].Anastasia.Holdren.文字版.epub|14602865|da0d336e107d9d65a715f598948eed7b|h=4efaeh3v2lbxuvydbqstysrxjnm37ntj|/)

[[Programming.HTML5.Applications(2011.11)].Zachary.Kessin.文字版.pdf](ed2k://|file|[Programming.HTML5.Applications(2011.11)].Zachary.Kessin.文字版.pdf|8444649|ad853743cde1dcb686f66a558df21ef0|h=w2wq3zntnwepxzr46g3ux2ldnthsiug5|/)

[[Programming.HTML5.Applications(2011.11)].Zachary.Kessin.文字版.epub](ed2k://|file|[Programming.HTML5.Applications(2011.11)].Zachary.Kessin.文字版.epub|1659089|c53523fdf6238f2975efbbc569417890|h=uquuwo4h4amsh7jy3pbris6taboc7y65|/)

[[bash.Cookbook(2007)].Carl.Albing.文字版.pdf](ed2k://|file|[bash.Cookbook(2007)].Carl.Albing.文字版.pdf|2149735|79bc72bfd053fa83cefd89b25b60adca|h=2v4wmzgec2fgdbhppdg3wgvncgjsofre|/)

[[bash.Cookbook(2007)].Carl.Albing.文字版.epub](ed2k://|file|[bash.Cookbook(2007)].Carl.Albing.文字版.epub|1601141|7de260d2241108b964faa403978dc41e|h=nu6fjqv5ptqct6wwvnubylof24hxgkml|/)

[[Data.Mashups.in.R(2011.3)].Jeremy.Leipzig.文字版.pdf](ed2k://|file|[Data.Mashups.in.R(2011.3)].Jeremy.Leipzig.文字版.pdf|5051977|99ad08e46835b501c54b902202415deb|h=s5sozz37wb7fdc6crfhhfgpa5fiah25e|/)

[[Data.Mashups.in.R(2011.3)].Jeremy.Leipzig.文字版.epub](ed2k://|file|[Data.Mashups.in.R(2011.3)].Jeremy.Leipzig.文字版.epub|1426752|e1dce42d0d16d988f056210112945b4e|h=mbqhk4mr4wzc5kfoahp3dzxuj36fx7pk|/)

[[Machine.Learning.for.Email(2011.10)].Drew.Conway.文字版.pdf](ed2k://|file|[Machine.Learning.for.Email(2011.10)].Drew.Conway.文字版.pdf|10222290|3ff6b438c5bc53fffe412199293542f0|h=a3mq3mcwlnoqy372vcstjicegz22qbkr|/)

[[Machine.Learning.for.Email(2011.10)].Drew.Conway.文字版.epub](ed2k://|file|[Machine.Learning.for.Email(2011.10)].Drew.Conway.文字版.epub|4620973|8d3429d5eddf59be27bdf6c0790dd4a5|h=ezzktie6vwte64pvr46wn4wtqlswtg4g|/)

[[DNS.and.Bind.Cookbook(2002)].Cricket.Liu.文字版.pdf](ed2k://|file|[DNS.and.Bind.Cookbook(2002)].Cricket.Liu.文字版.pdf|6885596|611e202b41300df9d6f7bc0ea70a0ef3|h=m62kjd3ggahsroh64jmi65ipkfa3ufdj|/)

[[DNS.and.Bind.Cookbook(2002)].Cricket.Liu.文字版.epub](ed2k://|file|[DNS.and.Bind.Cookbook(2002)].Cricket.Liu.文字版.epub|941326|b3a4f28c7c625de1a4835e8cfeb9ce24|h=oflhopeibkhf2vaqgh4jocmjbxkx37el|/)

[[Practical.C.Programming(1997,3rd)].Steve.Oualline.文字版.pdf](ed2k://|file|[Practical.C.Programming(1997,3rd)].Steve.Oualline.文字版.pdf|10822626|368bd59cee7e3e1e79cb244c79b20a51|h=helvyef55fkr6nosdkyqhbjtu5t2mvl3|/)

[[Practical.C.Programming(1997,3rd)].Steve.Oualline.文字版.epub](ed2k://|file|[Practical.C.Programming(1997,3rd)].Steve.Oualline.文字版.epub|4207973|07363a085a19436d3059e6bccff6a1c4|h=henlph5yiezugvjrntubnfgkhvhxmpzj|/)

[[Designing.Mobile.Interfaces(2011.11)].Steven.Hoober.文字版.pdf](ed2k://|file|[Designing.Mobile.Interfaces(2011.11)].Steven.Hoober.文字版.pdf|41000604|55adecafb8cb1ce6635ba28d9e7dcee1|h=iewhziosu3kkmwkgjhwmqer5ixevfhxd|/)

[[Designing.Mobile.Interfaces(2011.11)].Steven.Hoober.文字版.epub](ed2k://|file|[Designing.Mobile.Interfaces(2011.11)].Steven.Hoober.文字版.epub|20625041|f81454a3f9a2b582342c69d2eb40b5d8|h=krvmzl23it3zelavpsaa6rorz6dcoipv|/)

[[HTML5.Cookbook(2011.11)].Christopher.Schmitt.文字版.pdf](ed2k://|file|[HTML5.Cookbook(2011.11)].Christopher.Schmitt.文字版.pdf|21874571|861eee0a685fc20a60c3de710c086db1|h=tecj7h3r6n42szpcycgqwgvzujiavky7|/)

[[HTML5.Cookbook(2011.11)].Christopher.Schmitt.文字版.epub](ed2k://|file|[HTML5.Cookbook(2011.11)].Christopher.Schmitt.文字版.epub|6134048|0c6f028234b8a0c9ac331cf13b63c17e|h=2tetsafbaiti2izpiz654bbos4twxl7m|/)

[[Designing.Embedded.Hardware(2nd.2005)].John.Catsoulis.文字版.epub](ed2k://|file|[Designing.Embedded.Hardware(2nd.2005)].John.Catsoulis.文字版.epub|5730944|327ae10e945ee08d73d65c9001bccd32|h=dbgp4x2kyiqwv6tnfdkmybb5cq3mem7r|/)

[[Parallel.R(2011.10)].Q.Ethan.McCallum.文字版.pdf](ed2k://|file|[Parallel.R(2011.10)].Q.Ethan.McCallum.文字版.pdf|5900493|bd95291bcee77c8174b1108c0366b30b|h=bakpuevkkrnrclvouqg27424wwoebju3|/)

[[Parallel.R(2011.10)].Q.Ethan.McCallum.文字版.epub](ed2k://|file|[Parallel.R(2011.10)].Q.Ethan.McCallum.文字版.epub|883324|f881e628ae1cd5ec802d12a5b55fdcbd|h=tmhnndxth2bhp46j7uwseuwpigottttm|/)

[[XQuery(2007)].Priscilla.Walmsley.文字版.pdf](ed2k://|file|[XQuery(2007)].Priscilla.Walmsley.文字版.pdf|3638377|c3a19711fe04cb9d83a724f65a3322f3|h=zgfawgen5ese6kfuig45cnhf7sznmmpo|/)

[[Mastering.Algorithms.with.C(1999)].Kyle.Loudon.文字版.pdf](ed2k://|file|[Mastering.Algorithms.with.C(1999)].Kyle.Loudon.文字版.pdf|5287697|542cdc12b742714a86c264353c6ac6b2|h=zpu2cidwp3x7u7kdulkvtt23tqd4xsrj|/)

[[Mastering.Algorithms.with.C(1999)].Kyle.Loudon.文字版.epub](ed2k://|file|[Mastering.Algorithms.with.C(1999)].Kyle.Loudon.文字版.epub|4091713|af8a17e55660e4247721823608ae593d|h=6xlio5iqhchxv6kj3c4tz5bujvsh5zvs|/)

[[The.Art.of.Readable.Code(2011.11)].Dustin.Boswell.文字版.pdf](ed2k://|file|[The.Art.of.Readable.Code(2011.11)].Dustin.Boswell.文字版.pdf|26084237|3b9d1f03f12e91f88177643b7bba0b9e|h=22adppn5njfeldnyfgigl5xnlm3esbaf|/)

[[The.Art.of.Readable.Code(2011.11)].Dustin.Boswell.文字版.epub](ed2k://|file|[The.Art.of.Readable.Code(2011.11)].Dustin.Boswell.文字版.epub|7890176|dcf00f47572892e9379195bcc46104a2|h=tjuq2wfdoadjor5msyuke7dmozylpq3j|/)

[[Programming.Firefox(2007.4)].Kenneth.C.Feldt.文字版.pdf](ed2k://|file|[Programming.Firefox(2007.4)].Kenneth.C.Feldt.文字版.pdf|5835035|441DC0D2210427A7DEB44EBF032EC2F8|h=KFWL45HN3HRY6N7HTTHIBWDJLWM53ASN|/)

[[Making.Embedded.Systems(2011.10)].Elecia.White.文字版.pdf](ed2k://|file|[Making.Embedded.Systems(2011.10)].Elecia.White.文字版.pdf|13429714|a6e5df7e40925f7a490243738a159dcd|h=sqcprjd4lawccw6axjlbqk7f3nsndwg2|/)

[[Making.Embedded.Systems(2011.10)].Elecia.White.文字版.epub](ed2k://|file|[Making.Embedded.Systems(2011.10)].Elecia.White.文字版.epub|3837589|e0ba1bffc6dfa07b6f77f2531f3984ac|h=m6qeshiieadqv6xoryudnfxmnihnurax|/)

[[Python.and.AWS.Cookbook(2011.10)].Mitch.Garnaat.文字版.pdf](ed2k://|file|[Python.and.AWS.Cookbook(2011.10)].Mitch.Garnaat.文字版.pdf|4812393|7527380bfc5f979636bd6451b2bf3b0e|h=xta5eput26k7565sjudlhxgscjqfcj3l|/)

[[Python.and.AWS.Cookbook(2011.10)].Mitch.Garnaat.文字版.epub](ed2k://|file|[Python.and.AWS.Cookbook(2011.10)].Mitch.Garnaat.文字版.epub|771195|f0022f69c27ea4f018a0cba1a014e100|h=yplnxk2ruhel3uphc25lnzjnhxdevp5x|/)

[[REST.API.Design.Rulebook(2011.10)].Mark.Masse.文字版.pdf](ed2k://|file|[REST.API.Design.Rulebook(2011.10)].Mark.Masse.文字版.pdf|5246930|bf3da66f1f21301a32e49dd1ddfda500|h=2ojmfc3k6s7tcd4eyheqp2jwvdnnixwz|/)

[[REST.API.Design.Rulebook(2011.10)].Mark.Masse.文字版.epub](ed2k://|file|[REST.API.Design.Rulebook(2011.10)].Mark.Masse.文字版.epub|1091260|157dacb20f00e6182b6d0382f3411b47|h=as6nf5gz76l7ss3ww6bt5tq5vnwtxwg2|/)

[[Mastering.Search.Analytics：Measuring.SEO.SEM.and.Site.Search(2011.10)].Brent.Chaters.文字版.pdf](ed2k://|file|[Mastering.Search.Analytics：Measuring.SEO.SEM.and.Site.Search(2011.10)].Brent.Chaters.文字版.pdf|25007905|adf6759880c3ed27a2858476ae4dccdc|h=jyg3j62mgki6ibj3owijd6ebqnmmcyvq|/)

[[iOS.5.Programming.Cookbook(Early.Release)].Vandad.Nahavandipoor.文字版.pdf](ed2k://|file|[iOS.5.Programming.Cookbook(Early.Release)].Vandad.Nahavandipoor.文字版.pdf|26154140|3059f9de45fa0aae52b78294471daf86|h=ytgtc2iqdgrftnwzo5yrzkm7qinsoen7|/)

[[MacRuby.The.Definitive.Guide：Ruby.and.Cocoa.on.OS.X(2011.10)].Matt.Aimonetti.文字版.pdf](ed2k://|file|[MacRuby.The.Definitive.Guide：Ruby.and.Cocoa.on.OS.X(2011.10)].Matt.Aimonetti.文字版.pdf|11524294|95ed24b3b38f40b051c93005da102e46|h=mfx66yttm7woz2puem4yxidnzusssznz|/)

[[Automating.ActionScript.Projects.with.Eclipse.and.Ant(2011.9)].Sidney.de.Koning.文字版.pdf](ed2k://|file|[Automating.ActionScript.Projects.with.Eclipse.and.Ant(2011.9)].Sidney.de.Koning.文字版.pdf|6865115|2fa97e5a1151969cebb758963aee67d1|h=2iuxf5fi536xpjpeknydovt4z3ifwwme|/)

[[Automating.ActionScript.Projects.with.Eclipse.and.Ant(2011.9)].Sidney.de.Koning.文字版.epub](ed2k://|file|[Automating.ActionScript.Projects.with.Eclipse.and.Ant(2011.9)].Sidney.de.Koning.文字版.epub|1988257|79f9960e323a614386281f9bffe0877b|h=vbl7p2jqefxixirdjkuinew2dmtg5a4d|/)

[[Making.Things.See：3D.vision.with.Kinect,Processing,and.Arduino(Early.Release)].Greg.Borenstein.文字版.pdf](ed2k://|file|[Making.Things.See：3D.vision.with.Kinect,Processing,and.Arduino(Early.Release)].Greg.Borenstein.文字版.pdf|12940589|95125ef695d4c9a665e57507e174a2ff|h=r7heqddikkc6qghrvsxmlsdxu6hion7w|/)

[[HTML5.for.Publishers(2011.10)].Sanders.Kleinfeld.文字版.pdf](ed2k://|file|[HTML5.for.Publishers(2011.10)].Sanders.Kleinfeld.文字版.pdf|2138854|c1ef1468a0b7b11b408516f9b474dcfe|h=od5xemu2wur4q7urhqeyninthrmomyjo|/)

[[Meaningful.Use.and.Beyond：A.Guide.for.IT.Staff.in.Health.Care(2011.9)].Fred.Trotter.文字版.pdf](ed2k://|file|[Meaningful.Use.and.Beyond：A.Guide.for.IT.Staff.in.Health.Care(2011.9)].Fred.Trotter.文字版.pdf|6575105|1c5d44ffc2bc048c39f1db33f66c7f1e|h=k4mrarthqpw3jdmqmy6t6424tpqmwyu7|/)

[[What.Is.EPUB.3？(2011.9)].Matt.Garrish.文字版.pdf](ed2k://|file|[What.Is.EPUB.3？(2011.9)].Matt.Garrish.文字版.pdf|1135778|8073086a1b8ad6ac890764eb00376493|h=mgcaueq7jmjob3abda7jpni45gg2tepd|/)

[[Building.eCommerce.Applications(2011.10)].Developers.from.DevZone.文字版.pdf](ed2k://|file|[Building.eCommerce.Applications(2011.10)].Developers.from.DevZone.文字版.pdf|12601049|a75bab9095e074f1073f936a61c8911a|h=yqeoiimlchcroueccsqm4gpd2maef4af|/)

[[Embedded.Android：Porting.Extending.and.Customizing(Early.Release)].Karim.Yaghmour.文字版.pdf](ed2k://|file|[Embedded.Android：Porting.Extending.and.Customizing(Early.Release)].Karim.Yaghmour.文字版.pdf|1939164|5e801581dee7bd6c4dabae0b96ca0a66|h=szr4yj72qtpvf4cltbnfbksovdujc2ll|/)

[[Android.Cookbook(Early.Release)].Ian.F.Darwin.文字版.pdf](ed2k://|file|[Android.Cookbook(Early.Release)].Ian.F.Darwin.文字版.pdf|8891577|fcb0f1afeb87ce44298f416b75cb377d|h=dlsrltkkj7wm7kks4tparjf4phwtvkyd|/)

[[Making.Things.Talk(2nd.2011.9)].Tom.Igoe.文字版.pdf](ed2k://|file|[Making.Things.Talk(2nd.2011.9)].Tom.Igoe.文字版.pdf|52316424|4c7363eff2ae6cf15aa976decc8e557d|h=o4vze7p6pebimzoxa7ons2yc7jnlle7o|/)

[[Getting.Started.with.Arduino(2nd.2011.9)].Massimo.Banzi.文字版.pdf](ed2k://|file|[Getting.Started.with.Arduino(2nd.2011.9)].Massimo.Banzi.文字版.pdf|6747182|5d73bfddb5a51d721dc7078c1edf5902|h=nxev4npbnhmjedoh5fjkyrvknupdxc4k|/)

[[Getting.Started.with.Arduino(2nd.2011.9)].Massimo.Banzi.文字版.epub](ed2k://|file|[Getting.Started.with.Arduino(2nd.2011.9)].Massimo.Banzi.文字版.epub|3232646|8fee8a629dcbf30f4a00c1f5d671c028|h=y2qnjlnwfltpjgt4ci6gndfcnqi7qpba|/)

[[Privacy.and.Big.Data(2011.9)].Terence.Craig.文字版.pdf](ed2k://|file|[Privacy.and.Big.Data(2011.9)].Terence.Craig.文字版.pdf|6297717|11473eea7fc6122e8fa0b3551dc399ce|h=hiarz5p6txs5c6my4y3jiqg5xkaaybac|/)

[[Privacy.and.Big.Data(2011.9)].Terence.Craig.文字版.epub](ed2k://|file|[Privacy.and.Big.Data(2011.9)].Terence.Craig.文字版.epub|810086|b3e21682fa82b3c4beb6eee7e9772ac1|h=7m5ucneag5gfkhjhubzgb6krsfvzce6d|/)

[[What.Is.HTML5？(2011.7)].Brett.McLaughlin.文字版.pdf](ed2k://|file|[What.Is.HTML5？(2011.7)].Brett.McLaughlin.文字版.pdf|3586982|857a3f256fe44354d1dac4041b0c5b20|h=bqd7ojfbrzkqs2s2ig5nef4ygl7tqi57|/)

[[Programming.HTML5.Applications(Early.Release)].Zachary.Kessin.文字版.pdf](ed2k://|file|[Programming.HTML5.Applications(Early.Release)].Zachary.Kessin.文字版.pdf|1448836|4399a54747e27c382843dad6abdb6424|h=odmzugsyyhifj6j5b37yp6rgc3vo5ime|/)

[[Coding4fun：10.NetProgrammingProjects.for.Wiimote.Youtube.World.of.Warcraft.and.More(2008)].Dan.Fernandez.文字版.epub](ed2k://|file|[Coding4fun：10.NetProgrammingProjects.for.Wiimote.Youtube.World.of.Warcraft.and.More(2008)].Dan.Fernandez.文字版.epub|11055790|9b5a7fde8e52176144010e73c50b6e90|h=dbi7gdaywb6ckozqlhmbepwrblk2xyjh|/)

[[HTML5.Cookbook(Early.Release)].Christopher.Schmitt.文字版.pdf](ed2k://|file|[HTML5.Cookbook(Early.Release)].Christopher.Schmitt.文字版.pdf|6764847|1b29d74914ec93ef574f053986ea857f|h=i2rbrcdidhu7deehhumi2b43jdzpxyxp|/)

[[20.Recipes.for.Programming.MVC.3(2011.9)].Jamie.Munro.文字版.pdf](ed2k://|file|[20.Recipes.for.Programming.MVC.3(2011.9)].Jamie.Munro.文字版.pdf|5957118|c5b6866b46376201dae9213e2359b937|h=ofoxj3ybug3r6szvcsbt2clrdai2jl7q|/)

[[20.Recipes.for.Programming.MVC.3(2011.9)].Jamie.Munro.文字版.epub](ed2k://|file|[20.Recipes.for.Programming.MVC.3(2011.9)].Jamie.Munro.文字版.epub|1933410|252f7f113645863eadafdfc2700ca1ab|h=a52yxboukdzlutpo7wwmlxpdteczkrst|/)

[[Social.Network.Analysis.for.Startups(2011.9)].Maksim.Tsvetovat.文字版.pdf](ed2k://|file|[Social.Network.Analysis.for.Startups(2011.9)].Maksim.Tsvetovat.文字版.pdf|15993040|e1ef50962e781a59a9cc72d88266783e|h=ptm4oqx3jd7wkpfk22tja5ysdwyhdidw|/)

[[Social.Network.Analysis.for.Startups(2011.9)].Maksim.Tsvetovat.文字版.epub](ed2k://|file|[Social.Network.Analysis.for.Startups(2011.9)].Maksim.Tsvetovat.文字版.epub|5574406|f1c7952f8f4c2c886f248c665f66fde6|h=22kvutpnzmdkf7vhswfxuo5tlpbygmiz|/)

[[Getting.Started.with.RStudio：An.Integrated.Development.Environment.for.R(2011.9)].John.Verzani.文字版.pdf](ed2k://|file|[Getting.Started.with.RStudio：An.Integrated.Development.Environment.for.R(2011.9)].John.Verzani.文字版.pdf|8205429|e8e3f0833d7f41ee2ee26d24a3ad9745|h=w7izfo7ghh2l2xx4375lb7tr5ofd7m2l|/)

[[Getting.Started.with.RStudio：An.Integrated.Development.Environment.for.R(2011.9)].John.Verzani.文字版.epub](ed2k://|file|[Getting.Started.with.RStudio：An.Integrated.Development.Environment.for.R(2011.9)].John.Verzani.文字版.epub|3022998|c0554aa6303be61829a6efa29f3c5471|h=f6mgxfhptrkuaox7tlqmszbvfgcgy2n2|/)

[[Integrating.PHP.Projects.with.Jenkins(2011.9)].Sebastian.Bergmann.文字版.pdf](ed2k://|file|[Integrating.PHP.Projects.with.Jenkins(2011.9)].Sebastian.Bergmann.文字版.pdf|5742211|98e10761f5809e4fb095e15e460c54dd|h=sbsisjqtxol6m4bcymljtdqskjcjh5c2|/)

[[Integrating.PHP.Projects.with.Jenkins(2011.9)].Sebastian.Bergmann.文字版.epub](ed2k://|file|[Integrating.PHP.Projects.with.Jenkins(2011.9)].Sebastian.Bergmann.文字版.epub|2389809|32b971648f53d18c131ab70a55571d26|h=vijkmdnkf7c7gvtgpyjiljrw2qhp2umx|/)

[[MongoDB.and.Python(2011.9)].Niall.O'Higgins.文字版.pdf](ed2k://|file|[MongoDB.and.Python(2011.9)].Niall.O'Higgins.文字版.pdf|4786629|b9ebf81d69615e36a4a03b77153a25c8|h=lvtrqdk6fbjh56wrngfux5bq2gnpmgn4|/)

[[MongoDB.and.Python(2011.9)].Niall.O'Higgins.文字版.epub](ed2k://|file|[MongoDB.and.Python(2011.9)].Niall.O'Higgins.文字版.epub|629964|79dacb20b24e32f5d3d2788e74c4f71a|h=qsldhsrisoyekqe3dwst3i34c6hn3ldf|/)

[[Big.Data.Glossary：A.Guide.to.the.New.Generation.of.Data.Tools(2011.9)].Pete.Warden.文字版.pdf](ed2k://|file|[Big.Data.Glossary：A.Guide.to.the.New.Generation.of.Data.Tools(2011.9)].Pete.Warden.文字版.pdf|5919615|7dfa42090d27b0520db7d150f0fffa78|h=u24l2po3topdzmewdxuntoyv5iaf7j36|/)

[[Big.Data.Glossary：A.Guide.to.the.New.Generation.of.Data.Tools(2011.9)].Pete.Warden.文字版.epub](ed2k://|file|[Big.Data.Glossary：A.Guide.to.the.New.Generation.of.Data.Tools(2011.9)].Pete.Warden.文字版.epub|2852588|1ad9ddd8ef66356985fd1d6db469a126|h=mjo4rkui6kljkio43qqskxxxke5crke7|/)

[[Creating.HTML5.Animations.with.Flash.and.Wallaby(2011.9)].Ian.L.McLean.文字版.pdf](ed2k://|file|[Creating.HTML5.Animations.with.Flash.and.Wallaby(2011.9)].Ian.L.McLean.文字版.pdf|6248007|a347fdc8633a20bc3123c6129a1c7c80|h=bfecncoihqswrg37jbmyuaijcwvqjoys|/)

[[Creating.HTML5.Animations.with.Flash.and.Wallaby(2011.9)].Ian.L.McLean.文字版.epub](ed2k://|file|[Creating.HTML5.Animations.with.Flash.and.Wallaby(2011.9)].Ian.L.McLean.文字版.epub|1696532|ec7b86ba016de25c99f3c62cd3757f1d|h=vyoezhw2xaqjet3ktlattxfgxajvlq6j|/)

[[iOS.Sensor.Apps.with.Arduino(2011.9)].Alasdair.Allan.文字版.pdf](ed2k://|file|[iOS.Sensor.Apps.with.Arduino(2011.9)].Alasdair.Allan.文字版.pdf|43407619|068e75b3b92f757283337a9d74197742|h=5v3xmzsbnmko6k3d2ajgljfjg3e62gkr|/)

[[iOS.Sensor.Apps.with.Arduino(2011.9)].Alasdair.Allan.文字版.epub](ed2k://|file|[iOS.Sensor.Apps.with.Arduino(2011.9)].Alasdair.Allan.文字版.epub|8060436|8b1fcf0a3c294c9d97afaca3d00ef04c|h=qr2z7n4nmj4sjmje6tssmqep4ktw4wcf|/)

[[Developing.Enterprise.iOS.Applications(Early.Release)].James.Turner.文字版.pdf](ed2k://|file|[Developing.Enterprise.iOS.Applications(Early.Release)].James.Turner.文字版.pdf|4519313|2dd8b0fa58ee3d70ffa762d620cfb6c4|h=boiyt3mct6ysc3ug64lhs6jxomduan4n|/)

[[Developing.Enterprise.iOS.Applications(Early.Release)].James.Turner.文字版.epub](ed2k://|file|[Developing.Enterprise.iOS.Applications(Early.Release)].James.Turner.文字版.epub|4331266|2cc00a192f572ed22430ef8e329b5e73|h=hg6uil3zp3v6aflvms6gnnvistwjjt43|/)

[[Planning.and.Managing.Drupal.Projects：Drupal.for.Designers(2011.9)].Dani.Nordin.文字版.pdf](ed2k://|file|[Planning.and.Managing.Drupal.Projects：Drupal.for.Designers(2011.9)].Dani.Nordin.文字版.pdf|8309171|e43db6ca7ea90b1a38427f1a769bae75|h=fijkvsyihh3dta5sbwzz4sdssmhumv3m|/)

[[Planning.and.Managing.Drupal.Projects：Drupal.for.Designers(2011.9)].Dani.Nordin.文字版.epub](ed2k://|file|[Planning.and.Managing.Drupal.Projects：Drupal.for.Designers(2011.9)].Dani.Nordin.文字版.epub|3051193|99b2119701cd64b7420d6db51cb52417|h=dmegnk7rdr4fam42sk2punapmiog7zxa|/)

[[Planning.for.IPv6：Join.the.New.Internet(2011.9)].Silvia.Hagen.文字版.pdf](ed2k://|file|[Planning.for.IPv6：Join.the.New.Internet(2011.9)].Silvia.Hagen.文字版.pdf|2686956|b9bf413471d775faa1375fbfa6bd571e|h=hhgpxlwvagak66m5uxiv3n32rlttpzpl|/)

[[Planning.for.IPv6：Join.the.New.Internet(2011.9)].Silvia.Hagen.文字版.epub](ed2k://|file|[Planning.for.IPv6：Join.the.New.Internet(2011.9)].Silvia.Hagen.文字版.epub|844363|754950283828adedf1aad448bcefc13a|h=jl5hibtcq7mx4kmv5qstkci7gtvoyzxg|/)

[[Programming.Pig：Dataflow.Scripting.with.Hadoop(2011.9)].Alan.Gates.文字版.pdf](ed2k://|file|[Programming.Pig：Dataflow.Scripting.with.Hadoop(2011.9)].Alan.Gates.文字版.pdf|6723942|a314dfb44466938e85d2a6cb3b8a96b0|h=pk55naqthezehv57lq2szgm2w4iijmyv|/)

[[Programming.Pig：Dataflow.Scripting.with.Hadoop(2011.9)].Alan.Gates.文字版.epub](ed2k://|file|[Programming.Pig：Dataflow.Scripting.with.Hadoop(2011.9)].Alan.Gates.文字版.epub|2410606|148732fcfbcee84dec5b505212e1703c|h=malm2lzdg7777okbehrnxynas5apcdqv|/)

[[Programming.Razor：Tools.for.Templates.in.ASP.NET.MVC.or.WebMatrix(2011.9)].Jess.Chadwick.文字版.pdf](ed2k://|file|[Programming.Razor：Tools.for.Templates.in.ASP.NET.MVC.or.WebMatrix(2011.9)].Jess.Chadwick.文字版.pdf|4512135|f5d0530859bf58b9693472202d833cbf|h=qgmeh4d7uyq7skkr6fih6sqrz7qo3nuf|/)

[[Programming.Razor：Tools.for.Templates.in.ASP.NET.MVC.or.WebMatrix(2011.9)].Jess.Chadwick.文字版.epub](ed2k://|file|[Programming.Razor：Tools.for.Templates.in.ASP.NET.MVC.or.WebMatrix(2011.9)].Jess.Chadwick.文字版.epub|1183006|a3b8bf4a19529ce19fc2c4e55e0d57bc|h=3lnuxsd2mryizbfvqhi7r6uaknzsp65x|/)

[[Designing.Data.Visualizations(2011.9)].Julie.Steele.文字版.pdf](ed2k://|file|[Designing.Data.Visualizations(2011.9)].Julie.Steele.文字版.pdf|19749532|f953ef070235d9d7e83f66a064bc5c10|h=eqf5l7avejn6wlfvuhslmx2ewgvmke7u|/)

[[Designing.Data.Visualizations(2011.9)].Julie.Steele.文字版.epub](ed2k://|file|[Designing.Data.Visualizations(2011.9)].Julie.Steele.文字版.epub|3886479|e55e19fc37749fb73ff601789e7e2567|h=shgspguyvuawzli6ohjz6lw3qsysksj3|/)

[[Getting.Started.with.Processing(2010.6)].Casey.Reas.文字版.pdf](ed2k://|file|[Getting.Started.with.Processing(2010.6)].Casey.Reas.文字版.pdf|5004608|1FC2FDEE22D83971135C0CB0D64CFC8D|h=C66OFTJ2UH6B6DZYHQ7RRZEYG45VPTAN|/)

[[The.Twitter.Book(2011,2nd)].Tim.O'Reilly.文字版.pdf](ed2k://|file|[The.Twitter.Book(2011,2nd)].Tim.O'Reilly.文字版.pdf|17311550|42471f041e46bada27179c9d6488ea6d|h=5lclgcnt7lgg42ganuum5gmahodd5zko|/)

[[The.Twitter.Book(2011,2nd)].Tim.O'Reilly.文字版.epub](ed2k://|file|[The.Twitter.Book(2011,2nd)].Tim.O'Reilly.文字版.epub|10930876|864b29d38efa5f065a932a1306acfcc7|h=rmcjtv2enuqe3uvjz5qodwklb7okhdwq|/)

[[The.Twitter.Book(第1版)].(The.Twitter.Book).Tim.O'Reilly&Sarah.Milstein.文字版.pdf](ed2k://|file|[The.Twitter.Book(第1版)].(The.Twitter.Book).Tim.O'Reilly&Sarah.Milstein.文字版.pdf|64960802|49beed4468ac6171f7a43707271c1e7a|h=cm3jrr72nrbgv62x5sqmvvtm6qaqyk7d|/)

[[Learning.PHP.&.MySQL.(2nd.2007.8)].Michele.E.Davis.文字版.pdf](ed2k://|file|[Learning.PHP.&.MySQL.(2nd.2007.8)].Michele.E.Davis.文字版.pdf|6548537|46149ad817fc2f24b179cca407d30b36|h=s2z5gqch3rdh37o767mnlh52bik3mvat|/)

[[Secure.Programming.Cookbook.for.C.and.C.(2003.7)].John.Viega.文字版.pdf](ed2k://|file|[Secure.Programming.Cookbook.for.C.and.C.(2003.7)].John.Viega.文字版.pdf|4497204|37f96ece1ea9e05bcb4befc921bc8f5a|h=7tbuns6v36qficsc25lbajgrdddcpi6p|/)

[[Secure.Programming.Cookbook.for.C.and.C.(2003.7)].John.Viega.文字版.epub](ed2k://|file|[Secure.Programming.Cookbook.for.C.and.C.(2003.7)].John.Viega.文字版.epub|3512492|98acdf96f818aef9d2e4229199907cde|h=xufxsy5cueh2yh7caocayoodqkmyp4ki|/)

[[Internet.Core.Protocols.The.Definitive.Guide(2000.2)].Eric.Hall.文字版.pdf](ed2k://|file|[Internet.Core.Protocols.The.Definitive.Guide(2000.2)].Eric.Hall.文字版.pdf|5230173|2bb46f3fc200870d07404d1e4a9c8ef2|h=asmrr6fujs4md43z6z3nwt5k6nftryhb|/)

[[BGP(2002.9)].Iljitsch.van.Beijnum.文字版.pdf](ed2k://|file|[BGP(2002.9)].Iljitsch.van.Beijnum.文字版.pdf|2336027|00b7259653d82d3490b025952dc283d8|h=5dygvwxbzfpenst2vasqwguj432mxxmz|/)

[[BGP(2002.9)].Iljitsch.van.Beijnum.文字版.epub](ed2k://|file|[BGP(2002.9)].Iljitsch.van.Beijnum.文字版.epub|3706220|34c2c3a175f61d5df3bda04e5f15d4aa|h=sp43jkn2mys43guoqwudbaosdxuzrjvo|/)

[[Apache.Cookbook(2nd.2007.12)].Rich.Bowen.文字版.pdf](ed2k://|file|[Apache.Cookbook(2nd.2007.12)].Rich.Bowen.文字版.pdf|3092581|643f1fcc0b7b913eeb26bf4f1915dd96|h=7ypk4a4cizqz33hwpwmetuwzdt4ftwzl|/)

[[Apache.Cookbook(2nd.2007.12)].Rich.Bowen.文字版.epub](ed2k://|file|[Apache.Cookbook(2nd.2007.12)].Rich.Bowen.文字版.epub|1748870|d162e4be6e2176a77dc1527607c4ccd9|h=2sgpcpseppcvu2fya3mx6tndadkutfeb|/)

[[Ajax.The.Definitive.Guide(2008.1)].Anthony.T.Holdener.文字版.pdf](ed2k://|file|[Ajax.The.Definitive.Guide(2008.1)].Anthony.T.Holdener.文字版.pdf|13695899|de1617ba903c65a028a4c71db1dc49c2|h=5jrbrbhz37wdfvwcx5cchyzwcb7alnwh|/)

[[Ajax.The.Definitive.Guide(2008.1)].Anthony.T.Holdener.文字版.epub](ed2k://|file|[Ajax.The.Definitive.Guide(2008.1)].Anthony.T.Holdener.文字版.epub|14326581|7e060f370efa7a3abd29595ff7538349|h=hcxu225kiatu3ms7t2nqubecryumjphy|/)

[[HTML5.Media(2011.8)].Shelley.Powers.文字版.pdf](ed2k://|file|[HTML5.Media(2011.8)].Shelley.Powers.文字版.pdf|11666305|ca8df2e56a9a01d016761598271df2eb|h=3eo2dszxuy7wrpg4vt2g2o6dvyij2tj2|/)

[[HTML5.Media(2011.8)].Shelley.Powers.文字版.epub](ed2k://|file|[HTML5.Media(2011.8)].Shelley.Powers.文字版.epub|4088641|6591dcc94014623a6a01c18edcaddd8b|h=nbrlgsykfo2l52riyn5zlcpikbt3zgcy|/)

[[JavaScript.Web.Applications].Alex.MacCaw.文字版.pdf](ed2k://|file|[JavaScript.Web.Applications].Alex.MacCaw.文字版.pdf|10011539|4e0913dd11b56bb59411ac50f993ffd0|h=7fgsyb7o4ma534apivarbhjfpj6xbeue|/)

[[JavaScript.Web.Applications].Alex.MacCaw.文字版.epub](ed2k://|file|[JavaScript.Web.Applications].Alex.MacCaw.文字版.epub|2323210|c904a2da1045951c21fb40dcc4f1c115|h=2tlq5r4jrhjzbzdjpqpl5nq43prpmomu|/)

[[Developing.iOS.Applications.with.Flex.4.5].Rich.Tretola.文字版.pdf](ed2k://|file|[Developing.iOS.Applications.with.Flex.4.5].Rich.Tretola.文字版.pdf|14159803|b83bc86bb8e7790d6a1138f49b8be323|h=bpfc2apjlyb3i4646y2ac3ww43hpyt2n|/)

[[Developing.iOS.Applications.with.Flex.4.5].Rich.Tretola.文字版.epub](ed2k://|file|[Developing.iOS.Applications.with.Flex.4.5].Rich.Tretola.文字版.epub|6702383|8b277997b9bce332d12da6e494567aa0|h=lijqsvfttgkqftirbsjd74wahzb4zeyj|/)

[[HBase.The.Definitive.Guide].Lars.George.文字版.pdf](ed2k://|file|[HBase.The.Definitive.Guide].Lars.George.文字版.pdf|12733523|489c5de9f5bf388ef9509bc060317903|h=pwr5obbujsjmvhnlkqxeo647csbsoyak|/)

[[HBase.The.Definitive.Guide].Lars.George.文字版.epub](ed2k://|file|[HBase.The.Definitive.Guide].Lars.George.文字版.epub|4600618|6e7e8dc6de09c4d2575611828f5800b0|h=2dk6xkkvozep66uuovmiofeqpczpwjff|/)

[[Quick.Guide.to.Flash.Catalyst(第1版)].Rafiq.Elmansy.文字版.pdf](ed2k://|file|[Quick.Guide.to.Flash.Catalyst(第1版)].Rafiq.Elmansy.文字版.pdf|16180520|e9ab444cb487024c1441d3088872b89d|h=gqqc2cel3fpsnwdn5tweeto7p4t7axrn|/)

[[Quick.Guide.to.Flash.Catalyst(第1版)].Rafiq.Elmansy.文字版.epub](ed2k://|file|[Quick.Guide.to.Flash.Catalyst(第1版)].Rafiq.Elmansy.文字版.epub|11041141|544faad423b6911695cbf128264454c9|h=u5xofyww2pft6furxmqfudys3w2euprm|/)

[[Programming.Social.Applications(第1版)].Jonathan.LeBlanc.文字版.pdf](ed2k://|file|[Programming.Social.Applications(第1版)].Jonathan.LeBlanc.文字版.pdf|13642094|37790542df431fc2454279a9c5c0205f|h=bhhqxaf7zq7l56ic7yseqw5psmscnwsv|/)

[[Programming.Social.Applications(第1版)].Jonathan.LeBlanc.文字版.epub](ed2k://|file|[Programming.Social.Applications(第1版)].Jonathan.LeBlanc.文字版.epub|5948765|82a1476d18aa2afbac650b86c9f41170|h=cauxm7veerow24csxrh5qtektd7mg67u|/)

[[Making.Isometric.Social.Real-Time.Games.with.HTML5.CSS3.and.JavaScript(第1版)].Mario.Andres.Pagella.文字版.pdf](ed2k://|file|[Making.Isometric.Social.Real-Time.Games.with.HTML5.CSS3.and.JavaScript(第1版)].Mario.Andres.Pagella.文字版.pdf|8969493|780fefe1c8566d35a91545ee4097800e|h=zb3vbuzuuyuwql32u43aalqyaclo5kgq|/)

[[Making.Isometric.Social.Real-Time.Games.with.HTML5.CSS3.and.JavaScript(第1版)].Mario.Andres.Pagella.文字版.epub](ed2k://|file|[Making.Isometric.Social.Real-Time.Games.with.HTML5.CSS3.and.JavaScript(第1版)].Mario.Andres.Pagella.文字版.epub|2075174|160249a6047faf5f8ee63ee64cc534b1|h=cbm3uvfs5sdviftylg4rua5puschrzx6|/)

[[Getting.Started.with.Roo(第1版)].Josh.Long.文字版.pdf](ed2k://|file|[Getting.Started.with.Roo(第1版)].Josh.Long.文字版.pdf|9298877|619bffb8b2e4f20cf35ab4485ec0a625|h=kphsqzlenevohxoc7nxvpkr7hazjbtmz|/)

[[Getting.Started.with.Roo(第1版)].Josh.Long.文字版.epub](ed2k://|file|[Getting.Started.with.Roo(第1版)].Josh.Long.文字版.epub|2236542|2b73b9908bfe61d98fbb982d475ee251|h=rs5cr2rcey2uaseiqa2ie5gifibil7lc|/)

[[ActionScript.Developers.Guide.to.Robotlegs(第1版)].Joel.Hooks.文字版.pdf](ed2k://|file|[ActionScript.Developers.Guide.to.Robotlegs(第1版)].Joel.Hooks.文字版.pdf|12248022|a44e8f054953f22a1768ef01b43524a2|h=dxf2jbokxh7kcnbua4abe356rrpwku62|/)

[[ActionScript.Developers.Guide.to.Robotlegs(第1版)].Joel.Hooks.文字版.epub](ed2k://|file|[ActionScript.Developers.Guide.to.Robotlegs(第1版)].Joel.Hooks.文字版.epub|3357852|1e0c84c7fc48de7f7fc9bfc68dcb9b23|h=5ywtzqqarermwgvmi5jrsypmws2hr5ko|/)

[[Linux.Server.Hacks(第1版)].(Linux.Server.Hacks.2003).Rob.Flickenger.文字版.pdf](ed2k://|file|[Linux.Server.Hacks(第1版)].(Linux.Server.Hacks.2003).Rob.Flickenger.文字版.pdf|2319726|b11dcc6ce3b86b08a91db552c1c7a45c|h=dcshgst2z3xdiatss5sa2wjpcd3ngpoy|/)

[[Linux.Server.Hacks(第1版)].(Linux.Server.Hacks.2003).Rob.Flickenger.文字版.epub](ed2k://|file|[Linux.Server.Hacks(第1版)].(Linux.Server.Hacks.2003).Rob.Flickenger.文字版.epub|2809823|cbebb87ab4be96df871ef9cd9ea7f0e3|h=vttb3nv2jn6ri4drvt57mc6jvvtvxjrc|/)

[[Programming.PHP(第2版)].(Programming.PHP.2006).Rasmus.Lerdorf.文字版.pdf](ed2k://|file|[Programming.PHP(第2版)].(Programming.PHP.2006).Rasmus.Lerdorf.文字版.pdf|4702467|693a64f406f052be76b7aa077250cd94|h=3oskpvxbsj5kwhbg2x35wpetmsr3jlyq|/)

[[Programming.PHP(第2版)].(Programming.PHP.2006).Rasmus.Lerdorf.文字版.epub](ed2k://|file|[Programming.PHP(第2版)].(Programming.PHP.2006).Rasmus.Lerdorf.文字版.epub|5254456|43492f193b00a241f06e4032a3ca5390|h=lg7sg7f4vfrvbicerifhqrhz3yvsvthh|/)

[[Classic.Shell.Scripting(第1版)].(Classic.Shell.Scripting).Arnold.Robbins.文字版.pdf](ed2k://|file|[Classic.Shell.Scripting(第1版)].(Classic.Shell.Scripting).Arnold.Robbins.文字版.pdf|10349162|0ecb9fe4253f6a7227799410118e475c|h=6hgkmfj4rtkkw4jltefjdqo7atn4ewie|/)

[[Classic.Shell.Scripting(第1版)].(Classic.Shell.Scripting).Arnold.Robbins.文字版.epub](ed2k://|file|[Classic.Shell.Scripting(第1版)].(Classic.Shell.Scripting).Arnold.Robbins.文字版.epub|1318312|9e3e75daf5e8c23098f573c59934e950|h=vpuetabudvw7sdiueyykj4d65xxlgyle|/)

[[Cisco.IOS.Cookbook(第2版)].(Cisco.IOS.Cookbook.2006).Kevin.Dooley.文字版.pdf](ed2k://|file|[Cisco.IOS.Cookbook(第2版)].(Cisco.IOS.Cookbook.2006).Kevin.Dooley.文字版.pdf|5781822|26fd4b8358ca91a48196ceec6277176d|h=3kxsn25tirzs4doyipb23jvtjwdqqtka|/)

[[Cisco.IOS.Cookbook(第2版)].(Cisco.IOS.Cookbook.2006).Kevin.Dooley.文字版.epub](ed2k://|file|[Cisco.IOS.Cookbook(第2版)].(Cisco.IOS.Cookbook.2006).Kevin.Dooley.文字版.epub|4145430|b4ce7a33f4fcab5f32800ba7cefcb429|h=lx6aod7t6h7i56j5uoz73f7vhtgehsul|/)

[[Active.Directory.Cookbook(第3版)].(Active.Directory.Cookbook.2008).Laura.E.Hunter.文字版.pdf](ed2k://|file|[Active.Directory.Cookbook(第3版)].(Active.Directory.Cookbook.2008).Laura.E.Hunter.文字版.pdf|8138559|ff1feebc960ae6c700b537fe1b27bdb9|h=oduedlmam2un7siwexpqj3luj4ohqb56|/)

[[Active.Directory.Cookbook(第3版)].(Active.Directory.Cookbook.2008).Laura.E.Hunter.文字版.epub](ed2k://|file|[Active.Directory.Cookbook(第3版)].(Active.Directory.Cookbook.2008).Laura.E.Hunter.文字版.epub|2744971|6aedab6d7e854112eab0d4ce5dae5821|h=2qrf3elqzbjezcph65hyhy4w3bqzl4r5|/)

[[Programming.the.Semantic.Web(第1版,2009)].(Programming.the.Semantic.Web).Toby.Segaran.文字版.pdf](ed2k://|file|[Programming.the.Semantic.Web(第1版,2009)].(Programming.the.Semantic.Web).Toby.Segaran.文字版.pdf|5734964|8c1b38b4ac7b27c22eb62388f01b4835|h=u4wleo2sftw2qrot7azng5spxt7gxh6v|/)

[[Data.Analysis.with.Open.Source.Tools(第1版,2010)].(Data.Analysis.with.Open.Source.Tools).Philipp.K.Janert.文字版.pdf](ed2k://|file|[Data.Analysis.with.Open.Source.Tools(第1版,2010)].(Data.Analysis.with.Open.Source.Tools).Philipp.K.Janert.文字版.pdf|16138443|12d663641214b153dedaa251db8febd6|h=hszjz37jyfa736d25fsb4sqyfjehrohx|/)

[[Learning.Java(第3版,2005)].(Learning.Java).Patrick.Niemeyer.文字版.pdf](ed2k://|file|[Learning.Java(第3版,2005)].(Learning.Java).Patrick.Niemeyer.文字版.pdf|15256744|d39dbc04996c10d3990f5e6f84602796|h=dbk2lzwbw7ctvktdqt777thuncslocz5|/)

[[Killer.Game.Programming.in.Java(第1版,2005)].(Killer.Game.Programming.in.Java).Andrew.Davison.文字版.pdf](ed2k://|file|[Killer.Game.Programming.in.Java(第1版,2005)].(Killer.Game.Programming.in.Java).Andrew.Davison.文字版.pdf|10104366|c2e81b0f949fdc5f6b8295ab0c68a5be|h=skovil7phqemvckzpefuufueximq2wt3|/)

[[Codermetrics：Analytics.for.Improving.Software.Teams].(Codermetrics).Jonathan.Alexander.文字版.pdf](ed2k://|file|[Codermetrics：Analytics.for.Improving.Software.Teams].(Codermetrics).Jonathan.Alexander.文字版.pdf|7613893|6973cb94d3c81179d2893b5ec30449c8|h=aw5wcbgrmqk3f447jnsgyywk3fefvhya|/)

[[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.pdf](ed2k://|file|[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.pdf|7349542|20f408fc50440fb990816f130d4677ef|h=li2iuf7mbo42q6twsa4up2bametmbli2|/)

[[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.epub](ed2k://|file|[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.epub|2656548|8d4a469da4785fd587fe030dbe82047d|h=tmt755wg54blvjudo6x5ohgyaimu3s52|/)

[[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.mobi](ed2k://|file|[使用PHP和MySQL开发Web数据库应用(第2版,2004)].(Web.Database.Applications.with.PHP.and.MySQL).Hugh.E.Williams.文字版.mobi|14429328|4ca6d331c47a1bb876c1cec3453eb04b|h=eey77o35iturz3u6fiw42nw3et7xdnen|/)

[[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.pdf](ed2k://|file|[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.pdf|4014203|6b4be2126dbed66a87ba4f40b639c678|h=5jzbi654liqfifpytan7zvxqnqmmhq5m|/)

[[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.epub](ed2k://|file|[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.epub|3326227|938333dd3708510712803234de6e67cc|h=lafva54tbwt3tzzkdeobo6lk5njo6acu|/)

[[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.mobi](ed2k://|file|[Squid权威指南(第1版,2004)].(Squid.The.Definitive.Guide).Duane.Wessels.文字版.mobi|10611364|34b610b99d0d8a8cb13d546487707c3c|h=odn5nmij6myeauqsftp34xnqf36epc35|/)

[[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.pdf](ed2k://|file|[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.pdf|10653679|bf547fc0c9317a5a9e5579a3abadb311|h=maz4bf667qjqr7pe5uaynyl5eggvolgi|/)

[[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.epub](ed2k://|file|[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.epub|2038143|94fb60e79dd9802e675a8ff9d0f48946|h=ieojmqqplkw3oygemnw5t7ovinnphm6f|/)

[[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.mobi](ed2k://|file|[Java网络编程(第3版,2004)].(Java.Network.Programming).Elliotte.Rusty.Harold.文字版.mobi|9021773|07387d24a33ca1248848149a8db16bcd|h=5lekcfovhneo4vnkz7j5fhmij4gkhfod|/)

[[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.pdf](ed2k://|file|[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.pdf|5872409|43e737e1918ecbd232b9555c924ccb02|h=pnopcsn3aqmfjp55h5htv7fhu42v25s5|/)

[[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.epub](ed2k://|file|[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.epub|6029103|e7ce1061b67d856e24517c292ef324e1|h=ryhgvfh7ggwlttd735wl75lm7b5va2c5|/)

[[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.mobi](ed2k://|file|[Harnessing.Hibernate(第1版,2008)].(Harnessing.Hibernate).James.Elliott.文字版.mobi|13723292|8922f15779701890eacd1ddf7b5895a3|h=erez73ww6skj23yqspkudnml44dpfyut|/)

[[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.pdf](ed2k://|file|[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.pdf|7351241|3f82bd8ef42d11138053e9350604dd07|h=ob5hgbvrkbmo2cgyrvez5m7tpypdyeo6|/)

[[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.epub](ed2k://|file|[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.epub|6796753|158da49d20e448a68edaac81e8f887fb|h=fpi3aecbdai2wea5s3k73zuqvlbemb46|/)

[[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.mobi](ed2k://|file|[CSS权威指南(第3版,2006)].(CSS.The.Definitive.Guide).Eric.A.Meyer.文字版.mobi|21193672|76546043cb03994ad08077c02e6a7bff|h=ohvyrekhcoy2zzcnv6uyn7hrkh4mvrrw|/)

[[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.pdf](ed2k://|file|[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.pdf|5698381|f2a24648456ac43193cdd71bc1b28dc7|h=oftep5u2v6b2ox6cidrexebedujlkb4y|/)

[[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.epub](ed2k://|file|[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.epub|1220591|792289cf2bf236487ec269d034e41043|h=ne2czsupap6r3uzcqysuumed4p6rpcfz|/)

[[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.mobi](ed2k://|file|[Building.and.Testing.with.Gradle(第1版)].(Building.and.Testing.with.Gradle).T.Berglund&M.McCullough.文字版.mobi|2760747|5ad6f472e4dab6fffb4b6ff16a8c92ca|h=5rpin4etybilxqj53w4wqipxx7q3c5rg|/)

[[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.pdf](ed2k://|file|[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.pdf|7163967|60c03d99f4b5d8836703c8b356957f5c|h=v3uzf2x4cbvbkbksiefqdgekwkp5r7uy|/)

[[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.epub](ed2k://|file|[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.epub|786103|c1215a9e30f02a712aa9117627e0dfe9|h=m5z67w2wzquf5kitaf5gropvh5wcfi2p|/)

[[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.mobi](ed2k://|file|[GEO.CouchDB.and.Node.js入门].(Getting.Started.with.GEO.CouchDB.and.Node.js).Mick.Thompson.文字版.mobi|1851765|54666ad081ebc4ff33baae0e0da31c4e|h=k4i4qz2mbdooekgivy7iscfxfih5n6xf|/)

[[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.pdf](ed2k://|file|[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.pdf|23282357|a8f50e11fc98980a304934262db9aa3e|h=pf4aqj752dmxwxhx5krtwokifwipjfzp|/)

[[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.epub](ed2k://|file|[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.epub|20743043|911619dae638dc7e47643cd4d8c7d5ed|h=spvtzgjzalzhos2pytfnsax6dor4myzk|/)

[[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.mobi](ed2k://|file|[Jenkins权威指南].(Jenkins.The.Definitive.Guide).John.Ferguson.Smart.文字版.mobi|38435050|6b27bb444959ac73c57890b6c2ede5fa|h=b5ee3ys2lwfq3oqe2i3ttep3aehmazeh|/)

[[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.pdf](ed2k://|file|[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.pdf|7516026|2598acc2cd4da8847e41c90857910a8d|h=ulapphtcr4ihm7zoh2ti6447gnrimqru|/)

[[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.epub](ed2k://|file|[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.epub|1894126|49708c4e34e195c759c405c0236b16b0|h=3qe7emjwnsxigz63o2ym6t6u2e5rkjuk|/)

[[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.mobi](ed2k://|file|[Learning.SPARQL(第1版)].(Learning.SPARQL).Bob.DuCharme.文字版.mobi|4888322|d9371591920ee66c6ad311bdc12710b5|h=rxkh2lvsirmzb3bvopeq77vparpsdso4|/)

[[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.pdf](ed2k://|file|[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.pdf|14510366|10062f914baa1614830f6b430fc29b59|h=wnslyvnwwibr6crq7rl2xuufdouzlrfr|/)

[[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.epub](ed2k://|file|[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.epub|6071426|fbe34436cd8a6470d743e2f3573f2e68|h=ck6chigivrsdkcqgippq3lhdcc74fxnd|/)

[[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.mobi](ed2k://|file|[Designing.for.XOOPS(第1版)].(Designing.for.XOOPS).Sun.Ruoyu.文字版.mobi|10809803|aa678e28d1a55ca4a1801799c5ad5cb3|h=gxwfxmrcgf2huuucfhcbxnefmho7j7mn|/)

[[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.pdf](ed2k://|file|[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.pdf|7096437|95e1818fb1bbd86cc027a949692fd61a|h=vugxsvks2wfbisces2amivzbaalbaspf|/)

[[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.epub](ed2k://|file|[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.epub|2644803|a2970edb20d5eb0378417f4f27b5d3fd|h=bsocsol5wt2vwpvzmgep7bbqcx22q5cc|/)

[[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.mobi](ed2k://|file|[Building.on.SugarCRM(第1版)].(Building.on.SugarCRM).John.Mertic.文字版.mobi|5191162|801ca9f9439515cd418ae85fdc089a5b|h=otojvhktn6ppcrrygs4cdmlaj7ethzok|/)

[[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.pdf](ed2k://|file|[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.pdf|5094942|5c65396cc1e5384794a576445a15fbbe|h=7gwtov4bj4jpu27rhm7psiz25oek45hu|/)

[[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.epub](ed2k://|file|[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.epub|836282|108e93c4576751f1a9e15250f9318c36|h=bik5hd5je7jztdxssrvpcwbvji3wdfce|/)

[[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.mobi](ed2k://|file|[Practical.JIRA.Plugins(第1版)].(Practical.JIRA.Plugins).Matthew.B.Doar.文字版.mobi|2234846|9ef064c89683454710ade71dc5fbf472|h=urw3oaucf5ovqg6z65fq6djn7bsrr4mi|/)

[[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.pdf](ed2k://|file|[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.pdf|6199737|7b41c8c9c1ac08ec53456981a1c16557|h=uwmq4wxtoztvu7pf4tryd2xxdmwbyzam|/)

[[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.epub](ed2k://|file|[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.epub|1200793|3b98816202151d5d5deb9b99bcc97f17|h=5pvzqe3zbohyxt3p64cysiwack4ayey3|/)

[[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.mobi](ed2k://|file|[Deploying.OpenStack(第1版)].(Deploying.OpenStack).Ken.Pepple.文字版.mobi|2838005|e39fbb009e76bff4642a1f1b0196a11b|h=5ium5bzs6uhwa73xkuamvxmlpf4hwbsf|/)

[[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.pdf](ed2k://|file|[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.pdf|11107716|f521c5736076fcda2757d266cb3d8667|h=ubuonvrmbqsh3h22rx5ws3ify7osxfmm|/)

[[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.epub](ed2k://|file|[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.epub|10202217|7a3b9938820bbe422f30cfe5829cb688|h=txnlv2zge5yepswi7tjbb6m3apuh4gcv|/)

[[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.mobi](ed2k://|file|[Elastic.Beanstalk(第1版)].(Elastic.Beanstalk).J.v.Vliet&F.Paganelli&S.v.Wel&D.Dowd.文字版.mobi|14964930|8db10769bd5faff41773e7caae050e63|h=tp74cujyy5laoo6mwyak7jgklo2iw6jq|/)

[[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.pdf](ed2k://|file|[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.pdf|18117207|b1752a4e9344c30a018526e562886cba|h=wtimzjrdpov62gxcoh7antvlwacelupx|/)

[[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.epub](ed2k://|file|[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.epub|5452408|4d57bda73eade7c8ccabbabf2f5b1041|h=rvqvhayenoy3b6qu7tustjypcwa35iz4|/)

[[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.mobi](ed2k://|file|[Gamification.by.Design(第1版)].(Gamification.by.Design).Gabe.Zichermann&Christopher.Cunningham.文字版.mobi|11944384|1f6f338380f55a8470d383cd0c6b53c5|h=uiyn6vtijx3ertxhucsxqrgizi7dvymn|/)

[[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.pdf](ed2k://|file|[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.pdf|7414375|f246a6e15e71e3900a0ae717f3af3d78|h=cshasx2nedewle4eqsfn6wcwkd2gcb7p|/)

[[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.epub](ed2k://|file|[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.epub|798818|2a108c55a08c9a6a228a7ccce56b3b11|h=ejsktddtaxako2cyeakltkjvzsoa3ovl|/)

[[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.mobi](ed2k://|file|[Google.Power.Search(第1版)].(Google.Power.Search).Stephan.Spencer.文字版.mobi|2074871|0a354b0526fc83203451f19b5e9e60a3|h=ymtqw7c3lmhkl2svoox2eh3wf3eoe23c|/)

[[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.pdf](ed2k://|file|[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.pdf|5116625|1cee515ef5710ff7b2f4f51fca945235|h=zly7rxoybhxl6rv6g5o5favurh6vclst|/)

[[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.epub](ed2k://|file|[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.epub|631805|6231bf14a86080f205dc2fcca0ac05c7|h=ujyeclpfiycuel2fd2cvxgkhwfvbzlyg|/)

[[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.mobi](ed2k://|file|[Just.Spring(第1版)].(Just.Spring).Madhusudhan.Konda.文字版.mobi|1568810|259e63528282452ebf48e9d74b1a8112|h=ajh4jjnunx5xulohbn4hkasvoe752ldd|/)

[[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.pdf](ed2k://|file|[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.pdf|5137568|54eb58f736ebefd98674bace83041d50|h=2ecdz6gshsz7drz45zl4uxs463voali7|/)

[[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.epub](ed2k://|file|[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.epub|645736|3c665e7355ef779a9c899e19dc939e87|h=6gztqhmm2omwrqzl2pqwai5uysv5hqky|/)

[[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.mobi](ed2k://|file|[Redis.Cookbook(第1版)].(Redis.Cookbook).Tiago.Macedo&Fred.Oliveira.文字版.mobi|1729954|9c8b2ca2652e67a7a7d3651ec6aa5832|h=4xbh3icjlouwwvfgd37nr26v6rrqlnfx|/)

[[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.pdf](ed2k://|file|[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.pdf|5491830|df0770cfeaa330e8f9369a4ed9c6b7f3|h=fwrhpor6xps25gnfmzgsqbtalx7mxjze|/)

[[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.epub](ed2k://|file|[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.epub|673907|3fcc7edde8d824670be69427b7af83c7|h=j6n3zqasyfy3hwjliragi2uumhxbdrlh|/)

[[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.mobi](ed2k://|file|[Using.the.HTML5.Filesystem.API(第1版)].(Using.the.HTML5.Filesystem.API).Eric.Bidelman.文字版.mobi|1652133|38a3d708c675ca19d4cc0a33bb9714b8|h=66xaafk6ufbyve5dnredwveee2gtl5us|/)

[[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.pdf](ed2k://|file|[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.pdf|5006449|042b7eedc3d9bb531df6270c24290b61|h=sb5xdsibyslnfworj52jkzzrebpijgmr|/)

[[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.epub](ed2k://|file|[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.epub|2494998|c1c88af2382706a891d5169a2d5bdb9c|h=avbpxuj6l7ejulnd6otaelanlr5ct362|/)

[[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.mobi](ed2k://|file|[Ruby程序设计语言].(The.Ruby.Programming.Language).David.Flanagan&Yukihiro.Matsumoto.文字版.mobi|11969850|7832ba7c3d06c83286751bfa9047d17c|h=slxa45z7jt5anjsxyre4hkknmse3wbbu|/)

[[iOS4编程].(Programming.iOS.4).Matt.Neuburg.文字版.pdf](ed2k://|file|[iOS4编程].(Programming.iOS.4).Matt.Neuburg.文字版.pdf|17191058|9948d1bc6b39afd75510fe73de8818a7|h=vshwxbi6ajlms3d46vfsotcvqqhhp4ea|/)

[[iOS4编程].(Programming.iOS.4).Matt.Neuburg.文字版.epub](ed2k://|file|[iOS4编程].(Programming.iOS.4).Matt.Neuburg.文字版.epub|6080113|3289af6b592759fa0b3c7f0e07931ffa|h=csy6mdum3xniel6bdrkxy4muwggl2gy5|/)

[[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.pdf](ed2k://|file|[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.pdf|11201572|1964b685f2512620c882d99f5a6c2f87|h=3hhksxhr3fm2vmw35ye7wfggp63clr2y|/)

[[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.epub](ed2k://|file|[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.epub|4512525|a627bb6e40f1e4f01a9191726e8d9270|h=vwlne3l4anl6srvgfboqt33w5heti2gf|/)

[[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.mobi](ed2k://|file|[Android编程].(Programming.Android).Zigurd.Mednieks.文字版.mobi|12268885|ffa23de59585ac82f8b4c800b73ffb12|h=rlkh7f7dyu33p43wkqojm4crbj2luxm4|/)

[[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.pdf](ed2k://|file|[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.pdf|11516237|713556a9522433a757f52982be002cdf|h=yyghw5wlhnlzupljadesivxvdqwpxhoj|/)

[[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.epub](ed2k://|file|[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.epub|3976311|40b9b3e1cfe48d1876138a36c6ae1ea0|h=wfzgn6hethlp2pwbf2quflqfxvh6moui|/)

[[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.mobi](ed2k://|file|[Supercharged.JavaScript.Graphics(第1版)].(Supercharged.JavaScript.Graphics).Raffaele.Cecco.文字版.mobi|9355891|b07d54b04e89fd1bb0d5e14724a116ad|h=nokeexpvun6btqmw4ayndlui2uecplar|/)

[[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.pdf](ed2k://|file|[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.pdf|11844733|2e5fd8cb55dcb57be1afadf0f6c75208|h=xvrjfyaomumpks6wx74isvteey2lz7cq|/)

[[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.epub](ed2k://|file|[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.epub|6554380|0558ec2c87378515beab630af0c7e02b|h=5pkgj3bvzzldjshyridqiuasq7ywc3xh|/)

[[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.mobi](ed2k://|file|[iOS基础传感器].(Basic.Sensors.in.iOS).Alasdair.Allan.文字版.mobi|10312501|07599bd24992672a066c66d3414e4ec0|h=s7k535ty7batg2tyjdgakwfsjoyzifid|/)

[[C#4.0编程(第6版)].(Programming.C#4.0).Ian.Griffiths&Matthew.Adams&Jesse.Liberty.文字版.pdf](ed2k://|file|[C#4.0编程(第6版)].(Programming.C#4.0).Ian.Griffiths&Matthew.Adams&Jesse.Liberty.文字版.pdf|6869781|b9ea02b990cfd11dea5acd81d995403b|h=hmqynmm3pr7y2jqi7bqi5m4fvtmmxqto|/)

[[WCF服务编程].(Programming.WCF.Services).Juval.Lowy.文字版.pdf](ed2k://|file|[WCF服务编程].(Programming.WCF.Services).Juval.Lowy.文字版.pdf|10825435|3f345793e74c046c9039479dcfd34db6|h=k6ugxtjudydo6jgwtia2bzdeghnh34x2|/)

[[Scala编程].(Programming.Scala).Dean.Wampler&Alex.Payne.文字版.pdf](ed2k://|file|[Scala编程].(Programming.Scala).Dean.Wampler&Alex.Payne.文字版.pdf|5846931|c23a0b3464957bce4e3c0781b3e3649f|h=2thkpwkxopaczsd2hyaq3heiprkwhqyn|/)

[[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.pdf](ed2k://|file|[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.pdf|4840866|221da0e6cdad7384fb5e93fa57a9f0df|h=y6unmf6n35tvameyvx65lj5hfycvouvo|/)

[[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.epub](ed2k://|file|[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.epub|796532|87702409cdaa36a1aeea255658f387e0|h=2a7lp3vrnowgfmjllpkam2si4dhdclf7|/)

[[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.mobi](ed2k://|file|[面向Java开发者的函数式编程].(Functional.Programming.for.Java.Developers).Dean.Wampler.文字版.mobi|2017432|9624de0bded9564bbfbc76a6880db617|h=7ocuopg57elrzlm3wrkrdvyxdxz62ueh|/)

[[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.pdf](ed2k://|file|[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.pdf|5478794|a7da509e65563ceb74b22e1c52738528|h=lrvbomydjiryfxdfwoihek6b52jumzqr|/)

[[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.epub](ed2k://|file|[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.epub|4531837|786b95d7dcdfc400a2bcb695e23f9b21|h=4twgmvhxfmlfiulddjdlx7wlwrw6okrj|/)

[[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.mobi](ed2k://|file|[构建可扩展的Web站点].(Building.Scalable.Web.Sites).Cal.Henderson.文字版.mobi|10231201|fe4493dc35fdb773eb3bb31363278156|h=hm6pvhet5weqfspnyxid3twoxiqxqi2d|/)

[[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.pdf](ed2k://|file|[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.pdf|8070324|6adaa692fdd0149419716ea0cf19aff1|h=qnlptu4nkksjj7cys4sexrkctdjpiong|/)

[[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.epub](ed2k://|file|[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.epub|8464465|1f9b6c07459a3b9d2af500674623fbc7|h=br7ievawxnkuhral6heoyq3ava2x6qzt|/)

[[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.mobi](ed2k://|file|[Learning.WCF(第1版)].(Learning.WCF).Michele.Leroux.Bustamante.文字版.mobi|20672811|2f68a3e35052c4b234e2fdf6b614c99d|h=rlmts5ogch3o3aricdjv52xtbi2wvebz|/)

[[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.pdf](ed2k://|file|[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.pdf|5646750|d136ffae95408468d87867e9265fcfe0|h=jiw5qcr6ckg6bkumjxfakusqx5aoatgk|/)

[[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.epub](ed2k://|file|[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.epub|1536280|1360ee0f14438e124453bb31615a2e53|h=tdwxoenbq2t2ljsv5izvvhvylzbj5lgi|/)

[[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.mobi](ed2k://|file|[Java泛型和集合].(Java.Generics.and.Collections).Maurice.Naftalin&Philip.Wadler.文字版.mobi|5343620|1fda039b60dcdec28a4ac7a171111e80|h=xhoz5mmaxcpk642xryucvhqyv7edz4zf|/)

[[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.pdf](ed2k://|file|[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.pdf|6772300|6be6c23651b6569946c48e79239fea2c|h=juojei65tfamlqp3gcjpmpfhz75e2fxl|/)

[[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.epub](ed2k://|file|[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.epub|2265585|581b2874906c998d79c793a5c1c452f1|h=npvnazfgzifrljhpkirpt7lsvf7p5bg4|/)

[[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.mobi](ed2k://|file|[jQuery移动开发].(jQuery.Mobile).Jon.Reid.文字版.mobi|4708302|641b42012a5a526664eec611595fe94f|h=adivcehyd6qeoi6x5u3uwoz6hrkddvan|/)

[[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.pdf](ed2k://|file|[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.pdf|8998915|cc4dfca4d6ef51c79f93959b57abc0b6|h=lgsmxrcpukqfk3w7cdee7lskfboqo2hu|/)

[[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.epub](ed2k://|file|[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.epub|2694110|9b002262c86b7a429fad44678fd4603a|h=2tv2xxrhoouywxf2dvyzcyu3tpvyb3ss|/)

[[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.mobi](ed2k://|file|[Think.Stats(第1版)].(Think.Stats).Allen.B.Downey.文字版.mobi|6084203|f210916219c12539249b70baf17c3b15|h=haof6ctpoe5plr22yhbgu7bpqvurt5if|/)

[[SOA实践指南].(SOA.in.Practice).Nicolai.M.Josuttis.文字版.pdf](ed2k://|file|[SOA实践指南].(SOA.in.Practice).Nicolai.M.Josuttis.文字版.pdf|3363785|bfe52a2bb6da0ca1404c796ecdf2502f|h=g5b6o7wxqihq2iphutqbie4v6ay2u6xi|/)

[[Java.SOA.Cookbook(第1版)].(Java.SOA.Cookbook).Eben.Hewitt.文字版.pdf](ed2k://|file|[Java.SOA.Cookbook(第1版)].(Java.SOA.Cookbook).Eben.Hewitt.文字版.pdf|12788449|ae20dddbe590599038889c921da35156|h=lmecboxpk5itdkjzrdppqe7a7e53btcr|/)

[[Windows.PowerShell.Cookbook(第2版)].(Windows.PowerShell.Cookbook).Lee.Holmes.文字版.pdf](ed2k://|file|[Windows.PowerShell.Cookbook(第2版)].(Windows.PowerShell.Cookbook).Lee.Holmes.文字版.pdf|8358799|bc4d8e62b92c731f2b63328d956fe60f|h=l6wzy3ij5jqdfxk2uscnsthucgaddvsy|/)

[[C#3.0设计模式].(C#.3.0.Design.Patterns).Judith.Bishop.文字版.pdf](ed2k://|file|[C#3.0设计模式].(C#.3.0.Design.Patterns).Judith.Bishop.文字版.pdf|2886121|10cd3acdcfc59d725756e00a8bd1b8bc|h=wbzi4nvgltlqy6iak6hgmp77kdup6pd7|/)

[[Asterisk.Cookbook(第1版)].(Asterisk.Cookbook).Leif.Madsen&Russell.Bryant.文字版.pdf](ed2k://|file|[Asterisk.Cookbook(第1版)].(Asterisk.Cookbook).Leif.Madsen&Russell.Bryant.文字版.pdf|5828776|3c5c4743fb39dd324f765f4bddcf315d|h=iobg2rutx7ej4bg3y5ekqv6zqk7dso73|/)

[[Asterisk.Cookbook(第1版)].(Asterisk.Cookbook).Leif.Madsen&Russell.Bryant.文字版.epub](ed2k://|file|[Asterisk.Cookbook(第1版)].(Asterisk.Cookbook).Leif.Madsen&Russell.Bryant.文字版.epub|1206922|897f87e0f2fb3617dc522ee54eec4728|h=oafmzgh66lan2lnod5s3ibo3v3aprbhb|/)

[[Dojo权威指南].(Dojo.The.Definitive.Guide).Matthew.A.Russell.文字版.pdf](ed2k://|file|[Dojo权威指南].(Dojo.The.Definitive.Guide).Matthew.A.Russell.文字版.pdf|5028427|ab1fc48f6b720049a0806b8f8ff86773|h=whbo2wk64vnenyb7op2ssytntrg5xxrq|/)

[[R.Cookbook(第1版)].(R.Cookbook.1st.edition).Paul.Teetor.文字版.pdf](ed2k://|file|[R.Cookbook(第1版)].(R.Cookbook.1st.edition).Paul.Teetor.文字版.pdf|8778879|ac461ab554cf77312ffa2b73f2003245|h=lhozt2f64jgznmehhraif7ciutzqigi5|/)

[[R.Cookbook(第1版)].(R.Cookbook.1st.edition).Paul.Teetor.文字版.epub](ed2k://|file|[R.Cookbook(第1版)].(R.Cookbook.1st.edition).Paul.Teetor.文字版.epub|4479245|1625c8008fe163e71cb383dd4dea34bb|h=t4emfedhq66nr6wy6asdc5pc2wwhxp4n|/)

[[高性能网站建设指南].(High.Performance.Web.Sites).Steve.Souders.文字版.pdf](ed2k://|file|[高性能网站建设指南].(High.Performance.Web.Sites).Steve.Souders.文字版.pdf|2983393|63dc3839ea91f4bb67978aa8cdfbfd3c|h=dcljow4ctr2zeak5pnfy345hlfinm23v|/)

[[高性能网站建设指南].(High.Performance.Web.Sites).Steve.Souders.文字版.epub](ed2k://|file|[高性能网站建设指南].(High.Performance.Web.Sites).Steve.Souders.文字版.epub|2150765|346aa6c69ddb4e6d5f68ff2febfeb4ce|h=nhgynmke2gt3sk225ayuf6il3vnxbnze|/)

[[高性能网站建设进阶指南].(Even.Faster.Web.Sites).Steve.Souders.文字版.pdf](ed2k://|file|[高性能网站建设进阶指南].(Even.Faster.Web.Sites).Steve.Souders.文字版.pdf|3610929|f2a9307c996be31da404dc563fd1ca3b|h=htpqrdna5nczfojevt2v45acqhkl2anj|/)

[[高性能网站建设进阶指南].(Even.Faster.Web.Sites).Steve.Souders.文字版.epub](ed2k://|file|[高性能网站建设进阶指南].(Even.Faster.Web.Sites).Steve.Souders.文字版.epub|2546590|8529c5eaf3ce51456c275b9649224f2b|h=t74eg66o6dmkxlpllsjyzlrfxzgmnv3t|/)

[[使用Chef框架进行测试驱动基础设施开发].(Test-Driven.Infrastructure.with.Chef).Stephen.Nelson-Smith.文字版.pdf](ed2k://|file|[使用Chef框架进行测试驱动基础设施开发].(Test-Driven.Infrastructure.with.Chef).Stephen.Nelson-Smith.文字版.pdf|5219304|790a7cf8bf32421e7f90ffae093dc188|h=faapdjnu7ihy6tledj4nuirsvntctvlq|/)

[[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.pdf](ed2k://|file|[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.pdf|4901725|f6de8ec13ff149afa2f1f55fa044b80f|h=pa537gmxfc6orgencn33hg5jmx3uzzz6|/)

[[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.epub](ed2k://|file|[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.epub|611227|694ad32a6109bb93162fd90b3b2e421d|h=pjrbec7bbengeyumt5nn47xel4sbickl|/)

[[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.mobi](ed2k://|file|[使用Puppet框架管理基础设施].(Managing.Infrastructure.with.Puppet).James.Loope.文字版.mobi|1409727|d11d9fd122e09dd1d8bda68572ad974b|h=7qyttotoheshczbrgq5fzvlhhboz2vk5|/)

[[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.pdf](ed2k://|file|[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.pdf|14391887|426048a893f4f5f4dd576f42661ef5ae|h=n6dlix2p77niota2zgsgldskut4ucw4l|/)

[[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.epub](ed2k://|file|[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.epub|6106198|4c5b184f263a07c0dfe31f5ad316b768|h=w4erwwrqsxvnzrm2ehmmxrt222nb7b6q|/)

[[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.mobi](ed2k://|file|[Junos企业路由(第2版)].(Junos.Enterprise.Routing).P.Southwick&D.Marschke&H.Reynolds.文字版.mobi|16663798|6e925b662ad50d06325da474b43e8019|h=htznhrmpjyvbimb25qzw2wedf6zgeizg|/)

[[移植应用程序到IPv6].(Migrating.Applications.to.IPv6).Dan.York.文字版.pdf](ed2k://|file|[移植应用程序到IPv6].(Migrating.Applications.to.IPv6).Dan.York.文字版.pdf|4846658|c6c47fb7c40bae0410a1f352c83e14fd|h=f726qbrqm6usjjmcedxeaur6qh5mf2bg|/)

[[移植应用程序到IPv6].(Migrating.Applications.to.IPv6).Dan.York.文字版.epub](ed2k://|file|[移植应用程序到IPv6].(Migrating.Applications.to.IPv6).Dan.York.文字版.epub|655939|d72bb861549ce2b415cb5759b17c3d4a|h=xlodarmyxpbgegojv33ly6dmduzob6z4|/)

[[虚拟化：管理者指南].(Virtualization.A.Managers.Guide).Dan.Kusnetzky.文字版.pdf](ed2k://|file|[虚拟化：管理者指南].(Virtualization.A.Managers.Guide).Dan.Kusnetzky.文字版.pdf|7586287|7b2c1d1675097c03e33c54ba00d8661d|h=uqokjmrfti5ozflciqxxcqu5prhpgsl3|/)

[[虚拟化：管理者指南].(Virtualization.A.Managers.Guide).Dan.Kusnetzky.文字版.epub](ed2k://|file|[虚拟化：管理者指南].(Virtualization.A.Managers.Guide).Dan.Kusnetzky.文字版.epub|1989798|947abfd2650661176a4a802758bc3fa2|h=ndt6d6rolanoabdw2k5mvbuxenexkthn|/)

[[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.英文文字版.pdf](ed2k://|file|[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.英文文字版.pdf|3885516|f0dbf580537a82f360ecc551957fc9aa|h=whxtmi2g6kq2wnsla6vflkoy62ur24bd|/)

[[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.英文文字版.epub](ed2k://|file|[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.英文文字版.epub|3283672|0620a332e0cd3fb1306b959d17c8f8a0|h=p63lv4swcci64lturgqa6ooxp5lh6cxj|/)

[[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.中文翻译.pdf](ed2k://|file|[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.中文翻译.pdf|1716168|2f70a81607c51b39bd937d168d18b215|h=f4g2kk4paenili2jfwwps4lr77pulhrv|/)

[[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.中文扫描版(台湾).pdf](ed2k://|file|[ActionScript.3.0.Cookbook(第1版)].(ActionScript.3.0.Cookbook).J.Lott&D.Schall&K.Peters.中文扫描版(台湾).pdf|34913999|a6f49b4b33176eb71ecab87afa8c7c36|h=tvutgrf5ynvqihgfebfkjlqhx75vcyyy|/)

[[Asterisk权威文档(第3版)].(Asterisk.The.Definitive.Guide.3rd.Edition).L.Madsen&J.V.Meggelen&R.Bryant.文字版.pdf](ed2k://|file|[Asterisk权威文档(第3版)].(Asterisk.The.Definitive.Guide.3rd.Edition).L.Madsen&J.V.Meggelen&R.Bryant.文字版.pdf|14298368|5b6fdd952f6e3c3ff1cf1d63e4d32efa|h=qvzqdsby4qy4rrysire2eu5sejl5dter|/)

[[Asterisk权威文档(第3版)].(Asterisk.The.Definitive.Guide.3rd.Edition).L.Madsen&J.V.Meggelen&R.Bryant.文字版.epub](ed2k://|file|[Asterisk权威文档(第3版)].(Asterisk.The.Definitive.Guide.3rd.Edition).L.Madsen&J.V.Meggelen&R.Bryant.文字版.epub|3144354|0191e6ef16d7f08d9357b56373f5f849|h=erwj2bh4voju5q5yp72ua5v36cvzzm4x|/)

[[使用AdobeAIR开发Android应用程序].(Developing.Android.Applications.with.Adobe.AIR).Véronique.Brossier.文字版.pdf](ed2k://|file|[使用AdobeAIR开发Android应用程序].(Developing.Android.Applications.with.Adobe.AIR).Véronique.Brossier.文字版.pdf|8006280|c3cac3ed1e729e4159cde7d7d3286db1|h=ryhasituscv7mywspigas6gqy2tljweu|/)

[[使用AdobeAIR开发Android应用程序].(Developing.Android.Applications.with.Adobe.AIR).Véronique.Brossier.文字版.epub](ed2k://|file|[使用AdobeAIR开发Android应用程序].(Developing.Android.Applications.with.Adobe.AIR).Véronique.Brossier.文字版.epub|2782427|c75b7bb14f3411de9355ec02a1c1e845|h=oopocd3lmbsk6ipk2xe42hujpiaymehb|/)

[[为GoogleTV创建Web应用].(Building.Web.Apps.for.Google.TV).Andres.Ferrate.文字版.pdf](ed2k://|file|[为GoogleTV创建Web应用].(Building.Web.Apps.for.Google.TV).Andres.Ferrate.文字版.pdf|15701737|e13066655da94ea1e054ff2d34327e29|h=n5q3e6neonrid2zlplkfvuucmcvysj5h|/)

[[为GoogleTV创建Web应用].(Building.Web.Apps.for.Google.TV).Andres.Ferrate.文字版.epub](ed2k://|file|[为GoogleTV创建Web应用].(Building.Web.Apps.for.Google.TV).Andres.Ferrate.文字版.epub|3027061|acc87f99064c9047fe0eaa862f50be97|h=e3pnddniaz7kqp4vn2psr477nfu45zl6|/)

[[Python编程(第4版)].(Programming.Python.4th.Edition).Mark.Lutz.文字版.pdf](ed2k://|file|[Python编程(第4版)].(Programming.Python.4th.Edition).Mark.Lutz.文字版.pdf|30865216|dde9cd1094b39f16a2bd81f730d458b0|h=bh24lr43u5wbipzlcm72upjb2f6lpkav|/)

[[Python编程(第4版)].(Programming.Python.4th.Edition).Mark.Lutz.文字版.epub](ed2k://|file|[Python编程(第4版)].(Programming.Python.4th.Edition).Mark.Lutz.文字版.epub|21056015|5d690b8675b9b1ef33ea848eb0b11cd8|h=g3faxucvyigctvcx23hl2i6yylzlhwr6|/)

[[Perl语言入门(第6版)].(Learning.Perl).R.L.Schwartz&b.d.foy&T.Phoenix.文字版.pdf](ed2k://|file|[Perl语言入门(第6版)].(Learning.Perl).R.L.Schwartz&b.d.foy&T.Phoenix.文字版.pdf|8927511|cb91f16b6dd9f5b3dca9f086d3b00f2f|h=dkwcfnjimmurwexhfdkgkuycuc5psyxw|/)

[[Perl语言入门(第6版)].(Learning.Perl).R.L.Schwartz&b.d.foy&T.Phoenix.文字版.epub](ed2k://|file|[Perl语言入门(第6版)].(Learning.Perl).R.L.Schwartz&b.d.foy&T.Phoenix.文字版.epub|1267571|3b0b6982de1631136b3f82da228e819b|h=xujmpnkxnhhvhy3f6lz2yxom7ttnlsey|/)

[[Windows.Azure.编程].(Programming.Windows.Azure).Sriram.Krishnan.文字版.pdf](ed2k://|file|[Windows.Azure.编程].(Programming.Windows.Azure).Sriram.Krishnan.文字版.pdf|6944959|ca3c0878dc5fbad106849b9292bf065a|h=z2kjijeloxeljdpejoa2qxqju3bq5r5u|/)

[[Windows.Azure.编程].(Programming.Windows.Azure).Sriram.Krishnan.文字版.epub](ed2k://|file|[Windows.Azure.编程].(Programming.Windows.Azure).Sriram.Krishnan.文字版.epub|5296777|075f259a38d1022a148d660b459a8778|h=ic5ref3gbmy3a2vfcey2tccfcq5mg73t|/)

[[Flex.4.Cookbook(第1版)].(Flex.4.Cookbook).J.Noble&T.Anderson&G.Braithwaite&M.Casario&R.Tretola.文字版.pdf](ed2k://|file|[Flex.4.Cookbook(第1版)].(Flex.4.Cookbook).J.Noble&T.Anderson&G.Braithwaite&M.Casario&R.Tretola.文字版.pdf|7883913|dc4996f9bfc1e3d0b922aa3a31a9e718|h=nh2zugn6fdjgrhff74x2jw7ehipwnbog|/)

[[虚拟机Cookbook].(VMware.Cookbook).R.Troy&M.Helmke.文字版.pdf](ed2k://|file|[虚拟机Cookbook].(VMware.Cookbook).R.Troy&M.Helmke.文字版.pdf|7448326|9ae14321dab9cda2ff3d8d1bf330ea0d|h=rb2yi7sgzep7lcfzdcmjy3mmrh4avwvm|/)

[[Sphinx全文检索引擎介绍].(Introduction.to.Search.with.Sphinx).Andrew.Aksyonoff.文字版.pdf](ed2k://|file|[Sphinx全文检索引擎介绍].(Introduction.to.Search.with.Sphinx).Andrew.Aksyonoff.文字版.pdf|3672298|971631539208a4f4026d1a87e469c564|/)

[[Sphinx全文检索引擎介绍].(Introduction.to.Search.with.Sphinx).Andrew.Aksyonoff.文字版.epub](ed2k://|file|[Sphinx全文检索引擎介绍].(Introduction.to.Search.with.Sphinx).Andrew.Aksyonoff.文字版.epub|1294576|94dd8352b5c15ac27fc679caca94b1fc|/)

[[Scaling.CouchDB(第1版)].(Scaling.CouchDB).Bradley.Holt.文字版.pdf](ed2k://|file|[Scaling.CouchDB(第1版)].(Scaling.CouchDB).Bradley.Holt.文字版.pdf|4777343|7218b92333fe2913de597d62f432b2a1|/)

[[Scaling.CouchDB(第1版)].(Scaling.CouchDB).Bradley.Holt.文字版.epub](ed2k://|file|[Scaling.CouchDB(第1版)].(Scaling.CouchDB).Bradley.Holt.文字版.epub|719980|f4f5e17f19841e591327a51feac22403|/)

[[Ubuntu：构建与运行].(Ubuntu.Up.and.Running).Robin.Nixon.文字版.pdf](ed2k://|file|[Ubuntu：构建与运行].(Ubuntu.Up.and.Running).Robin.Nixon.文字版.pdf|18736549|25afc6e324cd507cbbaf44ed448255c2|h=qoyxpj34afqshtanqzfyogd2uyvnuguj|/)

[[CouchDB权威指南].(CouchDB：The.Definitive.Guide).J.C.Anderson&J.Lehnardt&N.Slater.文字版.pdf](ed2k://|file|[CouchDB权威指南].(CouchDB：The.Definitive.Guide).J.C.Anderson&J.Lehnardt&N.Slater.文字版.pdf|3186230|4aaa6d81e116b1bb8ead7bec3825654a|h=kkv53ok46gmy6uwzfzl2grydrbcsu773|/)

[[HTML&XHTML袖珍参考手册(第4版)].(HTML.and.XHTML.Pocket.Reference).Jennifer.Niederst.Robbins.文字版.pdf](ed2k://|file|[HTML&XHTML袖珍参考手册(第4版)].(HTML.and.XHTML.Pocket.Reference).Jennifer.Niederst.Robbins.文字版.pdf|2469399|15c15c907f665b8edd38166555a6709f|h=fa3ipzst3ible4eukfvtekag5gomb6mb|/)

[[C#4.0袖珍参考手册(第3版)].(C#.4.0.Pocket.Reference).Joseph.Albahari&Ben.Albahari.文字版.pdf](ed2k://|file|[C#4.0袖珍参考手册(第3版)].(C#.4.0.Pocket.Reference).Joseph.Albahari&Ben.Albahari.文字版.pdf|1971718|bd15435d2ce83aec1fc15e63940d8433|h=utjrk7yo5nbvce3udtx6zpkpoxbi4kys|/)

[[bash袖珍参考手册].(bash.Pocket.Reference).Arnold.Robbins.文字版.pdf](ed2k://|file|[bash袖珍参考手册].(bash.Pocket.Reference).Arnold.Robbins.文字版.pdf|1644790|415b294a9fc7909d015b28c4ac1474c5|h=i275qsgyupdipltiliewycm4d5nnb4ww|/)

[[Canvas袖珍参考手册(第1版)].(Canvas.Pocket.Reference).David.Flanagan.文字版.pdf](ed2k://|file|[Canvas袖珍参考手册(第1版)].(Canvas.Pocket.Reference).David.Flanagan.文字版.pdf|908719|067d2b1967c66f1d376dc62cd38155ce|h=glr2lc2pnf44tzu4xcuxcqlduz2mwlml|/)

[[Canvas袖珍参考手册(第1版)].(Canvas.Pocket.Reference).David.Flanagan.文字版.epub](ed2k://|file|[Canvas袖珍参考手册(第1版)].(Canvas.Pocket.Reference).David.Flanagan.文字版.epub|3073332|55d2b6ba95db186006b5db33a12bdb67|h=slc7rtkmuwncyck227qwd74jktiwuzaa|/)

[[Ajax应用程序安全].(Securing.Ajax.Applications).Christopher.Wells.文字版.pdf](ed2k://|file|[Ajax应用程序安全].(Securing.Ajax.Applications).Christopher.Wells.文字版.pdf|3532896|04f0b2f6fefde2744f9ae41a25046625|h=oiipskceoe7nmigqg4s5olvyjtyeyqhi|/)

[[高性能JavaScript编程].(High.Performance.JavaScript).Nicholas.C.Zakas.文字版.pdf](ed2k://|file|[高性能JavaScript编程].(High.Performance.JavaScript).Nicholas.C.Zakas.文字版.pdf|4559105|9f0bab7e239b769b5e0dfa7412af13d5|h=ctd5n7lymrjyvmxgvj6o4w2fytsn422g|/)

[[高性能JavaScript编程].(High.Performance.JavaScript).Nicholas.C.Zakas.中英对照版.pdf](ed2k://|file|[高性能JavaScript编程].(High.Performance.JavaScript).Nicholas.C.Zakas.中英对照版.pdf|3650048|926b8b396723a04856689c2db343edf1|h=op77l6njlkaae3fvxhyozgz2owsfibyi|/)

[[学习vi和Vim编辑器(第7版)].(Learning.the.vi.and.Vim.Editors.7th.Edition).A.Robbins&amp;E.Hannah&amp;L.Lamb.文字版.pdf](ed2k://|file|[学习vi和Vim编辑器(第7版)].(Learning.the.vi.and.Vim.Editors.7th.Edition).A.Robbins&amp;E.Hannah&amp;L.Lamb.文字版.pdf|7261405|5455292080649f489f820872f8915b54|h=5hj3tzg2rjvkbtnjr2jzgrs7z6ssrsjb|/)

[[jQuery袖珍参考手册].(jQuery.Pocket.Reference).David.Flanagan.文字版.pdf](ed2k://|file|[jQuery袖珍参考手册].(jQuery.Pocket.Reference).David.Flanagan.文字版.pdf|3373912|cacb91c933737ad0a9c3d5e8657d1a98|h=bloenweawpdb6wfcjhspet35q6h6sv7b|/)

[[jQuery袖珍参考手册].(jQuery.Pocket.Reference).David.Flanagan.文字版.epub](ed2k://|file|[jQuery袖珍参考手册].(jQuery.Pocket.Reference).David.Flanagan.文字版.epub|817731|acba8fb0e66fde252360eb6a02dd8321|h=eg676wawdpheigielnblubdgq4hqjqt6|/)

[[vi和Vim编辑器袖珍参考手册(第2版)].(vi.and.Vim.Editors.Pocket.Reference.2nd.Edition).Arnold.Robbins.文字版.pdf](ed2k://|file|[vi和Vim编辑器袖珍参考手册(第2版)].(vi.and.Vim.Editors.Pocket.Reference.2nd.Edition).Arnold.Robbins.文字版.pdf|15683793|2cfd64ee37ecf720623dbe154d343cf0|h=64qedtk4rf7gpmcqp65rqzyy5bjy5b4y|/)

[[vi和Vim编辑器袖珍参考手册(第2版)].(vi.and.Vim.Editors.Pocket.Reference.2nd.Edition).Arnold.Robbins.文字版.epub](ed2k://|file|[vi和Vim编辑器袖珍参考手册(第2版)].(vi.and.Vim.Editors.Pocket.Reference.2nd.Edition).Arnold.Robbins.文字版.epub|2643699|2e5f0d618d56be70c2278e1cc9c969e3|h=a54bymtficlnjivlxfbo2cgfztemmftz|/)

[[正则表达式袖珍参考手册(第2版)].(Regular.Expression.Pocket.Reference.2nd.Edition).Tony.Stubblebine.文字版.pdf](ed2k://|file|[正则表达式袖珍参考手册(第2版)].(Regular.Expression.Pocket.Reference.2nd.Edition).Tony.Stubblebine.文字版.pdf|1026322|8532409ffc7027ad1637355022e907e6|h=7mk4nnslfn3kyakdbzklhrcmkej2bzif|/)

[[建立网站的信誉系统].(Building.Web.Reputation.Systems).Randy.Farmer&Bryce.Glass.文字版.pdf](ed2k://|file|[建立网站的信誉系统].(Building.Web.Reputation.Systems).Randy.Farmer&Bryce.Glass.文字版.pdf|7627637|40e39b9c21f61e0e5eca2ae05ba99a80|h=hibibyjg45txtewmtnxomx66yjvukly5|/)

[[JavaScript模式].(JavaScript.Patterns).Stoyan.Stefanov.文字版.pdf](ed2k://|file|[JavaScript模式].(JavaScript.Patterns).Stoyan.Stefanov.文字版.pdf|3736678|dfe8af95f305ed82338fc9caf65056f5|h=oh2vyjzn7vfoqxzjb5nah3m2p6crgr3r|/)

[[实时用户体验](Building.the.Realtime.User.Experience).Ted.Roden.文字版.pdf](ed2k://|file|[实时用户体验](Building.the.Realtime.User.Experience).Ted.Roden.文字版.pdf|6002816|3c5c9f8fbf33c0cd0f91624ddcd80107|h=cqcjzok7obnusv5z4gbmp5ayyjdzmqgo|/)

[[黑客：下一代].(Hacking：The.Next.Generation).N.Dhanjani&B.Rios&B.Hardin.文字版.pdf](ed2k://|file|[黑客：下一代].(Hacking：The.Next.Generation).N.Dhanjani&B.Rios&B.Hardin.文字版.pdf|7211715|4089af8500254422581615f0e05dfb21|h=mjzlgzuccmmupafxilstyatyidpf7zq4|/)

[[数据源手册].(Data.Source.Handbook).Pete.Warden.文字版.pdf](ed2k://|file|[数据源手册].(Data.Source.Handbook).Pete.Warden.文字版.pdf|413955|76ab74c66f8363b6f04ec21c59a58996|h=cpguelzqnqrzjyanepjg2e3wqoaeme3j|/)

[[PayPal.APIs：构建和运行].(PayPal.APIs：Up.and.Running).Michael.Balderas.文字版.pdf](ed2k://|file|[PayPal.APIs：构建和运行].(PayPal.APIs：Up.and.Running).Michael.Balderas.文字版.pdf|1607484|2746829ea81492cf8d12f81592710a08|h=hn2eawj3q4fcpphnwvwsr7dpj6jwbg4a|/)

[[交互式编程].(Programming.Interactivity).文字版.pdf](ed2k://|file|[交互式编程].(Programming.Interactivity).文字版.pdf|13335633|e3ad2bb73dadc260990c0b9a02459a80|h=35hnxwcbnuywlw5e6a7ed5xpey5khpin|/)

[[iOS的图像和动画处理].(Graphics.and.Animation.on.iOS).Vandad.Nahavandipoor.文字版.pdf](ed2k://|file|[iOS的图像和动画处理].(Graphics.and.Animation.on.iOS).Vandad.Nahavandipoor.文字版.pdf|8356559|2570229a30086a3e74a7cf231f3112be|h=7dlg7kje5fpytie6iwruy7vder2ir6xq|/)

[[iOS的图像和动画处理].(Graphics.and.Animation.on.iOS).Vandad.Nahavandipoor.文字版.epub](ed2k://|file|[iOS的图像和动画处理].(Graphics.and.Animation.on.iOS).Vandad.Nahavandipoor.文字版.epub|2321077|31d6ebcde1c2df151dc155b97f3a38d4|h=zkxppa7inw5umdzgdxps4innspesg2dp|/)

[[编写苹果游戏中心应用程序].(Writing.Game.Center.Apps.in.iOS).Vandad.Nahavandipoor.文字版.pdf](ed2k://|file|[编写苹果游戏中心应用程序].(Writing.Game.Center.Apps.in.iOS).Vandad.Nahavandipoor.文字版.pdf|6039237|36b4c73cf860656ab1bb623f4cd09b86|h=273t4w7jcii3w5u4aefobqcfopedevlk|/)

[[编写苹果游戏中心应用程序].(Writing.Game.Center.Apps.in.iOS).Vandad.Nahavandipoor.文字版.epub](ed2k://|file|[编写苹果游戏中心应用程序].(Writing.Game.Center.Apps.in.iOS).Vandad.Nahavandipoor.文字版.epub|2347764|208285c5ba75edfd4c3ec404549173c5|h=a4mbm4hvo2ixsi6ad3uwlo5yaq6uudln|/)

[[给MongoDB开发者的50条建议].(50.Tips.and.Tricks.for.MongoDB.Developers).Kristina.Chodorow.文字版.pdf](ed2k://|file|[给MongoDB开发者的50条建议].(50.Tips.and.Tricks.for.MongoDB.Developers).Kristina.Chodorow.文字版.pdf|5601181|9f6464a3ac312f5020c470846916a80e|h=hsojsmytaqyttqdqu5rt42drhqbvstrk|/)

[[给MongoDB开发者的50条建议].(50.Tips.and.Tricks.for.MongoDB.Developers).Kristina.Chodorow.文字版.epub](ed2k://|file|[给MongoDB开发者的50条建议].(50.Tips.and.Tricks.for.MongoDB.Developers).Kristina.Chodorow.文字版.epub|769661|8dc2e0b3d12c84f09615479285fdf843|h=aecfbqab4hd676iubqsqm2cprjiw5vip|/)

[[网站优化].(Website.Optimization).Andrew.B.King.文字版.pdf](ed2k://|file|[网站优化].(Website.Optimization).Andrew.B.King.文字版.pdf|8257934|be8e0a7f5b52247e695979ea53b37080|h=sjj3qtb2kjvtljteqot4qtcpam2qep2t|/)

[[网站优化].(Website.Optimization).Andrew.B.King.文字版.epub](ed2k://|file|[网站优化].(Website.Optimization).Andrew.B.King.文字版.epub|12062683|8a439959936e184cf4b2b2f092e9eb6f|h=iidyz6o2q6fmppo4wnsmt62u2u4kjnhc|/)

[[物联网入门教程].(Getting.Started.with.the.Internet.of.Things).Cuno.Pfister.文字版.pdf](ed2k://|file|[物联网入门教程].(Getting.Started.with.the.Internet.of.Things).Cuno.Pfister.文字版.pdf|6504868|1cb6e868222b54a23818b57e723b7e09|h=hfbdtg5qigj5jckz564hrarfulusfogw|/)

[[物联网入门教程].(Getting.Started.with.the.Internet.of.Things).Cuno.Pfister.文字版.epub](ed2k://|file|[物联网入门教程].(Getting.Started.with.the.Internet.of.Things).Cuno.Pfister.文字版.epub|3617974|7b7c6bebe4b361aedd42dca78f8d875e|h=juqcgtmvifkext5rtdigb35yarqggpcc|/)

[[JavaScript权威指南(第6版)].(JavaScript：The.Definitive.Guide).David.Flanagan.文字版.pdf](ed2k://|file|[JavaScript权威指南(第6版)].(JavaScript：The.Definitive.Guide).David.Flanagan.文字版.pdf|14129653|eea8e6e3102374842002cb8ae5aebf72|h=63w4t4bip6u5z6degzas6f6kjg6gwlpu|/)

[[JavaScript权威指南(第6版)].(JavaScript：The.Definitive.Guide).David.Flanagan.文字版.epub](ed2k://|file|[JavaScript权威指南(第6版)].(JavaScript：The.Definitive.Guide).David.Flanagan.文字版.epub|3896344|fc4cc5ee4f59bb04789f880497decf06|h=h6hqmx22bs5lej35qnjptytmapswvpvj|/)

[[HTML5绘图].(HTML5.Canvas).Steve.Fulton&Jeff.Fulton.文字版.pdf](ed2k://|file|[HTML5绘图].(HTML5.Canvas).Steve.Fulton&Jeff.Fulton.文字版.pdf|15776863|18c183daaedf40b194a9b4c1c3bb1645|h=kjstblu3icj4l2sliuxrj7qmixtph3ws|/)

[[HTML5绘图].(HTML5.Canvas).Steve.Fulton&Jeff.Fulton.文字版.epub](ed2k://|file|[HTML5绘图].(HTML5.Canvas).Steve.Fulton&Jeff.Fulton.文字版.epub|5568471|bc77c2b992d6216dec7e227e386cb24b|h=55esk73faqdoyydbwuw6ocgiiz7ux3ym|/)

[[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.文字版.pdf](ed2k://|file|[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.文字版.pdf|6785187|cc2cac80450e67c097dbac5366983d89|h=m64dl3fbd5iq3co7fzklsiao6z35ympo|/)

[[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.文字版.epub](ed2k://|file|[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.文字版.epub|8460123|702262ca324a1dd9f934626360b88c78|h=4j5r6wewu6xv4rezd2ezrgjnl6yhnctj|/)

[[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.中文文字版.pdf](ed2k://|file|[XNA.4.0学习指南].(Learning.XNA.4.0).Aaron.Reed.中文文字版.pdf|8301224|bb400321acbd977ea990e4508aedaf84|h=aookpccfwycp6jufnakj2vfvpjsobd5e|/)

[[使用Python进行自然语言处理].(Natural.Language.Processing.with.Python).S.Bird&E.Klein&E.Loper.文字版.pdf](ed2k://|file|[使用Python进行自然语言处理].(Natural.Language.Processing.with.Python).S.Bird&E.Klein&E.Loper.文字版.pdf|5428298|6d1225cd6eec4bfdfc7c73ca21bed4a4|h=43fjjbc7na57qcqz5wgsxreg6lbkfdcf|/)

[[Network.Warrior.思科网络工程师必备手册(第2版)].(Network.Warrior.2nd.Edition).Gary.A.Donahue.文字版.pdf](ed2k://|file|[Network.Warrior.思科网络工程师必备手册(第2版)].(Network.Warrior.2nd.Edition).Gary.A.Donahue.文字版.pdf|30320792|76a4aa7a5d6cf56d18048cbd5433b6bb|h=jlihn4gtpslh6epa32wugpdk2g5j7ydw|/)

[[Network.Warrior.思科网络工程师必备手册(第2版)].(Network.Warrior.2nd.Edition).Gary.A.Donahue.文字版.epub](ed2k://|file|[Network.Warrior.思科网络工程师必备手册(第2版)].(Network.Warrior.2nd.Edition).Gary.A.Donahue.文字版.epub|8504253|0dfc926e5fe919b575f7ee45e1d86ffe|h=uxesce7ffvda4kr2hm3crpdecg5xgy2x|/)

[[JIRA实战管理].(Practical.JIRA.Administration).Matthew.B.Doar.文字版.pdf](ed2k://|file|[JIRA实战管理].(Practical.JIRA.Administration).Matthew.B.Doar.文字版.pdf|6345204|257c6b09688cefce22e1d3d61d42b60a|h=kxjmicyvjntporoigct2siw7y6nukthv|/)

[[JIRA实战管理].(Practical.JIRA.Administration).Matthew.B.Doar.文字版.epub](ed2k://|file|[JIRA实战管理].(Practical.JIRA.Administration).Matthew.B.Doar.文字版.epub|885145|f4dceca0538f822faf22cdc8f327456a|h=7djvpntvym6bxah7rnt3vl5hs3yqj6it|/)

[[核心网络协议袖珍手册].(Packet.Guide.to.Core.Network.Protocols).Bruce.Hartpence.文字版.pdf](ed2k://|file|[核心网络协议袖珍手册].(Packet.Guide.to.Core.Network.Protocols).Bruce.Hartpence.文字版.pdf|7690980|5a178a6ba7d0a758734523aee16af368|h=ionnec5hwnitcftntuwspamiaxmxlcbg|/)

[[核心网络协议袖珍手册].(Packet.Guide.to.Core.Network.Protocols).Bruce.Hartpence.文字版.epub](ed2k://|file|[核心网络协议袖珍手册].(Packet.Guide.to.Core.Network.Protocols).Bruce.Hartpence.文字版.epub|2669103|fc49dfb08842ef55c83a8597ef5be8d1|h=atcvhduabpowa5rrtdbmm76edfcuj66k|/)

[[HTML5地理位置定位].(HTML5.Geolocation).Anthony.T.Holdener.文字版.pdf](ed2k://|file|[HTML5地理位置定位].(HTML5.Geolocation).Anthony.T.Holdener.文字版.pdf|8226791|f35b53cd83d9c5f7302a5e9ef0ffe03a|h=ofz5gehizhnfor3ww2fdbyk7r6oosazh|/)

[[HTML5地理位置定位].(HTML5.Geolocation).Anthony.T.Holdener.文字版.epub](ed2k://|file|[HTML5地理位置定位].(HTML5.Geolocation).Anthony.T.Holdener.文字版.epub|1846800|04601c51ecafdd43034256a57807453a|h=rqvxshsbgiv5phwv25jow2qqzoibjv4q|/)

[[Mac.OS.X和iOS中的并行开发].(Concurrent.Programming.in.Mac.OS.X.and.iOS).Vandad.Nahavandipoor.文字版.pdf](ed2k://|file|[Mac.OS.X和iOS中的并行开发].(Concurrent.Programming.in.Mac.OS.X.and.iOS).Vandad.Nahavandipoor.文字版.pdf|4351628|84437bcad087f9cc4b6ac83a798743c1|h=7yyohco2vreurwmutzegjv2yqeispc3h|/)

[[Mac.OS.X和iOS中的并行开发].(Concurrent.Programming.in.Mac.OS.X.and.iOS).Vandad.Nahavandipoor.文字版.epub](ed2k://|file|[Mac.OS.X和iOS中的并行开发].(Concurrent.Programming.in.Mac.OS.X.and.iOS).Vandad.Nahavandipoor.文字版.epub|764324|e3aca30c90649c533d37b69d6e0f38af|h=bqayqegc3wb7c6ghe7oiq5pjgmxr6nqh|/)

[[使用HTML,CSS,JavaScript开发Android应用程序].(Building.Android.Apps.with.HTML.CSS.and.JavaScript).Jonathan.Stark.文字版.pdf](ed2k://|file|[使用HTML,CSS,JavaScript开发Android应用程序].(Building.Android.Apps.with.HTML.CSS.and.JavaScript).Jonathan.Stark.文字版.pdf|6135798|25d55f9306b70d0e4336992bf0bdf236|h=ywv3qtvecpiwvhstkvihgobn2iywe57k|/)

[[使用Flex4.5开发Android应用程序].(Developing.Android.Applications.with.Flex.4.5).Rich.Tretola.文字版.pdf](ed2k://|file|[使用Flex4.5开发Android应用程序].(Developing.Android.Applications.with.Flex.4.5).Rich.Tretola.文字版.pdf|9177600|c1d8659b2ddbeb95bd895db4bade0055|h=5omvno6mghmxl5q2p25nk7oc5wgo2seb|/)

[[使用Flex4.5开发Android应用程序].(Developing.Android.Applications.with.Flex.4.5).Rich.Tretola.文字版.epub](ed2k://|file|[使用Flex4.5开发Android应用程序].(Developing.Android.Applications.with.Flex.4.5).Rich.Tretola.文字版.epub|4457569|3962263b70d6ebdbdc81f65d85e8a498|h=rnjbkc7plmn4a2s5v5irf476fbvc7gla|/)

[[DNS.and.BIND.on.IPv6(第1版)].(DNS.and.BIND.on.IPv6).Cricket.Liu.文字版.pdf](ed2k://|file|[DNS.and.BIND.on.IPv6(第1版)].(DNS.and.BIND.on.IPv6).Cricket.Liu.文字版.pdf|4337674|44d0facf0d19ffc6f1a7686b44915257|h=sntfssb74k3juqota6rwzc4hch6mmmh4|/)

[[DNS.and.BIND.on.IPv6(第1版)].(DNS.and.BIND.on.IPv6).Cricket.Liu.文字版.epub](ed2k://|file|[DNS.and.BIND.on.IPv6(第1版)].(DNS.and.BIND.on.IPv6).Cricket.Liu.文字版.epub|826457|87c3184e83add2b1e76b9e0c9b7d3902|h=vvuzmrf6oxdxptqbdnnt3ki7sjumaeqb|/)

[[使用App.Inventor].(App.Inventor).D.Wolber&H.Abelson&E.Spertus&L.Looney.文字版.pdf](ed2k://|file|[使用App.Inventor].(App.Inventor).D.Wolber&H.Abelson&E.Spertus&L.Looney.文字版.pdf|31994562|704f124c694b806c5418b81432cf4d68|h=lfu4rhkehr7my3l6fyugf2byffiilvnm|/)

[[使用App.Inventor].(App.Inventor).D.Wolber&H.Abelson&E.Spertus&L.Looney.文字版.epub](ed2k://|file|[使用App.Inventor].(App.Inventor).D.Wolber&H.Abelson&E.Spertus&L.Looney.文字版.epub|16608144|20829f996a83e05907bcff2a8cab45c1|h=znclgzcjah5xvl7lk5ml2l3uf7ksgot2|/)

[[JavaScript语言精粹].(JavaScript.The.Good.Parts).Douglas.Crockford.文字版.pdf](ed2k://|file|[JavaScript语言精粹].(JavaScript.The.Good.Parts).Douglas.Crockford.文字版.pdf|6061574|7c3eb9aabf00336a1f7b72c31a56e66a|h=xbpaoir3f3acse7jalcsbgqx6t3albv6|/)

[[JavaScript语言精粹].(JavaScript.The.Good.Parts).Douglas.Crockford.文字版.epub](ed2k://|file|[JavaScript语言精粹].(JavaScript.The.Good.Parts).Douglas.Crockford.文字版.epub|3302436|befc59d4d4f472f8c5e60d9b51864ab0|h=aamgegbrczczmev4rkbpzcc4plorgjaw|/)

[[演讲之禅：一位技术演讲家的自白].(Confessions.of.a.Public.Speaker).Scott.Berkun.文字版.pdf](ed2k://|file|[演讲之禅：一位技术演讲家的自白].(Confessions.of.a.Public.Speaker).Scott.Berkun.文字版.pdf|3476756|d544afa9e1441146e26f87a87cda0a54|h=3enm2s2teeusxzgbmadlult3cah5vw5z|/)

[[演讲之禅：一位技术演讲家的自白].(Confessions.of.a.Public.Speaker).Scott.Berkun.文字版.epub](ed2k://|file|[演讲之禅：一位技术演讲家的自白].(Confessions.of.a.Public.Speaker).Scott.Berkun.文字版.epub|6430259|36b9ef41546ac532af6f32fc2313b1b9|h=jim2yemnzucdpwxjw6t34oykllli2ris|/)

[[演讲之禅：一位技术演讲家的自白].中文扫描版.pdf](ed2k://|file|[演讲之禅：一位技术演讲家的自白].中文扫描版.pdf|21608460|8ef061bbdbe2a92c6af2c92d8e8e139d|h=igcfnw5a6k2u6wwosllduordupdliyy2|/)

[[Erlang编程指南].(Erlang.Programming).Francesco.Cesarini&Simon.Thompson.文字版.pdf](ed2k://|file|[Erlang编程指南].(Erlang.Programming).Francesco.Cesarini&Simon.Thompson.文字版.pdf|5331118|835aeadee10b802ef8ca71384a40b650|h=mreglen2grmnytzzracws77idg7pyscl|/)

[[CSS.Cookbook(第3版)].(CSS.Cookbook.3rd.Edition).Christopher.Schmitt.文字版.pdf](ed2k://|file|[CSS.Cookbook(第3版)].(CSS.Cookbook.3rd.Edition).Christopher.Schmitt.文字版.pdf|32280107|0a45d329c4316e8ea5b0395f97c466b5|h=qbdq5he2whlaypptjcu3wkfohicylajd|/)

[[jQuery.Cookbook(第1版)].(jQuery.Cookbook).Cody.Lindley.文字版.pdf](ed2k://|file|[jQuery.Cookbook(第1版)].(jQuery.Cookbook).Cody.Lindley.文字版.pdf|4935434|4f1b62a97344f130ffc6ec9dc1251833|h=w63ftyyvkcgjgpu2jgirlb57u5qelmxq|/)

[[HTML&CSS精粹].(HTML&CSS：The.Good.Parts).Ben.Henick.文字版.pdf](ed2k://|file|[HTML&CSS精粹].(HTML&CSS：The.Good.Parts).Ben.Henick.文字版.pdf|6280337|17c343826766723843275fd67fa45134|h=luuua32bxzznvnevqcpoaohp56glj7de|/)

[[企业Rails].(Enterprise.Rails).Dan.Chak.文字版.pdf](ed2k://|file|[企业Rails].(Enterprise.Rails).Dan.Chak.文字版.pdf|8108179|b147b62a18ca9a56edcd901c2b353ba7|h=b4dhpfkt7rfjtxjtzhy7kn5alz7j7qlv|/)

[[Ruby最佳实践].(Ruby.Best.Practices).Gregory.T.Brown.文字版.pdf](ed2k://|file|[Ruby最佳实践].(Ruby.Best.Practices).Gregory.T.Brown.文字版.pdf|2097870|42ab57682db94865b6ccb64b01358778|h=sclzg225oevbce34ymc2ft7ijxc2xmu4|/)

[[Ruby.Cookbook(第1版)].(Ruby.Cookbook.1st.edition).Lucas.Carlson&Leonard.Richardson.文字版.pdf](ed2k://|file|[Ruby.Cookbook(第1版)].(Ruby.Cookbook.1st.edition).Lucas.Carlson&Leonard.Richardson.文字版.pdf|5253036|e69d995ef5f9763251c63efeb4198c9a|h=23eslkorkd6uykxos3qjnbpec7dhlfeh|/)

[[闭包权威指南].(Closure：The.Definitive.Guide).Michael.Bolin.文字版.pdf](ed2k://|file|[闭包权威指南].(Closure：The.Definitive.Guide).Michael.Bolin.文字版.pdf|6577187|c73f02bd5600a59887b01923fc85b0bb|h=3jxs735hngmlk5ruckkzlssfj2heyeg5|/)

[[Web操作].(Web.Operations：Keeping.the.Data.On.Time).John.Allspaw&Jesse.Robbins.文字版.pdf](ed2k://|file|[Web操作].(Web.Operations：Keeping.the.Data.On.Time).John.Allspaw&Jesse.Robbins.文字版.pdf|12993624|6b9a17a50ea889f8543e0caafed8d06a|h=pxid2f6o73jlono27u4v5iuewfyaltjt|/)

[[REST实战].(REST.in.Practice).Jim.Webber&Savas.Parastatidis&Ian.Robinson.文字版.pdf](ed2k://|file|[REST实战].(REST.in.Practice).Jim.Webber&Savas.Parastatidis&Ian.Robinson.文字版.pdf|12870907|98ce519e5bf48def32acf7612c696f2b|h=ksjp7csw2qsfjttdnafubkzydunspgjs|/)

[[PHP.MySQL.JavaScript学习手册].(Learning.PHP.MySQL.and.JavaScript).Robin.Nixon.文字版.pdf](ed2k://|file|[PHP.MySQL.JavaScript学习手册].(Learning.PHP.MySQL.and.JavaScript).Robin.Nixon.文字版.pdf|7758019|112f18486eefda9c3743fe013d75ba72|h=synuw7wn5cf7xfa2gkr5nrxjz6f5cadq|/)

[[Python袖珍参考手册(第4版)].(Python.Pocket.Reference.4th.Edition).Mark.Lutz文字版.pdf](ed2k://|file|[Python袖珍参考手册(第4版)].(Python.Pocket.Reference.4th.Edition).Mark.Lutz文字版.pdf|912729|4e447b6d6c5056355ab6f74dd504a819|h=es7oeyyrxomtvlwrptmh6lziv6yu6xjw|/)

[[架构师应该知道的97件事].(97.Things.Every.Software.Architect.Should.Know).Richard.Monson-Haefel.文字版.pdf](ed2k://|file|[架构师应该知道的97件事].(97.Things.Every.Software.Architect.Should.Know).Richard.Monson-Haefel.文字版.pdf|2026110|39361ec4973a6dbc3adf1e3a65720604|h=soer7mupnjlfoqo57mzwm6v2qvwrr2hi|/)

[[项目经理应该知道的97件事].(97.Things.Every.Project.Manager.Should.Know).Barbee.Davis.文字版.pdf](ed2k://|file|[项目经理应该知道的97件事].(97.Things.Every.Project.Manager.Should.Know).Barbee.Davis.文字版.pdf|4650885|16b281fd50f2b0ddc83a8ecf255c2071|h=ue3sxzatqd35nwux2qqoytw2q73c4xdp|/)

[[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.pdf](ed2k://|file|[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.pdf|2115864|543e8bbe016c73a5fe37c9dc5fede4aa|h=u4fkgwce7dzmht77hoatgf5cqbgci27t|/)

[[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.epub](ed2k://|file|[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.epub|3518729|39770981096faec671aa2b2ffee51c6f|h=t3mx7cpreoscq4zjlqhaywbgk4sl2zol|/)

[[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.mobi](ed2k://|file|[程序员应该知道的97件事].(97.Things.Every.Programmer.Should.Know).Kevlin.Henney.文字版.mobi|3855845|a76b376eb304cc598f81e389e89563a1|h=6z7j2hvlyituqmztkbydnkpao6srimzu|/)

[[JavaWeb服务：构建与运行].(Java.Web.Services：Up.and.Running).Martin.Kalin.文字版.pdf](ed2k://|file|[JavaWeb服务：构建与运行].(Java.Web.Services：Up.and.Running).Martin.Kalin.文字版.pdf|3749883|d58dec4c50b8622c05b569c90180820f|h=v7x4w5xrknrhhlcnkdc7pdb4ys3zmdkx|/)

[[REST架构的网络服务].(RESTful.Web.Services).Leonard.Richardson&Sam.Ruby.文字版.pdf](ed2k://|file|[REST架构的网络服务].(RESTful.Web.Services).Leonard.Richardson&Sam.Ruby.文字版.pdf|3223551|b03f7f69c2dd2a9f51a480891a7f4bd9|h=3k7jo3io3kjqlizva66z6rcch2ttgf3i|/)

[[集体智慧编程].(Programming.Collective.Intelligence).Toby.Segaran.文字版.pdf](ed2k://|file|[集体智慧编程].(Programming.Collective.Intelligence).Toby.Segaran.文字版.pdf|3462755|9a307aefcc97a754a7ce1a2b727649a4|h=d32zqrtsyvdfzioiikjhp4lt3qxmiugh|/)

[[集体智慧编程].(Programming.Collective.Intelligence).Toby.Segaran.文字版.epub](ed2k://|file|[集体智慧编程].(Programming.Collective.Intelligence).Toby.Segaran.文字版.epub|2531115|ED5B9405F89E6E1E17BC63169DC4087D|h=XQYQYXUTFCYPYGHRPMHZHUPKQQDBZPAE|/)

[[构建社交网络应用].(Building.Social.Web.Applications).Gavin.Bell.文字版.pdf](ed2k://|file|[构建社交网络应用].(Building.Social.Web.Applications).Gavin.Bell.文字版.pdf|15344039|fbf8d0875f3104291cb041d5bc9a55ab|h=5iakv4z3ba2zhzp4uezpp2v45zzcrtih|/)

[[最好的iPad应用].(Best.iPad.Apps).Peter.Meyers.文字版.pdf](ed2k://|file|[最好的iPad应用].(Best.iPad.Apps).Peter.Meyers.文字版.pdf|58269210|6b0a6f2fd5c5fdcd433ed4ba7d3ac345|h=uexwgoukskobo6s4hddw624fxz2vqghm|/)

[[最好的iPad应用].(Best.iPad.Apps).Peter.Meyers.文字版.epub](ed2k://|file|[最好的iPad应用].(Best.iPad.Apps).Peter.Meyers.文字版.epub|24219124|a29133ee60193a773d4c2d89f659a446|h=liykxr4hyzbjzyrtwbnfdihjrixmdttv|/)

[[Facebook效应].(The.Facebook.Effect).David.Kirkpatrick.英文文字版.epub](ed2k://|file|[Facebook效应].(The.Facebook.Effect).David.Kirkpatrick.英文文字版.epub|2678963|5e326ca0304c4459fa6cd5bbc20e1bcf|h=6sy7cktrcgivzcwdrdlg366xendxsfis|/)

[[Facebook效应].(The.Facebook.Effect).David.Kirkpatrick.英文文字版.mobi](ed2k://|file|[Facebook效应].(The.Facebook.Effect).David.Kirkpatrick.英文文字版.mobi|1637575|9e1c19e6ddc6ddf8c90a16a5bca872ae|h=h5lubs7ai5tkxfttcnbsrzo36pv45yft|/)

[[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文文字版.epub](ed2k://|file|[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文文字版.epub|4143133|4e00fb98d6ac3d2a6755525aaca48740|h=hc24murkcmkmfognbmsanlfmtohvkjis|/)

[[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文文字版.mobi](ed2k://|file|[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文文字版.mobi|1386251|dddfcd178ef2c591c0aeaaba7e384f95|h=qpzpd4ury2u4jff6dfqkd6jm4nqhrnd2|/)

[[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文有声版.rar](ed2k://|file|[斯蒂夫·沃兹尼亚克自传].(iWoz：Computer.Geek.to.Cult.Icon).Steve.Wozniak.英文有声版.rar|138110682|b9eeee857e3eaac052b19d8429e33a55|h=cmrafla5fu4fq6p4nmoqjj2vdyn7vh7q|/)

[[乔布斯语录].(I,Steve：Steve.Jobs.in.His.Own.Words).George.Beahm.英文文字版.epub](ed2k://|file|[乔布斯语录].(I,Steve：Steve.Jobs.in.His.Own.Words).George.Beahm.英文文字版.epub|789779|9b6dc00f2b6391355a8045e83d2bc09c|h=evz3eymqiycb56rzwr2hozenxgx3v74i|/)

[[乔布斯语录].(I,Steve：Steve.Jobs.in.His.Own.Words).George.Beahm.英文文字版.mobi](ed2k://|file|[乔布斯语录].(I,Steve：Steve.Jobs.in.His.Own.Words).George.Beahm.英文文字版.mobi|808764|aa88de2e901060ea32cf10d88d847b94|h=y5crimf4wfhsuhzjk2jzufmt3tabvxto|/)

[[手气不错：谷歌第59号员工的自白].(I'm.Feeling.Lucky).Douglas.Edwards.英文文字版.epub](ed2k://|file|[手气不错：谷歌第59号员工的自白].(I'm.Feeling.Lucky).Douglas.Edwards.英文文字版.epub|620818|62bb7a40c82e1320b5dc7b33023fcd39|h=v3sl5obz3x3hexsrggublfthbzbg3tcp|/)

[[手气不错：谷歌第59号员工的自白].(I'm.Feeling.Lucky).Douglas.Edwards.英文文字版.mobi](ed2k://|file|[手气不错：谷歌第59号员工的自白].(I'm.Feeling.Lucky).Douglas.Edwards.英文文字版.mobi|739276|b31a6df6babf0e21f859f97dd7ecedc0|h=eg3rxzjquwrng6prngroa7hclb32g2mn|/)

[[最好的iPhone应用(第2版)].(Best.iPhone.Apps.2nd.Edition).J.D.Biersdorfer.文字版.pdf](ed2k://|file|[最好的iPhone应用(第2版)].(Best.iPhone.Apps.2nd.Edition).J.D.Biersdorfer.文字版.pdf|14429060|b77b103389c671c14e475fec340771db|h=buku7up76sokbtylngkmw37xy7o56fwj|/)

[[最好的Android应用].(Best.Android.Apps).Mike.Hendrickson&Brian.Sawyer.文字版.pdf](ed2k://|file|[最好的Android应用].(Best.Android.Apps).Mike.Hendrickson&Brian.Sawyer.文字版.pdf|17698041|4d795961f4215885b1407f7207c3b4ac|h=sl43frhcd23ojkxe26iuhdcxutvvsw54|/)

[[云计算安全与隐私].(Cloud.Security.and.Privacy).T.Mather&S.Kumaraswamy&S.Latif.文字版.pdf](ed2k://|file|[云计算安全与隐私].(Cloud.Security.and.Privacy).T.Mather&S.Kumaraswamy&S.Latif.文字版.pdf|6566407|dceade91afbb36c97e03cb11294b18e4|h=xyh2oxlifo5ob6ioisbeg53wpxwi2jyw|/)

[[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.pdf](ed2k://|file|[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.pdf|3858018|dd53804bb5f40f3d55281dac89d2938a|h=lkmudg5zqvmi7gi54cby2kj3aafy3s4z|/)

[[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.epub](ed2k://|file|[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.epub|1016847|02feaba9df42f13ef20d27c8dbdfbc73|h=lopmi3yve4c7avqgoaymvqi7ego7tztw|/)

[[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.mobi](ed2k://|file|[云计算应用架构].(Cloud.Application.Architectures).George.Reese.文字版.mobi|4331499|18cc69df29790c8ff94a81ab81d1aa0e|h=p2pze5cwvaoulkuxxrn3r7bbpzrhwlan|/)

[[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.pdf](ed2k://|file|[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.pdf|10874897|60b2888cf8f7e39dcdd8fe9940abd790|h=zw3a5j2snwmffh6s65y7pog3fbpfjdnq|/)

[[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.epub](ed2k://|file|[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.epub|3432835|1073cf3a4edc66c9a3be5c61b3066c80|h=bnmwq5frq2n6dm3b56zegxy3xuwe23nl|/)

[[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.mobi](ed2k://|file|[Amazon.EC2编程].(Programming.Amazon.EC2).Jurg.van.Vliet.文字版.mobi|3911281|851d6a0f3a36237cf8ce83b13e56aeb3|h=dslkdyapk2n5724bokpxtcpr4j42bzrs|/)

[[Facebook.Cookbook(第1版)].(Building.Applications.to.Grow.Your.Facebook.Empire).Jay.Goldman.文字版.pdf](ed2k://|file|[Facebook.Cookbook(第1版)].(Building.Applications.to.Grow.Your.Facebook.Empire).Jay.Goldman.文字版.pdf|9286347|991f03eb69fdc28fcb4d8db4bb15f6b5|h=hmo2eai47g4choq4ezqq3zos4j65xyrw|/)

[[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.pdf](ed2k://|file|[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.pdf|5827526|664357c7763cd00791145106c72865a0|h=gwrlpzppyg6u47hkt6vloqlwuemejtes|/)

[[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.epub](ed2k://|file|[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.epub|3927802|057992830fffef9c9e2b6723911aee85|h=quc33qniqom2mqdr2xwpqjg3nwmfe3jq|/)

[[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.mobi](ed2k://|file|[HTML5揭秘].(HTML5：Up.and.Running).Mark.Pilgrim.文字版.mobi|6161783|62e91e0a773262e3d15588c648b31f33|h=ztxiaegplaeddn63cn5m4znpzxeohyu5|/)

[[Twitter.API：构建与运行].(Twitter.API：Up.and.Running).Kevin.Makice.文字版.pdf](ed2k://|file|[Twitter.API：构建与运行].(Twitter.API：Up.and.Running).Kevin.Makice.文字版.pdf|3057638|cfb2f42e643394241f98766c8d2194a4|h=3jjr5oxwojsrhn3i5hjsf2ujr6w5ca5o|/)

[[挖掘社交网络].(Mining.the.Social.Web).Matthew.A.Russell.文字版.pdf](ed2k://|file|[挖掘社交网络].(Mining.the.Social.Web).Matthew.A.Russell.文字版.pdf|7144449|778A16B9B0FD83D6DFB707B085D24E48|h=TCRAPSQUISRQ4SG4HOQYHAAREOVOWH3Y|/)

[[挖掘社交网络].(Mining.the.Social.Web).Matthew.A.Russell.文字版.epub](ed2k://|file|[挖掘社交网络].(Mining.the.Social.Web).Matthew.A.Russell.文字版.epub|8015906|74B74BF2122CEE7CF90B7660661D9CF1|h=PSC3B2P76AV5FRJ3B7A7X7J7QGBBS4F7|/)

[[MongoDB权威指南].(MongoDB：The.Definitive.Guide).K.Chodorow&M.Dirolf.文字版.pdf](ed2k://|file|[MongoDB权威指南].(MongoDB：The.Definitive.Guide).K.Chodorow&M.Dirolf.文字版.pdf|4066127|d04324e38d60dd9cf658c9440e3162f2|h=ijlbt3uwbbfuwkk4mbplhaqoxkurgupz|/)

[[Hadoop权威指南(第2版)].(Hadoop：The.Definitive.Guide).Tom.White.文字版.pdf](ed2k://|file|[Hadoop权威指南(第2版)].(Hadoop：The.Definitive.Guide).Tom.White.文字版.pdf|8028652|fb4a7620c63858e9abcb8763e76c6988|h=e5veo6kkaz7wsajskc7zvviizdijf46n|/)

[[企业级JavaBeans3.1(第6版)].(Enterprise.JavaBeans.3.1).Andrew.Lee.Rubinger&Bill.Burke.文字版.pdf](ed2k://|file|[企业级JavaBeans3.1(第6版)].(Enterprise.JavaBeans.3.1).Andrew.Lee.Rubinger&Bill.Burke.文字版.pdf|5460518|43fdcd84148e8a56772cc2400eb887c3|h=ti5eouhlyt3hv5u5qmbwkkr6vfqqvboz|/)

[[Google.App.Engine编程].(Programming.Google.Apps.Engine).Dan.Sanderson.文字版.pdf](ed2k://|file|[Google.App.Engine编程].(Programming.Google.Apps.Engine).Dan.Sanderson.文字版.pdf|3777511|9551dc2e04c800685de74f74e22c9f02|h=huo7g6yzzubqiy5ah6nmkh4e4upows4o|/)

[[Google.App.Engine开发].(Using.Google.App.Engine).Charles.Severance.文字版.pdf](ed2k://|file|[Google.App.Engine开发].(Using.Google.App.Engine).Charles.Severance.文字版.pdf|6903302|f3194251f72047affe73fa0564ad91e0|h=g2gffru7ducbmk2wsxr5v7tipjtmlfbw|/)

